/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:08 GMT-0400 (GMT-04:00)
 * Time: 0:54:8
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:08 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:8
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdPersonService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsPeople(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsPeople.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsPeople.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsPeople(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsPeople.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsPeople.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdPerson(newVdPerson) {
		try {
			let objVdPerson;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsPeople.primaryKeys._id.type.toString())) {
			    newVdPerson._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdPerson.id) {
              let max = await models.sequelize.vdsPeople.max('id');
              newVdPerson.id = newVdPerson.id ? newVdPerson.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdPerson = await models.sequelize.vdsPeople.create(newVdPerson);
			} else {
				objVdPerson = new models.mongoose.vdsPeople(newVdPerson);
				await objVdPerson.save();
			}
			return objVdPerson;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdPerson(_id, updateVdPerson) {
		try {
			let objVdPerson;
			if(sql) {
				objVdPerson = await models.sequelize.vdsPeople.findOne({where: { _id: util.String(_id) }});
				if (objVdPerson) {
					await models.sequelize.vdsPeople.update(updateVdPerson, { where: { _id: util.String(_id) } });
					objVdPerson = await models.sequelize.vdsPeople.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdPerson._id;
				objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({_id:_id}, {$set: updateVdPerson}, {new: true});
			}
			return objVdPerson;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdPerson(_id, query) {
		try {
			let objVdPerson;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdPerson = await models.sequelize.vdsPeople.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdPerson = await models.mongoose.vdsPeople.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdPerson;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdPerson(_id) {
		try {
			let objVdPerson;
			if(sql) {
				objVdPerson = await models.sequelize.vdsPeople.findOne({ where: { _id: util.String(_id) } });
				if (objVdPerson) {
					await models.sequelize.vdsPeople.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdPerson = await models.mongoose.vdsPeople.deleteOne({_id:util.String(_id)});
			}
			return objVdPerson;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({_id: Id});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({id: id});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerFirstName(perFirstName, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_first_name: perFirstName },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_first_name: perFirstName});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerSecondName(perSecondName, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_second_name: perSecondName },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_second_name: perSecondName});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerFirstLastname(perFirstLastname, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_first_lastname: perFirstLastname },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_first_lastname: perFirstLastname});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerSecondLastname(perSecondLastname, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_second_lastname: perSecondLastname },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_second_lastname: perSecondLastname});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerLicense(perLicense, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_license: perLicense },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_license: perLicense});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerLicenseComp(perLicenseComp, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_license_comp: perLicenseComp },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_license_comp: perLicenseComp});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerHomeAddress(perHomeAddress, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_home_address: perHomeAddress },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_home_address: perHomeAddress});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerMail(perMail, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_mail: perMail },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_mail: perMail});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerHomePhone(perHomePhone, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_home_phone: perHomePhone },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_home_phone: perHomePhone});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerCellphone(perCellphone, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_cellphone: perCellphone },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_cellphone: perCellphone});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerGroup(perGroup, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_group: perGroup },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_group: perGroup});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({createdById: createdbyid});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({updatedById: updatedbyid});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerParentId(perParentId, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_parent_id: perParentId },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_parent_id: perParentId});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerParTypeDocId(perParTypeDocId, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_par_type_doc_id: perParTypeDocId },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_par_type_doc_id: perParTypeDocId});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerParCityId(perParCityId, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_par_city_id: perParCityId },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_par_city_id: perParCityId});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerParSexId(perParSexId, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_par_sex_id: perParSexId },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_par_sex_id: perParSexId});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerParCountryId(perParCountryId, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_par_country_id: perParCountryId },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_par_country_id: perParCountryId});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerParNacionalityId(perParNacionalityId, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_par_nacionality_id: perParNacionalityId },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_par_nacionality_id: perParNacionalityId});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerParStatusId(perParStatusId, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_par_status_id: perParStatusId },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_par_status_id: perParStatusId});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPerBirthday(perBirthday, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { per_birthday: perBirthday },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({per_birthday: perBirthday});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({dueAt: dueat});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({createdAt: createdat});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOne({updatedAt: updatedat});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdPersonByUid(Id, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { _id: Id }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { _id: Id } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({_id: Id}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonById(id, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { id: id }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { id: id } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({id: id}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerFirstName(perFirstName, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_first_name: perFirstName }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_first_name: perFirstName } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_first_name: perFirstName}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerSecondName(perSecondName, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_second_name: perSecondName }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_second_name: perSecondName } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_second_name: perSecondName}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerFirstLastname(perFirstLastname, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_first_lastname: perFirstLastname }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_first_lastname: perFirstLastname } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_first_lastname: perFirstLastname}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerSecondLastname(perSecondLastname, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_second_lastname: perSecondLastname }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_second_lastname: perSecondLastname } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_second_lastname: perSecondLastname}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerLicense(perLicense, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_license: perLicense }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_license: perLicense } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_license: perLicense}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerLicenseComp(perLicenseComp, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_license_comp: perLicenseComp }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_license_comp: perLicenseComp } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_license_comp: perLicenseComp}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerHomeAddress(perHomeAddress, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_home_address: perHomeAddress }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_home_address: perHomeAddress } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_home_address: perHomeAddress}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerMail(perMail, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_mail: perMail }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_mail: perMail } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_mail: perMail}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerHomePhone(perHomePhone, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_home_phone: perHomePhone }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_home_phone: perHomePhone } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_home_phone: perHomePhone}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerCellphone(perCellphone, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_cellphone: perCellphone }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_cellphone: perCellphone } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_cellphone: perCellphone}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerGroup(perGroup, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_group: perGroup }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_group: perGroup } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_group: perGroup}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByCreatedbyid(createdbyid, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { createdById: createdbyid }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByUpdatedbyid(updatedbyid, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { updatedById: updatedbyid }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerParentId(perParentId, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_parent_id: perParentId }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_parent_id: perParentId } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_parent_id: perParentId}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerParTypeDocId(perParTypeDocId, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_par_type_doc_id: perParTypeDocId }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_par_type_doc_id: perParTypeDocId } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_par_type_doc_id: perParTypeDocId}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerParCityId(perParCityId, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_par_city_id: perParCityId }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_par_city_id: perParCityId } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_par_city_id: perParCityId}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerParSexId(perParSexId, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_par_sex_id: perParSexId }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_par_sex_id: perParSexId } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_par_sex_id: perParSexId}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerParCountryId(perParCountryId, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_par_country_id: perParCountryId }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_par_country_id: perParCountryId } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_par_country_id: perParCountryId}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerParNacionalityId(perParNacionalityId, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_par_nacionality_id: perParNacionalityId }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_par_nacionality_id: perParNacionalityId } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_par_nacionality_id: perParNacionalityId}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerParStatusId(perParStatusId, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_par_status_id: perParStatusId }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_par_status_id: perParStatusId } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_par_status_id: perParStatusId}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByPerBirthday(perBirthday, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { per_birthday: perBirthday }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { per_birthday: perBirthday } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({per_birthday: perBirthday}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByDueat(dueat, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { dueAt: dueat }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({dueAt: dueat}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByCreatedat(createdat, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { createdAt: createdat }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({createdAt: createdat}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonByUpdatedat(updatedat, updateVdPerson) {
    	try {
    		let objVdPerson;
    		if(sql) {
    			objVdPerson = await models.sequelize.vdsPeople.findOne({where: { updatedAt: updatedat }});
    			if (objVdPerson) {
    				objVdPerson = await models.sequelize.vdsPeople.update(updateVdPerson, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdPerson = await models.mongoose.vdsPeople.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdPerson}, {new: true});
    		}
    		return objVdPerson;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsPeoplePerParentWithPerFirstName(select = ['_id', 'per_first_name'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsPeople.findAll({
                    attributes: select,
                    where: { per_group: {[Op.like]: '%grp_per_parent%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsPeople.find({per_group: {$regex : ".*grp_per_parent.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsPerParTypeDocWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_per_par_type_doc%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_per_par_type_doc.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsPerParCityWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_per_par_city%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_per_par_city.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsPerParSexWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_per_par_sex%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_per_par_sex.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsPerParCountryWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_per_par_country%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_per_par_country.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsPerParNacionalityWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_per_par_nacionality%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_per_par_nacionality.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsPerParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_per_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_per_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsPeopleByCreatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsPeopleByUpdatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsPeopleByPerParent(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsPeopleByPerParTypeDoc(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsPeopleByPerParCity(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsPeopleByPerParSex(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsPeopleByPerParCountry(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsPeopleByPerParNacionality(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsPeopleByPerParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsPerParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdPerson, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdPerson = await models.sequelize.vdsPeople.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdPerson = await models.mongoose.vdsPeople.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdPerson;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsPeople.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsPeople.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsPeople = await models.sequelize.vdsPeople.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsPeople.length ; k++) {
    						let userVdPerson = userVdsPeople[k].dataValues;
    						let aUserVdPersonValues = Object.values(userVdPerson);
    						let aUserVdPersonFields = Object.keys(userVdPerson);
    						for (let n = 0 ; n < aUserVdPersonValues.length ; n++) {
    							let userVdPersonField = aUserVdPersonFields[n];
    							let userVdPersonValue = aUserVdPersonValues[n];
    							if (!dtValues.find(param => param.value == userVdPersonValue && param.field == userVdPersonField)) {
    								dtValues.push({value:userVdPersonValue, count:1, label:userVdPersonValue, field:userVdPersonField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdPersonValue && dtValue.field == userVdPersonField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsPeople];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdPersonService;
//</es-section>
