/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Time: 0:54:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:13
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdSexoService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsSexo(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsSexo.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsSexo.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsSexo(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsSexo.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsSexo.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdSexo(newVdSexo) {
		try {
			let objVdSexo;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsSexo.primaryKeys._id.type.toString())) {
			    newVdSexo._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdSexo.id) {
              let max = await models.sequelize.vdsSexo.max('id');
              newVdSexo.id = newVdSexo.id ? newVdSexo.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdSexo = await models.sequelize.vdsSexo.create(newVdSexo);
			} else {
				objVdSexo = new models.mongoose.vdsSexo(newVdSexo);
				await objVdSexo.save();
			}
			return objVdSexo;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdSexo(_id, updateVdSexo) {
		try {
			let objVdSexo;
			if(sql) {
				objVdSexo = await models.sequelize.vdsSexo.findOne({where: { _id: util.String(_id) }});
				if (objVdSexo) {
					await models.sequelize.vdsSexo.update(updateVdSexo, { where: { _id: util.String(_id) } });
					objVdSexo = await models.sequelize.vdsSexo.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdSexo._id;
				objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({_id:_id}, {$set: updateVdSexo}, {new: true});
			}
			return objVdSexo;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdSexo(_id, query) {
		try {
			let objVdSexo;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdSexo = await models.sequelize.vdsSexo.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdSexo = await models.mongoose.vdsSexo.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdSexo;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdSexo(_id) {
		try {
			let objVdSexo;
			if(sql) {
				objVdSexo = await models.sequelize.vdsSexo.findOne({ where: { _id: util.String(_id) } });
				if (objVdSexo) {
					await models.sequelize.vdsSexo.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdSexo = await models.mongoose.vdsSexo.deleteOne({_id:util.String(_id)});
			}
			return objVdSexo;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({_id: Id});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({id: id});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstado(estado, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado: estado },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({estado: estado});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDescripcion(descripcion, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { descripcion: descripcion },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({descripcion: descripcion});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({createdBy: createdby});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({updatedBy: updatedby});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({dueAt: dueat});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({createdAt: createdat});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOne({updatedAt: updatedat});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdSexoByUid(Id, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { _id: Id }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { _id: Id } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({_id: Id}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdSexoById(id, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { id: id }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { id: id } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({id: id}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdSexoByEstado(estado, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { estado: estado }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { estado: estado } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({estado: estado}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdSexoByDescripcion(descripcion, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { descripcion: descripcion }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { descripcion: descripcion } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({descripcion: descripcion}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdSexoByCreatedby(createdby, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { createdBy: createdby }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({createdBy: createdby}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdSexoByUpdatedby(updatedby, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { updatedBy: updatedby }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdSexoByDueat(dueat, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { dueAt: dueat }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({dueAt: dueat}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdSexoByCreatedat(createdat, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { createdAt: createdat }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({createdAt: createdat}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdSexoByUpdatedat(updatedat, updateVdSexo) {
    	try {
    		let objVdSexo;
    		if(sql) {
    			objVdSexo = await models.sequelize.vdsSexo.findOne({where: { updatedAt: updatedat }});
    			if (objVdSexo) {
    				objVdSexo = await models.sequelize.vdsSexo.update(updateVdSexo, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdSexo = await models.mongoose.vdsSexo.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdSexo}, {new: true});
    		}
    		return objVdSexo;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsSexo.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsSexo.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsSexo = await models.sequelize.vdsSexo.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsSexo.length ; k++) {
    						let userVdSexo = userVdsSexo[k].dataValues;
    						let aUserVdSexoValues = Object.values(userVdSexo);
    						let aUserVdSexoFields = Object.keys(userVdSexo);
    						for (let n = 0 ; n < aUserVdSexoValues.length ; n++) {
    							let userVdSexoField = aUserVdSexoFields[n];
    							let userVdSexoValue = aUserVdSexoValues[n];
    							if (!dtValues.find(param => param.value == userVdSexoValue && param.field == userVdSexoField)) {
    								dtValues.push({value:userVdSexoValue, count:1, label:userVdSexoValue, field:userVdSexoField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdSexoValue && dtValue.field == userVdSexoField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsSexo];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdSexoService;
//</es-section>
