/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:15 GMT-0400 (GMT-04:00)
 * Time: 0:54:15
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:15 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:15
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdViewService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsViews(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsViews.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsViews.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsViews(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsViews.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsViews.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdView(newVdView) {
		try {
			let objVdView;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsViews.primaryKeys._id.type.toString())) {
			    newVdView._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdView.id) {
              let max = await models.sequelize.vdsViews.max('id');
              newVdView.id = newVdView.id ? newVdView.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdView = await models.sequelize.vdsViews.create(newVdView);
			} else {
				objVdView = new models.mongoose.vdsViews(newVdView);
				await objVdView.save();
			}
			return objVdView;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdView(_id, updateVdView) {
		try {
			let objVdView;
			if(sql) {
				objVdView = await models.sequelize.vdsViews.findOne({where: { _id: util.String(_id) }});
				if (objVdView) {
					await models.sequelize.vdsViews.update(updateVdView, { where: { _id: util.String(_id) } });
					objVdView = await models.sequelize.vdsViews.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdView._id;
				objVdView = await models.mongoose.vdsViews.findOneAndUpdate({_id:_id}, {$set: updateVdView}, {new: true});
			}
			return objVdView;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdView(_id, query) {
		try {
			let objVdView;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdView = await models.sequelize.vdsViews.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdView = await models.mongoose.vdsViews.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdView;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdView(_id) {
		try {
			let objVdView;
			if(sql) {
				objVdView = await models.sequelize.vdsViews.findOne({ where: { _id: util.String(_id) } });
				if (objVdView) {
					await models.sequelize.vdsViews.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdView = await models.mongoose.vdsViews.deleteOne({_id:util.String(_id)});
			}
			return objVdView;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({_id: Id});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({id: id});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieCode(vieCode, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_code: vieCode },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_code: vieCode});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieDescription(vieDescription, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_description: vieDescription },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_description: vieDescription});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieRoute(vieRoute, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_route: vieRoute },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_route: vieRoute});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieParams(vieParams, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_params: vieParams },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_params: vieParams});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieIcon(vieIcon, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_icon: vieIcon },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_icon: vieIcon});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieGroup(vieGroup, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_group: vieGroup },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_group: vieGroup});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({createdBy: createdby});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({updatedBy: updatedby});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieModuleId(vieModuleId, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_module_id: vieModuleId },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_module_id: vieModuleId});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieReturnId(vieReturnId, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_return_id: vieReturnId },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_return_id: vieReturnId});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByVieParStatusId(vieParStatusId, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { vie_par_status_id: vieParStatusId },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({vie_par_status_id: vieParStatusId});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({dueAt: dueat});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({createdAt: createdat});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOne({updatedAt: updatedat});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdViewByUid(Id, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { _id: Id }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { _id: Id } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({_id: Id}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewById(id, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { id: id }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { id: id } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({id: id}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieCode(vieCode, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_code: vieCode }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_code: vieCode } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_code: vieCode}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieDescription(vieDescription, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_description: vieDescription }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_description: vieDescription } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_description: vieDescription}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieRoute(vieRoute, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_route: vieRoute }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_route: vieRoute } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_route: vieRoute}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieParams(vieParams, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_params: vieParams }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_params: vieParams } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_params: vieParams}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieIcon(vieIcon, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_icon: vieIcon }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_icon: vieIcon } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_icon: vieIcon}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieGroup(vieGroup, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_group: vieGroup }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_group: vieGroup } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_group: vieGroup}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByCreatedby(createdby, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { createdBy: createdby }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({createdBy: createdby}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByUpdatedby(updatedby, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { updatedBy: updatedby }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieModuleId(vieModuleId, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_module_id: vieModuleId }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_module_id: vieModuleId } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_module_id: vieModuleId}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieReturnId(vieReturnId, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_return_id: vieReturnId }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_return_id: vieReturnId } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_return_id: vieReturnId}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByVieParStatusId(vieParStatusId, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { vie_par_status_id: vieParStatusId }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { vie_par_status_id: vieParStatusId } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({vie_par_status_id: vieParStatusId}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByDueat(dueat, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { dueAt: dueat }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({dueAt: dueat}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByCreatedat(createdat, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { createdAt: createdat }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({createdAt: createdat}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdViewByUpdatedat(updatedat, updateVdView) {
    	try {
    		let objVdView;
    		if(sql) {
    			objVdView = await models.sequelize.vdsViews.findOne({where: { updatedAt: updatedat }});
    			if (objVdView) {
    				objVdView = await models.sequelize.vdsViews.update(updateVdView, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdView = await models.mongoose.vdsViews.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdView}, {new: true});
    		}
    		return objVdView;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsModulesVieModuleWithModCode(select = ['_id', 'mod_code'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsModules.findAll({
                    attributes: select,
                    where: { mod_group: {[Op.like]: '%grp_vie_module%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsModules.find({mod_group: {$regex : ".*grp_vie_module.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsViewsVieReturnWithVieCode(select = ['_id', 'vie_code'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsViews.findAll({
                    attributes: select,
                    where: { vie_group: {[Op.like]: '%grp_vie_return%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsViews.find({vie_group: {$regex : ".*grp_vie_return.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsVieParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_vie_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_vie_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsViewsByVieModule(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsVieParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdView, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdView;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsViewsByVieReturn(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsVieParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdView, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdView;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsViewsByVieParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsVieParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdView, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdView = await models.sequelize.vdsViews.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdView = await models.mongoose.vdsViews.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdView;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsViews.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsViews.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsViews = await models.sequelize.vdsViews.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsViews.length ; k++) {
    						let userVdView = userVdsViews[k].dataValues;
    						let aUserVdViewValues = Object.values(userVdView);
    						let aUserVdViewFields = Object.keys(userVdView);
    						for (let n = 0 ; n < aUserVdViewValues.length ; n++) {
    							let userVdViewField = aUserVdViewFields[n];
    							let userVdViewValue = aUserVdViewValues[n];
    							if (!dtValues.find(param => param.value == userVdViewValue && param.field == userVdViewField)) {
    								dtValues.push({value:userVdViewValue, count:1, label:userVdViewValue, field:userVdViewField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdViewValue && dtValue.field == userVdViewField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsViews];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdViewService;
//</es-section>
