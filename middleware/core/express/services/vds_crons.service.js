/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:59 GMT-0400 (GMT-04:00)
 * Time: 0:53:59
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:59 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:59
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdCronService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsCrons(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsCrons.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsCrons.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsCrons(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsCrons.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsCrons.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdCron(newVdCron) {
		try {
			let objVdCron;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsCrons.primaryKeys._id.type.toString())) {
			    newVdCron._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdCron.id) {
              let max = await models.sequelize.vdsCrons.max('id');
              newVdCron.id = newVdCron.id ? newVdCron.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdCron = await models.sequelize.vdsCrons.create(newVdCron);
			} else {
				objVdCron = new models.mongoose.vdsCrons(newVdCron);
				await objVdCron.save();
			}
			return objVdCron;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdCron(_id, updateVdCron) {
		try {
			let objVdCron;
			if(sql) {
				objVdCron = await models.sequelize.vdsCrons.findOne({where: { _id: util.String(_id) }});
				if (objVdCron) {
					await models.sequelize.vdsCrons.update(updateVdCron, { where: { _id: util.String(_id) } });
					objVdCron = await models.sequelize.vdsCrons.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdCron._id;
				objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({_id:_id}, {$set: updateVdCron}, {new: true});
			}
			return objVdCron;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdCron(_id, query) {
		try {
			let objVdCron;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdCron = await models.sequelize.vdsCrons.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdCron = await models.mongoose.vdsCrons.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdCron;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdCron(_id) {
		try {
			let objVdCron;
			if(sql) {
				objVdCron = await models.sequelize.vdsCrons.findOne({ where: { _id: util.String(_id) } });
				if (objVdCron) {
					await models.sequelize.vdsCrons.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdCron = await models.mongoose.vdsCrons.deleteOne({_id:util.String(_id)});
			}
			return objVdCron;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({_id: Id});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({id: id});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCroStatus(croStatus, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { cro_status: croStatus },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({cro_status: croStatus});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCroDescription(croDescription, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { cro_description: croDescription },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({cro_description: croDescription});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCroExpression(croExpression, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { cro_expression: croExpression },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({cro_expression: croExpression});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCroGroup(croGroup, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { cro_group: croGroup },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({cro_group: croGroup});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCroMaiId(croMaiId, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { cro_mai_id: croMaiId },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({cro_mai_id: croMaiId});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({createdBy: createdby});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({updatedBy: updatedby});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCroFunction(croFunction, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { cro_function: croFunction },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({cro_function: croFunction});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({dueAt: dueat});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({createdAt: createdat});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOne({updatedAt: updatedat});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdCronByUid(Id, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { _id: Id }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { _id: Id } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({_id: Id}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronById(id, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { id: id }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { id: id } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({id: id}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByCroStatus(croStatus, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { cro_status: croStatus }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { cro_status: croStatus } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({cro_status: croStatus}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByCroDescription(croDescription, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { cro_description: croDescription }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { cro_description: croDescription } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({cro_description: croDescription}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByCroExpression(croExpression, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { cro_expression: croExpression }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { cro_expression: croExpression } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({cro_expression: croExpression}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByCroGroup(croGroup, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { cro_group: croGroup }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { cro_group: croGroup } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({cro_group: croGroup}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByCroMaiId(croMaiId, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { cro_mai_id: croMaiId }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { cro_mai_id: croMaiId } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({cro_mai_id: croMaiId}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByCreatedby(createdby, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { createdBy: createdby }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({createdBy: createdby}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByUpdatedby(updatedby, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { updatedBy: updatedby }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByCroFunction(croFunction, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { cro_function: croFunction }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { cro_function: croFunction } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({cro_function: croFunction}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByDueat(dueat, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { dueAt: dueat }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({dueAt: dueat}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByCreatedat(createdat, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { createdAt: createdat }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({createdAt: createdat}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCronByUpdatedat(updatedat, updateVdCron) {
    	try {
    		let objVdCron;
    		if(sql) {
    			objVdCron = await models.sequelize.vdsCrons.findOne({where: { updatedAt: updatedat }});
    			if (objVdCron) {
    				objVdCron = await models.sequelize.vdsCrons.update(updateVdCron, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdCron = await models.mongoose.vdsCrons.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdCron}, {new: true});
    		}
    		return objVdCron;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsCrons.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsCrons.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsCrons = await models.sequelize.vdsCrons.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsCrons.length ; k++) {
    						let userVdCron = userVdsCrons[k].dataValues;
    						let aUserVdCronValues = Object.values(userVdCron);
    						let aUserVdCronFields = Object.keys(userVdCron);
    						for (let n = 0 ; n < aUserVdCronValues.length ; n++) {
    							let userVdCronField = aUserVdCronFields[n];
    							let userVdCronValue = aUserVdCronValues[n];
    							if (!dtValues.find(param => param.value == userVdCronValue && param.field == userVdCronField)) {
    								dtValues.push({value:userVdCronValue, count:1, label:userVdCronValue, field:userVdCronField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdCronValue && dtValue.field == userVdCronField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsCrons];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdCronService;
//</es-section>
