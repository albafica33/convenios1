/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Time: 0:54:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:1
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdEstadoCivilService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsEstadoCivil(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsEstadoCivil.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsEstadoCivil.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsEstadoCivil(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsEstadoCivil.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsEstadoCivil.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdEstadoCivil(newVdEstadoCivil) {
		try {
			let objVdEstadoCivil;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsEstadoCivil.primaryKeys._id.type.toString())) {
			    newVdEstadoCivil._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdEstadoCivil.id) {
              let max = await models.sequelize.vdsEstadoCivil.max('id');
              newVdEstadoCivil.id = newVdEstadoCivil.id ? newVdEstadoCivil.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.create(newVdEstadoCivil);
			} else {
				objVdEstadoCivil = new models.mongoose.vdsEstadoCivil(newVdEstadoCivil);
				await objVdEstadoCivil.save();
			}
			return objVdEstadoCivil;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdEstadoCivil(_id, updateVdEstadoCivil) {
		try {
			let objVdEstadoCivil;
			if(sql) {
				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { _id: util.String(_id) }});
				if (objVdEstadoCivil) {
					await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { _id: util.String(_id) } });
					objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdEstadoCivil._id;
				objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({_id:_id}, {$set: updateVdEstadoCivil}, {new: true});
			}
			return objVdEstadoCivil;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdEstadoCivil(_id, query) {
		try {
			let objVdEstadoCivil;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdEstadoCivil;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdEstadoCivil(_id) {
		try {
			let objVdEstadoCivil;
			if(sql) {
				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({ where: { _id: util.String(_id) } });
				if (objVdEstadoCivil) {
					await models.sequelize.vdsEstadoCivil.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.deleteOne({_id:util.String(_id)});
			}
			return objVdEstadoCivil;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({_id: Id});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({id: id});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstado(estado, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado: estado },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({estado: estado});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDescripcion(descripcion, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { descripcion: descripcion },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({descripcion: descripcion});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({createdBy: createdby});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({updatedBy: updatedby});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({dueAt: dueat});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({createdAt: createdat});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOne({updatedAt: updatedat});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdEstadoCivilByUid(Id, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { _id: Id }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { _id: Id } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({_id: Id}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdEstadoCivilById(id, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { id: id }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { id: id } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({id: id}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdEstadoCivilByEstado(estado, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { estado: estado }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { estado: estado } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({estado: estado}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdEstadoCivilByDescripcion(descripcion, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { descripcion: descripcion }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { descripcion: descripcion } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({descripcion: descripcion}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdEstadoCivilByCreatedby(createdby, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { createdBy: createdby }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({createdBy: createdby}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdEstadoCivilByUpdatedby(updatedby, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { updatedBy: updatedby }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdEstadoCivilByDueat(dueat, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { dueAt: dueat }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({dueAt: dueat}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdEstadoCivilByCreatedat(createdat, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { createdAt: createdat }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({createdAt: createdat}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdEstadoCivilByUpdatedat(updatedat, updateVdEstadoCivil) {
    	try {
    		let objVdEstadoCivil;
    		if(sql) {
    			objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.findOne({where: { updatedAt: updatedat }});
    			if (objVdEstadoCivil) {
    				objVdEstadoCivil = await models.sequelize.vdsEstadoCivil.update(updateVdEstadoCivil, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdEstadoCivil = await models.mongoose.vdsEstadoCivil.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdEstadoCivil}, {new: true});
    		}
    		return objVdEstadoCivil;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsEstadoCivil.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsEstadoCivil.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsEstadoCivil = await models.sequelize.vdsEstadoCivil.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsEstadoCivil.length ; k++) {
    						let userVdEstadoCivil = userVdsEstadoCivil[k].dataValues;
    						let aUserVdEstadoCivilValues = Object.values(userVdEstadoCivil);
    						let aUserVdEstadoCivilFields = Object.keys(userVdEstadoCivil);
    						for (let n = 0 ; n < aUserVdEstadoCivilValues.length ; n++) {
    							let userVdEstadoCivilField = aUserVdEstadoCivilFields[n];
    							let userVdEstadoCivilValue = aUserVdEstadoCivilValues[n];
    							if (!dtValues.find(param => param.value == userVdEstadoCivilValue && param.field == userVdEstadoCivilField)) {
    								dtValues.push({value:userVdEstadoCivilValue, count:1, label:userVdEstadoCivilValue, field:userVdEstadoCivilField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdEstadoCivilValue && dtValue.field == userVdEstadoCivilField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsEstadoCivil];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdEstadoCivilService;
//</es-section>
