/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:14 GMT-0400 (GMT-04:00)
 * Time: 0:54:14
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:14 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:14
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdUserRoleService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsUserRoles(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsUserRoles.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsUserRoles.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsUserRoles(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsUserRoles.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsUserRoles.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdUserRole(newVdUserRole) {
		try {
			let objVdUserRole;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsUserRoles.primaryKeys._id.type.toString())) {
			    newVdUserRole._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdUserRole.id) {
              let max = await models.sequelize.vdsUserRoles.max('id');
              newVdUserRole.id = newVdUserRole.id ? newVdUserRole.id : max ? max+1 : 1;
			}
			
			
			
            newVdUserRole.usr_id = newVdUserRole.usr_id ? newVdUserRole.usr_id : await Math.floor(Math.random() * 10000000000);
			
			if(sql) {
				objVdUserRole = await models.sequelize.vdsUserRoles.create(newVdUserRole);
			} else {
				objVdUserRole = new models.mongoose.vdsUserRoles(newVdUserRole);
				await objVdUserRole.save();
			}
			return objVdUserRole;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdUserRole(_id, updateVdUserRole) {
		try {
			let objVdUserRole;
			if(sql) {
				objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { _id: util.String(_id) }});
				if (objVdUserRole) {
					await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { _id: util.String(_id) } });
					objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdUserRole._id;
				objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({_id:_id}, {$set: updateVdUserRole}, {new: true});
			}
			return objVdUserRole;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdUserRole(_id, query) {
		try {
			let objVdUserRole;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdUserRole = await models.mongoose.vdsUserRoles.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdUserRole;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdUserRole(_id) {
		try {
			let objVdUserRole;
			if(sql) {
				objVdUserRole = await models.sequelize.vdsUserRoles.findOne({ where: { _id: util.String(_id) } });
				if (objVdUserRole) {
					await models.sequelize.vdsUserRoles.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdUserRole = await models.mongoose.vdsUserRoles.deleteOne({_id:util.String(_id)});
			}
			return objVdUserRole;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({_id: Id});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({id: id});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUsrRolGroup(usrRolGroup, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { usr_rol_group: usrRolGroup },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({usr_rol_group: usrRolGroup});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUsrId(usrId, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { usr_id: usrId },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({usr_id: usrId});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByRolId(rolId, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { rol_id: rolId },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({rol_id: rolId});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUsrRolParStatusId(usrRolParStatusId, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { usr_rol_par_status_id: usrRolParStatusId },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({usr_rol_par_status_id: usrRolParStatusId});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({createdById: createdbyid});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({updatedById: updatedbyid});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({dueAt: dueat});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({createdAt: createdat});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOne({updatedAt: updatedat});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdUserRoleByUid(Id, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { _id: Id }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { _id: Id } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({_id: Id}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleById(id, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { id: id }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { id: id } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({id: id}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByUsrRolGroup(usrRolGroup, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { usr_rol_group: usrRolGroup }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { usr_rol_group: usrRolGroup } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({usr_rol_group: usrRolGroup}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByUsrId(usrId, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { usr_id: usrId }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { usr_id: usrId } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({usr_id: usrId}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByRolId(rolId, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { rol_id: rolId }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { rol_id: rolId } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({rol_id: rolId}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByUsrRolParStatusId(usrRolParStatusId, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { usr_rol_par_status_id: usrRolParStatusId }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { usr_rol_par_status_id: usrRolParStatusId } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({usr_rol_par_status_id: usrRolParStatusId}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByCreatedbyid(createdbyid, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { createdById: createdbyid }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByUpdatedbyid(updatedbyid, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { updatedById: updatedbyid }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByDueat(dueat, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { dueAt: dueat }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({dueAt: dueat}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByCreatedat(createdat, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { createdAt: createdat }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({createdAt: createdat}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserRoleByUpdatedat(updatedat, updateVdUserRole) {
    	try {
    		let objVdUserRole;
    		if(sql) {
    			objVdUserRole = await models.sequelize.vdsUserRoles.findOne({where: { updatedAt: updatedat }});
    			if (objVdUserRole) {
    				objVdUserRole = await models.sequelize.vdsUserRoles.update(updateVdUserRole, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdUserRole = await models.mongoose.vdsUserRoles.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdUserRole}, {new: true});
    		}
    		return objVdUserRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsRolesRolWithRolCode(select = ['_id', 'rol_code'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsRoles.findAll({
                    attributes: select,
                    where: { rol_group: {[Op.like]: '%grp_rol%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsRoles.find({rol_group: {$regex : ".*grp_rol.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsUsrRolParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_usr_rol_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_usr_rol_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsUserRolesByUsr(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsUsrRolParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdUserRole, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdUserRole;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsUserRolesByRol(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsUsrRolParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdUserRole, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdUserRole;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsUserRolesByUsrRolParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsUsrRolParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdUserRole, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdUserRole;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsUserRolesByCreatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsUsrRolParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdUserRole, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdUserRole;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsUserRolesByUpdatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsUsrRolParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdUserRole, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdUserRole = await models.sequelize.vdsUserRoles.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdUserRole = await models.mongoose.vdsUserRoles.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdUserRole;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsUserRoles.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsUserRoles.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsUserRoles = await models.sequelize.vdsUserRoles.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsUserRoles.length ; k++) {
    						let userVdUserRole = userVdsUserRoles[k].dataValues;
    						let aUserVdUserRoleValues = Object.values(userVdUserRole);
    						let aUserVdUserRoleFields = Object.keys(userVdUserRole);
    						for (let n = 0 ; n < aUserVdUserRoleValues.length ; n++) {
    							let userVdUserRoleField = aUserVdUserRoleFields[n];
    							let userVdUserRoleValue = aUserVdUserRoleValues[n];
    							if (!dtValues.find(param => param.value == userVdUserRoleValue && param.field == userVdUserRoleField)) {
    								dtValues.push({value:userVdUserRoleValue, count:1, label:userVdUserRoleValue, field:userVdUserRoleField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdUserRoleValue && dtValue.field == userVdUserRoleField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsUserRoles];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdUserRoleService;
//</es-section>
