/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Time: 0:53:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:58
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class SequelizemetaService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllSequelizemeta(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.sequelizemeta.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['name','ASC']],
                });
			} else {
				return await models.mongoose.sequelizemeta.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllSequelizemeta(select = []) {
		try {
			if(sql) {
				return await models.sequelize.sequelizemeta.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.sequelizemeta.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addSequelizemeta(newSequelizemeta) {
		try {
			let objSequelizemeta;
			if(util.PrimaryKeyTypeIsString(models.sequelize.sequelizemeta.primaryKeys.name.type.toString())) {
			    newSequelizemeta.name = models.sequelize.objectId().toString();
		    }
			
			
			
			if(sql) {
				objSequelizemeta = await models.sequelize.sequelizemeta.create(newSequelizemeta);
			} else {
				objSequelizemeta = new models.mongoose.sequelizemeta(newSequelizemeta);
				await objSequelizemeta.save();
			}
			return objSequelizemeta;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateSequelizemeta(name, updateSequelizemeta) {
		try {
			let objSequelizemeta;
			if(sql) {
				objSequelizemeta = await models.sequelize.sequelizemeta.findOne({where: { name: util.String(name) }});
				if (objSequelizemeta) {
					await models.sequelize.sequelizemeta.update(updateSequelizemeta, { where: { name: util.String(name) } });
					objSequelizemeta = await models.sequelize.sequelizemeta.findOne({where: { name: util.String(name) }});
				}
			} else {
				delete updateSequelizemeta._id;
				objSequelizemeta = await models.mongoose.sequelizemeta.findOneAndUpdate({name:name}, {$set: updateSequelizemeta}, {new: true});
			}
			return objSequelizemeta;
		} catch (error) {
			throw error;
		}
	}

	static async getASequelizemeta(name, query) {
		try {
			let objSequelizemeta;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objSequelizemeta = await models.sequelize.sequelizemeta.findOne({
					    where: where && !where.where ? where : { name: util.String(name) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objSequelizemeta = await models.mongoose.sequelizemeta.find({name:util.String(name)}).select(query.select);
			}
			return objSequelizemeta;
		} catch (error) {
			throw error;
		}
	}

	static async deleteSequelizemeta(name) {
		try {
			let objSequelizemeta;
			if(sql) {
				objSequelizemeta = await models.sequelize.sequelizemeta.findOne({ where: { name: util.String(name) } });
				if (objSequelizemeta) {
					await models.sequelize.sequelizemeta.destroy({where: { name: util.String(name) }});
				}
			} else {
				objSequelizemeta = await models.mongoose.sequelizemeta.deleteOne({name:util.String(name)});
			}
			return objSequelizemeta;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByName(name, query = {}) {
    	try {
    		let objSequelizemeta;
    		if(sql) {
    			objSequelizemeta = await models.sequelize.sequelizemeta.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { name: name },
    			});
    		} else {
    			objSequelizemeta = await models.mongoose.sequelizemeta.findOne({name: name});
    		}
    		return objSequelizemeta;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateSequelizemetaByName(name, updateSequelizemeta) {
    	try {
    		let objSequelizemeta;
    		if(sql) {
    			objSequelizemeta = await models.sequelize.sequelizemeta.findOne({where: { name: name }});
    			if (objSequelizemeta) {
    				objSequelizemeta = await models.sequelize.sequelizemeta.update(updateSequelizemeta, { where: { name: name } });
    			}
    		} else {
    			objSequelizemeta = await models.mongoose.sequelizemeta.findOneAndUpdate({name: name}, {$set: updateSequelizemeta}, {new: true});
    		}
    		return objSequelizemeta;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.sequelizemeta.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.sequelizemeta.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userSequelizemeta = await models.sequelize.sequelizemeta.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userSequelizemeta.length ; k++) {
    						let userSequelizemeta = userSequelizemeta[k].dataValues;
    						let aUserSequelizemetaValues = Object.values(userSequelizemeta);
    						let aUserSequelizemetaFields = Object.keys(userSequelizemeta);
    						for (let n = 0 ; n < aUserSequelizemetaValues.length ; n++) {
    							let userSequelizemetaField = aUserSequelizemetaFields[n];
    							let userSequelizemetaValue = aUserSequelizemetaValues[n];
    							if (!dtValues.find(param => param.value == userSequelizemetaValue && param.field == userSequelizemetaField)) {
    								dtValues.push({value:userSequelizemetaValue, count:1, label:userSequelizemetaValue, field:userSequelizemetaField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userSequelizemetaValue && dtValue.field == userSequelizemetaField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userSequelizemeta];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = SequelizemetaService;
//</es-section>
