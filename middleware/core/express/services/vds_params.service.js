/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:07 GMT-0400 (GMT-04:00)
 * Time: 0:54:7
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:07 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:7
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdParamService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsParams(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsParams.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsParams.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsParams(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsParams.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsParams.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdParam(newVdParam) {
		try {
			let objVdParam;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsParams.primaryKeys._id.type.toString())) {
			    newVdParam._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdParam.id) {
              let max = await models.sequelize.vdsParams.max('id');
              newVdParam.id = newVdParam.id ? newVdParam.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdParam = await models.sequelize.vdsParams.create(newVdParam);
			} else {
				objVdParam = new models.mongoose.vdsParams(newVdParam);
				await objVdParam.save();
			}
			return objVdParam;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdParam(_id, updateVdParam) {
		try {
			let objVdParam;
			if(sql) {
				objVdParam = await models.sequelize.vdsParams.findOne({where: { _id: util.String(_id) }});
				if (objVdParam) {
					await models.sequelize.vdsParams.update(updateVdParam, { where: { _id: util.String(_id) } });
					objVdParam = await models.sequelize.vdsParams.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdParam._id;
				objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({_id:_id}, {$set: updateVdParam}, {new: true});
			}
			return objVdParam;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdParam(_id, query) {
		try {
			let objVdParam;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdParam = await models.sequelize.vdsParams.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdParam = await models.mongoose.vdsParams.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdParam;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdParam(_id) {
		try {
			let objVdParam;
			if(sql) {
				objVdParam = await models.sequelize.vdsParams.findOne({ where: { _id: util.String(_id) } });
				if (objVdParam) {
					await models.sequelize.vdsParams.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdParam = await models.mongoose.vdsParams.deleteOne({_id:util.String(_id)});
			}
			return objVdParam;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({_id: Id});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({id: id});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByParOrder(parOrder, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { par_order: parOrder },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({par_order: parOrder});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByParCod(parCod, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { par_cod: parCod },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({par_cod: parCod});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByParDescription(parDescription, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { par_description: parDescription },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({par_description: parDescription});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByParAbbr(parAbbr, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { par_abbr: parAbbr },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({par_abbr: parAbbr});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByParGroup(parGroup, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { par_group: parGroup },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({par_group: parGroup});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({createdById: createdbyid});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({updatedById: updatedbyid});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByParDictionaryId(parDictionaryId, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { par_dictionary_id: parDictionaryId },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({par_dictionary_id: parDictionaryId});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByParStatusId(parStatusId, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { par_status_id: parStatusId },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({par_status_id: parStatusId});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({dueAt: dueat});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({createdAt: createdat});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOne({updatedAt: updatedat});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdParamByUid(Id, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { _id: Id }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { _id: Id } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({_id: Id}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamById(id, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { id: id }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { id: id } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({id: id}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByParOrder(parOrder, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { par_order: parOrder }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { par_order: parOrder } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({par_order: parOrder}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByParCod(parCod, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { par_cod: parCod }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { par_cod: parCod } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({par_cod: parCod}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByParDescription(parDescription, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { par_description: parDescription }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { par_description: parDescription } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({par_description: parDescription}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByParAbbr(parAbbr, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { par_abbr: parAbbr }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { par_abbr: parAbbr } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({par_abbr: parAbbr}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByParGroup(parGroup, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { par_group: parGroup }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { par_group: parGroup } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({par_group: parGroup}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByCreatedbyid(createdbyid, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { createdById: createdbyid }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByUpdatedbyid(updatedbyid, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { updatedById: updatedbyid }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByParDictionaryId(parDictionaryId, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { par_dictionary_id: parDictionaryId }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { par_dictionary_id: parDictionaryId } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({par_dictionary_id: parDictionaryId}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByParStatusId(parStatusId, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { par_status_id: parStatusId }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { par_status_id: parStatusId } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({par_status_id: parStatusId}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByDueat(dueat, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { dueAt: dueat }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({dueAt: dueat}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByCreatedat(createdat, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { createdAt: createdat }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({createdAt: createdat}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdParamByUpdatedat(updatedat, updateVdParam) {
    	try {
    		let objVdParam;
    		if(sql) {
    			objVdParam = await models.sequelize.vdsParams.findOne({where: { updatedAt: updatedat }});
    			if (objVdParam) {
    				objVdParam = await models.sequelize.vdsParams.update(updateVdParam, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdParam = await models.mongoose.vdsParams.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdParam}, {new: true});
    		}
    		return objVdParam;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsDictionariesParDictionaryWithDicCode(select = ['_id', 'dic_code'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsDictionaries.findAll({
                    attributes: select,
                    where: { dic_group: {[Op.like]: '%grp_par_dictionary%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsDictionaries.find({dic_group: {$regex : ".*grp_par_dictionary.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsParamsByCreatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdParam, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdParam;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsParamsByUpdatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdParam, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdParam;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsParamsByParDictionary(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdParam, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdParam;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsParamsByParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdParam, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdParam = await models.sequelize.vdsParams.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdParam = await models.mongoose.vdsParams.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdParam;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsParams.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsParams.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsParams = await models.sequelize.vdsParams.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsParams.length ; k++) {
    						let userVdParam = userVdsParams[k].dataValues;
    						let aUserVdParamValues = Object.values(userVdParam);
    						let aUserVdParamFields = Object.keys(userVdParam);
    						for (let n = 0 ; n < aUserVdParamValues.length ; n++) {
    							let userVdParamField = aUserVdParamFields[n];
    							let userVdParamValue = aUserVdParamValues[n];
    							if (!dtValues.find(param => param.value == userVdParamValue && param.field == userVdParamField)) {
    								dtValues.push({value:userVdParamValue, count:1, label:userVdParamValue, field:userVdParamField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdParamValue && dtValue.field == userVdParamField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsParams];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdParamService;
//</es-section>
