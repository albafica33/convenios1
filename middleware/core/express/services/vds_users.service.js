/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Time: 0:54:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:13
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdUserService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsUsers(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsUsers.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsUsers.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsUsers(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsUsers.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsUsers.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdUser(newVdUser) {
		try {
			let objVdUser;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsUsers.primaryKeys._id.type.toString())) {
			    newVdUser._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdUser.id) {
              let max = await models.sequelize.vdsUsers.max('id');
              newVdUser.id = newVdUser.id ? newVdUser.id : max ? max+1 : 1;
			}
			
			
			newVdUser.user_hash = await bcrypt.hash(newVdUser.user_hash, bcrypt.genSaltSync(8)).then(encripted => {return encripted});
			
			
			if(sql) {
				objVdUser = await models.sequelize.vdsUsers.create(newVdUser);
			} else {
				objVdUser = new models.mongoose.vdsUsers(newVdUser);
				await objVdUser.save();
			}
			return objVdUser;
		} catch (error) {
			throw error;
		}
	}
	
	static async findOneByUserNameAndUserHash(userName, userHash, query = {}) {
    	try {
    		let objUser;
		    userHash = await bcrypt.hash(userHash);
    		if(sql) {
    			objUser = await models.sequelize[esConfig.esUser].findOne({
    				attributes:query.select ? query.select.split(',') : null,
    				where: { user_name: userName, user_hash: userHash},
    			});
    		} else {
    			objUser = await models.mongoose[esConfig.esUser].findOne({user_name: userName, user_hash: userHash});
    		}
    		return objUser;
    	} catch (error) {
    		throw error;
    	}
    }
    
	static async updateVdUser(_id, updateVdUser) {
		try {
			let objVdUser;
			if(sql) {
				objVdUser = await models.sequelize.vdsUsers.findOne({where: { _id: util.String(_id) }});
				if (objVdUser) {
					await models.sequelize.vdsUsers.update(updateVdUser, { where: { _id: util.String(_id) } });
					objVdUser = await models.sequelize.vdsUsers.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdUser._id;
				objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({_id:_id}, {$set: updateVdUser}, {new: true});
			}
			return objVdUser;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdUser(_id, query) {
		try {
			let objVdUser;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdUser = await models.sequelize.vdsUsers.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdUser = await models.mongoose.vdsUsers.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdUser;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdUser(_id) {
		try {
			let objVdUser;
			if(sql) {
				objVdUser = await models.sequelize.vdsUsers.findOne({ where: { _id: util.String(_id) } });
				if (objVdUser) {
					await models.sequelize.vdsUsers.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdUser = await models.mongoose.vdsUsers.deleteOne({_id:util.String(_id)});
			}
			return objVdUser;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({_id: Id});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({id: id});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstado(estado, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado: estado },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({estado: estado});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUserName(userName, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { user_name: userName },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({user_name: userName});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUserHash(userHash, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { user_hash: userHash },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({user_hash: userHash});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCorreo(correo, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { correo: correo },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({correo: correo});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByRecordatorio(recordatorio, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { recordatorio: recordatorio },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({recordatorio: recordatorio});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({createdBy: createdby});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({updatedBy: updatedby});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdPersona(idPersona, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_persona: idPersona },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({id_persona: idPersona});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({dueAt: dueat});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({createdAt: createdat});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOne({updatedAt: updatedat});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdUserByUid(Id, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { _id: Id }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { _id: Id } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({_id: Id}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserById(id, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { id: id }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { id: id } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({id: id}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByEstado(estado, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { estado: estado }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { estado: estado } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({estado: estado}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByUserName(userName, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { user_name: userName }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { user_name: userName } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({user_name: userName}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByUserHash(userHash, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { user_hash: userHash }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { user_hash: userHash } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({user_hash: userHash}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByCorreo(correo, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { correo: correo }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { correo: correo } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({correo: correo}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByRecordatorio(recordatorio, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { recordatorio: recordatorio }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { recordatorio: recordatorio } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({recordatorio: recordatorio}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByCreatedby(createdby, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { createdBy: createdby }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({createdBy: createdby}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByUpdatedby(updatedby, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { updatedBy: updatedby }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByIdPersona(idPersona, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { id_persona: idPersona }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { id_persona: idPersona } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({id_persona: idPersona}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByDueat(dueat, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { dueAt: dueat }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({dueAt: dueat}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByCreatedat(createdat, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { createdAt: createdat }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({createdAt: createdat}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdUserByUpdatedat(updatedat, updateVdUser) {
    	try {
    		let objVdUser;
    		if(sql) {
    			objVdUser = await models.sequelize.vdsUsers.findOne({where: { updatedAt: updatedat }});
    			if (objVdUser) {
    				objVdUser = await models.sequelize.vdsUsers.update(updateVdUser, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdUser = await models.mongoose.vdsUsers.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdUser}, {new: true});
    		}
    		return objVdUser;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsUsers.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsUsers.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsUsers = await models.sequelize.vdsUsers.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsUsers.length ; k++) {
    						let userVdUser = userVdsUsers[k].dataValues;
    						let aUserVdUserValues = Object.values(userVdUser);
    						let aUserVdUserFields = Object.keys(userVdUser);
    						for (let n = 0 ; n < aUserVdUserValues.length ; n++) {
    							let userVdUserField = aUserVdUserFields[n];
    							let userVdUserValue = aUserVdUserValues[n];
    							if (!dtValues.find(param => param.value == userVdUserValue && param.field == userVdUserField)) {
    								dtValues.push({value:userVdUserValue, count:1, label:userVdUserValue, field:userVdUserField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdUserValue && dtValue.field == userVdUserField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsUsers];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdUserService;
//</es-section>
