
/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:10 GMT-0400 (GMT-04:00)
 * Time: 0:54:10
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:10 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:10
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdPersonaService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsPersona(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsPersona.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsPersona.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsPersona(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsPersona.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsPersona.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdPersona(newVdPersona) {
		try {
			let objVdPersona;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsPersona.primaryKeys._id.type.toString())) {
			    newVdPersona._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdPersona.id) {
              let max = await models.sequelize.vdsPersona.max('id');
              newVdPersona.id = newVdPersona.id ? newVdPersona.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdPersona = await models.sequelize.vdsPersona.create(newVdPersona);
			} else {
				objVdPersona = new models.mongoose.vdsPersona(newVdPersona);
				await objVdPersona.save();
			}
			return objVdPersona;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdPersona(_id, updateVdPersona) {
		try {
			let objVdPersona;
			if(sql) {
				objVdPersona = await models.sequelize.vdsPersona.findOne({where: { _id: util.String(_id) }});
				if (objVdPersona) {
					await models.sequelize.vdsPersona.update(updateVdPersona, { where: { _id: util.String(_id) } });
					objVdPersona = await models.sequelize.vdsPersona.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdPersona._id;
				objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({_id:_id}, {$set: updateVdPersona}, {new: true});
			}
			return objVdPersona;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdPersona(_id, query) {
		try {
			let objVdPersona;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdPersona = await models.sequelize.vdsPersona.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdPersona = await models.mongoose.vdsPersona.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdPersona;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdPersona(_id) {
		try {
			let objVdPersona;
			if(sql) {
				objVdPersona = await models.sequelize.vdsPersona.findOne({ where: { _id: util.String(_id) } });
				if (objVdPersona) {
					await models.sequelize.vdsPersona.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdPersona = await models.mongoose.vdsPersona.deleteOne({_id:util.String(_id)});
			}
			return objVdPersona;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({_id: Id});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({id: id});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstado(estado, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado: estado },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({estado: estado});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByTelefono(telefono, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { telefono: telefono },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({telefono: telefono});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCelular(celular, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { celular: celular },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({celular: celular});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByNombres(nombres, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { nombres: nombres },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({nombres: nombres});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPaterno(paterno, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { paterno: paterno },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({paterno: paterno});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaterno(materno, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { materno: materno },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({materno: materno});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCasada(casada, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { casada: casada },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({casada: casada});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCi(ci, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { ci: ci },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({ci: ci});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCorreo(correo, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { correo: correo },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({correo: correo});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDireccion(direccion, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { direccion: direccion },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({direccion: direccion});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({createdBy: createdby});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({updatedBy: updatedby});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCiExpedido(ciExpedido, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { ci_expedido: ciExpedido },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({ci_expedido: ciExpedido});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdEstadoCivil(idEstadoCivil, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_estado_civil: idEstadoCivil },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({id_estado_civil: idEstadoCivil});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdSexo(idSexo, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_sexo: idSexo },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({id_sexo: idSexo});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdMunicipio(idMunicipio, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_municipio: idMunicipio },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({id_municipio: idMunicipio});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdProvincia(idProvincia, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_provincia: idProvincia },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({id_provincia: idProvincia});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdCiudad(idCiudad, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_ciudad: idCiudad },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({id_ciudad: idCiudad});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdPais(idPais, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_pais: idPais },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({id_pais: idPais});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({dueAt: dueat});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({createdAt: createdat});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOne({updatedAt: updatedat});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdPersonaByUid(Id, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { _id: Id }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { _id: Id } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({_id: Id}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaById(id, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { id: id }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { id: id } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({id: id}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByEstado(estado, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { estado: estado }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { estado: estado } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({estado: estado}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByTelefono(telefono, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { telefono: telefono }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { telefono: telefono } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({telefono: telefono}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByCelular(celular, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { celular: celular }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { celular: celular } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({celular: celular}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByNombres(nombres, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { nombres: nombres }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { nombres: nombres } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({nombres: nombres}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByPaterno(paterno, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { paterno: paterno }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { paterno: paterno } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({paterno: paterno}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByMaterno(materno, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { materno: materno }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { materno: materno } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({materno: materno}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByCasada(casada, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { casada: casada }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { casada: casada } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({casada: casada}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByCi(ci, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { ci: ci }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { ci: ci } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({ci: ci}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByCorreo(correo, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { correo: correo }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { correo: correo } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({correo: correo}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByDireccion(direccion, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { direccion: direccion }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { direccion: direccion } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({direccion: direccion}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByCreatedby(createdby, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { createdBy: createdby }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({createdBy: createdby}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByUpdatedby(updatedby, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { updatedBy: updatedby }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByCiExpedido(ciExpedido, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { ci_expedido: ciExpedido }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { ci_expedido: ciExpedido } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({ci_expedido: ciExpedido}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByIdEstadoCivil(idEstadoCivil, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { id_estado_civil: idEstadoCivil }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { id_estado_civil: idEstadoCivil } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({id_estado_civil: idEstadoCivil}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByIdSexo(idSexo, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { id_sexo: idSexo }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { id_sexo: idSexo } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({id_sexo: idSexo}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByIdMunicipio(idMunicipio, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { id_municipio: idMunicipio }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { id_municipio: idMunicipio } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({id_municipio: idMunicipio}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByIdProvincia(idProvincia, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { id_provincia: idProvincia }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { id_provincia: idProvincia } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({id_provincia: idProvincia}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByIdCiudad(idCiudad, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { id_ciudad: idCiudad }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { id_ciudad: idCiudad } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({id_ciudad: idCiudad}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByIdPais(idPais, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { id_pais: idPais }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { id_pais: idPais } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({id_pais: idPais}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByDueat(dueat, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { dueAt: dueat }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({dueAt: dueat}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByCreatedat(createdat, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { createdAt: createdat }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({createdAt: createdat}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPersonaByUpdatedat(updatedat, updateVdPersona) {
    	try {
    		let objVdPersona;
    		if(sql) {
    			objVdPersona = await models.sequelize.vdsPersona.findOne({where: { updatedAt: updatedat }});
    			if (objVdPersona) {
    				objVdPersona = await models.sequelize.vdsPersona.update(updateVdPersona, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdPersona = await models.mongoose.vdsPersona.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdPersona}, {new: true});
    		}
    		return objVdPersona;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsPersona.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsPersona.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsPersona = await models.sequelize.vdsPersona.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsPersona.length ; k++) {
    						let userVdPersona = userVdsPersona[k].dataValues;
    						let aUserVdPersonaValues = Object.values(userVdPersona);
    						let aUserVdPersonaFields = Object.keys(userVdPersona);
    						for (let n = 0 ; n < aUserVdPersonaValues.length ; n++) {
    							let userVdPersonaField = aUserVdPersonaFields[n];
    							let userVdPersonaValue = aUserVdPersonaValues[n];
    							if (!dtValues.find(param => param.value == userVdPersonaValue && param.field == userVdPersonaField)) {
    								dtValues.push({value:userVdPersonaValue, count:1, label:userVdPersonaValue, field:userVdPersonaField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdPersonaValue && dtValue.field == userVdPersonaField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsPersona];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdPersonaService;
//</es-section>
