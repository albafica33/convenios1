/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Time: 0:54:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:11
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdRegUsuarioService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsRegUsuario(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsRegUsuario.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsRegUsuario.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsRegUsuario(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsRegUsuario.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsRegUsuario.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdRegUsuario(newVdRegUsuario) {
		try {
			let objVdRegUsuario;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsRegUsuario.primaryKeys._id.type.toString())) {
			    newVdRegUsuario._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdRegUsuario.id) {
              let max = await models.sequelize.vdsRegUsuario.max('id');
              newVdRegUsuario.id = newVdRegUsuario.id ? newVdRegUsuario.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdRegUsuario = await models.sequelize.vdsRegUsuario.create(newVdRegUsuario);
			} else {
				objVdRegUsuario = new models.mongoose.vdsRegUsuario(newVdRegUsuario);
				await objVdRegUsuario.save();
			}
			return objVdRegUsuario;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdRegUsuario(_id, updateVdRegUsuario) {
		try {
			let objVdRegUsuario;
			if(sql) {
				objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { _id: util.String(_id) }});
				if (objVdRegUsuario) {
					await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { _id: util.String(_id) } });
					objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdRegUsuario._id;
				objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({_id:_id}, {$set: updateVdRegUsuario}, {new: true});
			}
			return objVdRegUsuario;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdRegUsuario(_id, query) {
		try {
			let objVdRegUsuario;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdRegUsuario = await models.mongoose.vdsRegUsuario.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdRegUsuario;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdRegUsuario(_id) {
		try {
			let objVdRegUsuario;
			if(sql) {
				objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({ where: { _id: util.String(_id) } });
				if (objVdRegUsuario) {
					await models.sequelize.vdsRegUsuario.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdRegUsuario = await models.mongoose.vdsRegUsuario.deleteOne({_id:util.String(_id)});
			}
			return objVdRegUsuario;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({_id: Id});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({id: id});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdUsuario(idUsuario, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_usuario: idUsuario },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({id_usuario: idUsuario});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdReg(idReg, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_reg: idReg },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({id_reg: idReg});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({createdById: createdbyid});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({updatedById: updatedbyid});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({dueAt: dueat});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({createdAt: createdat});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOne({updatedAt: updatedat});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdRegUsuarioByUid(Id, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { _id: Id }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { _id: Id } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({_id: Id}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRegUsuarioById(id, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { id: id }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { id: id } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({id: id}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRegUsuarioByIdUsuario(idUsuario, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { id_usuario: idUsuario }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { id_usuario: idUsuario } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({id_usuario: idUsuario}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRegUsuarioByIdReg(idReg, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { id_reg: idReg }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { id_reg: idReg } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({id_reg: idReg}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRegUsuarioByCreatedbyid(createdbyid, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { createdById: createdbyid }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRegUsuarioByUpdatedbyid(updatedbyid, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { updatedById: updatedbyid }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRegUsuarioByDueat(dueat, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { dueAt: dueat }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({dueAt: dueat}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRegUsuarioByCreatedat(createdat, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { createdAt: createdat }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({createdAt: createdat}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRegUsuarioByUpdatedat(updatedat, updateVdRegUsuario) {
    	try {
    		let objVdRegUsuario;
    		if(sql) {
    			objVdRegUsuario = await models.sequelize.vdsRegUsuario.findOne({where: { updatedAt: updatedat }});
    			if (objVdRegUsuario) {
    				objVdRegUsuario = await models.sequelize.vdsRegUsuario.update(updateVdRegUsuario, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdRegUsuario = await models.mongoose.vdsRegUsuario.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdRegUsuario}, {new: true});
    		}
    		return objVdRegUsuario;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsRegUsuario.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsRegUsuario.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsRegUsuario = await models.sequelize.vdsRegUsuario.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsRegUsuario.length ; k++) {
    						let userVdRegUsuario = userVdsRegUsuario[k].dataValues;
    						let aUserVdRegUsuarioValues = Object.values(userVdRegUsuario);
    						let aUserVdRegUsuarioFields = Object.keys(userVdRegUsuario);
    						for (let n = 0 ; n < aUserVdRegUsuarioValues.length ; n++) {
    							let userVdRegUsuarioField = aUserVdRegUsuarioFields[n];
    							let userVdRegUsuarioValue = aUserVdRegUsuarioValues[n];
    							if (!dtValues.find(param => param.value == userVdRegUsuarioValue && param.field == userVdRegUsuarioField)) {
    								dtValues.push({value:userVdRegUsuarioValue, count:1, label:userVdRegUsuarioValue, field:userVdRegUsuarioField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdRegUsuarioValue && dtValue.field == userVdRegUsuarioField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsRegUsuario];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdRegUsuarioService;
//</es-section>
