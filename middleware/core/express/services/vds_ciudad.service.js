/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Time: 0:53:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:58
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdCiudadService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsCiudad(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsCiudad.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsCiudad.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsCiudad(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsCiudad.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsCiudad.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdCiudad(newVdCiudad) {
		try {
			let objVdCiudad;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsCiudad.primaryKeys._id.type.toString())) {
			    newVdCiudad._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdCiudad.id) {
              let max = await models.sequelize.vdsCiudad.max('id');
              newVdCiudad.id = newVdCiudad.id ? newVdCiudad.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdCiudad = await models.sequelize.vdsCiudad.create(newVdCiudad);
			} else {
				objVdCiudad = new models.mongoose.vdsCiudad(newVdCiudad);
				await objVdCiudad.save();
			}
			return objVdCiudad;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdCiudad(_id, updateVdCiudad) {
		try {
			let objVdCiudad;
			if(sql) {
				objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { _id: util.String(_id) }});
				if (objVdCiudad) {
					await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { _id: util.String(_id) } });
					objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdCiudad._id;
				objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({_id:_id}, {$set: updateVdCiudad}, {new: true});
			}
			return objVdCiudad;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdCiudad(_id, query) {
		try {
			let objVdCiudad;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdCiudad = await models.sequelize.vdsCiudad.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdCiudad = await models.mongoose.vdsCiudad.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdCiudad;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdCiudad(_id) {
		try {
			let objVdCiudad;
			if(sql) {
				objVdCiudad = await models.sequelize.vdsCiudad.findOne({ where: { _id: util.String(_id) } });
				if (objVdCiudad) {
					await models.sequelize.vdsCiudad.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdCiudad = await models.mongoose.vdsCiudad.deleteOne({_id:util.String(_id)});
			}
			return objVdCiudad;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({_id: Id});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({id: id});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstado(estado, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado: estado },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({estado: estado});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCiudad(ciudad, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { ciudad: ciudad },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({ciudad: ciudad});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByAbrev(abrev, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { abrev: abrev },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({abrev: abrev});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({createdBy: createdby});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({updatedBy: updatedby});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdPais(idPais, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_pais: idPais },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({id_pais: idPais});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({dueAt: dueat});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({createdAt: createdat});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOne({updatedAt: updatedat});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdCiudadByUid(Id, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { _id: Id }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { _id: Id } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({_id: Id}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadById(id, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { id: id }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { id: id } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({id: id}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByEstado(estado, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { estado: estado }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { estado: estado } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({estado: estado}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByCiudad(ciudad, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { ciudad: ciudad }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { ciudad: ciudad } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({ciudad: ciudad}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByAbrev(abrev, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { abrev: abrev }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { abrev: abrev } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({abrev: abrev}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByCreatedby(createdby, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { createdBy: createdby }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({createdBy: createdby}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByUpdatedby(updatedby, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { updatedBy: updatedby }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByIdPais(idPais, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { id_pais: idPais }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { id_pais: idPais } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({id_pais: idPais}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByDueat(dueat, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { dueAt: dueat }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({dueAt: dueat}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByCreatedat(createdat, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { createdAt: createdat }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({createdAt: createdat}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdCiudadByUpdatedat(updatedat, updateVdCiudad) {
    	try {
    		let objVdCiudad;
    		if(sql) {
    			objVdCiudad = await models.sequelize.vdsCiudad.findOne({where: { updatedAt: updatedat }});
    			if (objVdCiudad) {
    				objVdCiudad = await models.sequelize.vdsCiudad.update(updateVdCiudad, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdCiudad = await models.mongoose.vdsCiudad.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdCiudad}, {new: true});
    		}
    		return objVdCiudad;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsCiudad.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsCiudad.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsCiudad = await models.sequelize.vdsCiudad.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsCiudad.length ; k++) {
    						let userVdCiudad = userVdsCiudad[k].dataValues;
    						let aUserVdCiudadValues = Object.values(userVdCiudad);
    						let aUserVdCiudadFields = Object.keys(userVdCiudad);
    						for (let n = 0 ; n < aUserVdCiudadValues.length ; n++) {
    							let userVdCiudadField = aUserVdCiudadFields[n];
    							let userVdCiudadValue = aUserVdCiudadValues[n];
    							if (!dtValues.find(param => param.value == userVdCiudadValue && param.field == userVdCiudadField)) {
    								dtValues.push({value:userVdCiudadValue, count:1, label:userVdCiudadValue, field:userVdCiudadField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdCiudadValue && dtValue.field == userVdCiudadField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsCiudad];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdCiudadService;
//</es-section>
