/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Time: 0:54:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:1
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdLogService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsLogs(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsLogs.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsLogs.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsLogs(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsLogs.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsLogs.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdLog(newVdLog) {
		try {
			let objVdLog;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsLogs.primaryKeys._id.type.toString())) {
			    newVdLog._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdLog.id) {
              let max = await models.sequelize.vdsLogs.max('id');
              newVdLog.id = newVdLog.id ? newVdLog.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdLog = await models.sequelize.vdsLogs.create(newVdLog);
			} else {
				objVdLog = new models.mongoose.vdsLogs(newVdLog);
				await objVdLog.save();
			}
			return objVdLog;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdLog(_id, updateVdLog) {
		try {
			let objVdLog;
			if(sql) {
				objVdLog = await models.sequelize.vdsLogs.findOne({where: { _id: util.String(_id) }});
				if (objVdLog) {
					await models.sequelize.vdsLogs.update(updateVdLog, { where: { _id: util.String(_id) } });
					objVdLog = await models.sequelize.vdsLogs.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdLog._id;
				objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({_id:_id}, {$set: updateVdLog}, {new: true});
			}
			return objVdLog;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdLog(_id, query) {
		try {
			let objVdLog;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdLog = await models.sequelize.vdsLogs.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdLog = await models.mongoose.vdsLogs.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdLog;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdLog(_id) {
		try {
			let objVdLog;
			if(sql) {
				objVdLog = await models.sequelize.vdsLogs.findOne({ where: { _id: util.String(_id) } });
				if (objVdLog) {
					await models.sequelize.vdsLogs.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdLog = await models.mongoose.vdsLogs.deleteOne({_id:util.String(_id)});
			}
			return objVdLog;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({_id: Id});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({id: id});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByLogObjId(logObjId, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { log_obj_id: logObjId },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({log_obj_id: logObjId});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByLogDescription(logDescription, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { log_description: logDescription },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({log_description: logDescription});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByLogGroup(logGroup, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { log_group: logGroup },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({log_group: logGroup});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({createdById: createdbyid});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({updatedById: updatedbyid});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByLogParStatusId(logParStatusId, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { log_par_status_id: logParStatusId },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({log_par_status_id: logParStatusId});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({dueAt: dueat});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({createdAt: createdat});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOne({updatedAt: updatedat});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdLogByUid(Id, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { _id: Id }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { _id: Id } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({_id: Id}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogById(id, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { id: id }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { id: id } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({id: id}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByLogObjId(logObjId, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { log_obj_id: logObjId }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { log_obj_id: logObjId } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({log_obj_id: logObjId}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByLogDescription(logDescription, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { log_description: logDescription }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { log_description: logDescription } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({log_description: logDescription}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByLogGroup(logGroup, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { log_group: logGroup }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { log_group: logGroup } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({log_group: logGroup}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByCreatedbyid(createdbyid, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { createdById: createdbyid }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByUpdatedbyid(updatedbyid, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { updatedById: updatedbyid }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByLogParStatusId(logParStatusId, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { log_par_status_id: logParStatusId }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { log_par_status_id: logParStatusId } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({log_par_status_id: logParStatusId}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByDueat(dueat, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { dueAt: dueat }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({dueAt: dueat}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByCreatedat(createdat, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { createdAt: createdat }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({createdAt: createdat}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdLogByUpdatedat(updatedat, updateVdLog) {
    	try {
    		let objVdLog;
    		if(sql) {
    			objVdLog = await models.sequelize.vdsLogs.findOne({where: { updatedAt: updatedat }});
    			if (objVdLog) {
    				objVdLog = await models.sequelize.vdsLogs.update(updateVdLog, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdLog = await models.mongoose.vdsLogs.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdLog}, {new: true});
    		}
    		return objVdLog;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsLogParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_log_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_log_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsLogsByCreatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsLogParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdLog, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdLog;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsLogsByUpdatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsLogParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdLog, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdLog;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsLogsByLogParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsLogParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdLog, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdLog = await models.sequelize.vdsLogs.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdLog = await models.mongoose.vdsLogs.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdLog;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsLogs.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsLogs.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsLogs = await models.sequelize.vdsLogs.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsLogs.length ; k++) {
    						let userVdLog = userVdsLogs[k].dataValues;
    						let aUserVdLogValues = Object.values(userVdLog);
    						let aUserVdLogFields = Object.keys(userVdLog);
    						for (let n = 0 ; n < aUserVdLogValues.length ; n++) {
    							let userVdLogField = aUserVdLogFields[n];
    							let userVdLogValue = aUserVdLogValues[n];
    							if (!dtValues.find(param => param.value == userVdLogValue && param.field == userVdLogField)) {
    								dtValues.push({value:userVdLogValue, count:1, label:userVdLogValue, field:userVdLogField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdLogValue && dtValue.field == userVdLogField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsLogs];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdLogService;
//</es-section>
