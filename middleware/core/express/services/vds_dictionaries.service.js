/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:00 GMT-0400 (GMT-04:00)
 * Time: 0:54:0
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:00 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:0
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdDictionaryService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsDictionaries(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsDictionaries.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsDictionaries.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsDictionaries(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsDictionaries.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsDictionaries.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdDictionary(newVdDictionary) {
		try {
			let objVdDictionary;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsDictionaries.primaryKeys._id.type.toString())) {
			    newVdDictionary._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdDictionary.id) {
              let max = await models.sequelize.vdsDictionaries.max('id');
              newVdDictionary.id = newVdDictionary.id ? newVdDictionary.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdDictionary = await models.sequelize.vdsDictionaries.create(newVdDictionary);
			} else {
				objVdDictionary = new models.mongoose.vdsDictionaries(newVdDictionary);
				await objVdDictionary.save();
			}
			return objVdDictionary;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdDictionary(_id, updateVdDictionary) {
		try {
			let objVdDictionary;
			if(sql) {
				objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { _id: util.String(_id) }});
				if (objVdDictionary) {
					await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { _id: util.String(_id) } });
					objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdDictionary._id;
				objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({_id:_id}, {$set: updateVdDictionary}, {new: true});
			}
			return objVdDictionary;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdDictionary(_id, query) {
		try {
			let objVdDictionary;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdDictionary = await models.mongoose.vdsDictionaries.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdDictionary;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdDictionary(_id) {
		try {
			let objVdDictionary;
			if(sql) {
				objVdDictionary = await models.sequelize.vdsDictionaries.findOne({ where: { _id: util.String(_id) } });
				if (objVdDictionary) {
					await models.sequelize.vdsDictionaries.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdDictionary = await models.mongoose.vdsDictionaries.deleteOne({_id:util.String(_id)});
			}
			return objVdDictionary;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({_id: Id});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({id: id});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDicCode(dicCode, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dic_code: dicCode },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({dic_code: dicCode});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDicDescription(dicDescription, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dic_description: dicDescription },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({dic_description: dicDescription});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDicGroup(dicGroup, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dic_group: dicGroup },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({dic_group: dicGroup});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDicParStatusId(dicParStatusId, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dic_par_status_id: dicParStatusId },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({dic_par_status_id: dicParStatusId});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({createdById: createdbyid});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({updatedById: updatedbyid});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({dueAt: dueat});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({createdAt: createdat});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOne({updatedAt: updatedat});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdDictionaryByUid(Id, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { _id: Id }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { _id: Id } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({_id: Id}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryById(id, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { id: id }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { id: id } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({id: id}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByDicCode(dicCode, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { dic_code: dicCode }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { dic_code: dicCode } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({dic_code: dicCode}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByDicDescription(dicDescription, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { dic_description: dicDescription }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { dic_description: dicDescription } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({dic_description: dicDescription}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByDicGroup(dicGroup, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { dic_group: dicGroup }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { dic_group: dicGroup } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({dic_group: dicGroup}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByDicParStatusId(dicParStatusId, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { dic_par_status_id: dicParStatusId }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { dic_par_status_id: dicParStatusId } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({dic_par_status_id: dicParStatusId}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByCreatedbyid(createdbyid, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { createdById: createdbyid }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByUpdatedbyid(updatedbyid, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { updatedById: updatedbyid }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByDueat(dueat, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { dueAt: dueat }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({dueAt: dueat}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByCreatedat(createdat, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { createdAt: createdat }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({createdAt: createdat}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdDictionaryByUpdatedat(updatedat, updateVdDictionary) {
    	try {
    		let objVdDictionary;
    		if(sql) {
    			objVdDictionary = await models.sequelize.vdsDictionaries.findOne({where: { updatedAt: updatedat }});
    			if (objVdDictionary) {
    				objVdDictionary = await models.sequelize.vdsDictionaries.update(updateVdDictionary, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdDictionary = await models.mongoose.vdsDictionaries.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdDictionary}, {new: true});
    		}
    		return objVdDictionary;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsParamsDicParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_dic_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_dic_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsDictionariesByDicParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsDicParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdDictionary, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdDictionary;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsDictionariesByCreatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsDicParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdDictionary, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdDictionary;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsDictionariesByUpdatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsDicParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdDictionary, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdDictionary = await models.sequelize.vdsDictionaries.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdDictionary = await models.mongoose.vdsDictionaries.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdDictionary;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsDictionaries.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsDictionaries.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsDictionaries = await models.sequelize.vdsDictionaries.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsDictionaries.length ; k++) {
    						let userVdDictionary = userVdsDictionaries[k].dataValues;
    						let aUserVdDictionaryValues = Object.values(userVdDictionary);
    						let aUserVdDictionaryFields = Object.keys(userVdDictionary);
    						for (let n = 0 ; n < aUserVdDictionaryValues.length ; n++) {
    							let userVdDictionaryField = aUserVdDictionaryFields[n];
    							let userVdDictionaryValue = aUserVdDictionaryValues[n];
    							if (!dtValues.find(param => param.value == userVdDictionaryValue && param.field == userVdDictionaryField)) {
    								dtValues.push({value:userVdDictionaryValue, count:1, label:userVdDictionaryValue, field:userVdDictionaryField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdDictionaryValue && dtValue.field == userVdDictionaryField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsDictionaries];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdDictionaryService;
//</es-section>
