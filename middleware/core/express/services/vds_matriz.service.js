/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:03 GMT-0400 (GMT-04:00)
 * Time: 0:54:3
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:03 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:3
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdMatrizService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsMatriz(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsMatriz.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsMatriz.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsMatriz(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsMatriz.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsMatriz.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdMatriz(newVdMatriz) {
		try {
			let objVdMatriz;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsMatriz.primaryKeys._id.type.toString())) {
			    newVdMatriz._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdMatriz.id) {
              let max = await models.sequelize.vdsMatriz.max('id');
              newVdMatriz.id = newVdMatriz.id ? newVdMatriz.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdMatriz = await models.sequelize.vdsMatriz.create(newVdMatriz);
			} else {
				objVdMatriz = new models.mongoose.vdsMatriz(newVdMatriz);
				await objVdMatriz.save();
			}
			return objVdMatriz;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdMatriz(_id, updateVdMatriz) {
		try {
			let objVdMatriz;
			if(sql) {
				objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { _id: util.String(_id) }});
				if (objVdMatriz) {
					await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { _id: util.String(_id) } });
					objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdMatriz._id;
				objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({_id:_id}, {$set: updateVdMatriz}, {new: true});
			}
			return objVdMatriz;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdMatriz(_id, query) {
		try {
			let objVdMatriz;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdMatriz = await models.sequelize.vdsMatriz.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdMatriz = await models.mongoose.vdsMatriz.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdMatriz;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdMatriz(_id) {
		try {
			let objVdMatriz;
			if(sql) {
				objVdMatriz = await models.sequelize.vdsMatriz.findOne({ where: { _id: util.String(_id) } });
				if (objVdMatriz) {
					await models.sequelize.vdsMatriz.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdMatriz = await models.mongoose.vdsMatriz.deleteOne({_id:util.String(_id)});
			}
			return objVdMatriz;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({_id: Id});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({id: id});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstadoCumplimiento(estadoCumplimiento, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado_cumplimiento: estadoCumplimiento },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({estado_cumplimiento: estadoCumplimiento});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({createdById: createdbyid});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({updatedById: updatedbyid});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCompromiso(compromiso, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { compromiso: compromiso },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({compromiso: compromiso});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEntidadResponsable(entidadResponsable, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { entidad_responsable: entidadResponsable },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({entidad_responsable: entidadResponsable});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByContacto(contacto, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { contacto: contacto },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({contacto: contacto});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByObservacion(observacion, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { observacion: observacion },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({observacion: observacion});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByTema(tema, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { tema: tema },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({tema: tema});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByFechaCumplimiento(fechaCumplimiento, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { fecha_cumplimiento: fechaCumplimiento },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({fecha_cumplimiento: fechaCumplimiento});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByFechaModificacion(fechaModificacion, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { fecha_modificacion: fechaModificacion },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({fecha_modificacion: fechaModificacion});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({dueAt: dueat});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({createdAt: createdat});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOne({updatedAt: updatedat});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdMatrizByUid(Id, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { _id: Id }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { _id: Id } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({_id: Id}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizById(id, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { id: id }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { id: id } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({id: id}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByEstadoCumplimiento(estadoCumplimiento, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { estado_cumplimiento: estadoCumplimiento }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { estado_cumplimiento: estadoCumplimiento } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({estado_cumplimiento: estadoCumplimiento}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByCreatedbyid(createdbyid, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { createdById: createdbyid }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByUpdatedbyid(updatedbyid, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { updatedById: updatedbyid }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByCompromiso(compromiso, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { compromiso: compromiso }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { compromiso: compromiso } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({compromiso: compromiso}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByEntidadResponsable(entidadResponsable, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { entidad_responsable: entidadResponsable }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { entidad_responsable: entidadResponsable } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({entidad_responsable: entidadResponsable}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByContacto(contacto, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { contacto: contacto }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { contacto: contacto } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({contacto: contacto}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByObservacion(observacion, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { observacion: observacion }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { observacion: observacion } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({observacion: observacion}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByTema(tema, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { tema: tema }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { tema: tema } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({tema: tema}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByFechaCumplimiento(fechaCumplimiento, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { fecha_cumplimiento: fechaCumplimiento }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { fecha_cumplimiento: fechaCumplimiento } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({fecha_cumplimiento: fechaCumplimiento}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByFechaModificacion(fechaModificacion, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { fecha_modificacion: fechaModificacion }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { fecha_modificacion: fechaModificacion } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({fecha_modificacion: fechaModificacion}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByDueat(dueat, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { dueAt: dueat }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({dueAt: dueat}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByCreatedat(createdat, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { createdAt: createdat }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({createdAt: createdat}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMatrizByUpdatedat(updatedat, updateVdMatriz) {
    	try {
    		let objVdMatriz;
    		if(sql) {
    			objVdMatriz = await models.sequelize.vdsMatriz.findOne({where: { updatedAt: updatedat }});
    			if (objVdMatriz) {
    				objVdMatriz = await models.sequelize.vdsMatriz.update(updateVdMatriz, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdMatriz = await models.mongoose.vdsMatriz.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdMatriz}, {new: true});
    		}
    		return objVdMatriz;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsMatriz.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsMatriz.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsMatriz = await models.sequelize.vdsMatriz.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsMatriz.length ; k++) {
    						let userVdMatriz = userVdsMatriz[k].dataValues;
    						let aUserVdMatrizValues = Object.values(userVdMatriz);
    						let aUserVdMatrizFields = Object.keys(userVdMatriz);
    						for (let n = 0 ; n < aUserVdMatrizValues.length ; n++) {
    							let userVdMatrizField = aUserVdMatrizFields[n];
    							let userVdMatrizValue = aUserVdMatrizValues[n];
    							if (!dtValues.find(param => param.value == userVdMatrizValue && param.field == userVdMatrizField)) {
    								dtValues.push({value:userVdMatrizValue, count:1, label:userVdMatrizValue, field:userVdMatrizField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdMatrizValue && dtValue.field == userVdMatrizField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsMatriz];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdMatrizService;
//</es-section>
