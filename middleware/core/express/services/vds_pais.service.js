/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:06 GMT-0400 (GMT-04:00)
 * Time: 0:54:6
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:06 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:6
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdPaiService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsPais(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsPais.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsPais.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsPais(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsPais.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsPais.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdPai(newVdPai) {
		try {
			let objVdPai;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsPais.primaryKeys._id.type.toString())) {
			    newVdPai._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdPai.id) {
              let max = await models.sequelize.vdsPais.max('id');
              newVdPai.id = newVdPai.id ? newVdPai.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdPai = await models.sequelize.vdsPais.create(newVdPai);
			} else {
				objVdPai = new models.mongoose.vdsPais(newVdPai);
				await objVdPai.save();
			}
			return objVdPai;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdPai(_id, updateVdPai) {
		try {
			let objVdPai;
			if(sql) {
				objVdPai = await models.sequelize.vdsPais.findOne({where: { _id: util.String(_id) }});
				if (objVdPai) {
					await models.sequelize.vdsPais.update(updateVdPai, { where: { _id: util.String(_id) } });
					objVdPai = await models.sequelize.vdsPais.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdPai._id;
				objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({_id:_id}, {$set: updateVdPai}, {new: true});
			}
			return objVdPai;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdPai(_id, query) {
		try {
			let objVdPai;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdPai = await models.sequelize.vdsPais.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdPai = await models.mongoose.vdsPais.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdPai;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdPai(_id) {
		try {
			let objVdPai;
			if(sql) {
				objVdPai = await models.sequelize.vdsPais.findOne({ where: { _id: util.String(_id) } });
				if (objVdPai) {
					await models.sequelize.vdsPais.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdPai = await models.mongoose.vdsPais.deleteOne({_id:util.String(_id)});
			}
			return objVdPai;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({_id: Id});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({id: id});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstado(estado, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado: estado },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({estado: estado});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByPais(pais, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { pais: pais },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({pais: pais});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByAbreviacion(abreviacion, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { abreviacion: abreviacion },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({abreviacion: abreviacion});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({createdBy: createdby});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({updatedBy: updatedby});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({dueAt: dueat});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({createdAt: createdat});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOne({updatedAt: updatedat});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdPaiByUid(Id, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { _id: Id }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { _id: Id } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({_id: Id}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiById(id, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { id: id }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { id: id } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({id: id}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiByEstado(estado, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { estado: estado }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { estado: estado } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({estado: estado}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiByPais(pais, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { pais: pais }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { pais: pais } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({pais: pais}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiByAbreviacion(abreviacion, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { abreviacion: abreviacion }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { abreviacion: abreviacion } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({abreviacion: abreviacion}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiByCreatedby(createdby, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { createdBy: createdby }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({createdBy: createdby}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiByUpdatedby(updatedby, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { updatedBy: updatedby }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiByDueat(dueat, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { dueAt: dueat }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({dueAt: dueat}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiByCreatedat(createdat, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { createdAt: createdat }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({createdAt: createdat}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdPaiByUpdatedat(updatedat, updateVdPai) {
    	try {
    		let objVdPai;
    		if(sql) {
    			objVdPai = await models.sequelize.vdsPais.findOne({where: { updatedAt: updatedat }});
    			if (objVdPai) {
    				objVdPai = await models.sequelize.vdsPais.update(updateVdPai, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdPai = await models.mongoose.vdsPais.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdPai}, {new: true});
    		}
    		return objVdPai;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsPais.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsPais.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsPais = await models.sequelize.vdsPais.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsPais.length ; k++) {
    						let userVdPai = userVdsPais[k].dataValues;
    						let aUserVdPaiValues = Object.values(userVdPai);
    						let aUserVdPaiFields = Object.keys(userVdPai);
    						for (let n = 0 ; n < aUserVdPaiValues.length ; n++) {
    							let userVdPaiField = aUserVdPaiFields[n];
    							let userVdPaiValue = aUserVdPaiValues[n];
    							if (!dtValues.find(param => param.value == userVdPaiValue && param.field == userVdPaiField)) {
    								dtValues.push({value:userVdPaiValue, count:1, label:userVdPaiValue, field:userVdPaiField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdPaiValue && dtValue.field == userVdPaiField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsPais];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdPaiService;
//</es-section>
