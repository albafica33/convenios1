/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Time: 0:54:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:11
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdProvinciaService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsProvincia(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsProvincia.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsProvincia.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsProvincia(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsProvincia.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsProvincia.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdProvincia(newVdProvincia) {
		try {
			let objVdProvincia;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsProvincia.primaryKeys._id.type.toString())) {
			    newVdProvincia._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdProvincia.id) {
              let max = await models.sequelize.vdsProvincia.max('id');
              newVdProvincia.id = newVdProvincia.id ? newVdProvincia.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdProvincia = await models.sequelize.vdsProvincia.create(newVdProvincia);
			} else {
				objVdProvincia = new models.mongoose.vdsProvincia(newVdProvincia);
				await objVdProvincia.save();
			}
			return objVdProvincia;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdProvincia(_id, updateVdProvincia) {
		try {
			let objVdProvincia;
			if(sql) {
				objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { _id: util.String(_id) }});
				if (objVdProvincia) {
					await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { _id: util.String(_id) } });
					objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdProvincia._id;
				objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({_id:_id}, {$set: updateVdProvincia}, {new: true});
			}
			return objVdProvincia;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdProvincia(_id, query) {
		try {
			let objVdProvincia;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdProvincia = await models.sequelize.vdsProvincia.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdProvincia = await models.mongoose.vdsProvincia.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdProvincia;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdProvincia(_id) {
		try {
			let objVdProvincia;
			if(sql) {
				objVdProvincia = await models.sequelize.vdsProvincia.findOne({ where: { _id: util.String(_id) } });
				if (objVdProvincia) {
					await models.sequelize.vdsProvincia.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdProvincia = await models.mongoose.vdsProvincia.deleteOne({_id:util.String(_id)});
			}
			return objVdProvincia;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({_id: Id});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({id: id});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstado(estado, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado: estado },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({estado: estado});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByProvincia(provincia, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { provincia: provincia },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({provincia: provincia});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByAbreviacion(abreviacion, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { abreviacion: abreviacion },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({abreviacion: abreviacion});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({createdBy: createdby});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({updatedBy: updatedby});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdCiudad(idCiudad, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_ciudad: idCiudad },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({id_ciudad: idCiudad});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({dueAt: dueat});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({createdAt: createdat});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOne({updatedAt: updatedat});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdProvinciaByUid(Id, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { _id: Id }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { _id: Id } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({_id: Id}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaById(id, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { id: id }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { id: id } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({id: id}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByEstado(estado, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { estado: estado }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { estado: estado } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({estado: estado}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByProvincia(provincia, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { provincia: provincia }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { provincia: provincia } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({provincia: provincia}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByAbreviacion(abreviacion, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { abreviacion: abreviacion }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { abreviacion: abreviacion } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({abreviacion: abreviacion}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByCreatedby(createdby, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { createdBy: createdby }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({createdBy: createdby}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByUpdatedby(updatedby, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { updatedBy: updatedby }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByIdCiudad(idCiudad, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { id_ciudad: idCiudad }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { id_ciudad: idCiudad } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({id_ciudad: idCiudad}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByDueat(dueat, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { dueAt: dueat }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({dueAt: dueat}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByCreatedat(createdat, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { createdAt: createdat }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({createdAt: createdat}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdProvinciaByUpdatedat(updatedat, updateVdProvincia) {
    	try {
    		let objVdProvincia;
    		if(sql) {
    			objVdProvincia = await models.sequelize.vdsProvincia.findOne({where: { updatedAt: updatedat }});
    			if (objVdProvincia) {
    				objVdProvincia = await models.sequelize.vdsProvincia.update(updateVdProvincia, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdProvincia = await models.mongoose.vdsProvincia.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdProvincia}, {new: true});
    		}
    		return objVdProvincia;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsProvincia.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsProvincia.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsProvincia = await models.sequelize.vdsProvincia.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsProvincia.length ; k++) {
    						let userVdProvincia = userVdsProvincia[k].dataValues;
    						let aUserVdProvinciaValues = Object.values(userVdProvincia);
    						let aUserVdProvinciaFields = Object.keys(userVdProvincia);
    						for (let n = 0 ; n < aUserVdProvinciaValues.length ; n++) {
    							let userVdProvinciaField = aUserVdProvinciaFields[n];
    							let userVdProvinciaValue = aUserVdProvinciaValues[n];
    							if (!dtValues.find(param => param.value == userVdProvinciaValue && param.field == userVdProvinciaField)) {
    								dtValues.push({value:userVdProvinciaValue, count:1, label:userVdProvinciaValue, field:userVdProvinciaField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdProvinciaValue && dtValue.field == userVdProvinciaField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsProvincia];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdProvinciaService;
//</es-section>
