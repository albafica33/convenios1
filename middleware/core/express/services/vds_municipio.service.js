/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Time: 0:54:5
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:5
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdMunicipioService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsMunicipio(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsMunicipio.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsMunicipio.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsMunicipio(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsMunicipio.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsMunicipio.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdMunicipio(newVdMunicipio) {
		try {
			let objVdMunicipio;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsMunicipio.primaryKeys._id.type.toString())) {
			    newVdMunicipio._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdMunicipio.id) {
              let max = await models.sequelize.vdsMunicipio.max('id');
              newVdMunicipio.id = newVdMunicipio.id ? newVdMunicipio.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdMunicipio = await models.sequelize.vdsMunicipio.create(newVdMunicipio);
			} else {
				objVdMunicipio = new models.mongoose.vdsMunicipio(newVdMunicipio);
				await objVdMunicipio.save();
			}
			return objVdMunicipio;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdMunicipio(_id, updateVdMunicipio) {
		try {
			let objVdMunicipio;
			if(sql) {
				objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { _id: util.String(_id) }});
				if (objVdMunicipio) {
					await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { _id: util.String(_id) } });
					objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdMunicipio._id;
				objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({_id:_id}, {$set: updateVdMunicipio}, {new: true});
			}
			return objVdMunicipio;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdMunicipio(_id, query) {
		try {
			let objVdMunicipio;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdMunicipio = await models.mongoose.vdsMunicipio.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdMunicipio;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdMunicipio(_id) {
		try {
			let objVdMunicipio;
			if(sql) {
				objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({ where: { _id: util.String(_id) } });
				if (objVdMunicipio) {
					await models.sequelize.vdsMunicipio.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdMunicipio = await models.mongoose.vdsMunicipio.deleteOne({_id:util.String(_id)});
			}
			return objVdMunicipio;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({_id: Id});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({id: id});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByEstado(estado, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { estado: estado },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({estado: estado});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMunicipio(municipio, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { municipio: municipio },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({municipio: municipio});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({createdBy: createdby});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({updatedBy: updatedby});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByIdCiudad(idCiudad, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id_ciudad: idCiudad },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({id_ciudad: idCiudad});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({dueAt: dueat});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({createdAt: createdat});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOne({updatedAt: updatedat});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdMunicipioByUid(Id, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { _id: Id }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { _id: Id } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({_id: Id}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioById(id, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { id: id }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { id: id } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({id: id}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioByEstado(estado, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { estado: estado }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { estado: estado } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({estado: estado}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioByMunicipio(municipio, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { municipio: municipio }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { municipio: municipio } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({municipio: municipio}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioByCreatedby(createdby, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { createdBy: createdby }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({createdBy: createdby}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioByUpdatedby(updatedby, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { updatedBy: updatedby }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioByIdCiudad(idCiudad, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { id_ciudad: idCiudad }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { id_ciudad: idCiudad } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({id_ciudad: idCiudad}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioByDueat(dueat, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { dueAt: dueat }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({dueAt: dueat}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioByCreatedat(createdat, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { createdAt: createdat }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({createdAt: createdat}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMunicipioByUpdatedat(updatedat, updateVdMunicipio) {
    	try {
    		let objVdMunicipio;
    		if(sql) {
    			objVdMunicipio = await models.sequelize.vdsMunicipio.findOne({where: { updatedAt: updatedat }});
    			if (objVdMunicipio) {
    				objVdMunicipio = await models.sequelize.vdsMunicipio.update(updateVdMunicipio, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdMunicipio = await models.mongoose.vdsMunicipio.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdMunicipio}, {new: true});
    		}
    		return objVdMunicipio;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsMunicipio.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsMunicipio.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsMunicipio = await models.sequelize.vdsMunicipio.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsMunicipio.length ; k++) {
    						let userVdMunicipio = userVdsMunicipio[k].dataValues;
    						let aUserVdMunicipioValues = Object.values(userVdMunicipio);
    						let aUserVdMunicipioFields = Object.keys(userVdMunicipio);
    						for (let n = 0 ; n < aUserVdMunicipioValues.length ; n++) {
    							let userVdMunicipioField = aUserVdMunicipioFields[n];
    							let userVdMunicipioValue = aUserVdMunicipioValues[n];
    							if (!dtValues.find(param => param.value == userVdMunicipioValue && param.field == userVdMunicipioField)) {
    								dtValues.push({value:userVdMunicipioValue, count:1, label:userVdMunicipioValue, field:userVdMunicipioField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdMunicipioValue && dtValue.field == userVdMunicipioField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsMunicipio];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdMunicipioService;
//</es-section>
