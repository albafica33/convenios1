/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:12 GMT-0400 (GMT-04:00)
 * Time: 0:54:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:12 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:12
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdRoleService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsRoles(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsRoles.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsRoles.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsRoles(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsRoles.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsRoles.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdRole(newVdRole) {
		try {
			let objVdRole;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsRoles.primaryKeys._id.type.toString())) {
			    newVdRole._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdRole.id) {
              let max = await models.sequelize.vdsRoles.max('id');
              newVdRole.id = newVdRole.id ? newVdRole.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdRole = await models.sequelize.vdsRoles.create(newVdRole);
			} else {
				objVdRole = new models.mongoose.vdsRoles(newVdRole);
				await objVdRole.save();
			}
			return objVdRole;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdRole(_id, updateVdRole) {
		try {
			let objVdRole;
			if(sql) {
				objVdRole = await models.sequelize.vdsRoles.findOne({where: { _id: util.String(_id) }});
				if (objVdRole) {
					await models.sequelize.vdsRoles.update(updateVdRole, { where: { _id: util.String(_id) } });
					objVdRole = await models.sequelize.vdsRoles.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdRole._id;
				objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({_id:_id}, {$set: updateVdRole}, {new: true});
			}
			return objVdRole;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdRole(_id, query) {
		try {
			let objVdRole;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdRole = await models.sequelize.vdsRoles.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdRole = await models.mongoose.vdsRoles.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdRole;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdRole(_id) {
		try {
			let objVdRole;
			if(sql) {
				objVdRole = await models.sequelize.vdsRoles.findOne({ where: { _id: util.String(_id) } });
				if (objVdRole) {
					await models.sequelize.vdsRoles.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdRole = await models.mongoose.vdsRoles.deleteOne({_id:util.String(_id)});
			}
			return objVdRole;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({_id: Id});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({id: id});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByRolCode(rolCode, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { rol_code: rolCode },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({rol_code: rolCode});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByRolDescription(rolDescription, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { rol_description: rolDescription },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({rol_description: rolDescription});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByRolAbbr(rolAbbr, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { rol_abbr: rolAbbr },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({rol_abbr: rolAbbr});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByRolGroup(rolGroup, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { rol_group: rolGroup },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({rol_group: rolGroup});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({createdById: createdbyid});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({updatedById: updatedbyid});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByRolParStatusId(rolParStatusId, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { rol_par_status_id: rolParStatusId },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({rol_par_status_id: rolParStatusId});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({dueAt: dueat});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({createdAt: createdat});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOne({updatedAt: updatedat});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdRoleByUid(Id, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { _id: Id }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { _id: Id } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({_id: Id}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleById(id, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { id: id }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { id: id } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({id: id}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByRolCode(rolCode, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { rol_code: rolCode }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { rol_code: rolCode } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({rol_code: rolCode}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByRolDescription(rolDescription, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { rol_description: rolDescription }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { rol_description: rolDescription } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({rol_description: rolDescription}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByRolAbbr(rolAbbr, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { rol_abbr: rolAbbr }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { rol_abbr: rolAbbr } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({rol_abbr: rolAbbr}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByRolGroup(rolGroup, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { rol_group: rolGroup }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { rol_group: rolGroup } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({rol_group: rolGroup}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByCreatedbyid(createdbyid, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { createdById: createdbyid }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByUpdatedbyid(updatedbyid, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { updatedById: updatedbyid }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByRolParStatusId(rolParStatusId, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { rol_par_status_id: rolParStatusId }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { rol_par_status_id: rolParStatusId } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({rol_par_status_id: rolParStatusId}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByDueat(dueat, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { dueAt: dueat }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({dueAt: dueat}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByCreatedat(createdat, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { createdAt: createdat }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({createdAt: createdat}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdRoleByUpdatedat(updatedat, updateVdRole) {
    	try {
    		let objVdRole;
    		if(sql) {
    			objVdRole = await models.sequelize.vdsRoles.findOne({where: { updatedAt: updatedat }});
    			if (objVdRole) {
    				objVdRole = await models.sequelize.vdsRoles.update(updateVdRole, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdRole = await models.mongoose.vdsRoles.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdRole}, {new: true});
    		}
    		return objVdRole;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsRolParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_rol_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_rol_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsRolesByCreatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsRolParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdRole, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdRole;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsRolesByUpdatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsRolParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdRole, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdRole;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsRolesByRolParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsRolParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdRole, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdRole = await models.sequelize.vdsRoles.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdRole = await models.mongoose.vdsRoles.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdRole;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsRoles.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsRoles.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsRoles = await models.sequelize.vdsRoles.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsRoles.length ; k++) {
    						let userVdRole = userVdsRoles[k].dataValues;
    						let aUserVdRoleValues = Object.values(userVdRole);
    						let aUserVdRoleFields = Object.keys(userVdRole);
    						for (let n = 0 ; n < aUserVdRoleValues.length ; n++) {
    							let userVdRoleField = aUserVdRoleFields[n];
    							let userVdRoleValue = aUserVdRoleValues[n];
    							if (!dtValues.find(param => param.value == userVdRoleValue && param.field == userVdRoleField)) {
    								dtValues.push({value:userVdRoleValue, count:1, label:userVdRoleValue, field:userVdRoleField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdRoleValue && dtValue.field == userVdRoleField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsRoles];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdRoleService;
//</es-section>
