/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:02 GMT-0400 (GMT-04:00)
 * Time: 0:54:2
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:02 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:2
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdMailService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsMails(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsMails.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsMails.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsMails(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsMails.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsMails.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdMail(newVdMail) {
		try {
			let objVdMail;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsMails.primaryKeys._id.type.toString())) {
			    newVdMail._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdMail.id) {
              let max = await models.sequelize.vdsMails.max('id');
              newVdMail.id = newVdMail.id ? newVdMail.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdMail = await models.sequelize.vdsMails.create(newVdMail);
			} else {
				objVdMail = new models.mongoose.vdsMails(newVdMail);
				await objVdMail.save();
			}
			return objVdMail;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdMail(_id, updateVdMail) {
		try {
			let objVdMail;
			if(sql) {
				objVdMail = await models.sequelize.vdsMails.findOne({where: { _id: util.String(_id) }});
				if (objVdMail) {
					await models.sequelize.vdsMails.update(updateVdMail, { where: { _id: util.String(_id) } });
					objVdMail = await models.sequelize.vdsMails.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdMail._id;
				objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({_id:_id}, {$set: updateVdMail}, {new: true});
			}
			return objVdMail;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdMail(_id, query) {
		try {
			let objVdMail;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdMail = await models.sequelize.vdsMails.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdMail = await models.mongoose.vdsMails.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdMail;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdMail(_id) {
		try {
			let objVdMail;
			if(sql) {
				objVdMail = await models.sequelize.vdsMails.findOne({ where: { _id: util.String(_id) } });
				if (objVdMail) {
					await models.sequelize.vdsMails.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdMail = await models.mongoose.vdsMails.deleteOne({_id:util.String(_id)});
			}
			return objVdMail;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({_id: Id});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({id: id});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiPort(maiPort, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_port: maiPort },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_port: maiPort});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiDescription(maiDescription, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_description: maiDescription },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_description: maiDescription});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiUserAccount(maiUserAccount, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_user_account: maiUserAccount },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_user_account: maiUserAccount});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiUserPassword(maiUserPassword, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_user_password: maiUserPassword },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_user_password: maiUserPassword});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiHost(maiHost, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_host: maiHost },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_host: maiHost});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiProtocol(maiProtocol, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_protocol: maiProtocol },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_protocol: maiProtocol});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiBusId(maiBusId, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_bus_id: maiBusId },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_bus_id: maiBusId});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiParStatusId(maiParStatusId, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_par_status_id: maiParStatusId },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_par_status_id: maiParStatusId});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiGroup(maiGroup, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_group: maiGroup },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_group: maiGroup});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiSubject(maiSubject, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_subject: maiSubject },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_subject: maiSubject});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiTo(maiTo, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_to: maiTo },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_to: maiTo});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedby(updatedby, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedBy: updatedby },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({updatedBy: updatedby});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedby(createdby, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdBy: createdby },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({createdBy: createdby});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiBcc(maiBcc, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_bcc: maiBcc },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_bcc: maiBcc});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiCc(maiCc, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_cc: maiCc },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_cc: maiCc});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiText(maiText, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_text: maiText },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_text: maiText});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByMaiHtml(maiHtml, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mai_html: maiHtml },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({mai_html: maiHtml});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({dueAt: dueat});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({createdAt: createdat});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOne({updatedAt: updatedat});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdMailByUid(Id, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { _id: Id }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { _id: Id } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({_id: Id}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailById(id, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { id: id }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { id: id } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({id: id}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiPort(maiPort, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_port: maiPort }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_port: maiPort } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_port: maiPort}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiDescription(maiDescription, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_description: maiDescription }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_description: maiDescription } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_description: maiDescription}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiUserAccount(maiUserAccount, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_user_account: maiUserAccount }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_user_account: maiUserAccount } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_user_account: maiUserAccount}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiUserPassword(maiUserPassword, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_user_password: maiUserPassword }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_user_password: maiUserPassword } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_user_password: maiUserPassword}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiHost(maiHost, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_host: maiHost }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_host: maiHost } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_host: maiHost}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiProtocol(maiProtocol, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_protocol: maiProtocol }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_protocol: maiProtocol } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_protocol: maiProtocol}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiBusId(maiBusId, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_bus_id: maiBusId }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_bus_id: maiBusId } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_bus_id: maiBusId}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiParStatusId(maiParStatusId, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_par_status_id: maiParStatusId }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_par_status_id: maiParStatusId } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_par_status_id: maiParStatusId}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiGroup(maiGroup, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_group: maiGroup }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_group: maiGroup } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_group: maiGroup}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiSubject(maiSubject, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_subject: maiSubject }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_subject: maiSubject } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_subject: maiSubject}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiTo(maiTo, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_to: maiTo }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_to: maiTo } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_to: maiTo}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByUpdatedby(updatedby, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { updatedBy: updatedby }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { updatedBy: updatedby } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({updatedBy: updatedby}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByCreatedby(createdby, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { createdBy: createdby }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { createdBy: createdby } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({createdBy: createdby}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiBcc(maiBcc, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_bcc: maiBcc }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_bcc: maiBcc } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_bcc: maiBcc}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiCc(maiCc, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_cc: maiCc }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_cc: maiCc } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_cc: maiCc}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiText(maiText, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_text: maiText }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_text: maiText } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_text: maiText}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByMaiHtml(maiHtml, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { mai_html: maiHtml }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { mai_html: maiHtml } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({mai_html: maiHtml}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByDueat(dueat, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { dueAt: dueat }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({dueAt: dueat}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByCreatedat(createdat, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { createdAt: createdat }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({createdAt: createdat}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdMailByUpdatedat(updatedat, updateVdMail) {
    	try {
    		let objVdMail;
    		if(sql) {
    			objVdMail = await models.sequelize.vdsMails.findOne({where: { updatedAt: updatedat }});
    			if (objVdMail) {
    				objVdMail = await models.sequelize.vdsMails.update(updateVdMail, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdMail = await models.mongoose.vdsMails.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdMail}, {new: true});
    		}
    		return objVdMail;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsParamsMaiParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_mai_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_mai_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsMailsByMaiParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsMaiParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdMail, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdMail = await models.sequelize.vdsMails.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdMail = await models.sequelize.vdsMails.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdMail = await models.sequelize.vdsMails.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdMail = await models.mongoose.vdsMails.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdMail = await models.mongoose.vdsMails.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdMail = await models.mongoose.vdsMails.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdMail;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsMails.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsMails.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsMails = await models.sequelize.vdsMails.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsMails.length ; k++) {
    						let userVdMail = userVdsMails[k].dataValues;
    						let aUserVdMailValues = Object.values(userVdMail);
    						let aUserVdMailFields = Object.keys(userVdMail);
    						for (let n = 0 ; n < aUserVdMailValues.length ; n++) {
    							let userVdMailField = aUserVdMailFields[n];
    							let userVdMailValue = aUserVdMailValues[n];
    							if (!dtValues.find(param => param.value == userVdMailValue && param.field == userVdMailField)) {
    								dtValues.push({value:userVdMailValue, count:1, label:userVdMailValue, field:userVdMailField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdMailValue && dtValue.field == userVdMailField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsMails];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdMailService;
//</es-section>
