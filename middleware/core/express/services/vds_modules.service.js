/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:04 GMT-0400 (GMT-04:00)
 * Time: 0:54:4
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:04 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:4
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');
const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class VdModuleService {

    //<es-section>

    //</es-section>

	//<es-section>
	
	static async getAllVdsModules(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.vdsModules.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['_id','ASC']],
                });
			} else {
				return await models.mongoose.vdsModules.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllVdsModules(select = []) {
		try {
			if(sql) {
				return await models.sequelize.vdsModules.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.vdsModules.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addVdModule(newVdModule) {
		try {
			let objVdModule;
			if(util.PrimaryKeyTypeIsString(models.sequelize.vdsModules.primaryKeys._id.type.toString())) {
			    newVdModule._id = models.sequelize.objectId().toString();
		    }
			
			if(!newVdModule.id) {
              let max = await models.sequelize.vdsModules.max('id');
              newVdModule.id = newVdModule.id ? newVdModule.id : max ? max+1 : 1;
			}
			
			
			
			if(sql) {
				objVdModule = await models.sequelize.vdsModules.create(newVdModule);
			} else {
				objVdModule = new models.mongoose.vdsModules(newVdModule);
				await objVdModule.save();
			}
			return objVdModule;
		} catch (error) {
			throw error;
		}
	}
	
	static async updateVdModule(_id, updateVdModule) {
		try {
			let objVdModule;
			if(sql) {
				objVdModule = await models.sequelize.vdsModules.findOne({where: { _id: util.String(_id) }});
				if (objVdModule) {
					await models.sequelize.vdsModules.update(updateVdModule, { where: { _id: util.String(_id) } });
					objVdModule = await models.sequelize.vdsModules.findOne({where: { _id: util.String(_id) }});
				}
			} else {
				delete updateVdModule._id;
				objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({_id:_id}, {$set: updateVdModule}, {new: true});
			}
			return objVdModule;
		} catch (error) {
			throw error;
		}
	}

	static async getAVdModule(_id, query) {
		try {
			let objVdModule;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objVdModule = await models.sequelize.vdsModules.findOne({
					    where: where && !where.where ? where : { _id: util.String(_id) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objVdModule = await models.mongoose.vdsModules.find({_id:util.String(_id)}).select(query.select);
			}
			return objVdModule;
		} catch (error) {
			throw error;
		}
	}

	static async deleteVdModule(_id) {
		try {
			let objVdModule;
			if(sql) {
				objVdModule = await models.sequelize.vdsModules.findOne({ where: { _id: util.String(_id) } });
				if (objVdModule) {
					await models.sequelize.vdsModules.destroy({where: { _id: util.String(_id) }});
				}
			} else {
				objVdModule = await models.mongoose.vdsModules.deleteOne({_id:util.String(_id)});
			}
			return objVdModule;
		} catch (error) {
			throw error;
		}
	}
	
	
	static async findOneByUid(Id, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { _id: Id },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({_id: Id});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneById(id, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { id: id },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({id: id});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByModCode(modCode, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mod_code: modCode },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({mod_code: modCode});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByModDescription(modDescription, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mod_description: modDescription },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({mod_description: modDescription});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByModAbbr(modAbbr, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mod_abbr: modAbbr },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({mod_abbr: modAbbr});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByModIcon(modIcon, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mod_icon: modIcon },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({mod_icon: modIcon});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByModGroup(modGroup, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mod_group: modGroup },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({mod_group: modGroup});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedbyid(createdbyid, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdById: createdbyid },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({createdById: createdbyid});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedbyid(updatedbyid, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedById: updatedbyid },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({updatedById: updatedbyid});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByModParentId(modParentId, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mod_parent_id: modParentId },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({mod_parent_id: modParentId});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByModParStatusId(modParStatusId, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { mod_par_status_id: modParStatusId },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({mod_par_status_id: modParStatusId});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByDueat(dueat, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { dueAt: dueat },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({dueAt: dueat});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByCreatedat(createdat, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { createdAt: createdat },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({createdAt: createdat});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async findOneByUpdatedat(updatedat, query = {}) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { updatedAt: updatedat },
    			});
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOne({updatedAt: updatedat});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async updateVdModuleByUid(Id, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { _id: Id }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { _id: Id } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({_id: Id}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleById(id, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { id: id }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { id: id } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({id: id}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByModCode(modCode, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { mod_code: modCode }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { mod_code: modCode } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({mod_code: modCode}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByModDescription(modDescription, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { mod_description: modDescription }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { mod_description: modDescription } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({mod_description: modDescription}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByModAbbr(modAbbr, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { mod_abbr: modAbbr }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { mod_abbr: modAbbr } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({mod_abbr: modAbbr}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByModIcon(modIcon, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { mod_icon: modIcon }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { mod_icon: modIcon } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({mod_icon: modIcon}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByModGroup(modGroup, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { mod_group: modGroup }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { mod_group: modGroup } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({mod_group: modGroup}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByCreatedbyid(createdbyid, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { createdById: createdbyid }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { createdById: createdbyid } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({createdById: createdbyid}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByUpdatedbyid(updatedbyid, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { updatedById: updatedbyid }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { updatedById: updatedbyid } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({updatedById: updatedbyid}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByModParentId(modParentId, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { mod_parent_id: modParentId }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { mod_parent_id: modParentId } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({mod_parent_id: modParentId}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByModParStatusId(modParStatusId, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { mod_par_status_id: modParStatusId }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { mod_par_status_id: modParStatusId } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({mod_par_status_id: modParStatusId}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByDueat(dueat, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { dueAt: dueat }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { dueAt: dueat } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({dueAt: dueat}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByCreatedat(createdat, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { createdAt: createdat }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { createdAt: createdat } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({createdAt: createdat}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	static async updateVdModuleByUpdatedat(updatedat, updateVdModule) {
    	try {
    		let objVdModule;
    		if(sql) {
    			objVdModule = await models.sequelize.vdsModules.findOne({where: { updatedAt: updatedat }});
    			if (objVdModule) {
    				objVdModule = await models.sequelize.vdsModules.update(updateVdModule, { where: { updatedAt: updatedat } });
    			}
    		} else {
    			objVdModule = await models.mongoose.vdsModules.findOneAndUpdate({updatedAt: updatedat}, {$set: updateVdModule}, {new: true});
    		}
    		return objVdModule;
    	} catch (error) {
    		throw error;
    	}
    }
	
	
	static async findVdsUserRolesCreatedbyWithUsrRolGroup(select = ['_id', 'createdById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_createdBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_createdBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsUserRolesUpdatedbyWithUsrRolGroup(select = ['_id', 'updatedById'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsUserRoles.findAll({
                    attributes: select,
                    where: { usr_rol_group: {[Op.like]: '%grp_updatedBy%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsUserRoles.find({usr_rol_group: {$regex : ".*grp_updatedBy.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	static async findVdsParamsModParStatusWithParOrder(select = ['_id', 'par_order'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.vdsParams.findAll({
                    attributes: select,
                    where: { par_group: {[Op.like]: '%grp_mod_par_status%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['_id','DESC']],
    		    });
    		} else {
    			return await models.mongoose.vdsParams.find({par_group: {$regex : ".*grp_mod_par_status.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async filterVdsModulesByCreatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsModParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdModule, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdModule;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsModulesByUpdatedby(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsModParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdModule, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdModule;
    	} catch (error) {
            throw error;
    	}
    }
	
	static async filterVdsModulesByModParStatus(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findVdsParamsModParStatusWithParCod(['_id', 'par_abbr']);
        	let objVdModule, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objVdModule = await models.sequelize.vdsModules.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objVdModule = await models.mongoose.vdsModules.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objVdModule;
    	} catch (error) {
            throw error;
    	}
    }
	
	
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.vdsModules.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.vdsModules.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userVdsModules = await models.sequelize.vdsModules.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userVdsModules.length ; k++) {
    						let userVdModule = userVdsModules[k].dataValues;
    						let aUserVdModuleValues = Object.values(userVdModule);
    						let aUserVdModuleFields = Object.keys(userVdModule);
    						for (let n = 0 ; n < aUserVdModuleValues.length ; n++) {
    							let userVdModuleField = aUserVdModuleFields[n];
    							let userVdModuleValue = aUserVdModuleValues[n];
    							if (!dtValues.find(param => param.value == userVdModuleValue && param.field == userVdModuleField)) {
    								dtValues.push({value:userVdModuleValue, count:1, label:userVdModuleValue, field:userVdModuleField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userVdModuleValue && dtValue.field == userVdModuleField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userVdsModules];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	
	
	//</es-section>
}

//<es-section>
module.exports = VdModuleService;
//</es-section>
