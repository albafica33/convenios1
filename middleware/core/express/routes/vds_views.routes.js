/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:15 GMT-0400 (GMT-04:00)
 * Time: 0:54:15
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:15 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:15
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsViewsCtrl = require("../controllers/vds_views.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-views/findVdsModulesVieModuleWithModCode`, (req, res) => vdsViewsCtrl.findVdsModulesVieModuleWithModCode(req, res));
router.get(`/api-${sys}/vds-views/filterVdsViewsByVieModule/:vieModuleId/`, (req, res) => vdsViewsCtrl.filterVdsViewsByVieModule(req, res));

router.get(`/api-${sys}/vds-views/findVdsViewsVieReturnWithVieCode`, (req, res) => vdsViewsCtrl.findVdsViewsVieReturnWithVieCode(req, res));
router.get(`/api-${sys}/vds-views/filterVdsViewsByVieReturn/:vieReturnId/`, (req, res) => vdsViewsCtrl.filterVdsViewsByVieReturn(req, res));

router.get(`/api-${sys}/vds-views/findVdsParamsVieParStatusWithParOrder`, (req, res) => vdsViewsCtrl.findVdsParamsVieParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-views/filterVdsViewsByVieParStatus/:vieParStatusId/`, (req, res) => vdsViewsCtrl.filterVdsViewsByVieParStatus(req, res));



router.get(`/api-${sys}/vds-views/findOneByUid/:Id`, (req, res) => vdsViewsCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-views/findOneById/:id`, (req, res) => vdsViewsCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieCode/:vieCode`, (req, res) => vdsViewsCtrl.findOneByVieCode(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieDescription/:vieDescription`, (req, res) => vdsViewsCtrl.findOneByVieDescription(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieRoute/:vieRoute`, (req, res) => vdsViewsCtrl.findOneByVieRoute(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieParams/:vieParams`, (req, res) => vdsViewsCtrl.findOneByVieParams(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieIcon/:vieIcon`, (req, res) => vdsViewsCtrl.findOneByVieIcon(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieGroup/:vieGroup`, (req, res) => vdsViewsCtrl.findOneByVieGroup(req, res));

router.get(`/api-${sys}/vds-views/findOneByCreatedby/:createdby`, (req, res) => vdsViewsCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-views/findOneByUpdatedby/:updatedby`, (req, res) => vdsViewsCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieModuleId/:vieModuleId`, (req, res) => vdsViewsCtrl.findOneByVieModuleId(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieReturnId/:vieReturnId`, (req, res) => vdsViewsCtrl.findOneByVieReturnId(req, res));

router.get(`/api-${sys}/vds-views/findOneByVieParStatusId/:vieParStatusId`, (req, res) => vdsViewsCtrl.findOneByVieParStatusId(req, res));

router.get(`/api-${sys}/vds-views/findOneByDueat/:dueat`, (req, res) => vdsViewsCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-views/findOneByCreatedat/:createdat`, (req, res) => vdsViewsCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-views/findOneByUpdatedat/:updatedat`, (req, res) => vdsViewsCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-views/updateVdViewByUid`, (req, res) => vdsViewsCtrl.updateVdViewByUid(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewById`, (req, res) => vdsViewsCtrl.updateVdViewById(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieCode`, (req, res) => vdsViewsCtrl.updateVdViewByVieCode(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieDescription`, (req, res) => vdsViewsCtrl.updateVdViewByVieDescription(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieRoute`, (req, res) => vdsViewsCtrl.updateVdViewByVieRoute(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieParams`, (req, res) => vdsViewsCtrl.updateVdViewByVieParams(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieIcon`, (req, res) => vdsViewsCtrl.updateVdViewByVieIcon(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieGroup`, (req, res) => vdsViewsCtrl.updateVdViewByVieGroup(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByCreatedby`, (req, res) => vdsViewsCtrl.updateVdViewByCreatedby(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByUpdatedby`, (req, res) => vdsViewsCtrl.updateVdViewByUpdatedby(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieModuleId`, (req, res) => vdsViewsCtrl.updateVdViewByVieModuleId(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieReturnId`, (req, res) => vdsViewsCtrl.updateVdViewByVieReturnId(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByVieParStatusId`, (req, res) => vdsViewsCtrl.updateVdViewByVieParStatusId(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByDueat`, (req, res) => vdsViewsCtrl.updateVdViewByDueat(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByCreatedat`, (req, res) => vdsViewsCtrl.updateVdViewByCreatedat(req, res));

router.post(`/api-${sys}/vds-views/updateVdViewByUpdatedat`, (req, res) => vdsViewsCtrl.updateVdViewByUpdatedat(req, res));



router.get(`/api-${sys}/vds-views/findVdsModulesVieModuleWithModCode`, (req, res) => vdsViewsCtrl.findVdsModulesVieModuleWithModCode(req, res));

router.get(`/api-${sys}/vds-views/findVdsViewsVieReturnWithVieCode`, (req, res) => vdsViewsCtrl.findVdsViewsVieReturnWithVieCode(req, res));

router.get(`/api-${sys}/vds-views/findVdsParamsVieParStatusWithParOrder`, (req, res) => vdsViewsCtrl.findVdsParamsVieParStatusWithParOrder(req, res));



router.get(`/api-${sys}/vds-views/`, (req, res) => vdsViewsCtrl.getAllVdsViews(req, res));
router.post(`/api-${sys}/vds-views/datatable/`, (req, res) => vdsViewsCtrl.getAllVdsViews(req, res));
router.post(`/api-${sys}/vds-views/`, (req, res) => vdsViewsCtrl.addVdView(req, res));
router.get(`/api-${sys}/vds-views/:Id`, (req, res) => vdsViewsCtrl.getAVdView(req, res));
router.put(`/api-${sys}/vds-views/:Id`, (req, res) => vdsViewsCtrl.updateVdView(req, res));
router.delete(`/api-${sys}/vds-views/:Id`, (req, res) => vdsViewsCtrl.deleteVdView(req, res));

//</es-section>
module.exports = router;
