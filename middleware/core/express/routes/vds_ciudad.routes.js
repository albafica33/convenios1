/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Time: 0:53:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:58
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsCiudadCtrl = require("../controllers/vds_ciudad.controller");
//</es-section>
//<es-section>



router.get(`/api-${sys}/vds-ciudad/findOneByUid/:Id`, (req, res) => vdsCiudadCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneById/:id`, (req, res) => vdsCiudadCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByEstado/:estado`, (req, res) => vdsCiudadCtrl.findOneByEstado(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByCiudad/:ciudad`, (req, res) => vdsCiudadCtrl.findOneByCiudad(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByAbrev/:abrev`, (req, res) => vdsCiudadCtrl.findOneByAbrev(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByCreatedby/:createdby`, (req, res) => vdsCiudadCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByUpdatedby/:updatedby`, (req, res) => vdsCiudadCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByIdPais/:idPais`, (req, res) => vdsCiudadCtrl.findOneByIdPais(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByDueat/:dueat`, (req, res) => vdsCiudadCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByCreatedat/:createdat`, (req, res) => vdsCiudadCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-ciudad/findOneByUpdatedat/:updatedat`, (req, res) => vdsCiudadCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByUid`, (req, res) => vdsCiudadCtrl.updateVdCiudadByUid(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadById`, (req, res) => vdsCiudadCtrl.updateVdCiudadById(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByEstado`, (req, res) => vdsCiudadCtrl.updateVdCiudadByEstado(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByCiudad`, (req, res) => vdsCiudadCtrl.updateVdCiudadByCiudad(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByAbrev`, (req, res) => vdsCiudadCtrl.updateVdCiudadByAbrev(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByCreatedby`, (req, res) => vdsCiudadCtrl.updateVdCiudadByCreatedby(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByUpdatedby`, (req, res) => vdsCiudadCtrl.updateVdCiudadByUpdatedby(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByIdPais`, (req, res) => vdsCiudadCtrl.updateVdCiudadByIdPais(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByDueat`, (req, res) => vdsCiudadCtrl.updateVdCiudadByDueat(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByCreatedat`, (req, res) => vdsCiudadCtrl.updateVdCiudadByCreatedat(req, res));

router.post(`/api-${sys}/vds-ciudad/updateVdCiudadByUpdatedat`, (req, res) => vdsCiudadCtrl.updateVdCiudadByUpdatedat(req, res));





router.get(`/api-${sys}/vds-ciudad/`, (req, res) => vdsCiudadCtrl.getAllVdsCiudad(req, res));
router.post(`/api-${sys}/vds-ciudad/datatable/`, (req, res) => vdsCiudadCtrl.getAllVdsCiudad(req, res));
router.post(`/api-${sys}/vds-ciudad/`, (req, res) => vdsCiudadCtrl.addVdCiudad(req, res));
router.get(`/api-${sys}/vds-ciudad/:Id`, (req, res) => vdsCiudadCtrl.getAVdCiudad(req, res));
router.put(`/api-${sys}/vds-ciudad/:Id`, (req, res) => vdsCiudadCtrl.updateVdCiudad(req, res));
router.delete(`/api-${sys}/vds-ciudad/:Id`, (req, res) => vdsCiudadCtrl.deleteVdCiudad(req, res));

//</es-section>
module.exports = router;
