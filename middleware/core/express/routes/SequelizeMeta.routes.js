/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Time: 0:53:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:58
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const sequelizemetaCtrl = require("../controllers/sequelizemeta.controller");
//</es-section>
//<es-section>



router.get(`/api-${sys}/sequelizemeta/findOneByName/:name`, (req, res) => sequelizemetaCtrl.findOneByName(req, res));



router.post(`/api-${sys}/sequelizemeta/updateSequelizemetaByName`, (req, res) => sequelizemetaCtrl.updateSequelizemetaByName(req, res));





router.get(`/api-${sys}/sequelizemeta/`, (req, res) => sequelizemetaCtrl.getAllSequelizemeta(req, res));
router.post(`/api-${sys}/sequelizemeta/datatable/`, (req, res) => sequelizemetaCtrl.getAllSequelizemeta(req, res));
router.post(`/api-${sys}/sequelizemeta/`, (req, res) => sequelizemetaCtrl.addSequelizemeta(req, res));
router.get(`/api-${sys}/sequelizemeta/:name`, (req, res) => sequelizemetaCtrl.getASequelizemeta(req, res));
router.put(`/api-${sys}/sequelizemeta/:name`, (req, res) => sequelizemetaCtrl.updateSequelizemeta(req, res));
router.delete(`/api-${sys}/sequelizemeta/:name`, (req, res) => sequelizemetaCtrl.deleteSequelizemeta(req, res));

//</es-section>
module.exports = router;
