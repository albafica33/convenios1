/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Time: 0:54:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:11
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsRegUsuarioCtrl = require("../controllers/vds_reg_usuario.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-reg-usuario/findVdsUsersUsuarioWithEstado`, (req, res) => vdsRegUsuarioCtrl.findVdsUsersUsuarioWithEstado(req, res));
router.get(`/api-${sys}/vds-reg-usuario/filterVdsRegUsuarioByUsuario/:idUsuario/`, (req, res) => vdsRegUsuarioCtrl.filterVdsRegUsuarioByUsuario(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findVdsMatrizRegWithEstadoCumplimiento`, (req, res) => vdsRegUsuarioCtrl.findVdsMatrizRegWithEstadoCumplimiento(req, res));
router.get(`/api-${sys}/vds-reg-usuario/filterVdsRegUsuarioByReg/:idReg/`, (req, res) => vdsRegUsuarioCtrl.filterVdsRegUsuarioByReg(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsRegUsuarioCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-reg-usuario/filterVdsRegUsuarioByCreatedby/:createdbyid/`, (req, res) => vdsRegUsuarioCtrl.filterVdsRegUsuarioByCreatedby(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsRegUsuarioCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-reg-usuario/filterVdsRegUsuarioByUpdatedby/:updatedbyid/`, (req, res) => vdsRegUsuarioCtrl.filterVdsRegUsuarioByUpdatedby(req, res));



router.get(`/api-${sys}/vds-reg-usuario/findOneByUid/:Id`, (req, res) => vdsRegUsuarioCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findOneById/:id`, (req, res) => vdsRegUsuarioCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findOneByIdUsuario/:idUsuario`, (req, res) => vdsRegUsuarioCtrl.findOneByIdUsuario(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findOneByIdReg/:idReg`, (req, res) => vdsRegUsuarioCtrl.findOneByIdReg(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsRegUsuarioCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsRegUsuarioCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findOneByDueat/:dueat`, (req, res) => vdsRegUsuarioCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findOneByCreatedat/:createdat`, (req, res) => vdsRegUsuarioCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findOneByUpdatedat/:updatedat`, (req, res) => vdsRegUsuarioCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioByUid`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioByUid(req, res));

router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioById`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioById(req, res));

router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioByIdUsuario`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioByIdUsuario(req, res));

router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioByIdReg`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioByIdReg(req, res));

router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioByCreatedbyid`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioByUpdatedbyid`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioByDueat`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioByDueat(req, res));

router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioByCreatedat`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioByCreatedat(req, res));

router.post(`/api-${sys}/vds-reg-usuario/updateVdRegUsuarioByUpdatedat`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuarioByUpdatedat(req, res));



router.get(`/api-${sys}/vds-reg-usuario/findVdsUsersUsuarioWithEstado`, (req, res) => vdsRegUsuarioCtrl.findVdsUsersUsuarioWithEstado(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findVdsMatrizRegWithEstadoCumplimiento`, (req, res) => vdsRegUsuarioCtrl.findVdsMatrizRegWithEstadoCumplimiento(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsRegUsuarioCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-reg-usuario/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsRegUsuarioCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));



router.get(`/api-${sys}/vds-reg-usuario/`, (req, res) => vdsRegUsuarioCtrl.getAllVdsRegUsuario(req, res));
router.post(`/api-${sys}/vds-reg-usuario/datatable/`, (req, res) => vdsRegUsuarioCtrl.getAllVdsRegUsuario(req, res));
router.post(`/api-${sys}/vds-reg-usuario/`, (req, res) => vdsRegUsuarioCtrl.addVdRegUsuario(req, res));
router.get(`/api-${sys}/vds-reg-usuario/:Id`, (req, res) => vdsRegUsuarioCtrl.getAVdRegUsuario(req, res));
router.put(`/api-${sys}/vds-reg-usuario/:Id`, (req, res) => vdsRegUsuarioCtrl.updateVdRegUsuario(req, res));
router.delete(`/api-${sys}/vds-reg-usuario/:Id`, (req, res) => vdsRegUsuarioCtrl.deleteVdRegUsuario(req, res));

//</es-section>
module.exports = router;
