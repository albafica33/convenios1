/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:14 GMT-0400 (GMT-04:00)
 * Time: 0:54:14
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:14 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:14
 *
 * Caution: es-sections will be replaced by script execution
 */

module.exports = function (app) {
// Routes API
    //<es-section>
	
	app.use("/", require("./routes/sequelizemeta.routes"));
	
	app.use("/", require("./routes/vds_ciudad.routes"));
	
	app.use("/", require("./routes/vds_crons.routes"));
	
	app.use("/", require("./routes/vds_dictionaries.routes"));
	
	app.use("/", require("./routes/vds_estado_civil.routes"));
	
	app.use("/", require("./routes/vds_logs.routes"));
	
	app.use("/", require("./routes/vds_mails.routes"));
	
	app.use("/", require("./routes/vds_matriz.routes"));
	
	app.use("/", require("./routes/vds_modules.routes"));
	
	app.use("/", require("./routes/vds_municipio.routes"));
	
	app.use("/", require("./routes/vds_pais.routes"));
	
	app.use("/", require("./routes/vds_params.routes"));
	
	app.use("/", require("./routes/vds_people.routes"));
	
	app.use("/", require("./routes/vds_persona.routes"));
	
	app.use("/", require("./routes/vds_provincia.routes"));
	
	app.use("/", require("./routes/vds_reg_usuario.routes"));
	
	app.use("/", require("./routes/vds_roles.routes"));
	
	app.use("/", require("./routes/vds_sexo.routes"));
	
	app.use("/", require("./routes/vds_users.routes"));
	
	app.use("/", require("./routes/vds_user_roles.routes"));
	
	app.use("/", require("./routes/vds_views.routes"));
	
	//</es-section>
}
