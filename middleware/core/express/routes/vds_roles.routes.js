/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:12 GMT-0400 (GMT-04:00)
 * Time: 0:54:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:12 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:12
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsRolesCtrl = require("../controllers/vds_roles.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-roles/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsRolesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-roles/filterVdsRolesByCreatedby/:createdbyid/`, (req, res) => vdsRolesCtrl.filterVdsRolesByCreatedby(req, res));

router.get(`/api-${sys}/vds-roles/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsRolesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-roles/filterVdsRolesByUpdatedby/:updatedbyid/`, (req, res) => vdsRolesCtrl.filterVdsRolesByUpdatedby(req, res));

router.get(`/api-${sys}/vds-roles/findVdsParamsRolParStatusWithParOrder`, (req, res) => vdsRolesCtrl.findVdsParamsRolParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-roles/filterVdsRolesByRolParStatus/:rolParStatusId/`, (req, res) => vdsRolesCtrl.filterVdsRolesByRolParStatus(req, res));



router.get(`/api-${sys}/vds-roles/findOneByUid/:Id`, (req, res) => vdsRolesCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-roles/findOneById/:id`, (req, res) => vdsRolesCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-roles/findOneByRolCode/:rolCode`, (req, res) => vdsRolesCtrl.findOneByRolCode(req, res));

router.get(`/api-${sys}/vds-roles/findOneByRolDescription/:rolDescription`, (req, res) => vdsRolesCtrl.findOneByRolDescription(req, res));

router.get(`/api-${sys}/vds-roles/findOneByRolAbbr/:rolAbbr`, (req, res) => vdsRolesCtrl.findOneByRolAbbr(req, res));

router.get(`/api-${sys}/vds-roles/findOneByRolGroup/:rolGroup`, (req, res) => vdsRolesCtrl.findOneByRolGroup(req, res));

router.get(`/api-${sys}/vds-roles/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsRolesCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-roles/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsRolesCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-roles/findOneByRolParStatusId/:rolParStatusId`, (req, res) => vdsRolesCtrl.findOneByRolParStatusId(req, res));

router.get(`/api-${sys}/vds-roles/findOneByDueat/:dueat`, (req, res) => vdsRolesCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-roles/findOneByCreatedat/:createdat`, (req, res) => vdsRolesCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-roles/findOneByUpdatedat/:updatedat`, (req, res) => vdsRolesCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-roles/updateVdRoleByUid`, (req, res) => vdsRolesCtrl.updateVdRoleByUid(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleById`, (req, res) => vdsRolesCtrl.updateVdRoleById(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByRolCode`, (req, res) => vdsRolesCtrl.updateVdRoleByRolCode(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByRolDescription`, (req, res) => vdsRolesCtrl.updateVdRoleByRolDescription(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByRolAbbr`, (req, res) => vdsRolesCtrl.updateVdRoleByRolAbbr(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByRolGroup`, (req, res) => vdsRolesCtrl.updateVdRoleByRolGroup(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByCreatedbyid`, (req, res) => vdsRolesCtrl.updateVdRoleByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByUpdatedbyid`, (req, res) => vdsRolesCtrl.updateVdRoleByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByRolParStatusId`, (req, res) => vdsRolesCtrl.updateVdRoleByRolParStatusId(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByDueat`, (req, res) => vdsRolesCtrl.updateVdRoleByDueat(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByCreatedat`, (req, res) => vdsRolesCtrl.updateVdRoleByCreatedat(req, res));

router.post(`/api-${sys}/vds-roles/updateVdRoleByUpdatedat`, (req, res) => vdsRolesCtrl.updateVdRoleByUpdatedat(req, res));



router.get(`/api-${sys}/vds-roles/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsRolesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-roles/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsRolesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-roles/findVdsParamsRolParStatusWithParOrder`, (req, res) => vdsRolesCtrl.findVdsParamsRolParStatusWithParOrder(req, res));



router.get(`/api-${sys}/vds-roles/`, (req, res) => vdsRolesCtrl.getAllVdsRoles(req, res));
router.post(`/api-${sys}/vds-roles/datatable/`, (req, res) => vdsRolesCtrl.getAllVdsRoles(req, res));
router.post(`/api-${sys}/vds-roles/`, (req, res) => vdsRolesCtrl.addVdRole(req, res));
router.get(`/api-${sys}/vds-roles/:Id`, (req, res) => vdsRolesCtrl.getAVdRole(req, res));
router.put(`/api-${sys}/vds-roles/:Id`, (req, res) => vdsRolesCtrl.updateVdRole(req, res));
router.delete(`/api-${sys}/vds-roles/:Id`, (req, res) => vdsRolesCtrl.deleteVdRole(req, res));

//</es-section>
module.exports = router;
