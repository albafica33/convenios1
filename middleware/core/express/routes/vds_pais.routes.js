/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Time: 0:54:5
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:5
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsPaisCtrl = require("../controllers/vds_pais.controller");
//</es-section>
//<es-section>



router.get(`/api-${sys}/vds-pais/findOneByUid/:Id`, (req, res) => vdsPaisCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-pais/findOneById/:id`, (req, res) => vdsPaisCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-pais/findOneByEstado/:estado`, (req, res) => vdsPaisCtrl.findOneByEstado(req, res));

router.get(`/api-${sys}/vds-pais/findOneByPais/:pais`, (req, res) => vdsPaisCtrl.findOneByPais(req, res));

router.get(`/api-${sys}/vds-pais/findOneByAbreviacion/:abreviacion`, (req, res) => vdsPaisCtrl.findOneByAbreviacion(req, res));

router.get(`/api-${sys}/vds-pais/findOneByCreatedby/:createdby`, (req, res) => vdsPaisCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-pais/findOneByUpdatedby/:updatedby`, (req, res) => vdsPaisCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-pais/findOneByDueat/:dueat`, (req, res) => vdsPaisCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-pais/findOneByCreatedat/:createdat`, (req, res) => vdsPaisCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-pais/findOneByUpdatedat/:updatedat`, (req, res) => vdsPaisCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-pais/updateVdPaiByUid`, (req, res) => vdsPaisCtrl.updateVdPaiByUid(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiById`, (req, res) => vdsPaisCtrl.updateVdPaiById(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiByEstado`, (req, res) => vdsPaisCtrl.updateVdPaiByEstado(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiByPais`, (req, res) => vdsPaisCtrl.updateVdPaiByPais(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiByAbreviacion`, (req, res) => vdsPaisCtrl.updateVdPaiByAbreviacion(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiByCreatedby`, (req, res) => vdsPaisCtrl.updateVdPaiByCreatedby(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiByUpdatedby`, (req, res) => vdsPaisCtrl.updateVdPaiByUpdatedby(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiByDueat`, (req, res) => vdsPaisCtrl.updateVdPaiByDueat(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiByCreatedat`, (req, res) => vdsPaisCtrl.updateVdPaiByCreatedat(req, res));

router.post(`/api-${sys}/vds-pais/updateVdPaiByUpdatedat`, (req, res) => vdsPaisCtrl.updateVdPaiByUpdatedat(req, res));





router.get(`/api-${sys}/vds-pais/`, (req, res) => vdsPaisCtrl.getAllVdsPais(req, res));
router.post(`/api-${sys}/vds-pais/datatable/`, (req, res) => vdsPaisCtrl.getAllVdsPais(req, res));
router.post(`/api-${sys}/vds-pais/`, (req, res) => vdsPaisCtrl.addVdPai(req, res));
router.get(`/api-${sys}/vds-pais/:Id`, (req, res) => vdsPaisCtrl.getAVdPai(req, res));
router.put(`/api-${sys}/vds-pais/:Id`, (req, res) => vdsPaisCtrl.updateVdPai(req, res));
router.delete(`/api-${sys}/vds-pais/:Id`, (req, res) => vdsPaisCtrl.deleteVdPai(req, res));

//</es-section>
module.exports = router;
