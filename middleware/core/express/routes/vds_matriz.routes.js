/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:03 GMT-0400 (GMT-04:00)
 * Time: 0:54:3
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:03 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:3
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsMatrizCtrl = require("../controllers/vds_matriz.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-matriz/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsMatrizCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-matriz/filterVdsMatrizByCreatedby/:createdbyid/`, (req, res) => vdsMatrizCtrl.filterVdsMatrizByCreatedby(req, res));

router.get(`/api-${sys}/vds-matriz/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsMatrizCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-matriz/filterVdsMatrizByUpdatedby/:updatedbyid/`, (req, res) => vdsMatrizCtrl.filterVdsMatrizByUpdatedby(req, res));



router.get(`/api-${sys}/vds-matriz/findOneByUid/:Id`, (req, res) => vdsMatrizCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-matriz/findOneById/:id`, (req, res) => vdsMatrizCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByEstadoCumplimiento/:estadoCumplimiento`, (req, res) => vdsMatrizCtrl.findOneByEstadoCumplimiento(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsMatrizCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsMatrizCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByCompromiso/:compromiso`, (req, res) => vdsMatrizCtrl.findOneByCompromiso(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByEntidadResponsable/:entidadResponsable`, (req, res) => vdsMatrizCtrl.findOneByEntidadResponsable(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByContacto/:contacto`, (req, res) => vdsMatrizCtrl.findOneByContacto(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByObservacion/:observacion`, (req, res) => vdsMatrizCtrl.findOneByObservacion(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByTema/:tema`, (req, res) => vdsMatrizCtrl.findOneByTema(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByFechaCumplimiento/:fechaCumplimiento`, (req, res) => vdsMatrizCtrl.findOneByFechaCumplimiento(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByFechaModificacion/:fechaModificacion`, (req, res) => vdsMatrizCtrl.findOneByFechaModificacion(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByDueat/:dueat`, (req, res) => vdsMatrizCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByCreatedat/:createdat`, (req, res) => vdsMatrizCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-matriz/findOneByUpdatedat/:updatedat`, (req, res) => vdsMatrizCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-matriz/updateVdMatrizByUid`, (req, res) => vdsMatrizCtrl.updateVdMatrizByUid(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizById`, (req, res) => vdsMatrizCtrl.updateVdMatrizById(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByEstadoCumplimiento`, (req, res) => vdsMatrizCtrl.updateVdMatrizByEstadoCumplimiento(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByCreatedbyid`, (req, res) => vdsMatrizCtrl.updateVdMatrizByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByUpdatedbyid`, (req, res) => vdsMatrizCtrl.updateVdMatrizByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByCompromiso`, (req, res) => vdsMatrizCtrl.updateVdMatrizByCompromiso(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByEntidadResponsable`, (req, res) => vdsMatrizCtrl.updateVdMatrizByEntidadResponsable(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByContacto`, (req, res) => vdsMatrizCtrl.updateVdMatrizByContacto(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByObservacion`, (req, res) => vdsMatrizCtrl.updateVdMatrizByObservacion(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByTema`, (req, res) => vdsMatrizCtrl.updateVdMatrizByTema(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByFechaCumplimiento`, (req, res) => vdsMatrizCtrl.updateVdMatrizByFechaCumplimiento(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByFechaModificacion`, (req, res) => vdsMatrizCtrl.updateVdMatrizByFechaModificacion(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByDueat`, (req, res) => vdsMatrizCtrl.updateVdMatrizByDueat(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByCreatedat`, (req, res) => vdsMatrizCtrl.updateVdMatrizByCreatedat(req, res));

router.post(`/api-${sys}/vds-matriz/updateVdMatrizByUpdatedat`, (req, res) => vdsMatrizCtrl.updateVdMatrizByUpdatedat(req, res));



router.get(`/api-${sys}/vds-matriz/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsMatrizCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-matriz/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsMatrizCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));



router.get(`/api-${sys}/vds-matriz/`, (req, res) => vdsMatrizCtrl.getAllVdsMatriz(req, res));
router.post(`/api-${sys}/vds-matriz/datatable/`, (req, res) => vdsMatrizCtrl.getAllVdsMatriz(req, res));
router.post(`/api-${sys}/vds-matriz/`, (req, res) => vdsMatrizCtrl.addVdMatriz(req, res));
router.get(`/api-${sys}/vds-matriz/:Id`, (req, res) => vdsMatrizCtrl.getAVdMatriz(req, res));
router.put(`/api-${sys}/vds-matriz/:Id`, (req, res) => vdsMatrizCtrl.updateVdMatriz(req, res));
router.delete(`/api-${sys}/vds-matriz/:Id`, (req, res) => vdsMatrizCtrl.deleteVdMatriz(req, res));

//</es-section>
module.exports = router;
