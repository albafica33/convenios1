/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Time: 0:54:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:1
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsEstadoCivilCtrl = require("../controllers/vds_estado_civil.controller");
//</es-section>
//<es-section>



router.get(`/api-${sys}/vds-estado-civil/findOneByUid/:Id`, (req, res) => vdsEstadoCivilCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-estado-civil/findOneById/:id`, (req, res) => vdsEstadoCivilCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-estado-civil/findOneByEstado/:estado`, (req, res) => vdsEstadoCivilCtrl.findOneByEstado(req, res));

router.get(`/api-${sys}/vds-estado-civil/findOneByDescripcion/:descripcion`, (req, res) => vdsEstadoCivilCtrl.findOneByDescripcion(req, res));

router.get(`/api-${sys}/vds-estado-civil/findOneByCreatedby/:createdby`, (req, res) => vdsEstadoCivilCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-estado-civil/findOneByUpdatedby/:updatedby`, (req, res) => vdsEstadoCivilCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-estado-civil/findOneByDueat/:dueat`, (req, res) => vdsEstadoCivilCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-estado-civil/findOneByCreatedat/:createdat`, (req, res) => vdsEstadoCivilCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-estado-civil/findOneByUpdatedat/:updatedat`, (req, res) => vdsEstadoCivilCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilByUid`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilByUid(req, res));

router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilById`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilById(req, res));

router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilByEstado`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilByEstado(req, res));

router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilByDescripcion`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilByDescripcion(req, res));

router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilByCreatedby`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilByCreatedby(req, res));

router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilByUpdatedby`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilByUpdatedby(req, res));

router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilByDueat`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilByDueat(req, res));

router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilByCreatedat`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilByCreatedat(req, res));

router.post(`/api-${sys}/vds-estado-civil/updateVdEstadoCivilByUpdatedat`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivilByUpdatedat(req, res));





router.get(`/api-${sys}/vds-estado-civil/`, (req, res) => vdsEstadoCivilCtrl.getAllVdsEstadoCivil(req, res));
router.post(`/api-${sys}/vds-estado-civil/datatable/`, (req, res) => vdsEstadoCivilCtrl.getAllVdsEstadoCivil(req, res));
router.post(`/api-${sys}/vds-estado-civil/`, (req, res) => vdsEstadoCivilCtrl.addVdEstadoCivil(req, res));
router.get(`/api-${sys}/vds-estado-civil/:Id`, (req, res) => vdsEstadoCivilCtrl.getAVdEstadoCivil(req, res));
router.put(`/api-${sys}/vds-estado-civil/:Id`, (req, res) => vdsEstadoCivilCtrl.updateVdEstadoCivil(req, res));
router.delete(`/api-${sys}/vds-estado-civil/:Id`, (req, res) => vdsEstadoCivilCtrl.deleteVdEstadoCivil(req, res));

//</es-section>
module.exports = router;
