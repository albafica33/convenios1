/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:07 GMT-0400 (GMT-04:00)
 * Time: 0:54:7
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:07 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:7
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsParamsCtrl = require("../controllers/vds_params.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-params/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsParamsCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-params/filterVdsParamsByCreatedby/:createdbyid/`, (req, res) => vdsParamsCtrl.filterVdsParamsByCreatedby(req, res));

router.get(`/api-${sys}/vds-params/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsParamsCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-params/filterVdsParamsByUpdatedby/:updatedbyid/`, (req, res) => vdsParamsCtrl.filterVdsParamsByUpdatedby(req, res));

router.get(`/api-${sys}/vds-params/findVdsDictionariesParDictionaryWithDicCode`, (req, res) => vdsParamsCtrl.findVdsDictionariesParDictionaryWithDicCode(req, res));
router.get(`/api-${sys}/vds-params/filterVdsParamsByParDictionary/:parDictionaryId/`, (req, res) => vdsParamsCtrl.filterVdsParamsByParDictionary(req, res));

router.get(`/api-${sys}/vds-params/findVdsParamsParStatusWithParOrder`, (req, res) => vdsParamsCtrl.findVdsParamsParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-params/filterVdsParamsByParStatus/:parStatusId/`, (req, res) => vdsParamsCtrl.filterVdsParamsByParStatus(req, res));



router.get(`/api-${sys}/vds-params/findOneByUid/:Id`, (req, res) => vdsParamsCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-params/findOneById/:id`, (req, res) => vdsParamsCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-params/findOneByParOrder/:parOrder`, (req, res) => vdsParamsCtrl.findOneByParOrder(req, res));

router.get(`/api-${sys}/vds-params/findOneByParCod/:parCod`, (req, res) => vdsParamsCtrl.findOneByParCod(req, res));

router.get(`/api-${sys}/vds-params/findOneByParDescription/:parDescription`, (req, res) => vdsParamsCtrl.findOneByParDescription(req, res));

router.get(`/api-${sys}/vds-params/findOneByParAbbr/:parAbbr`, (req, res) => vdsParamsCtrl.findOneByParAbbr(req, res));

router.get(`/api-${sys}/vds-params/findOneByParGroup/:parGroup`, (req, res) => vdsParamsCtrl.findOneByParGroup(req, res));

router.get(`/api-${sys}/vds-params/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsParamsCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-params/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsParamsCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-params/findOneByParDictionaryId/:parDictionaryId`, (req, res) => vdsParamsCtrl.findOneByParDictionaryId(req, res));

router.get(`/api-${sys}/vds-params/findOneByParStatusId/:parStatusId`, (req, res) => vdsParamsCtrl.findOneByParStatusId(req, res));

router.get(`/api-${sys}/vds-params/findOneByDueat/:dueat`, (req, res) => vdsParamsCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-params/findOneByCreatedat/:createdat`, (req, res) => vdsParamsCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-params/findOneByUpdatedat/:updatedat`, (req, res) => vdsParamsCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-params/updateVdParamByUid`, (req, res) => vdsParamsCtrl.updateVdParamByUid(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamById`, (req, res) => vdsParamsCtrl.updateVdParamById(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByParOrder`, (req, res) => vdsParamsCtrl.updateVdParamByParOrder(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByParCod`, (req, res) => vdsParamsCtrl.updateVdParamByParCod(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByParDescription`, (req, res) => vdsParamsCtrl.updateVdParamByParDescription(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByParAbbr`, (req, res) => vdsParamsCtrl.updateVdParamByParAbbr(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByParGroup`, (req, res) => vdsParamsCtrl.updateVdParamByParGroup(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByCreatedbyid`, (req, res) => vdsParamsCtrl.updateVdParamByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByUpdatedbyid`, (req, res) => vdsParamsCtrl.updateVdParamByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByParDictionaryId`, (req, res) => vdsParamsCtrl.updateVdParamByParDictionaryId(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByParStatusId`, (req, res) => vdsParamsCtrl.updateVdParamByParStatusId(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByDueat`, (req, res) => vdsParamsCtrl.updateVdParamByDueat(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByCreatedat`, (req, res) => vdsParamsCtrl.updateVdParamByCreatedat(req, res));

router.post(`/api-${sys}/vds-params/updateVdParamByUpdatedat`, (req, res) => vdsParamsCtrl.updateVdParamByUpdatedat(req, res));



router.get(`/api-${sys}/vds-params/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsParamsCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-params/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsParamsCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-params/findVdsDictionariesParDictionaryWithDicCode`, (req, res) => vdsParamsCtrl.findVdsDictionariesParDictionaryWithDicCode(req, res));

router.get(`/api-${sys}/vds-params/findVdsParamsParStatusWithParOrder`, (req, res) => vdsParamsCtrl.findVdsParamsParStatusWithParOrder(req, res));



router.get(`/api-${sys}/vds-params/`, (req, res) => vdsParamsCtrl.getAllVdsParams(req, res));
router.post(`/api-${sys}/vds-params/datatable/`, (req, res) => vdsParamsCtrl.getAllVdsParams(req, res));
router.post(`/api-${sys}/vds-params/`, (req, res) => vdsParamsCtrl.addVdParam(req, res));
router.get(`/api-${sys}/vds-params/:Id`, (req, res) => vdsParamsCtrl.getAVdParam(req, res));
router.put(`/api-${sys}/vds-params/:Id`, (req, res) => vdsParamsCtrl.updateVdParam(req, res));
router.delete(`/api-${sys}/vds-params/:Id`, (req, res) => vdsParamsCtrl.deleteVdParam(req, res));

//</es-section>
module.exports = router;
