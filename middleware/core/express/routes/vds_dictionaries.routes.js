/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:00 GMT-0400 (GMT-04:00)
 * Time: 0:54:0
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:00 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:0
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsDictionariesCtrl = require("../controllers/vds_dictionaries.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-dictionaries/findVdsParamsDicParStatusWithParOrder`, (req, res) => vdsDictionariesCtrl.findVdsParamsDicParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-dictionaries/filterVdsDictionariesByDicParStatus/:dicParStatusId/`, (req, res) => vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus(req, res));

router.get(`/api-${sys}/vds-dictionaries/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsDictionariesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-dictionaries/filterVdsDictionariesByCreatedby/:createdbyid/`, (req, res) => vdsDictionariesCtrl.filterVdsDictionariesByCreatedby(req, res));

router.get(`/api-${sys}/vds-dictionaries/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsDictionariesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-dictionaries/filterVdsDictionariesByUpdatedby/:updatedbyid/`, (req, res) => vdsDictionariesCtrl.filterVdsDictionariesByUpdatedby(req, res));



router.get(`/api-${sys}/vds-dictionaries/findOneByUid/:Id`, (req, res) => vdsDictionariesCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneById/:id`, (req, res) => vdsDictionariesCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByDicCode/:dicCode`, (req, res) => vdsDictionariesCtrl.findOneByDicCode(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByDicDescription/:dicDescription`, (req, res) => vdsDictionariesCtrl.findOneByDicDescription(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByDicGroup/:dicGroup`, (req, res) => vdsDictionariesCtrl.findOneByDicGroup(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByDicParStatusId/:dicParStatusId`, (req, res) => vdsDictionariesCtrl.findOneByDicParStatusId(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsDictionariesCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsDictionariesCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByDueat/:dueat`, (req, res) => vdsDictionariesCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByCreatedat/:createdat`, (req, res) => vdsDictionariesCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-dictionaries/findOneByUpdatedat/:updatedat`, (req, res) => vdsDictionariesCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByUid`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByUid(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryById`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryById(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByDicCode`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByDicCode(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByDicDescription`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByDicDescription(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByDicGroup`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByDicGroup(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByDicParStatusId`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByDicParStatusId(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByCreatedbyid`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByUpdatedbyid`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByDueat`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByDueat(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByCreatedat`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByCreatedat(req, res));

router.post(`/api-${sys}/vds-dictionaries/updateVdDictionaryByUpdatedat`, (req, res) => vdsDictionariesCtrl.updateVdDictionaryByUpdatedat(req, res));



router.get(`/api-${sys}/vds-dictionaries/findVdsParamsDicParStatusWithParOrder`, (req, res) => vdsDictionariesCtrl.findVdsParamsDicParStatusWithParOrder(req, res));

router.get(`/api-${sys}/vds-dictionaries/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsDictionariesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-dictionaries/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsDictionariesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));



router.get(`/api-${sys}/vds-dictionaries/`, (req, res) => vdsDictionariesCtrl.getAllVdsDictionaries(req, res));
router.post(`/api-${sys}/vds-dictionaries/datatable/`, (req, res) => vdsDictionariesCtrl.getAllVdsDictionaries(req, res));
router.post(`/api-${sys}/vds-dictionaries/`, (req, res) => vdsDictionariesCtrl.addVdDictionary(req, res));
router.get(`/api-${sys}/vds-dictionaries/:Id`, (req, res) => vdsDictionariesCtrl.getAVdDictionary(req, res));
router.put(`/api-${sys}/vds-dictionaries/:Id`, (req, res) => vdsDictionariesCtrl.updateVdDictionary(req, res));
router.delete(`/api-${sys}/vds-dictionaries/:Id`, (req, res) => vdsDictionariesCtrl.deleteVdDictionary(req, res));

//</es-section>
module.exports = router;
