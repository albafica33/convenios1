/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:10 GMT-0400 (GMT-04:00)
 * Time: 0:54:10
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:10 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:10
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsPersonaCtrl = require("../controllers/vds_persona.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-persona/findVdsCiudadCiExpedoWithEstado`, (req, res) => vdsPersonaCtrl.findVdsCiudadCiExpedoWithEstado(req, res));
router.get(`/api-${sys}/vds-persona/filterVdsPersonaByCiExpedo/:ciExpedido/`, (req, res) => vdsPersonaCtrl.filterVdsPersonaByCiExpedo(req, res));

router.get(`/api-${sys}/vds-persona/findVdsEstadoCivilEstadoCivilWithEstado`, (req, res) => vdsPersonaCtrl.findVdsEstadoCivilEstadoCivilWithEstado(req, res));
router.get(`/api-${sys}/vds-persona/filterVdsPersonaByEstadoCivil/:idEstadoCivil/`, (req, res) => vdsPersonaCtrl.filterVdsPersonaByEstadoCivil(req, res));

router.get(`/api-${sys}/vds-persona/findVdsSexoSexoWithEstado`, (req, res) => vdsPersonaCtrl.findVdsSexoSexoWithEstado(req, res));
router.get(`/api-${sys}/vds-persona/filterVdsPersonaBySexo/:idSexo/`, (req, res) => vdsPersonaCtrl.filterVdsPersonaBySexo(req, res));

router.get(`/api-${sys}/vds-persona/findVdsMunicipioMunicipioWithEstado`, (req, res) => vdsPersonaCtrl.findVdsMunicipioMunicipioWithEstado(req, res));
router.get(`/api-${sys}/vds-persona/filterVdsPersonaByMunicipio/:idMunicipio/`, (req, res) => vdsPersonaCtrl.filterVdsPersonaByMunicipio(req, res));

router.get(`/api-${sys}/vds-persona/findVdsProvinciaProvinciaWithEstado`, (req, res) => vdsPersonaCtrl.findVdsProvinciaProvinciaWithEstado(req, res));
router.get(`/api-${sys}/vds-persona/filterVdsPersonaByProvincia/:idProvincia/`, (req, res) => vdsPersonaCtrl.filterVdsPersonaByProvincia(req, res));

router.get(`/api-${sys}/vds-persona/findVdsCiudadCiudadWithEstado`, (req, res) => vdsPersonaCtrl.findVdsCiudadCiudadWithEstado(req, res));
router.get(`/api-${sys}/vds-persona/filterVdsPersonaByCiudad/:idCiudad/`, (req, res) => vdsPersonaCtrl.filterVdsPersonaByCiudad(req, res));

router.get(`/api-${sys}/vds-persona/findVdsPaisPaisWithEstado`, (req, res) => vdsPersonaCtrl.findVdsPaisPaisWithEstado(req, res));
router.get(`/api-${sys}/vds-persona/filterVdsPersonaByPais/:idPais/`, (req, res) => vdsPersonaCtrl.filterVdsPersonaByPais(req, res));



router.get(`/api-${sys}/vds-persona/findOneByUid/:Id`, (req, res) => vdsPersonaCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-persona/findOneById/:id`, (req, res) => vdsPersonaCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-persona/findOneByEstado/:estado`, (req, res) => vdsPersonaCtrl.findOneByEstado(req, res));

router.get(`/api-${sys}/vds-persona/findOneByTelefono/:telefono`, (req, res) => vdsPersonaCtrl.findOneByTelefono(req, res));

router.get(`/api-${sys}/vds-persona/findOneByCelular/:celular`, (req, res) => vdsPersonaCtrl.findOneByCelular(req, res));

router.get(`/api-${sys}/vds-persona/findOneByNombres/:nombres`, (req, res) => vdsPersonaCtrl.findOneByNombres(req, res));

router.get(`/api-${sys}/vds-persona/findOneByPaterno/:paterno`, (req, res) => vdsPersonaCtrl.findOneByPaterno(req, res));

router.get(`/api-${sys}/vds-persona/findOneByMaterno/:materno`, (req, res) => vdsPersonaCtrl.findOneByMaterno(req, res));

router.get(`/api-${sys}/vds-persona/findOneByCasada/:casada`, (req, res) => vdsPersonaCtrl.findOneByCasada(req, res));

router.get(`/api-${sys}/vds-persona/findOneByCi/:ci`, (req, res) => vdsPersonaCtrl.findOneByCi(req, res));

router.get(`/api-${sys}/vds-persona/findOneByCorreo/:correo`, (req, res) => vdsPersonaCtrl.findOneByCorreo(req, res));

router.get(`/api-${sys}/vds-persona/findOneByDireccion/:direccion`, (req, res) => vdsPersonaCtrl.findOneByDireccion(req, res));

router.get(`/api-${sys}/vds-persona/findOneByCreatedby/:createdby`, (req, res) => vdsPersonaCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-persona/findOneByUpdatedby/:updatedby`, (req, res) => vdsPersonaCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-persona/findOneByCiExpedido/:ciExpedido`, (req, res) => vdsPersonaCtrl.findOneByCiExpedido(req, res));

router.get(`/api-${sys}/vds-persona/findOneByIdEstadoCivil/:idEstadoCivil`, (req, res) => vdsPersonaCtrl.findOneByIdEstadoCivil(req, res));

router.get(`/api-${sys}/vds-persona/findOneByIdSexo/:idSexo`, (req, res) => vdsPersonaCtrl.findOneByIdSexo(req, res));

router.get(`/api-${sys}/vds-persona/findOneByIdMunicipio/:idMunicipio`, (req, res) => vdsPersonaCtrl.findOneByIdMunicipio(req, res));

router.get(`/api-${sys}/vds-persona/findOneByIdProvincia/:idProvincia`, (req, res) => vdsPersonaCtrl.findOneByIdProvincia(req, res));

router.get(`/api-${sys}/vds-persona/findOneByIdCiudad/:idCiudad`, (req, res) => vdsPersonaCtrl.findOneByIdCiudad(req, res));

router.get(`/api-${sys}/vds-persona/findOneByIdPais/:idPais`, (req, res) => vdsPersonaCtrl.findOneByIdPais(req, res));

router.get(`/api-${sys}/vds-persona/findOneByDueat/:dueat`, (req, res) => vdsPersonaCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-persona/findOneByCreatedat/:createdat`, (req, res) => vdsPersonaCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-persona/findOneByUpdatedat/:updatedat`, (req, res) => vdsPersonaCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-persona/updateVdPersonaByUid`, (req, res) => vdsPersonaCtrl.updateVdPersonaByUid(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaById`, (req, res) => vdsPersonaCtrl.updateVdPersonaById(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByEstado`, (req, res) => vdsPersonaCtrl.updateVdPersonaByEstado(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByTelefono`, (req, res) => vdsPersonaCtrl.updateVdPersonaByTelefono(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByCelular`, (req, res) => vdsPersonaCtrl.updateVdPersonaByCelular(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByNombres`, (req, res) => vdsPersonaCtrl.updateVdPersonaByNombres(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByPaterno`, (req, res) => vdsPersonaCtrl.updateVdPersonaByPaterno(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByMaterno`, (req, res) => vdsPersonaCtrl.updateVdPersonaByMaterno(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByCasada`, (req, res) => vdsPersonaCtrl.updateVdPersonaByCasada(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByCi`, (req, res) => vdsPersonaCtrl.updateVdPersonaByCi(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByCorreo`, (req, res) => vdsPersonaCtrl.updateVdPersonaByCorreo(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByDireccion`, (req, res) => vdsPersonaCtrl.updateVdPersonaByDireccion(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByCreatedby`, (req, res) => vdsPersonaCtrl.updateVdPersonaByCreatedby(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByUpdatedby`, (req, res) => vdsPersonaCtrl.updateVdPersonaByUpdatedby(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByCiExpedido`, (req, res) => vdsPersonaCtrl.updateVdPersonaByCiExpedido(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByIdEstadoCivil`, (req, res) => vdsPersonaCtrl.updateVdPersonaByIdEstadoCivil(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByIdSexo`, (req, res) => vdsPersonaCtrl.updateVdPersonaByIdSexo(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByIdMunicipio`, (req, res) => vdsPersonaCtrl.updateVdPersonaByIdMunicipio(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByIdProvincia`, (req, res) => vdsPersonaCtrl.updateVdPersonaByIdProvincia(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByIdCiudad`, (req, res) => vdsPersonaCtrl.updateVdPersonaByIdCiudad(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByIdPais`, (req, res) => vdsPersonaCtrl.updateVdPersonaByIdPais(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByDueat`, (req, res) => vdsPersonaCtrl.updateVdPersonaByDueat(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByCreatedat`, (req, res) => vdsPersonaCtrl.updateVdPersonaByCreatedat(req, res));

router.post(`/api-${sys}/vds-persona/updateVdPersonaByUpdatedat`, (req, res) => vdsPersonaCtrl.updateVdPersonaByUpdatedat(req, res));



router.get(`/api-${sys}/vds-persona/findVdsCiudadCiExpedoWithEstado`, (req, res) => vdsPersonaCtrl.findVdsCiudadCiExpedoWithEstado(req, res));

router.get(`/api-${sys}/vds-persona/findVdsEstadoCivilEstadoCivilWithEstado`, (req, res) => vdsPersonaCtrl.findVdsEstadoCivilEstadoCivilWithEstado(req, res));

router.get(`/api-${sys}/vds-persona/findVdsSexoSexoWithEstado`, (req, res) => vdsPersonaCtrl.findVdsSexoSexoWithEstado(req, res));

router.get(`/api-${sys}/vds-persona/findVdsMunicipioMunicipioWithEstado`, (req, res) => vdsPersonaCtrl.findVdsMunicipioMunicipioWithEstado(req, res));

router.get(`/api-${sys}/vds-persona/findVdsProvinciaProvinciaWithEstado`, (req, res) => vdsPersonaCtrl.findVdsProvinciaProvinciaWithEstado(req, res));

router.get(`/api-${sys}/vds-persona/findVdsCiudadCiudadWithEstado`, (req, res) => vdsPersonaCtrl.findVdsCiudadCiudadWithEstado(req, res));

router.get(`/api-${sys}/vds-persona/findVdsPaisPaisWithEstado`, (req, res) => vdsPersonaCtrl.findVdsPaisPaisWithEstado(req, res));



router.get(`/api-${sys}/vds-persona/`, (req, res) => vdsPersonaCtrl.getAllVdsPersona(req, res));
router.post(`/api-${sys}/vds-persona/datatable/`, (req, res) => vdsPersonaCtrl.getAllVdsPersona(req, res));
router.post(`/api-${sys}/vds-persona/`, (req, res) => vdsPersonaCtrl.addVdPersona(req, res));
router.get(`/api-${sys}/vds-persona/:Id`, (req, res) => vdsPersonaCtrl.getAVdPersona(req, res));
router.put(`/api-${sys}/vds-persona/:Id`, (req, res) => vdsPersonaCtrl.updateVdPersona(req, res));
router.delete(`/api-${sys}/vds-persona/:Id`, (req, res) => vdsPersonaCtrl.deleteVdPersona(req, res));

//</es-section>
module.exports = router;
