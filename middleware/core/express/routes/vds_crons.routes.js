/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:59 GMT-0400 (GMT-04:00)
 * Time: 0:53:59
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:59 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:59
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsCronsCtrl = require("../controllers/vds_crons.controller");
//</es-section>
//<es-section>



router.get(`/api-${sys}/vds-crons/findOneByUid/:Id`, (req, res) => vdsCronsCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-crons/findOneById/:id`, (req, res) => vdsCronsCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-crons/findOneByCroStatus/:croStatus`, (req, res) => vdsCronsCtrl.findOneByCroStatus(req, res));

router.get(`/api-${sys}/vds-crons/findOneByCroDescription/:croDescription`, (req, res) => vdsCronsCtrl.findOneByCroDescription(req, res));

router.get(`/api-${sys}/vds-crons/findOneByCroExpression/:croExpression`, (req, res) => vdsCronsCtrl.findOneByCroExpression(req, res));

router.get(`/api-${sys}/vds-crons/findOneByCroGroup/:croGroup`, (req, res) => vdsCronsCtrl.findOneByCroGroup(req, res));

router.get(`/api-${sys}/vds-crons/findOneByCroMaiId/:croMaiId`, (req, res) => vdsCronsCtrl.findOneByCroMaiId(req, res));

router.get(`/api-${sys}/vds-crons/findOneByCreatedby/:createdby`, (req, res) => vdsCronsCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-crons/findOneByUpdatedby/:updatedby`, (req, res) => vdsCronsCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-crons/findOneByCroFunction/:croFunction`, (req, res) => vdsCronsCtrl.findOneByCroFunction(req, res));

router.get(`/api-${sys}/vds-crons/findOneByDueat/:dueat`, (req, res) => vdsCronsCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-crons/findOneByCreatedat/:createdat`, (req, res) => vdsCronsCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-crons/findOneByUpdatedat/:updatedat`, (req, res) => vdsCronsCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-crons/updateVdCronByUid`, (req, res) => vdsCronsCtrl.updateVdCronByUid(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronById`, (req, res) => vdsCronsCtrl.updateVdCronById(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByCroStatus`, (req, res) => vdsCronsCtrl.updateVdCronByCroStatus(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByCroDescription`, (req, res) => vdsCronsCtrl.updateVdCronByCroDescription(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByCroExpression`, (req, res) => vdsCronsCtrl.updateVdCronByCroExpression(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByCroGroup`, (req, res) => vdsCronsCtrl.updateVdCronByCroGroup(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByCroMaiId`, (req, res) => vdsCronsCtrl.updateVdCronByCroMaiId(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByCreatedby`, (req, res) => vdsCronsCtrl.updateVdCronByCreatedby(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByUpdatedby`, (req, res) => vdsCronsCtrl.updateVdCronByUpdatedby(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByCroFunction`, (req, res) => vdsCronsCtrl.updateVdCronByCroFunction(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByDueat`, (req, res) => vdsCronsCtrl.updateVdCronByDueat(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByCreatedat`, (req, res) => vdsCronsCtrl.updateVdCronByCreatedat(req, res));

router.post(`/api-${sys}/vds-crons/updateVdCronByUpdatedat`, (req, res) => vdsCronsCtrl.updateVdCronByUpdatedat(req, res));





router.get(`/api-${sys}/vds-crons/`, (req, res) => vdsCronsCtrl.getAllVdsCrons(req, res));
router.post(`/api-${sys}/vds-crons/datatable/`, (req, res) => vdsCronsCtrl.getAllVdsCrons(req, res));
router.post(`/api-${sys}/vds-crons/`, (req, res) => vdsCronsCtrl.addVdCron(req, res));
router.get(`/api-${sys}/vds-crons/:Id`, (req, res) => vdsCronsCtrl.getAVdCron(req, res));
router.put(`/api-${sys}/vds-crons/:Id`, (req, res) => vdsCronsCtrl.updateVdCron(req, res));
router.delete(`/api-${sys}/vds-crons/:Id`, (req, res) => vdsCronsCtrl.deleteVdCron(req, res));

//</es-section>
module.exports = router;
