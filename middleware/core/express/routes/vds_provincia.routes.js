/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Time: 0:54:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:11
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsProvinciaCtrl = require("../controllers/vds_provincia.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-provincia/findVdsCiudadCiudadWithEstado`, (req, res) => vdsProvinciaCtrl.findVdsCiudadCiudadWithEstado(req, res));
router.get(`/api-${sys}/vds-provincia/filterVdsProvinciaByCiudad/:idCiudad/`, (req, res) => vdsProvinciaCtrl.filterVdsProvinciaByCiudad(req, res));



router.get(`/api-${sys}/vds-provincia/findOneByUid/:Id`, (req, res) => vdsProvinciaCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-provincia/findOneById/:id`, (req, res) => vdsProvinciaCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByEstado/:estado`, (req, res) => vdsProvinciaCtrl.findOneByEstado(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByProvincia/:provincia`, (req, res) => vdsProvinciaCtrl.findOneByProvincia(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByAbreviacion/:abreviacion`, (req, res) => vdsProvinciaCtrl.findOneByAbreviacion(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByCreatedby/:createdby`, (req, res) => vdsProvinciaCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByUpdatedby/:updatedby`, (req, res) => vdsProvinciaCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByIdCiudad/:idCiudad`, (req, res) => vdsProvinciaCtrl.findOneByIdCiudad(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByDueat/:dueat`, (req, res) => vdsProvinciaCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByCreatedat/:createdat`, (req, res) => vdsProvinciaCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-provincia/findOneByUpdatedat/:updatedat`, (req, res) => vdsProvinciaCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByUid`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByUid(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaById`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaById(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByEstado`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByEstado(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByProvincia`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByProvincia(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByAbreviacion`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByAbreviacion(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByCreatedby`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByCreatedby(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByUpdatedby`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByUpdatedby(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByIdCiudad`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByIdCiudad(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByDueat`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByDueat(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByCreatedat`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByCreatedat(req, res));

router.post(`/api-${sys}/vds-provincia/updateVdProvinciaByUpdatedat`, (req, res) => vdsProvinciaCtrl.updateVdProvinciaByUpdatedat(req, res));



router.get(`/api-${sys}/vds-provincia/findVdsCiudadCiudadWithEstado`, (req, res) => vdsProvinciaCtrl.findVdsCiudadCiudadWithEstado(req, res));



router.get(`/api-${sys}/vds-provincia/`, (req, res) => vdsProvinciaCtrl.getAllVdsProvincia(req, res));
router.post(`/api-${sys}/vds-provincia/datatable/`, (req, res) => vdsProvinciaCtrl.getAllVdsProvincia(req, res));
router.post(`/api-${sys}/vds-provincia/`, (req, res) => vdsProvinciaCtrl.addVdProvincia(req, res));
router.get(`/api-${sys}/vds-provincia/:Id`, (req, res) => vdsProvinciaCtrl.getAVdProvincia(req, res));
router.put(`/api-${sys}/vds-provincia/:Id`, (req, res) => vdsProvinciaCtrl.updateVdProvincia(req, res));
router.delete(`/api-${sys}/vds-provincia/:Id`, (req, res) => vdsProvinciaCtrl.deleteVdProvincia(req, res));

//</es-section>
module.exports = router;
