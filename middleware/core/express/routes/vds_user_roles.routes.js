/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:14 GMT-0400 (GMT-04:00)
 * Time: 0:54:14
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:14 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:14
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsUserRolesCtrl = require("../controllers/vds_user_roles.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-user-roles/findVdsUsersUsrWithEstado`, (req, res) => vdsUserRolesCtrl.findVdsUsersUsrWithEstado(req, res));
router.get(`/api-${sys}/vds-user-roles/filterVdsUserRolesByUsr/:usrId/`, (req, res) => vdsUserRolesCtrl.filterVdsUserRolesByUsr(req, res));

router.get(`/api-${sys}/vds-user-roles/findVdsRolesRolWithRolCode`, (req, res) => vdsUserRolesCtrl.findVdsRolesRolWithRolCode(req, res));
router.get(`/api-${sys}/vds-user-roles/filterVdsUserRolesByRol/:rolId/`, (req, res) => vdsUserRolesCtrl.filterVdsUserRolesByRol(req, res));

router.get(`/api-${sys}/vds-user-roles/findVdsParamsUsrRolParStatusWithParOrder`, (req, res) => vdsUserRolesCtrl.findVdsParamsUsrRolParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-user-roles/filterVdsUserRolesByUsrRolParStatus/:usrRolParStatusId/`, (req, res) => vdsUserRolesCtrl.filterVdsUserRolesByUsrRolParStatus(req, res));

router.get(`/api-${sys}/vds-user-roles/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsUserRolesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-user-roles/filterVdsUserRolesByCreatedby/:createdbyid/`, (req, res) => vdsUserRolesCtrl.filterVdsUserRolesByCreatedby(req, res));

router.get(`/api-${sys}/vds-user-roles/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsUserRolesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-user-roles/filterVdsUserRolesByUpdatedby/:updatedbyid/`, (req, res) => vdsUserRolesCtrl.filterVdsUserRolesByUpdatedby(req, res));



router.get(`/api-${sys}/vds-user-roles/findOneByUid/:Id`, (req, res) => vdsUserRolesCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneById/:id`, (req, res) => vdsUserRolesCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByUsrRolGroup/:usrRolGroup`, (req, res) => vdsUserRolesCtrl.findOneByUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByUsrId/:usrId`, (req, res) => vdsUserRolesCtrl.findOneByUsrId(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByRolId/:rolId`, (req, res) => vdsUserRolesCtrl.findOneByRolId(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByUsrRolParStatusId/:usrRolParStatusId`, (req, res) => vdsUserRolesCtrl.findOneByUsrRolParStatusId(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsUserRolesCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsUserRolesCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByDueat/:dueat`, (req, res) => vdsUserRolesCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByCreatedat/:createdat`, (req, res) => vdsUserRolesCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-user-roles/findOneByUpdatedat/:updatedat`, (req, res) => vdsUserRolesCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByUid`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByUid(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleById`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleById(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByUsrRolGroup`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByUsrRolGroup(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByUsrId`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByUsrId(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByRolId`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByRolId(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByUsrRolParStatusId`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByUsrRolParStatusId(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByCreatedbyid`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByUpdatedbyid`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByDueat`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByDueat(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByCreatedat`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByCreatedat(req, res));

router.post(`/api-${sys}/vds-user-roles/updateVdUserRoleByUpdatedat`, (req, res) => vdsUserRolesCtrl.updateVdUserRoleByUpdatedat(req, res));



router.get(`/api-${sys}/vds-user-roles/findVdsUsersUsrWithEstado`, (req, res) => vdsUserRolesCtrl.findVdsUsersUsrWithEstado(req, res));

router.get(`/api-${sys}/vds-user-roles/findVdsRolesRolWithRolCode`, (req, res) => vdsUserRolesCtrl.findVdsRolesRolWithRolCode(req, res));

router.get(`/api-${sys}/vds-user-roles/findVdsParamsUsrRolParStatusWithParOrder`, (req, res) => vdsUserRolesCtrl.findVdsParamsUsrRolParStatusWithParOrder(req, res));

router.get(`/api-${sys}/vds-user-roles/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsUserRolesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-user-roles/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsUserRolesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));



router.get(`/api-${sys}/vds-user-roles/`, (req, res) => vdsUserRolesCtrl.getAllVdsUserRoles(req, res));
router.post(`/api-${sys}/vds-user-roles/datatable/`, (req, res) => vdsUserRolesCtrl.getAllVdsUserRoles(req, res));
router.post(`/api-${sys}/vds-user-roles/`, (req, res) => vdsUserRolesCtrl.addVdUserRole(req, res));
router.get(`/api-${sys}/vds-user-roles/:Id`, (req, res) => vdsUserRolesCtrl.getAVdUserRole(req, res));
router.put(`/api-${sys}/vds-user-roles/:Id`, (req, res) => vdsUserRolesCtrl.updateVdUserRole(req, res));
router.delete(`/api-${sys}/vds-user-roles/:Id`, (req, res) => vdsUserRolesCtrl.deleteVdUserRole(req, res));

//</es-section>
module.exports = router;
