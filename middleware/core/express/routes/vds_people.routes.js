/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:08 GMT-0400 (GMT-04:00)
 * Time: 0:54:8
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:08 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:8
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsPeopleCtrl = require("../controllers/vds_people.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-people/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsPeopleCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByCreatedby/:createdbyid/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByCreatedby(req, res));

router.get(`/api-${sys}/vds-people/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsPeopleCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByUpdatedby/:updatedbyid/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByUpdatedby(req, res));

router.get(`/api-${sys}/vds-people/findVdsPeoplePerParentWithPerFirstName`, (req, res) => vdsPeopleCtrl.findVdsPeoplePerParentWithPerFirstName(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByPerParent/:perParentId/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByPerParent(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParTypeDocWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParTypeDocWithParOrder(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByPerParTypeDoc/:perParTypeDocId/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByPerParTypeDoc(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParCityWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParCityWithParOrder(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByPerParCity/:perParCityId/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByPerParCity(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParSexWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParSexWithParOrder(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByPerParSex/:perParSexId/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByPerParSex(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParCountryWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParCountryWithParOrder(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByPerParCountry/:perParCountryId/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByPerParCountry(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParNacionalityWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParNacionalityWithParOrder(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByPerParNacionality/:perParNacionalityId/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByPerParNacionality(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParStatusWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-people/filterVdsPeopleByPerParStatus/:perParStatusId/`, (req, res) => vdsPeopleCtrl.filterVdsPeopleByPerParStatus(req, res));



router.get(`/api-${sys}/vds-people/findOneByUid/:Id`, (req, res) => vdsPeopleCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-people/findOneById/:id`, (req, res) => vdsPeopleCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerFirstName/:perFirstName`, (req, res) => vdsPeopleCtrl.findOneByPerFirstName(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerSecondName/:perSecondName`, (req, res) => vdsPeopleCtrl.findOneByPerSecondName(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerFirstLastname/:perFirstLastname`, (req, res) => vdsPeopleCtrl.findOneByPerFirstLastname(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerSecondLastname/:perSecondLastname`, (req, res) => vdsPeopleCtrl.findOneByPerSecondLastname(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerLicense/:perLicense`, (req, res) => vdsPeopleCtrl.findOneByPerLicense(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerLicenseComp/:perLicenseComp`, (req, res) => vdsPeopleCtrl.findOneByPerLicenseComp(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerHomeAddress/:perHomeAddress`, (req, res) => vdsPeopleCtrl.findOneByPerHomeAddress(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerMail/:perMail`, (req, res) => vdsPeopleCtrl.findOneByPerMail(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerHomePhone/:perHomePhone`, (req, res) => vdsPeopleCtrl.findOneByPerHomePhone(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerCellphone/:perCellphone`, (req, res) => vdsPeopleCtrl.findOneByPerCellphone(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerGroup/:perGroup`, (req, res) => vdsPeopleCtrl.findOneByPerGroup(req, res));

router.get(`/api-${sys}/vds-people/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsPeopleCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-people/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsPeopleCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerParentId/:perParentId`, (req, res) => vdsPeopleCtrl.findOneByPerParentId(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerParTypeDocId/:perParTypeDocId`, (req, res) => vdsPeopleCtrl.findOneByPerParTypeDocId(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerParCityId/:perParCityId`, (req, res) => vdsPeopleCtrl.findOneByPerParCityId(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerParSexId/:perParSexId`, (req, res) => vdsPeopleCtrl.findOneByPerParSexId(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerParCountryId/:perParCountryId`, (req, res) => vdsPeopleCtrl.findOneByPerParCountryId(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerParNacionalityId/:perParNacionalityId`, (req, res) => vdsPeopleCtrl.findOneByPerParNacionalityId(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerParStatusId/:perParStatusId`, (req, res) => vdsPeopleCtrl.findOneByPerParStatusId(req, res));

router.get(`/api-${sys}/vds-people/findOneByPerBirthday/:perBirthday`, (req, res) => vdsPeopleCtrl.findOneByPerBirthday(req, res));

router.get(`/api-${sys}/vds-people/findOneByDueat/:dueat`, (req, res) => vdsPeopleCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-people/findOneByCreatedat/:createdat`, (req, res) => vdsPeopleCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-people/findOneByUpdatedat/:updatedat`, (req, res) => vdsPeopleCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-people/updateVdPersonByUid`, (req, res) => vdsPeopleCtrl.updateVdPersonByUid(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonById`, (req, res) => vdsPeopleCtrl.updateVdPersonById(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerFirstName`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerFirstName(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerSecondName`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerSecondName(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerFirstLastname`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerFirstLastname(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerSecondLastname`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerSecondLastname(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerLicense`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerLicense(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerLicenseComp`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerLicenseComp(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerHomeAddress`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerHomeAddress(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerMail`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerMail(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerHomePhone`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerHomePhone(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerCellphone`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerCellphone(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerGroup`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerGroup(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByCreatedbyid`, (req, res) => vdsPeopleCtrl.updateVdPersonByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByUpdatedbyid`, (req, res) => vdsPeopleCtrl.updateVdPersonByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerParentId`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerParentId(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerParTypeDocId`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerParTypeDocId(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerParCityId`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerParCityId(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerParSexId`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerParSexId(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerParCountryId`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerParCountryId(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerParNacionalityId`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerParNacionalityId(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerParStatusId`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerParStatusId(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByPerBirthday`, (req, res) => vdsPeopleCtrl.updateVdPersonByPerBirthday(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByDueat`, (req, res) => vdsPeopleCtrl.updateVdPersonByDueat(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByCreatedat`, (req, res) => vdsPeopleCtrl.updateVdPersonByCreatedat(req, res));

router.post(`/api-${sys}/vds-people/updateVdPersonByUpdatedat`, (req, res) => vdsPeopleCtrl.updateVdPersonByUpdatedat(req, res));



router.get(`/api-${sys}/vds-people/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsPeopleCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-people/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsPeopleCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-people/findVdsPeoplePerParentWithPerFirstName`, (req, res) => vdsPeopleCtrl.findVdsPeoplePerParentWithPerFirstName(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParTypeDocWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParTypeDocWithParOrder(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParCityWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParCityWithParOrder(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParSexWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParSexWithParOrder(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParCountryWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParCountryWithParOrder(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParNacionalityWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParNacionalityWithParOrder(req, res));

router.get(`/api-${sys}/vds-people/findVdsParamsPerParStatusWithParOrder`, (req, res) => vdsPeopleCtrl.findVdsParamsPerParStatusWithParOrder(req, res));



router.get(`/api-${sys}/vds-people/`, (req, res) => vdsPeopleCtrl.getAllVdsPeople(req, res));
router.post(`/api-${sys}/vds-people/datatable/`, (req, res) => vdsPeopleCtrl.getAllVdsPeople(req, res));
router.post(`/api-${sys}/vds-people/`, (req, res) => vdsPeopleCtrl.addVdPerson(req, res));
router.get(`/api-${sys}/vds-people/:Id`, (req, res) => vdsPeopleCtrl.getAVdPerson(req, res));
router.put(`/api-${sys}/vds-people/:Id`, (req, res) => vdsPeopleCtrl.updateVdPerson(req, res));
router.delete(`/api-${sys}/vds-people/:Id`, (req, res) => vdsPeopleCtrl.deleteVdPerson(req, res));

//</es-section>
module.exports = router;
