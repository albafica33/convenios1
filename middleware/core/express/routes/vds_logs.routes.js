/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Time: 0:54:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:1
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsLogsCtrl = require("../controllers/vds_logs.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-logs/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsLogsCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-logs/filterVdsLogsByCreatedby/:createdbyid/`, (req, res) => vdsLogsCtrl.filterVdsLogsByCreatedby(req, res));

router.get(`/api-${sys}/vds-logs/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsLogsCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-logs/filterVdsLogsByUpdatedby/:updatedbyid/`, (req, res) => vdsLogsCtrl.filterVdsLogsByUpdatedby(req, res));

router.get(`/api-${sys}/vds-logs/findVdsParamsLogParStatusWithParOrder`, (req, res) => vdsLogsCtrl.findVdsParamsLogParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-logs/filterVdsLogsByLogParStatus/:logParStatusId/`, (req, res) => vdsLogsCtrl.filterVdsLogsByLogParStatus(req, res));



router.get(`/api-${sys}/vds-logs/findOneByUid/:Id`, (req, res) => vdsLogsCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-logs/findOneById/:id`, (req, res) => vdsLogsCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-logs/findOneByLogObjId/:logObjId`, (req, res) => vdsLogsCtrl.findOneByLogObjId(req, res));

router.get(`/api-${sys}/vds-logs/findOneByLogDescription/:logDescription`, (req, res) => vdsLogsCtrl.findOneByLogDescription(req, res));

router.get(`/api-${sys}/vds-logs/findOneByLogGroup/:logGroup`, (req, res) => vdsLogsCtrl.findOneByLogGroup(req, res));

router.get(`/api-${sys}/vds-logs/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsLogsCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-logs/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsLogsCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-logs/findOneByLogParStatusId/:logParStatusId`, (req, res) => vdsLogsCtrl.findOneByLogParStatusId(req, res));

router.get(`/api-${sys}/vds-logs/findOneByDueat/:dueat`, (req, res) => vdsLogsCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-logs/findOneByCreatedat/:createdat`, (req, res) => vdsLogsCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-logs/findOneByUpdatedat/:updatedat`, (req, res) => vdsLogsCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-logs/updateVdLogByUid`, (req, res) => vdsLogsCtrl.updateVdLogByUid(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogById`, (req, res) => vdsLogsCtrl.updateVdLogById(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByLogObjId`, (req, res) => vdsLogsCtrl.updateVdLogByLogObjId(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByLogDescription`, (req, res) => vdsLogsCtrl.updateVdLogByLogDescription(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByLogGroup`, (req, res) => vdsLogsCtrl.updateVdLogByLogGroup(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByCreatedbyid`, (req, res) => vdsLogsCtrl.updateVdLogByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByUpdatedbyid`, (req, res) => vdsLogsCtrl.updateVdLogByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByLogParStatusId`, (req, res) => vdsLogsCtrl.updateVdLogByLogParStatusId(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByDueat`, (req, res) => vdsLogsCtrl.updateVdLogByDueat(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByCreatedat`, (req, res) => vdsLogsCtrl.updateVdLogByCreatedat(req, res));

router.post(`/api-${sys}/vds-logs/updateVdLogByUpdatedat`, (req, res) => vdsLogsCtrl.updateVdLogByUpdatedat(req, res));



router.get(`/api-${sys}/vds-logs/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsLogsCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-logs/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsLogsCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-logs/findVdsParamsLogParStatusWithParOrder`, (req, res) => vdsLogsCtrl.findVdsParamsLogParStatusWithParOrder(req, res));



router.get(`/api-${sys}/vds-logs/`, (req, res) => vdsLogsCtrl.getAllVdsLogs(req, res));
router.post(`/api-${sys}/vds-logs/datatable/`, (req, res) => vdsLogsCtrl.getAllVdsLogs(req, res));
router.post(`/api-${sys}/vds-logs/`, (req, res) => vdsLogsCtrl.addVdLog(req, res));
router.get(`/api-${sys}/vds-logs/:Id`, (req, res) => vdsLogsCtrl.getAVdLog(req, res));
router.put(`/api-${sys}/vds-logs/:Id`, (req, res) => vdsLogsCtrl.updateVdLog(req, res));
router.delete(`/api-${sys}/vds-logs/:Id`, (req, res) => vdsLogsCtrl.deleteVdLog(req, res));

//</es-section>
module.exports = router;
