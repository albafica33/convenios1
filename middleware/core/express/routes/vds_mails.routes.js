/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:02 GMT-0400 (GMT-04:00)
 * Time: 0:54:2
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:02 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:2
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsMailsCtrl = require("../controllers/vds_mails.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-mails/findVdsParamsMaiParStatusWithParOrder`, (req, res) => vdsMailsCtrl.findVdsParamsMaiParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-mails/filterVdsMailsByMaiParStatus/:maiParStatusId/`, (req, res) => vdsMailsCtrl.filterVdsMailsByMaiParStatus(req, res));



router.get(`/api-${sys}/vds-mails/findOneByUid/:Id`, (req, res) => vdsMailsCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-mails/findOneById/:id`, (req, res) => vdsMailsCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiPort/:maiPort`, (req, res) => vdsMailsCtrl.findOneByMaiPort(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiDescription/:maiDescription`, (req, res) => vdsMailsCtrl.findOneByMaiDescription(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiUserAccount/:maiUserAccount`, (req, res) => vdsMailsCtrl.findOneByMaiUserAccount(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiUserPassword/:maiUserPassword`, (req, res) => vdsMailsCtrl.findOneByMaiUserPassword(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiHost/:maiHost`, (req, res) => vdsMailsCtrl.findOneByMaiHost(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiProtocol/:maiProtocol`, (req, res) => vdsMailsCtrl.findOneByMaiProtocol(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiBusId/:maiBusId`, (req, res) => vdsMailsCtrl.findOneByMaiBusId(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiParStatusId/:maiParStatusId`, (req, res) => vdsMailsCtrl.findOneByMaiParStatusId(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiGroup/:maiGroup`, (req, res) => vdsMailsCtrl.findOneByMaiGroup(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiSubject/:maiSubject`, (req, res) => vdsMailsCtrl.findOneByMaiSubject(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiTo/:maiTo`, (req, res) => vdsMailsCtrl.findOneByMaiTo(req, res));

router.get(`/api-${sys}/vds-mails/findOneByUpdatedby/:updatedby`, (req, res) => vdsMailsCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-mails/findOneByCreatedby/:createdby`, (req, res) => vdsMailsCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiBcc/:maiBcc`, (req, res) => vdsMailsCtrl.findOneByMaiBcc(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiCc/:maiCc`, (req, res) => vdsMailsCtrl.findOneByMaiCc(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiText/:maiText`, (req, res) => vdsMailsCtrl.findOneByMaiText(req, res));

router.get(`/api-${sys}/vds-mails/findOneByMaiHtml/:maiHtml`, (req, res) => vdsMailsCtrl.findOneByMaiHtml(req, res));

router.get(`/api-${sys}/vds-mails/findOneByDueat/:dueat`, (req, res) => vdsMailsCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-mails/findOneByCreatedat/:createdat`, (req, res) => vdsMailsCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-mails/findOneByUpdatedat/:updatedat`, (req, res) => vdsMailsCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-mails/updateVdMailByUid`, (req, res) => vdsMailsCtrl.updateVdMailByUid(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailById`, (req, res) => vdsMailsCtrl.updateVdMailById(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiPort`, (req, res) => vdsMailsCtrl.updateVdMailByMaiPort(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiDescription`, (req, res) => vdsMailsCtrl.updateVdMailByMaiDescription(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiUserAccount`, (req, res) => vdsMailsCtrl.updateVdMailByMaiUserAccount(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiUserPassword`, (req, res) => vdsMailsCtrl.updateVdMailByMaiUserPassword(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiHost`, (req, res) => vdsMailsCtrl.updateVdMailByMaiHost(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiProtocol`, (req, res) => vdsMailsCtrl.updateVdMailByMaiProtocol(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiBusId`, (req, res) => vdsMailsCtrl.updateVdMailByMaiBusId(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiParStatusId`, (req, res) => vdsMailsCtrl.updateVdMailByMaiParStatusId(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiGroup`, (req, res) => vdsMailsCtrl.updateVdMailByMaiGroup(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiSubject`, (req, res) => vdsMailsCtrl.updateVdMailByMaiSubject(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiTo`, (req, res) => vdsMailsCtrl.updateVdMailByMaiTo(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByUpdatedby`, (req, res) => vdsMailsCtrl.updateVdMailByUpdatedby(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByCreatedby`, (req, res) => vdsMailsCtrl.updateVdMailByCreatedby(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiBcc`, (req, res) => vdsMailsCtrl.updateVdMailByMaiBcc(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiCc`, (req, res) => vdsMailsCtrl.updateVdMailByMaiCc(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiText`, (req, res) => vdsMailsCtrl.updateVdMailByMaiText(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByMaiHtml`, (req, res) => vdsMailsCtrl.updateVdMailByMaiHtml(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByDueat`, (req, res) => vdsMailsCtrl.updateVdMailByDueat(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByCreatedat`, (req, res) => vdsMailsCtrl.updateVdMailByCreatedat(req, res));

router.post(`/api-${sys}/vds-mails/updateVdMailByUpdatedat`, (req, res) => vdsMailsCtrl.updateVdMailByUpdatedat(req, res));



router.get(`/api-${sys}/vds-mails/findVdsParamsMaiParStatusWithParOrder`, (req, res) => vdsMailsCtrl.findVdsParamsMaiParStatusWithParOrder(req, res));



router.get(`/api-${sys}/vds-mails/`, (req, res) => vdsMailsCtrl.getAllVdsMails(req, res));
router.post(`/api-${sys}/vds-mails/datatable/`, (req, res) => vdsMailsCtrl.getAllVdsMails(req, res));
router.post(`/api-${sys}/vds-mails/`, (req, res) => vdsMailsCtrl.addVdMail(req, res));
router.get(`/api-${sys}/vds-mails/:Id`, (req, res) => vdsMailsCtrl.getAVdMail(req, res));
router.put(`/api-${sys}/vds-mails/:Id`, (req, res) => vdsMailsCtrl.updateVdMail(req, res));
router.delete(`/api-${sys}/vds-mails/:Id`, (req, res) => vdsMailsCtrl.deleteVdMail(req, res));

//</es-section>
module.exports = router;
