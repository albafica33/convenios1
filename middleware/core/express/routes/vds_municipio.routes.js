/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Time: 0:54:5
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:5
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsMunicipioCtrl = require("../controllers/vds_municipio.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-municipio/findVdsCiudadCiudadWithEstado`, (req, res) => vdsMunicipioCtrl.findVdsCiudadCiudadWithEstado(req, res));
router.get(`/api-${sys}/vds-municipio/filterVdsMunicipioByCiudad/:idCiudad/`, (req, res) => vdsMunicipioCtrl.filterVdsMunicipioByCiudad(req, res));



router.get(`/api-${sys}/vds-municipio/findOneByUid/:Id`, (req, res) => vdsMunicipioCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-municipio/findOneById/:id`, (req, res) => vdsMunicipioCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-municipio/findOneByEstado/:estado`, (req, res) => vdsMunicipioCtrl.findOneByEstado(req, res));

router.get(`/api-${sys}/vds-municipio/findOneByMunicipio/:municipio`, (req, res) => vdsMunicipioCtrl.findOneByMunicipio(req, res));

router.get(`/api-${sys}/vds-municipio/findOneByCreatedby/:createdby`, (req, res) => vdsMunicipioCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-municipio/findOneByUpdatedby/:updatedby`, (req, res) => vdsMunicipioCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-municipio/findOneByIdCiudad/:idCiudad`, (req, res) => vdsMunicipioCtrl.findOneByIdCiudad(req, res));

router.get(`/api-${sys}/vds-municipio/findOneByDueat/:dueat`, (req, res) => vdsMunicipioCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-municipio/findOneByCreatedat/:createdat`, (req, res) => vdsMunicipioCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-municipio/findOneByUpdatedat/:updatedat`, (req, res) => vdsMunicipioCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByUid`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByUid(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioById`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioById(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByEstado`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByEstado(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByMunicipio`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByMunicipio(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByCreatedby`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByCreatedby(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByUpdatedby`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByUpdatedby(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByIdCiudad`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByIdCiudad(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByDueat`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByDueat(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByCreatedat`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByCreatedat(req, res));

router.post(`/api-${sys}/vds-municipio/updateVdMunicipioByUpdatedat`, (req, res) => vdsMunicipioCtrl.updateVdMunicipioByUpdatedat(req, res));



router.get(`/api-${sys}/vds-municipio/findVdsCiudadCiudadWithEstado`, (req, res) => vdsMunicipioCtrl.findVdsCiudadCiudadWithEstado(req, res));



router.get(`/api-${sys}/vds-municipio/`, (req, res) => vdsMunicipioCtrl.getAllVdsMunicipio(req, res));
router.post(`/api-${sys}/vds-municipio/datatable/`, (req, res) => vdsMunicipioCtrl.getAllVdsMunicipio(req, res));
router.post(`/api-${sys}/vds-municipio/`, (req, res) => vdsMunicipioCtrl.addVdMunicipio(req, res));
router.get(`/api-${sys}/vds-municipio/:Id`, (req, res) => vdsMunicipioCtrl.getAVdMunicipio(req, res));
router.put(`/api-${sys}/vds-municipio/:Id`, (req, res) => vdsMunicipioCtrl.updateVdMunicipio(req, res));
router.delete(`/api-${sys}/vds-municipio/:Id`, (req, res) => vdsMunicipioCtrl.deleteVdMunicipio(req, res));

//</es-section>
module.exports = router;
