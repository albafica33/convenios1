/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:04 GMT-0400 (GMT-04:00)
 * Time: 0:54:4
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:04 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:4
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsModulesCtrl = require("../controllers/vds_modules.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-modules/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsModulesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-modules/filterVdsModulesByCreatedby/:createdbyid/`, (req, res) => vdsModulesCtrl.filterVdsModulesByCreatedby(req, res));

router.get(`/api-${sys}/vds-modules/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsModulesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));
router.get(`/api-${sys}/vds-modules/filterVdsModulesByUpdatedby/:updatedbyid/`, (req, res) => vdsModulesCtrl.filterVdsModulesByUpdatedby(req, res));

router.get(`/api-${sys}/vds-modules/findVdsParamsModParStatusWithParOrder`, (req, res) => vdsModulesCtrl.findVdsParamsModParStatusWithParOrder(req, res));
router.get(`/api-${sys}/vds-modules/filterVdsModulesByModParStatus/:modParStatusId/`, (req, res) => vdsModulesCtrl.filterVdsModulesByModParStatus(req, res));



router.get(`/api-${sys}/vds-modules/findOneByUid/:Id`, (req, res) => vdsModulesCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-modules/findOneById/:id`, (req, res) => vdsModulesCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-modules/findOneByModCode/:modCode`, (req, res) => vdsModulesCtrl.findOneByModCode(req, res));

router.get(`/api-${sys}/vds-modules/findOneByModDescription/:modDescription`, (req, res) => vdsModulesCtrl.findOneByModDescription(req, res));

router.get(`/api-${sys}/vds-modules/findOneByModAbbr/:modAbbr`, (req, res) => vdsModulesCtrl.findOneByModAbbr(req, res));

router.get(`/api-${sys}/vds-modules/findOneByModIcon/:modIcon`, (req, res) => vdsModulesCtrl.findOneByModIcon(req, res));

router.get(`/api-${sys}/vds-modules/findOneByModGroup/:modGroup`, (req, res) => vdsModulesCtrl.findOneByModGroup(req, res));

router.get(`/api-${sys}/vds-modules/findOneByCreatedbyid/:createdbyid`, (req, res) => vdsModulesCtrl.findOneByCreatedbyid(req, res));

router.get(`/api-${sys}/vds-modules/findOneByUpdatedbyid/:updatedbyid`, (req, res) => vdsModulesCtrl.findOneByUpdatedbyid(req, res));

router.get(`/api-${sys}/vds-modules/findOneByModParentId/:modParentId`, (req, res) => vdsModulesCtrl.findOneByModParentId(req, res));

router.get(`/api-${sys}/vds-modules/findOneByModParStatusId/:modParStatusId`, (req, res) => vdsModulesCtrl.findOneByModParStatusId(req, res));

router.get(`/api-${sys}/vds-modules/findOneByDueat/:dueat`, (req, res) => vdsModulesCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-modules/findOneByCreatedat/:createdat`, (req, res) => vdsModulesCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-modules/findOneByUpdatedat/:updatedat`, (req, res) => vdsModulesCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-modules/updateVdModuleByUid`, (req, res) => vdsModulesCtrl.updateVdModuleByUid(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleById`, (req, res) => vdsModulesCtrl.updateVdModuleById(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByModCode`, (req, res) => vdsModulesCtrl.updateVdModuleByModCode(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByModDescription`, (req, res) => vdsModulesCtrl.updateVdModuleByModDescription(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByModAbbr`, (req, res) => vdsModulesCtrl.updateVdModuleByModAbbr(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByModIcon`, (req, res) => vdsModulesCtrl.updateVdModuleByModIcon(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByModGroup`, (req, res) => vdsModulesCtrl.updateVdModuleByModGroup(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByCreatedbyid`, (req, res) => vdsModulesCtrl.updateVdModuleByCreatedbyid(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByUpdatedbyid`, (req, res) => vdsModulesCtrl.updateVdModuleByUpdatedbyid(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByModParentId`, (req, res) => vdsModulesCtrl.updateVdModuleByModParentId(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByModParStatusId`, (req, res) => vdsModulesCtrl.updateVdModuleByModParStatusId(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByDueat`, (req, res) => vdsModulesCtrl.updateVdModuleByDueat(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByCreatedat`, (req, res) => vdsModulesCtrl.updateVdModuleByCreatedat(req, res));

router.post(`/api-${sys}/vds-modules/updateVdModuleByUpdatedat`, (req, res) => vdsModulesCtrl.updateVdModuleByUpdatedat(req, res));



router.get(`/api-${sys}/vds-modules/findVdsUserRolesCreatedbyWithUsrRolGroup`, (req, res) => vdsModulesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-modules/findVdsUserRolesUpdatedbyWithUsrRolGroup`, (req, res) => vdsModulesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup(req, res));

router.get(`/api-${sys}/vds-modules/findVdsParamsModParStatusWithParOrder`, (req, res) => vdsModulesCtrl.findVdsParamsModParStatusWithParOrder(req, res));



router.get(`/api-${sys}/vds-modules/`, (req, res) => vdsModulesCtrl.getAllVdsModules(req, res));
router.post(`/api-${sys}/vds-modules/datatable/`, (req, res) => vdsModulesCtrl.getAllVdsModules(req, res));
router.post(`/api-${sys}/vds-modules/`, (req, res) => vdsModulesCtrl.addVdModule(req, res));
router.get(`/api-${sys}/vds-modules/:Id`, (req, res) => vdsModulesCtrl.getAVdModule(req, res));
router.put(`/api-${sys}/vds-modules/:Id`, (req, res) => vdsModulesCtrl.updateVdModule(req, res));
router.delete(`/api-${sys}/vds-modules/:Id`, (req, res) => vdsModulesCtrl.deleteVdModule(req, res));

//</es-section>
module.exports = router;
