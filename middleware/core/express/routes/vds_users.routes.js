/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Time: 0:54:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:13
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsUsersCtrl = require("../controllers/vds_users.controller");
//</es-section>
//<es-section>

router.get(`/api-${sys}/vds-users/findVdsPersonaPersonaWithEstado`, (req, res) => vdsUsersCtrl.findVdsPersonaPersonaWithEstado(req, res));
router.get(`/api-${sys}/vds-users/filterVdsUsersByPersona/:idPersona/`, (req, res) => vdsUsersCtrl.filterVdsUsersByPersona(req, res));



router.get(`/api-${sys}/vds-users/findOneByUid/:Id`, (req, res) => vdsUsersCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-users/findOneById/:id`, (req, res) => vdsUsersCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-users/findOneByEstado/:estado`, (req, res) => vdsUsersCtrl.findOneByEstado(req, res));

router.get(`/api-${sys}/vds-users/findOneByUserName/:userName`, (req, res) => vdsUsersCtrl.findOneByUserName(req, res));

router.get(`/api-${sys}/vds-users/findOneByUserHash/:userHash`, (req, res) => vdsUsersCtrl.findOneByUserHash(req, res));

router.get(`/api-${sys}/vds-users/findOneByCorreo/:correo`, (req, res) => vdsUsersCtrl.findOneByCorreo(req, res));

router.get(`/api-${sys}/vds-users/findOneByRecordatorio/:recordatorio`, (req, res) => vdsUsersCtrl.findOneByRecordatorio(req, res));

router.get(`/api-${sys}/vds-users/findOneByCreatedby/:createdby`, (req, res) => vdsUsersCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-users/findOneByUpdatedby/:updatedby`, (req, res) => vdsUsersCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-users/findOneByIdPersona/:idPersona`, (req, res) => vdsUsersCtrl.findOneByIdPersona(req, res));

router.get(`/api-${sys}/vds-users/findOneByDueat/:dueat`, (req, res) => vdsUsersCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-users/findOneByCreatedat/:createdat`, (req, res) => vdsUsersCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-users/findOneByUpdatedat/:updatedat`, (req, res) => vdsUsersCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-users/updateVdUserByUid`, (req, res) => vdsUsersCtrl.updateVdUserByUid(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserById`, (req, res) => vdsUsersCtrl.updateVdUserById(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByEstado`, (req, res) => vdsUsersCtrl.updateVdUserByEstado(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByUserName`, (req, res) => vdsUsersCtrl.updateVdUserByUserName(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByUserHash`, (req, res) => vdsUsersCtrl.updateVdUserByUserHash(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByCorreo`, (req, res) => vdsUsersCtrl.updateVdUserByCorreo(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByRecordatorio`, (req, res) => vdsUsersCtrl.updateVdUserByRecordatorio(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByCreatedby`, (req, res) => vdsUsersCtrl.updateVdUserByCreatedby(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByUpdatedby`, (req, res) => vdsUsersCtrl.updateVdUserByUpdatedby(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByIdPersona`, (req, res) => vdsUsersCtrl.updateVdUserByIdPersona(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByDueat`, (req, res) => vdsUsersCtrl.updateVdUserByDueat(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByCreatedat`, (req, res) => vdsUsersCtrl.updateVdUserByCreatedat(req, res));

router.post(`/api-${sys}/vds-users/updateVdUserByUpdatedat`, (req, res) => vdsUsersCtrl.updateVdUserByUpdatedat(req, res));



router.get(`/api-${sys}/vds-users/findVdsPersonaPersonaWithEstado`, (req, res) => vdsUsersCtrl.findVdsPersonaPersonaWithEstado(req, res));



router.get(`/api-${sys}/vds-users/`, (req, res) => vdsUsersCtrl.getAllVdsUsers(req, res));
router.post(`/api-${sys}/vds-users/datatable/`, (req, res) => vdsUsersCtrl.getAllVdsUsers(req, res));
router.post(`/api-${sys}/vds-users/`, (req, res) => vdsUsersCtrl.addVdUser(req, res));
router.get(`/api-${sys}/vds-users/:Id`, (req, res) => vdsUsersCtrl.getAVdUser(req, res));
router.put(`/api-${sys}/vds-users/:Id`, (req, res) => vdsUsersCtrl.updateVdUser(req, res));
router.delete(`/api-${sys}/vds-users/:Id`, (req, res) => vdsUsersCtrl.deleteVdUser(req, res));

//</es-section>
module.exports = router;
