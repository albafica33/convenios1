/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Time: 0:54:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:13
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
const router = express.Router();
//const authenticateToken = require("../../../config/token");

//<es-section>
const vdsSexoCtrl = require("../controllers/vds_sexo.controller");
//</es-section>
//<es-section>



router.get(`/api-${sys}/vds-sexo/findOneByUid/:Id`, (req, res) => vdsSexoCtrl.findOneByUid(req, res));

router.get(`/api-${sys}/vds-sexo/findOneById/:id`, (req, res) => vdsSexoCtrl.findOneById(req, res));

router.get(`/api-${sys}/vds-sexo/findOneByEstado/:estado`, (req, res) => vdsSexoCtrl.findOneByEstado(req, res));

router.get(`/api-${sys}/vds-sexo/findOneByDescripcion/:descripcion`, (req, res) => vdsSexoCtrl.findOneByDescripcion(req, res));

router.get(`/api-${sys}/vds-sexo/findOneByCreatedby/:createdby`, (req, res) => vdsSexoCtrl.findOneByCreatedby(req, res));

router.get(`/api-${sys}/vds-sexo/findOneByUpdatedby/:updatedby`, (req, res) => vdsSexoCtrl.findOneByUpdatedby(req, res));

router.get(`/api-${sys}/vds-sexo/findOneByDueat/:dueat`, (req, res) => vdsSexoCtrl.findOneByDueat(req, res));

router.get(`/api-${sys}/vds-sexo/findOneByCreatedat/:createdat`, (req, res) => vdsSexoCtrl.findOneByCreatedat(req, res));

router.get(`/api-${sys}/vds-sexo/findOneByUpdatedat/:updatedat`, (req, res) => vdsSexoCtrl.findOneByUpdatedat(req, res));



router.post(`/api-${sys}/vds-sexo/updateVdSexoByUid`, (req, res) => vdsSexoCtrl.updateVdSexoByUid(req, res));

router.post(`/api-${sys}/vds-sexo/updateVdSexoById`, (req, res) => vdsSexoCtrl.updateVdSexoById(req, res));

router.post(`/api-${sys}/vds-sexo/updateVdSexoByEstado`, (req, res) => vdsSexoCtrl.updateVdSexoByEstado(req, res));

router.post(`/api-${sys}/vds-sexo/updateVdSexoByDescripcion`, (req, res) => vdsSexoCtrl.updateVdSexoByDescripcion(req, res));

router.post(`/api-${sys}/vds-sexo/updateVdSexoByCreatedby`, (req, res) => vdsSexoCtrl.updateVdSexoByCreatedby(req, res));

router.post(`/api-${sys}/vds-sexo/updateVdSexoByUpdatedby`, (req, res) => vdsSexoCtrl.updateVdSexoByUpdatedby(req, res));

router.post(`/api-${sys}/vds-sexo/updateVdSexoByDueat`, (req, res) => vdsSexoCtrl.updateVdSexoByDueat(req, res));

router.post(`/api-${sys}/vds-sexo/updateVdSexoByCreatedat`, (req, res) => vdsSexoCtrl.updateVdSexoByCreatedat(req, res));

router.post(`/api-${sys}/vds-sexo/updateVdSexoByUpdatedat`, (req, res) => vdsSexoCtrl.updateVdSexoByUpdatedat(req, res));





router.get(`/api-${sys}/vds-sexo/`, (req, res) => vdsSexoCtrl.getAllVdsSexo(req, res));
router.post(`/api-${sys}/vds-sexo/datatable/`, (req, res) => vdsSexoCtrl.getAllVdsSexo(req, res));
router.post(`/api-${sys}/vds-sexo/`, (req, res) => vdsSexoCtrl.addVdSexo(req, res));
router.get(`/api-${sys}/vds-sexo/:Id`, (req, res) => vdsSexoCtrl.getAVdSexo(req, res));
router.put(`/api-${sys}/vds-sexo/:Id`, (req, res) => vdsSexoCtrl.updateVdSexo(req, res));
router.delete(`/api-${sys}/vds-sexo/:Id`, (req, res) => vdsSexoCtrl.deleteVdSexo(req, res));

//</es-section>
module.exports = router;
