/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:59 GMT-0400 (GMT-04:00)
 * Time: 0:53:59
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:59 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:59
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdCronService = require('../services/vds_crons.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsCronsCtrl = {};
vdsCronsCtrl.service = vdCronService;


vdsCronsCtrl.getAllVdsCrons = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsCrons.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsCrons.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsCrons] = await vdCronService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsCrons.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsCrons = await vdCronService.getAllVdsCrons(req.query);
        objVdsCrons = util.setRoot(objVdsCrons,req.query.root);
        if (objVdsCrons && objVdsCrons.rows && objVdsCrons.count) {
            util.setSuccess(200, 'VdsCrons retrieved', objVdsCrons.rows, objVdsCrons.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdCron found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.getAVdCron = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdCron = await vdCronService.getAVdCron(Id, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.addVdCron = async (req, res) => {
    try {
        const objVdCron = await vdCronService.addVdCron(req.body);
        util.setSuccess(201, 'VdCron Added!', objVdCron);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.updateVdCron = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdCron = await vdCronService.updateVdCron(Id, req.body);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdCron updated', objVdCron);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsCronsCtrl.deleteVdCron = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdCron = await vdCronService.deleteVdCron(Id);
        if (objVdCron) {
            util.setSuccess(200, 'VdCron deleted', objVdCron);
        } else {
            util.setError(404, `VdCron with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsCronsCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdCron = await vdCronService.findOneByUid(Id, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdCron = await vdCronService.findOneById(id, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByCroStatus = async (req, res) => {
    try {
        const { croStatus } = req.params;
        const objVdCron = await vdCronService.findOneByCroStatus(croStatus, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByCroDescription = async (req, res) => {
    try {
        const { croDescription } = req.params;
        const objVdCron = await vdCronService.findOneByCroDescription(croDescription, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByCroExpression = async (req, res) => {
    try {
        const { croExpression } = req.params;
        const objVdCron = await vdCronService.findOneByCroExpression(croExpression, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByCroGroup = async (req, res) => {
    try {
        const { croGroup } = req.params;
        const objVdCron = await vdCronService.findOneByCroGroup(croGroup, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByCroMaiId = async (req, res) => {
    try {
        const { croMaiId } = req.params;
        const objVdCron = await vdCronService.findOneByCroMaiId(croMaiId, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdCron = await vdCronService.findOneByCreatedby(createdby, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdCron = await vdCronService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByCroFunction = async (req, res) => {
    try {
        const { croFunction } = req.params;
        const objVdCron = await vdCronService.findOneByCroFunction(croFunction, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdCron = await vdCronService.findOneByDueat(dueat, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdCron = await vdCronService.findOneByCreatedat(createdat, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCronsCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdCron = await vdCronService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdCron) {
            util.setError(404, `Cannot find vdCron with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCron', objVdCron);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsCronsCtrl.updateVdCronByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByUid(Id, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronById(id, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByCroStatus = async (req, res) => {
     const { croStatus } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByCroStatus(croStatus, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByCroDescription = async (req, res) => {
     const { croDescription } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByCroDescription(croDescription, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByCroExpression = async (req, res) => {
     const { croExpression } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByCroExpression(croExpression, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByCroGroup = async (req, res) => {
     const { croGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByCroGroup(croGroup, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByCroMaiId = async (req, res) => {
     const { croMaiId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByCroMaiId(croMaiId, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByCreatedby(createdby, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByUpdatedby(updatedby, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByCroFunction = async (req, res) => {
     const { croFunction } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByCroFunction(croFunction, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByDueat(dueat, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByCreatedat(createdat, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCronsCtrl.updateVdCronByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCron = await vdCronService.updateVdCronByUpdatedat(updatedat, req.body);
            if (!objVdCron) {
                util.setError(404, `Cannot find vdCron with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCron updated', objVdCron);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}







//</es-section>

//<es-section>
module.exports = vdsCronsCtrl;
//</es-section>
