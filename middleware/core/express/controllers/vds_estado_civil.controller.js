/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:00 GMT-0400 (GMT-04:00)
 * Time: 0:54:0
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:00 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:0
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdEstadoCivilService = require('../services/vds_estado_civil.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsEstadoCivilCtrl = {};
vdsEstadoCivilCtrl.service = vdEstadoCivilService;


vdsEstadoCivilCtrl.getAllVdsEstadoCivil = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsEstadoCivil.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsEstadoCivil.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsEstadoCivil] = await vdEstadoCivilService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsEstadoCivil.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsEstadoCivil = await vdEstadoCivilService.getAllVdsEstadoCivil(req.query);
        objVdsEstadoCivil = util.setRoot(objVdsEstadoCivil,req.query.root);
        if (objVdsEstadoCivil && objVdsEstadoCivil.rows && objVdsEstadoCivil.count) {
            util.setSuccess(200, 'VdsEstadoCivil retrieved', objVdsEstadoCivil.rows, objVdsEstadoCivil.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdEstadoCivil found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.getAVdEstadoCivil = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdEstadoCivil = await vdEstadoCivilService.getAVdEstadoCivil(Id, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.addVdEstadoCivil = async (req, res) => {
    try {
        const objVdEstadoCivil = await vdEstadoCivilService.addVdEstadoCivil(req.body);
        util.setSuccess(201, 'VdEstadoCivil Added!', objVdEstadoCivil);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.updateVdEstadoCivil = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivil(Id, req.body);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsEstadoCivilCtrl.deleteVdEstadoCivil = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdEstadoCivil = await vdEstadoCivilService.deleteVdEstadoCivil(Id);
        if (objVdEstadoCivil) {
            util.setSuccess(200, 'VdEstadoCivil deleted', objVdEstadoCivil);
        } else {
            util.setError(404, `VdEstadoCivil with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsEstadoCivilCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneByUid(Id, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneById(id, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.findOneByEstado = async (req, res) => {
    try {
        const { estado } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneByEstado(estado, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.findOneByDescripcion = async (req, res) => {
    try {
        const { descripcion } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneByDescripcion(descripcion, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneByCreatedby(createdby, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneByDueat(dueat, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneByCreatedat(createdat, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsEstadoCivilCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdEstadoCivil = await vdEstadoCivilService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdEstadoCivil) {
            util.setError(404, `Cannot find vdEstadoCivil with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdEstadoCivil', objVdEstadoCivil);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsEstadoCivilCtrl.updateVdEstadoCivilByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilByUid(Id, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsEstadoCivilCtrl.updateVdEstadoCivilById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilById(id, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsEstadoCivilCtrl.updateVdEstadoCivilByEstado = async (req, res) => {
     const { estado } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilByEstado(estado, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsEstadoCivilCtrl.updateVdEstadoCivilByDescripcion = async (req, res) => {
     const { descripcion } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilByDescripcion(descripcion, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsEstadoCivilCtrl.updateVdEstadoCivilByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilByCreatedby(createdby, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsEstadoCivilCtrl.updateVdEstadoCivilByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilByUpdatedby(updatedby, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsEstadoCivilCtrl.updateVdEstadoCivilByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilByDueat(dueat, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsEstadoCivilCtrl.updateVdEstadoCivilByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilByCreatedat(createdat, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsEstadoCivilCtrl.updateVdEstadoCivilByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdEstadoCivil = await vdEstadoCivilService.updateVdEstadoCivilByUpdatedat(updatedat, req.body);
            if (!objVdEstadoCivil) {
                util.setError(404, `Cannot find vdEstadoCivil with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdEstadoCivil updated', objVdEstadoCivil);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}







//</es-section>

//<es-section>
module.exports = vdsEstadoCivilCtrl;
//</es-section>
