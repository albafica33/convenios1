/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Time: 0:54:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:01 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:1
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdLogService = require('../services/vds_logs.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsLogsCtrl = {};
vdsLogsCtrl.service = vdLogService;


vdsLogsCtrl.getAllVdsLogs = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsLogs.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsLogs.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsLogs] = await vdLogService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsLogs.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsLogs = await vdLogService.getAllVdsLogs(req.query);
        objVdsLogs = util.setRoot(objVdsLogs,req.query.root);
        if (objVdsLogs && objVdsLogs.rows && objVdsLogs.count) {
            util.setSuccess(200, 'VdsLogs retrieved', objVdsLogs.rows, objVdsLogs.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.getAVdLog = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdLog = await vdLogService.getAVdLog(Id, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.addVdLog = async (req, res) => {
    try {
        const objVdLog = await vdLogService.addVdLog(req.body);
        util.setSuccess(201, 'VdLog Added!', objVdLog);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.updateVdLog = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdLog = await vdLogService.updateVdLog(Id, req.body);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdLog updated', objVdLog);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsLogsCtrl.deleteVdLog = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdLog = await vdLogService.deleteVdLog(Id);
        if (objVdLog) {
            util.setSuccess(200, 'VdLog deleted', objVdLog);
        } else {
            util.setError(404, `VdLog with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsLogsCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdLog = await vdLogService.findOneByUid(Id, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdLog = await vdLogService.findOneById(id, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByLogObjId = async (req, res) => {
    try {
        const { logObjId } = req.params;
        const objVdLog = await vdLogService.findOneByLogObjId(logObjId, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByLogDescription = async (req, res) => {
    try {
        const { logDescription } = req.params;
        const objVdLog = await vdLogService.findOneByLogDescription(logDescription, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByLogGroup = async (req, res) => {
    try {
        const { logGroup } = req.params;
        const objVdLog = await vdLogService.findOneByLogGroup(logGroup, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdLog = await vdLogService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdLog = await vdLogService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByLogParStatusId = async (req, res) => {
    try {
        const { logParStatusId } = req.params;
        const objVdLog = await vdLogService.findOneByLogParStatusId(logParStatusId, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdLog = await vdLogService.findOneByDueat(dueat, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdLog = await vdLogService.findOneByCreatedat(createdat, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdLog = await vdLogService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdLog) {
            util.setError(404, `Cannot find vdLog with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdLog', objVdLog);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsLogsCtrl.updateVdLogByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByUid(Id, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogById(id, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByLogObjId = async (req, res) => {
     const { logObjId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByLogObjId(logObjId, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByLogDescription = async (req, res) => {
     const { logDescription } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByLogDescription(logDescription, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByLogGroup = async (req, res) => {
     const { logGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByLogGroup(logGroup, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByCreatedbyid(createdbyid, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByUpdatedbyid(updatedbyid, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByLogParStatusId = async (req, res) => {
     const { logParStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByLogParStatusId(logParStatusId, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByDueat(dueat, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByCreatedat(createdat, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsLogsCtrl.updateVdLogByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdLog = await vdLogService.updateVdLogByUpdatedat(updatedat, req.body);
            if (!objVdLog) {
                util.setError(404, `Cannot find vdLog with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdLog updated', objVdLog);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsLogsCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsLogs = await vdLogService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsLogs = await vdLogService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.findVdsParamsLogParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsLogs = await vdLogService.findVdsParamsLogParStatusWithParOrder(select, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(Id, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(id, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { logObjId } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(logObjId, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { logDescription } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(logDescription, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { logGroup } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(logGroup, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(createdbyid, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByUpdatedby(updatedbyid, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByLogParStatus = async (req, res) => {
    try {
        const { logParStatusId } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByLogParStatus(logParStatusId, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(dueat, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(createdat, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsLogsCtrl.filterVdsLogsByCreatedby = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsLogs = await vdLogService.filterVdsLogsByCreatedby(updatedat, req.query);
        if (vdsLogs.length > 0) {
            util.setSuccess(200, 'VdsLogs retrieved', vdsLogs);
        } else {
            util.setSuccess(200, 'No vdLog found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsLogsCtrl;
//</es-section>
