/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:12 GMT-0400 (GMT-04:00)
 * Time: 0:54:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:12 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:12
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdRoleService = require('../services/vds_roles.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsRolesCtrl = {};
vdsRolesCtrl.service = vdRoleService;


vdsRolesCtrl.getAllVdsRoles = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsRoles.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsRoles.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsRoles] = await vdRoleService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsRoles.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsRoles = await vdRoleService.getAllVdsRoles(req.query);
        objVdsRoles = util.setRoot(objVdsRoles,req.query.root);
        if (objVdsRoles && objVdsRoles.rows && objVdsRoles.count) {
            util.setSuccess(200, 'VdsRoles retrieved', objVdsRoles.rows, objVdsRoles.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.getAVdRole = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdRole = await vdRoleService.getAVdRole(Id, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.addVdRole = async (req, res) => {
    try {
        const objVdRole = await vdRoleService.addVdRole(req.body);
        util.setSuccess(201, 'VdRole Added!', objVdRole);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.updateVdRole = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdRole = await vdRoleService.updateVdRole(Id, req.body);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdRole updated', objVdRole);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsRolesCtrl.deleteVdRole = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdRole = await vdRoleService.deleteVdRole(Id);
        if (objVdRole) {
            util.setSuccess(200, 'VdRole deleted', objVdRole);
        } else {
            util.setError(404, `VdRole with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsRolesCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdRole = await vdRoleService.findOneByUid(Id, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdRole = await vdRoleService.findOneById(id, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByRolCode = async (req, res) => {
    try {
        const { rolCode } = req.params;
        const objVdRole = await vdRoleService.findOneByRolCode(rolCode, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByRolDescription = async (req, res) => {
    try {
        const { rolDescription } = req.params;
        const objVdRole = await vdRoleService.findOneByRolDescription(rolDescription, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByRolAbbr = async (req, res) => {
    try {
        const { rolAbbr } = req.params;
        const objVdRole = await vdRoleService.findOneByRolAbbr(rolAbbr, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByRolGroup = async (req, res) => {
    try {
        const { rolGroup } = req.params;
        const objVdRole = await vdRoleService.findOneByRolGroup(rolGroup, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdRole = await vdRoleService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdRole = await vdRoleService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByRolParStatusId = async (req, res) => {
    try {
        const { rolParStatusId } = req.params;
        const objVdRole = await vdRoleService.findOneByRolParStatusId(rolParStatusId, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdRole = await vdRoleService.findOneByDueat(dueat, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdRole = await vdRoleService.findOneByCreatedat(createdat, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdRole = await vdRoleService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdRole) {
            util.setError(404, `Cannot find vdRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRole', objVdRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsRolesCtrl.updateVdRoleByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByUid(Id, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleById(id, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByRolCode = async (req, res) => {
     const { rolCode } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByRolCode(rolCode, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByRolDescription = async (req, res) => {
     const { rolDescription } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByRolDescription(rolDescription, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByRolAbbr = async (req, res) => {
     const { rolAbbr } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByRolAbbr(rolAbbr, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByRolGroup = async (req, res) => {
     const { rolGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByRolGroup(rolGroup, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByCreatedbyid(createdbyid, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByUpdatedbyid(updatedbyid, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByRolParStatusId = async (req, res) => {
     const { rolParStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByRolParStatusId(rolParStatusId, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByDueat(dueat, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByCreatedat(createdat, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRolesCtrl.updateVdRoleByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRole = await vdRoleService.updateVdRoleByUpdatedat(updatedat, req.body);
            if (!objVdRole) {
                util.setError(404, `Cannot find vdRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRole updated', objVdRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsRolesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsRoles = await vdRoleService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsRoles = await vdRoleService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.findVdsParamsRolParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsRoles = await vdRoleService.findVdsParamsRolParStatusWithParOrder(select, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(Id, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(id, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { rolCode } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(rolCode, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { rolDescription } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(rolDescription, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { rolAbbr } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(rolAbbr, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { rolGroup } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(rolGroup, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(createdbyid, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByUpdatedby(updatedbyid, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByRolParStatus = async (req, res) => {
    try {
        const { rolParStatusId } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByRolParStatus(rolParStatusId, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(dueat, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(createdat, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRolesCtrl.filterVdsRolesByCreatedby = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsRoles = await vdRoleService.filterVdsRolesByCreatedby(updatedat, req.query);
        if (vdsRoles.length > 0) {
            util.setSuccess(200, 'VdsRoles retrieved', vdsRoles);
        } else {
            util.setSuccess(200, 'No vdRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsRolesCtrl;
//</es-section>
