/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Time: 0:54:5
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:5
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdMunicipioService = require('../services/vds_municipio.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsMunicipioCtrl = {};
vdsMunicipioCtrl.service = vdMunicipioService;


vdsMunicipioCtrl.getAllVdsMunicipio = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsMunicipio.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsMunicipio.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsMunicipio] = await vdMunicipioService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsMunicipio.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsMunicipio = await vdMunicipioService.getAllVdsMunicipio(req.query);
        objVdsMunicipio = util.setRoot(objVdsMunicipio,req.query.root);
        if (objVdsMunicipio && objVdsMunicipio.rows && objVdsMunicipio.count) {
            util.setSuccess(200, 'VdsMunicipio retrieved', objVdsMunicipio.rows, objVdsMunicipio.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.getAVdMunicipio = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdMunicipio = await vdMunicipioService.getAVdMunicipio(Id, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.addVdMunicipio = async (req, res) => {
    try {
        const objVdMunicipio = await vdMunicipioService.addVdMunicipio(req.body);
        util.setSuccess(201, 'VdMunicipio Added!', objVdMunicipio);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.updateVdMunicipio = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdMunicipio = await vdMunicipioService.updateVdMunicipio(Id, req.body);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsMunicipioCtrl.deleteVdMunicipio = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdMunicipio = await vdMunicipioService.deleteVdMunicipio(Id);
        if (objVdMunicipio) {
            util.setSuccess(200, 'VdMunicipio deleted', objVdMunicipio);
        } else {
            util.setError(404, `VdMunicipio with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsMunicipioCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByUid(Id, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneById(id, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneByEstado = async (req, res) => {
    try {
        const { estado } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByEstado(estado, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneByMunicipio = async (req, res) => {
    try {
        const { municipio } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByMunicipio(municipio, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByCreatedby(createdby, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneByIdCiudad = async (req, res) => {
    try {
        const { idCiudad } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByIdCiudad(idCiudad, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByDueat(dueat, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByCreatedat(createdat, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdMunicipio = await vdMunicipioService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdMunicipio) {
            util.setError(404, `Cannot find vdMunicipio with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMunicipio', objVdMunicipio);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsMunicipioCtrl.updateVdMunicipioByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByUid(Id, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioById(id, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioByEstado = async (req, res) => {
     const { estado } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByEstado(estado, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioByMunicipio = async (req, res) => {
     const { municipio } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByMunicipio(municipio, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByCreatedby(createdby, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByUpdatedby(updatedby, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioByIdCiudad = async (req, res) => {
     const { idCiudad } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByIdCiudad(idCiudad, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByDueat(dueat, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByCreatedat(createdat, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMunicipioCtrl.updateVdMunicipioByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMunicipio = await vdMunicipioService.updateVdMunicipioByUpdatedat(updatedat, req.body);
            if (!objVdMunicipio) {
                util.setError(404, `Cannot find vdMunicipio with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMunicipio updated', objVdMunicipio);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsMunicipioCtrl.findVdsCiudadCiudadWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsMunicipio = await vdMunicipioService.findVdsCiudadCiudadWithEstado(select, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(Id, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(id, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { estado } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(estado, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { municipio } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(municipio, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { createdby } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(createdby, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(updatedby, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { idCiudad } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(idCiudad, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(dueat, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(createdat, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMunicipioCtrl.filterVdsMunicipioByCiudad = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsMunicipio = await vdMunicipioService.filterVdsMunicipioByCiudad(updatedat, req.query);
        if (vdsMunicipio.length > 0) {
            util.setSuccess(200, 'VdsMunicipio retrieved', vdsMunicipio);
        } else {
            util.setSuccess(200, 'No vdMunicipio found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsMunicipioCtrl;
//</es-section>
