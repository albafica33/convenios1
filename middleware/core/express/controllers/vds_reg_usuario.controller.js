/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Time: 0:54:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:11
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdRegUsuarioService = require('../services/vds_reg_usuario.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsRegUsuarioCtrl = {};
vdsRegUsuarioCtrl.service = vdRegUsuarioService;


vdsRegUsuarioCtrl.getAllVdsRegUsuario = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsRegUsuario.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsRegUsuario.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsRegUsuario] = await vdRegUsuarioService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsRegUsuario.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsRegUsuario = await vdRegUsuarioService.getAllVdsRegUsuario(req.query);
        objVdsRegUsuario = util.setRoot(objVdsRegUsuario,req.query.root);
        if (objVdsRegUsuario && objVdsRegUsuario.rows && objVdsRegUsuario.count) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', objVdsRegUsuario.rows, objVdsRegUsuario.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.getAVdRegUsuario = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdRegUsuario = await vdRegUsuarioService.getAVdRegUsuario(Id, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.addVdRegUsuario = async (req, res) => {
    try {
        const objVdRegUsuario = await vdRegUsuarioService.addVdRegUsuario(req.body);
        util.setSuccess(201, 'VdRegUsuario Added!', objVdRegUsuario);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.updateVdRegUsuario = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuario(Id, req.body);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsRegUsuarioCtrl.deleteVdRegUsuario = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdRegUsuario = await vdRegUsuarioService.deleteVdRegUsuario(Id);
        if (objVdRegUsuario) {
            util.setSuccess(200, 'VdRegUsuario deleted', objVdRegUsuario);
        } else {
            util.setError(404, `VdRegUsuario with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsRegUsuarioCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneByUid(Id, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneById(id, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findOneByIdUsuario = async (req, res) => {
    try {
        const { idUsuario } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneByIdUsuario(idUsuario, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findOneByIdReg = async (req, res) => {
    try {
        const { idReg } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneByIdReg(idReg, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneByDueat(dueat, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneByCreatedat(createdat, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdRegUsuario = await vdRegUsuarioService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdRegUsuario) {
            util.setError(404, `Cannot find vdRegUsuario with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdRegUsuario', objVdRegUsuario);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsRegUsuarioCtrl.updateVdRegUsuarioByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioByUid(Id, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRegUsuarioCtrl.updateVdRegUsuarioById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioById(id, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRegUsuarioCtrl.updateVdRegUsuarioByIdUsuario = async (req, res) => {
     const { idUsuario } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioByIdUsuario(idUsuario, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRegUsuarioCtrl.updateVdRegUsuarioByIdReg = async (req, res) => {
     const { idReg } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioByIdReg(idReg, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRegUsuarioCtrl.updateVdRegUsuarioByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioByCreatedbyid(createdbyid, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRegUsuarioCtrl.updateVdRegUsuarioByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioByUpdatedbyid(updatedbyid, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRegUsuarioCtrl.updateVdRegUsuarioByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioByDueat(dueat, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRegUsuarioCtrl.updateVdRegUsuarioByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioByCreatedat(createdat, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsRegUsuarioCtrl.updateVdRegUsuarioByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdRegUsuario = await vdRegUsuarioService.updateVdRegUsuarioByUpdatedat(updatedat, req.body);
            if (!objVdRegUsuario) {
                util.setError(404, `Cannot find vdRegUsuario with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdRegUsuario updated', objVdRegUsuario);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsRegUsuarioCtrl.findVdsUsersUsuarioWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.findVdsUsersUsuarioWithEstado(select, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findVdsMatrizRegWithEstadoCumplimiento = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.findVdsMatrizRegWithEstadoCumplimiento(select, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsRegUsuarioCtrl.filterVdsRegUsuarioByUsuario = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByUsuario(Id, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.filterVdsRegUsuarioByUsuario = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByUsuario(id, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.filterVdsRegUsuarioByUsuario = async (req, res) => {
    try {
        const { idUsuario } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByUsuario(idUsuario, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.filterVdsRegUsuarioByReg = async (req, res) => {
    try {
        const { idReg } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByReg(idReg, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.filterVdsRegUsuarioByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByCreatedby(createdbyid, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.filterVdsRegUsuarioByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByUpdatedby(updatedbyid, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.filterVdsRegUsuarioByUsuario = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByUsuario(dueat, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.filterVdsRegUsuarioByUsuario = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByUsuario(createdat, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsRegUsuarioCtrl.filterVdsRegUsuarioByUsuario = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsRegUsuario = await vdRegUsuarioService.filterVdsRegUsuarioByUsuario(updatedat, req.query);
        if (vdsRegUsuario.length > 0) {
            util.setSuccess(200, 'VdsRegUsuario retrieved', vdsRegUsuario);
        } else {
            util.setSuccess(200, 'No vdRegUsuario found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsRegUsuarioCtrl;
//</es-section>
