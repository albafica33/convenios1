/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:02 GMT-0400 (GMT-04:00)
 * Time: 0:54:2
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:02 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:2
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdMailService = require('../services/vds_mails.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsMailsCtrl = {};
vdsMailsCtrl.service = vdMailService;


vdsMailsCtrl.getAllVdsMails = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsMails.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsMails.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsMails] = await vdMailService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsMails.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsMails = await vdMailService.getAllVdsMails(req.query);
        objVdsMails = util.setRoot(objVdsMails,req.query.root);
        if (objVdsMails && objVdsMails.rows && objVdsMails.count) {
            util.setSuccess(200, 'VdsMails retrieved', objVdsMails.rows, objVdsMails.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.getAVdMail = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdMail = await vdMailService.getAVdMail(Id, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.addVdMail = async (req, res) => {
    try {
        const objVdMail = await vdMailService.addVdMail(req.body);
        util.setSuccess(201, 'VdMail Added!', objVdMail);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.updateVdMail = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdMail = await vdMailService.updateVdMail(Id, req.body);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdMail updated', objVdMail);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsMailsCtrl.deleteVdMail = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdMail = await vdMailService.deleteVdMail(Id);
        if (objVdMail) {
            util.setSuccess(200, 'VdMail deleted', objVdMail);
        } else {
            util.setError(404, `VdMail with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsMailsCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdMail = await vdMailService.findOneByUid(Id, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdMail = await vdMailService.findOneById(id, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiPort = async (req, res) => {
    try {
        const { maiPort } = req.params;
        const objVdMail = await vdMailService.findOneByMaiPort(maiPort, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiDescription = async (req, res) => {
    try {
        const { maiDescription } = req.params;
        const objVdMail = await vdMailService.findOneByMaiDescription(maiDescription, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiUserAccount = async (req, res) => {
    try {
        const { maiUserAccount } = req.params;
        const objVdMail = await vdMailService.findOneByMaiUserAccount(maiUserAccount, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiUserPassword = async (req, res) => {
    try {
        const { maiUserPassword } = req.params;
        const objVdMail = await vdMailService.findOneByMaiUserPassword(maiUserPassword, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiHost = async (req, res) => {
    try {
        const { maiHost } = req.params;
        const objVdMail = await vdMailService.findOneByMaiHost(maiHost, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiProtocol = async (req, res) => {
    try {
        const { maiProtocol } = req.params;
        const objVdMail = await vdMailService.findOneByMaiProtocol(maiProtocol, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiBusId = async (req, res) => {
    try {
        const { maiBusId } = req.params;
        const objVdMail = await vdMailService.findOneByMaiBusId(maiBusId, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiParStatusId = async (req, res) => {
    try {
        const { maiParStatusId } = req.params;
        const objVdMail = await vdMailService.findOneByMaiParStatusId(maiParStatusId, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiGroup = async (req, res) => {
    try {
        const { maiGroup } = req.params;
        const objVdMail = await vdMailService.findOneByMaiGroup(maiGroup, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiSubject = async (req, res) => {
    try {
        const { maiSubject } = req.params;
        const objVdMail = await vdMailService.findOneByMaiSubject(maiSubject, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiTo = async (req, res) => {
    try {
        const { maiTo } = req.params;
        const objVdMail = await vdMailService.findOneByMaiTo(maiTo, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdMail = await vdMailService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdMail = await vdMailService.findOneByCreatedby(createdby, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiBcc = async (req, res) => {
    try {
        const { maiBcc } = req.params;
        const objVdMail = await vdMailService.findOneByMaiBcc(maiBcc, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiCc = async (req, res) => {
    try {
        const { maiCc } = req.params;
        const objVdMail = await vdMailService.findOneByMaiCc(maiCc, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiText = async (req, res) => {
    try {
        const { maiText } = req.params;
        const objVdMail = await vdMailService.findOneByMaiText(maiText, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByMaiHtml = async (req, res) => {
    try {
        const { maiHtml } = req.params;
        const objVdMail = await vdMailService.findOneByMaiHtml(maiHtml, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdMail = await vdMailService.findOneByDueat(dueat, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdMail = await vdMailService.findOneByCreatedat(createdat, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdMail = await vdMailService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdMail) {
            util.setError(404, `Cannot find vdMail with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMail', objVdMail);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsMailsCtrl.updateVdMailByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByUid(Id, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailById(id, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiPort = async (req, res) => {
     const { maiPort } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiPort(maiPort, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiDescription = async (req, res) => {
     const { maiDescription } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiDescription(maiDescription, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiUserAccount = async (req, res) => {
     const { maiUserAccount } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiUserAccount(maiUserAccount, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiUserPassword = async (req, res) => {
     const { maiUserPassword } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiUserPassword(maiUserPassword, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiHost = async (req, res) => {
     const { maiHost } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiHost(maiHost, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiProtocol = async (req, res) => {
     const { maiProtocol } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiProtocol(maiProtocol, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiBusId = async (req, res) => {
     const { maiBusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiBusId(maiBusId, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiParStatusId = async (req, res) => {
     const { maiParStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiParStatusId(maiParStatusId, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiGroup = async (req, res) => {
     const { maiGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiGroup(maiGroup, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiSubject = async (req, res) => {
     const { maiSubject } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiSubject(maiSubject, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiTo = async (req, res) => {
     const { maiTo } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiTo(maiTo, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByUpdatedby(updatedby, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByCreatedby(createdby, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiBcc = async (req, res) => {
     const { maiBcc } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiBcc(maiBcc, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiCc = async (req, res) => {
     const { maiCc } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiCc(maiCc, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiText = async (req, res) => {
     const { maiText } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiText(maiText, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByMaiHtml = async (req, res) => {
     const { maiHtml } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByMaiHtml(maiHtml, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByDueat(dueat, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByCreatedat(createdat, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMailsCtrl.updateVdMailByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMail = await vdMailService.updateVdMailByUpdatedat(updatedat, req.body);
            if (!objVdMail) {
                util.setError(404, `Cannot find vdMail with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMail updated', objVdMail);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsMailsCtrl.findVdsParamsMaiParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsMails = await vdMailService.findVdsParamsMaiParStatusWithParOrder(select, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(Id, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(id, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiPort } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiPort, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiDescription } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiDescription, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiUserAccount } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiUserAccount, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiUserPassword } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiUserPassword, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiHost } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiHost, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiProtocol } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiProtocol, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiBusId } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiBusId, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiParStatusId } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiParStatusId, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiGroup } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiGroup, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiSubject } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiSubject, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiTo } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiTo, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(updatedby, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { createdby } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(createdby, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiBcc } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiBcc, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiCc } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiCc, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiText } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiText, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { maiHtml } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(maiHtml, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(dueat, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(createdat, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMailsCtrl.filterVdsMailsByMaiParStatus = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsMails = await vdMailService.filterVdsMailsByMaiParStatus(updatedat, req.query);
        if (vdsMails.length > 0) {
            util.setSuccess(200, 'VdsMails retrieved', vdsMails);
        } else {
            util.setSuccess(200, 'No vdMail found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsMailsCtrl;
//</es-section>
