/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:00 GMT-0400 (GMT-04:00)
 * Time: 0:54:0
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:00 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:0
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdDictionaryService = require('../services/vds_dictionaries.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsDictionariesCtrl = {};
vdsDictionariesCtrl.service = vdDictionaryService;


vdsDictionariesCtrl.getAllVdsDictionaries = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsDictionaries.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsDictionaries.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsDictionaries] = await vdDictionaryService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsDictionaries.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsDictionaries = await vdDictionaryService.getAllVdsDictionaries(req.query);
        objVdsDictionaries = util.setRoot(objVdsDictionaries,req.query.root);
        if (objVdsDictionaries && objVdsDictionaries.rows && objVdsDictionaries.count) {
            util.setSuccess(200, 'VdsDictionaries retrieved', objVdsDictionaries.rows, objVdsDictionaries.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.getAVdDictionary = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdDictionary = await vdDictionaryService.getAVdDictionary(Id, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.addVdDictionary = async (req, res) => {
    try {
        const objVdDictionary = await vdDictionaryService.addVdDictionary(req.body);
        util.setSuccess(201, 'VdDictionary Added!', objVdDictionary);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.updateVdDictionary = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdDictionary = await vdDictionaryService.updateVdDictionary(Id, req.body);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsDictionariesCtrl.deleteVdDictionary = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdDictionary = await vdDictionaryService.deleteVdDictionary(Id);
        if (objVdDictionary) {
            util.setSuccess(200, 'VdDictionary deleted', objVdDictionary);
        } else {
            util.setError(404, `VdDictionary with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsDictionariesCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByUid(Id, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneById(id, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByDicCode = async (req, res) => {
    try {
        const { dicCode } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByDicCode(dicCode, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByDicDescription = async (req, res) => {
    try {
        const { dicDescription } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByDicDescription(dicDescription, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByDicGroup = async (req, res) => {
    try {
        const { dicGroup } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByDicGroup(dicGroup, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByDicParStatusId = async (req, res) => {
    try {
        const { dicParStatusId } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByDicParStatusId(dicParStatusId, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByDueat(dueat, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByCreatedat(createdat, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdDictionary = await vdDictionaryService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdDictionary) {
            util.setError(404, `Cannot find vdDictionary with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdDictionary', objVdDictionary);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsDictionariesCtrl.updateVdDictionaryByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByUid(Id, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryById(id, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByDicCode = async (req, res) => {
     const { dicCode } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByDicCode(dicCode, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByDicDescription = async (req, res) => {
     const { dicDescription } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByDicDescription(dicDescription, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByDicGroup = async (req, res) => {
     const { dicGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByDicGroup(dicGroup, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByDicParStatusId = async (req, res) => {
     const { dicParStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByDicParStatusId(dicParStatusId, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByCreatedbyid(createdbyid, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByUpdatedbyid(updatedbyid, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByDueat(dueat, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByCreatedat(createdat, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsDictionariesCtrl.updateVdDictionaryByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdDictionary = await vdDictionaryService.updateVdDictionaryByUpdatedat(updatedat, req.body);
            if (!objVdDictionary) {
                util.setError(404, `Cannot find vdDictionary with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdDictionary updated', objVdDictionary);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsDictionariesCtrl.findVdsParamsDicParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsDictionaries = await vdDictionaryService.findVdsParamsDicParStatusWithParOrder(select, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsDictionaries = await vdDictionaryService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsDictionaries = await vdDictionaryService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(Id, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(id, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { dicCode } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(dicCode, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { dicDescription } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(dicDescription, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { dicGroup } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(dicGroup, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { dicParStatusId } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(dicParStatusId, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByCreatedby(createdbyid, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByUpdatedby(updatedbyid, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(dueat, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(createdat, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsDictionariesCtrl.filterVdsDictionariesByDicParStatus = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsDictionaries = await vdDictionaryService.filterVdsDictionariesByDicParStatus(updatedat, req.query);
        if (vdsDictionaries.length > 0) {
            util.setSuccess(200, 'VdsDictionaries retrieved', vdsDictionaries);
        } else {
            util.setSuccess(200, 'No vdDictionary found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsDictionariesCtrl;
//</es-section>
