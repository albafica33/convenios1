/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:03 GMT-0400 (GMT-04:00)
 * Time: 0:54:3
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:03 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:3
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdMatrizService = require('../services/vds_matriz.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsMatrizCtrl = {};
vdsMatrizCtrl.service = vdMatrizService;


vdsMatrizCtrl.getAllVdsMatriz = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsMatriz.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsMatriz.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsMatriz] = await vdMatrizService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsMatriz.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsMatriz = await vdMatrizService.getAllVdsMatriz(req.query);
        objVdsMatriz = util.setRoot(objVdsMatriz,req.query.root);
        if (objVdsMatriz && objVdsMatriz.rows && objVdsMatriz.count) {
            util.setSuccess(200, 'VdsMatriz retrieved', objVdsMatriz.rows, objVdsMatriz.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.getAVdMatriz = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdMatriz = await vdMatrizService.getAVdMatriz(Id, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.addVdMatriz = async (req, res) => {
    try {
        const objVdMatriz = await vdMatrizService.addVdMatriz(req.body);
        util.setSuccess(201, 'VdMatriz Added!', objVdMatriz);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.updateVdMatriz = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdMatriz = await vdMatrizService.updateVdMatriz(Id, req.body);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsMatrizCtrl.deleteVdMatriz = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdMatriz = await vdMatrizService.deleteVdMatriz(Id);
        if (objVdMatriz) {
            util.setSuccess(200, 'VdMatriz deleted', objVdMatriz);
        } else {
            util.setError(404, `VdMatriz with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsMatrizCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByUid(Id, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdMatriz = await vdMatrizService.findOneById(id, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByEstadoCumplimiento = async (req, res) => {
    try {
        const { estadoCumplimiento } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByEstadoCumplimiento(estadoCumplimiento, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByCompromiso = async (req, res) => {
    try {
        const { compromiso } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByCompromiso(compromiso, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByEntidadResponsable = async (req, res) => {
    try {
        const { entidadResponsable } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByEntidadResponsable(entidadResponsable, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByContacto = async (req, res) => {
    try {
        const { contacto } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByContacto(contacto, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByObservacion = async (req, res) => {
    try {
        const { observacion } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByObservacion(observacion, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByTema = async (req, res) => {
    try {
        const { tema } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByTema(tema, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByFechaCumplimiento = async (req, res) => {
    try {
        const { fechaCumplimiento } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByFechaCumplimiento(fechaCumplimiento, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByFechaModificacion = async (req, res) => {
    try {
        const { fechaModificacion } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByFechaModificacion(fechaModificacion, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByDueat(dueat, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByCreatedat(createdat, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdMatriz = await vdMatrizService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdMatriz) {
            util.setError(404, `Cannot find vdMatriz with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdMatriz', objVdMatriz);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsMatrizCtrl.updateVdMatrizByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByUid(Id, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizById(id, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByEstadoCumplimiento = async (req, res) => {
     const { estadoCumplimiento } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByEstadoCumplimiento(estadoCumplimiento, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByCreatedbyid(createdbyid, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByUpdatedbyid(updatedbyid, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByCompromiso = async (req, res) => {
     const { compromiso } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByCompromiso(compromiso, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByEntidadResponsable = async (req, res) => {
     const { entidadResponsable } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByEntidadResponsable(entidadResponsable, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByContacto = async (req, res) => {
     const { contacto } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByContacto(contacto, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByObservacion = async (req, res) => {
     const { observacion } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByObservacion(observacion, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByTema = async (req, res) => {
     const { tema } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByTema(tema, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByFechaCumplimiento = async (req, res) => {
     const { fechaCumplimiento } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByFechaCumplimiento(fechaCumplimiento, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByFechaModificacion = async (req, res) => {
     const { fechaModificacion } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByFechaModificacion(fechaModificacion, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByDueat(dueat, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByCreatedat(createdat, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsMatrizCtrl.updateVdMatrizByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdMatriz = await vdMatrizService.updateVdMatrizByUpdatedat(updatedat, req.body);
            if (!objVdMatriz) {
                util.setError(404, `Cannot find vdMatriz with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdMatriz updated', objVdMatriz);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsMatrizCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsMatriz = await vdMatrizService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsMatriz = await vdMatrizService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(Id, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(id, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { estadoCumplimiento } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(estadoCumplimiento, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(createdbyid, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByUpdatedby(updatedbyid, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { compromiso } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(compromiso, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { entidadResponsable } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(entidadResponsable, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { contacto } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(contacto, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { observacion } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(observacion, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { tema } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(tema, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { fechaCumplimiento } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(fechaCumplimiento, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { fechaModificacion } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(fechaModificacion, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(dueat, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(createdat, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsMatrizCtrl.filterVdsMatrizByCreatedby = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsMatriz = await vdMatrizService.filterVdsMatrizByCreatedby(updatedat, req.query);
        if (vdsMatriz.length > 0) {
            util.setSuccess(200, 'VdsMatriz retrieved', vdsMatriz);
        } else {
            util.setSuccess(200, 'No vdMatriz found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsMatrizCtrl;
//</es-section>
