/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Time: 0:53:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:58
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const sequelizemetaService = require('../services/sequelizemeta.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const sequelizemetaCtrl = {};
sequelizemetaCtrl.service = sequelizemetaService;


sequelizemetaCtrl.getAllSequelizemeta = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.sequelizemeta.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.sequelizemeta.rawAttributes,req.body);
        // const [searchPanes,searches,userSequelizemeta] = await sequelizemetaService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.sequelizemeta.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objSequelizemeta = await sequelizemetaService.getAllSequelizemeta(req.query);
        objSequelizemeta = util.setRoot(objSequelizemeta,req.query.root);
        if (objSequelizemeta && objSequelizemeta.rows && objSequelizemeta.count) {
            util.setSuccess(200, 'Sequelizemeta retrieved', objSequelizemeta.rows, objSequelizemeta.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No sequelizemeta found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

sequelizemetaCtrl.getASequelizemeta = async (req, res) => {
    try {
        const { name } = req.params;
        if (!util.isString(name)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objSequelizemeta = await sequelizemetaService.getASequelizemeta(name, req.query);
        if (!objSequelizemeta) {
            util.setError(404, `Cannot find sequelizemeta with the id ${name}`);
        } else {
            util.setSuccess(200, 'Found sequelizemeta', objSequelizemeta);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

sequelizemetaCtrl.addSequelizemeta = async (req, res) => {
    try {
        const objSequelizemeta = await sequelizemetaService.addSequelizemeta(req.body);
        util.setSuccess(201, 'Sequelizemeta Added!', objSequelizemeta);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

sequelizemetaCtrl.updateSequelizemeta = async (req, res) => {
    try {
        const { name } = req.params;
        if (!util.isString(name)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objSequelizemeta = await sequelizemetaService.updateSequelizemeta(name, req.body);
        if (!objSequelizemeta) {
            util.setError(404, `Cannot find sequelizemeta with the id: ${name}`);
        } else {
            util.setSuccess(200, 'Sequelizemeta updated', objSequelizemeta);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

sequelizemetaCtrl.deleteSequelizemeta = async (req, res) => {
    try {
        const { name } = req.params;
        if (!util.isString(name)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objSequelizemeta = await sequelizemetaService.deleteSequelizemeta(name);
        if (objSequelizemeta) {
            util.setSuccess(200, 'Sequelizemeta deleted', objSequelizemeta);
        } else {
            util.setError(404, `Sequelizemeta with the id ${name} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



sequelizemetaCtrl.findOneByName = async (req, res) => {
    try {
        const { name } = req.params;
        const objSequelizemeta = await sequelizemetaService.findOneByName(name, req.query);
        if (!objSequelizemeta) {
            util.setError(404, `Cannot find sequelizemeta with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found sequelizemeta', objSequelizemeta);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



sequelizemetaCtrl.updateSequelizemetaByName = async (req, res) => {
     const { name } = req.params;
        try {
            if (!util.isString(name)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objSequelizemeta = await sequelizemetaService.updateSequelizemetaByName(name, req.body);
            if (!objSequelizemeta) {
                util.setError(404, `Cannot find sequelizemeta with the id: ${name}`);
            } else {
                util.setSuccess(200, 'Sequelizemeta updated', objSequelizemeta);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}







//</es-section>

//<es-section>
module.exports = sequelizemetaCtrl;
//</es-section>
