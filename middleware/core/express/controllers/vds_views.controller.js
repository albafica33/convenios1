/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:15 GMT-0400 (GMT-04:00)
 * Time: 0:54:15
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:15 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:15
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdViewService = require('../services/vds_views.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsViewsCtrl = {};
vdsViewsCtrl.service = vdViewService;


vdsViewsCtrl.getAllVdsViews = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsViews.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsViews.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsViews] = await vdViewService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsViews.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsViews = await vdViewService.getAllVdsViews(req.query);
        objVdsViews = util.setRoot(objVdsViews,req.query.root);
        if (objVdsViews && objVdsViews.rows && objVdsViews.count) {
            util.setSuccess(200, 'VdsViews retrieved', objVdsViews.rows, objVdsViews.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.getAVdView = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdView = await vdViewService.getAVdView(Id, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.addVdView = async (req, res) => {
    try {
        const objVdView = await vdViewService.addVdView(req.body);
        util.setSuccess(201, 'VdView Added!', objVdView);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.updateVdView = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdView = await vdViewService.updateVdView(Id, req.body);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdView updated', objVdView);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsViewsCtrl.deleteVdView = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdView = await vdViewService.deleteVdView(Id);
        if (objVdView) {
            util.setSuccess(200, 'VdView deleted', objVdView);
        } else {
            util.setError(404, `VdView with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsViewsCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdView = await vdViewService.findOneByUid(Id, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdView = await vdViewService.findOneById(id, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieCode = async (req, res) => {
    try {
        const { vieCode } = req.params;
        const objVdView = await vdViewService.findOneByVieCode(vieCode, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieDescription = async (req, res) => {
    try {
        const { vieDescription } = req.params;
        const objVdView = await vdViewService.findOneByVieDescription(vieDescription, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieRoute = async (req, res) => {
    try {
        const { vieRoute } = req.params;
        const objVdView = await vdViewService.findOneByVieRoute(vieRoute, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieParams = async (req, res) => {
    try {
        const { vieParams } = req.params;
        const objVdView = await vdViewService.findOneByVieParams(vieParams, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieIcon = async (req, res) => {
    try {
        const { vieIcon } = req.params;
        const objVdView = await vdViewService.findOneByVieIcon(vieIcon, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieGroup = async (req, res) => {
    try {
        const { vieGroup } = req.params;
        const objVdView = await vdViewService.findOneByVieGroup(vieGroup, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdView = await vdViewService.findOneByCreatedby(createdby, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdView = await vdViewService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieModuleId = async (req, res) => {
    try {
        const { vieModuleId } = req.params;
        const objVdView = await vdViewService.findOneByVieModuleId(vieModuleId, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieReturnId = async (req, res) => {
    try {
        const { vieReturnId } = req.params;
        const objVdView = await vdViewService.findOneByVieReturnId(vieReturnId, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByVieParStatusId = async (req, res) => {
    try {
        const { vieParStatusId } = req.params;
        const objVdView = await vdViewService.findOneByVieParStatusId(vieParStatusId, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdView = await vdViewService.findOneByDueat(dueat, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdView = await vdViewService.findOneByCreatedat(createdat, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdView = await vdViewService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdView) {
            util.setError(404, `Cannot find vdView with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdView', objVdView);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsViewsCtrl.updateVdViewByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByUid(Id, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewById(id, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieCode = async (req, res) => {
     const { vieCode } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieCode(vieCode, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieDescription = async (req, res) => {
     const { vieDescription } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieDescription(vieDescription, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieRoute = async (req, res) => {
     const { vieRoute } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieRoute(vieRoute, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieParams = async (req, res) => {
     const { vieParams } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieParams(vieParams, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieIcon = async (req, res) => {
     const { vieIcon } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieIcon(vieIcon, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieGroup = async (req, res) => {
     const { vieGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieGroup(vieGroup, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByCreatedby(createdby, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByUpdatedby(updatedby, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieModuleId = async (req, res) => {
     const { vieModuleId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieModuleId(vieModuleId, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieReturnId = async (req, res) => {
     const { vieReturnId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieReturnId(vieReturnId, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByVieParStatusId = async (req, res) => {
     const { vieParStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByVieParStatusId(vieParStatusId, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByDueat(dueat, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByCreatedat(createdat, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsViewsCtrl.updateVdViewByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdView = await vdViewService.updateVdViewByUpdatedat(updatedat, req.body);
            if (!objVdView) {
                util.setError(404, `Cannot find vdView with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdView updated', objVdView);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsViewsCtrl.findVdsModulesVieModuleWithModCode = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsViews = await vdViewService.findVdsModulesVieModuleWithModCode(select, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findVdsViewsVieReturnWithVieCode = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsViews = await vdViewService.findVdsViewsVieReturnWithVieCode(select, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.findVdsParamsVieParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsViews = await vdViewService.findVdsParamsVieParStatusWithParOrder(select, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(Id, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(id, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { vieCode } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(vieCode, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { vieDescription } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(vieDescription, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { vieRoute } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(vieRoute, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { vieParams } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(vieParams, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { vieIcon } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(vieIcon, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { vieGroup } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(vieGroup, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { createdby } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(createdby, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(updatedby, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { vieModuleId } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(vieModuleId, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieReturn = async (req, res) => {
    try {
        const { vieReturnId } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieReturn(vieReturnId, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieParStatus = async (req, res) => {
    try {
        const { vieParStatusId } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieParStatus(vieParStatusId, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(dueat, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(createdat, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsViewsCtrl.filterVdsViewsByVieModule = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsViews = await vdViewService.filterVdsViewsByVieModule(updatedat, req.query);
        if (vdsViews.length > 0) {
            util.setSuccess(200, 'VdsViews retrieved', vdsViews);
        } else {
            util.setSuccess(200, 'No vdView found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsViewsCtrl;
//</es-section>
