/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Time: 0:54:5
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:05 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:5
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdPaiService = require('../services/vds_pais.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsPaisCtrl = {};
vdsPaisCtrl.service = vdPaiService;


vdsPaisCtrl.getAllVdsPais = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsPais.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsPais.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsPais] = await vdPaiService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsPais.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsPais = await vdPaiService.getAllVdsPais(req.query);
        objVdsPais = util.setRoot(objVdsPais,req.query.root);
        if (objVdsPais && objVdsPais.rows && objVdsPais.count) {
            util.setSuccess(200, 'VdsPais retrieved', objVdsPais.rows, objVdsPais.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdPai found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.getAVdPai = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdPai = await vdPaiService.getAVdPai(Id, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.addVdPai = async (req, res) => {
    try {
        const objVdPai = await vdPaiService.addVdPai(req.body);
        util.setSuccess(201, 'VdPai Added!', objVdPai);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.updateVdPai = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdPai = await vdPaiService.updateVdPai(Id, req.body);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdPai updated', objVdPai);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsPaisCtrl.deleteVdPai = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdPai = await vdPaiService.deleteVdPai(Id);
        if (objVdPai) {
            util.setSuccess(200, 'VdPai deleted', objVdPai);
        } else {
            util.setError(404, `VdPai with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsPaisCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdPai = await vdPaiService.findOneByUid(Id, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdPai = await vdPaiService.findOneById(id, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneByEstado = async (req, res) => {
    try {
        const { estado } = req.params;
        const objVdPai = await vdPaiService.findOneByEstado(estado, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneByPais = async (req, res) => {
    try {
        const { pais } = req.params;
        const objVdPai = await vdPaiService.findOneByPais(pais, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneByAbreviacion = async (req, res) => {
    try {
        const { abreviacion } = req.params;
        const objVdPai = await vdPaiService.findOneByAbreviacion(abreviacion, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdPai = await vdPaiService.findOneByCreatedby(createdby, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdPai = await vdPaiService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdPai = await vdPaiService.findOneByDueat(dueat, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdPai = await vdPaiService.findOneByCreatedat(createdat, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPaisCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdPai = await vdPaiService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdPai) {
            util.setError(404, `Cannot find vdPai with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPai', objVdPai);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsPaisCtrl.updateVdPaiByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByUid(Id, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiById(id, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiByEstado = async (req, res) => {
     const { estado } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByEstado(estado, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiByPais = async (req, res) => {
     const { pais } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByPais(pais, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiByAbreviacion = async (req, res) => {
     const { abreviacion } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByAbreviacion(abreviacion, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByCreatedby(createdby, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByUpdatedby(updatedby, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByDueat(dueat, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByCreatedat(createdat, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPaisCtrl.updateVdPaiByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPai = await vdPaiService.updateVdPaiByUpdatedat(updatedat, req.body);
            if (!objVdPai) {
                util.setError(404, `Cannot find vdPai with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPai updated', objVdPai);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}







//</es-section>

//<es-section>
module.exports = vdsPaisCtrl;
//</es-section>
