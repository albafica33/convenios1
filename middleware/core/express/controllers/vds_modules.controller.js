/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:04 GMT-0400 (GMT-04:00)
 * Time: 0:54:4
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:04 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:4
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdModuleService = require('../services/vds_modules.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsModulesCtrl = {};
vdsModulesCtrl.service = vdModuleService;


vdsModulesCtrl.getAllVdsModules = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsModules.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsModules.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsModules] = await vdModuleService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsModules.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsModules = await vdModuleService.getAllVdsModules(req.query);
        objVdsModules = util.setRoot(objVdsModules,req.query.root);
        if (objVdsModules && objVdsModules.rows && objVdsModules.count) {
            util.setSuccess(200, 'VdsModules retrieved', objVdsModules.rows, objVdsModules.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.getAVdModule = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdModule = await vdModuleService.getAVdModule(Id, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.addVdModule = async (req, res) => {
    try {
        const objVdModule = await vdModuleService.addVdModule(req.body);
        util.setSuccess(201, 'VdModule Added!', objVdModule);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.updateVdModule = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdModule = await vdModuleService.updateVdModule(Id, req.body);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdModule updated', objVdModule);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsModulesCtrl.deleteVdModule = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdModule = await vdModuleService.deleteVdModule(Id);
        if (objVdModule) {
            util.setSuccess(200, 'VdModule deleted', objVdModule);
        } else {
            util.setError(404, `VdModule with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsModulesCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdModule = await vdModuleService.findOneByUid(Id, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdModule = await vdModuleService.findOneById(id, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByModCode = async (req, res) => {
    try {
        const { modCode } = req.params;
        const objVdModule = await vdModuleService.findOneByModCode(modCode, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByModDescription = async (req, res) => {
    try {
        const { modDescription } = req.params;
        const objVdModule = await vdModuleService.findOneByModDescription(modDescription, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByModAbbr = async (req, res) => {
    try {
        const { modAbbr } = req.params;
        const objVdModule = await vdModuleService.findOneByModAbbr(modAbbr, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByModIcon = async (req, res) => {
    try {
        const { modIcon } = req.params;
        const objVdModule = await vdModuleService.findOneByModIcon(modIcon, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByModGroup = async (req, res) => {
    try {
        const { modGroup } = req.params;
        const objVdModule = await vdModuleService.findOneByModGroup(modGroup, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdModule = await vdModuleService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdModule = await vdModuleService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByModParentId = async (req, res) => {
    try {
        const { modParentId } = req.params;
        const objVdModule = await vdModuleService.findOneByModParentId(modParentId, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByModParStatusId = async (req, res) => {
    try {
        const { modParStatusId } = req.params;
        const objVdModule = await vdModuleService.findOneByModParStatusId(modParStatusId, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdModule = await vdModuleService.findOneByDueat(dueat, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdModule = await vdModuleService.findOneByCreatedat(createdat, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdModule = await vdModuleService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdModule) {
            util.setError(404, `Cannot find vdModule with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdModule', objVdModule);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsModulesCtrl.updateVdModuleByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByUid(Id, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleById(id, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByModCode = async (req, res) => {
     const { modCode } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByModCode(modCode, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByModDescription = async (req, res) => {
     const { modDescription } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByModDescription(modDescription, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByModAbbr = async (req, res) => {
     const { modAbbr } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByModAbbr(modAbbr, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByModIcon = async (req, res) => {
     const { modIcon } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByModIcon(modIcon, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByModGroup = async (req, res) => {
     const { modGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByModGroup(modGroup, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByCreatedbyid(createdbyid, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByUpdatedbyid(updatedbyid, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByModParentId = async (req, res) => {
     const { modParentId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByModParentId(modParentId, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByModParStatusId = async (req, res) => {
     const { modParStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByModParStatusId(modParStatusId, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByDueat(dueat, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByCreatedat(createdat, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsModulesCtrl.updateVdModuleByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdModule = await vdModuleService.updateVdModuleByUpdatedat(updatedat, req.body);
            if (!objVdModule) {
                util.setError(404, `Cannot find vdModule with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdModule updated', objVdModule);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsModulesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsModules = await vdModuleService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsModules = await vdModuleService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.findVdsParamsModParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsModules = await vdModuleService.findVdsParamsModParStatusWithParOrder(select, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(Id, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(id, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { modCode } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(modCode, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { modDescription } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(modDescription, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { modAbbr } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(modAbbr, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { modIcon } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(modIcon, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { modGroup } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(modGroup, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(createdbyid, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByUpdatedby(updatedbyid, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { modParentId } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(modParentId, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByModParStatus = async (req, res) => {
    try {
        const { modParStatusId } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByModParStatus(modParStatusId, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(dueat, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(createdat, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsModulesCtrl.filterVdsModulesByCreatedby = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsModules = await vdModuleService.filterVdsModulesByCreatedby(updatedat, req.query);
        if (vdsModules.length > 0) {
            util.setSuccess(200, 'VdsModules retrieved', vdsModules);
        } else {
            util.setSuccess(200, 'No vdModule found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsModulesCtrl;
//</es-section>
