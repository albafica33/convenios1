/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Time: 0:54:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:11 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:11
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdProvinciaService = require('../services/vds_provincia.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsProvinciaCtrl = {};
vdsProvinciaCtrl.service = vdProvinciaService;


vdsProvinciaCtrl.getAllVdsProvincia = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsProvincia.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsProvincia.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsProvincia] = await vdProvinciaService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsProvincia.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsProvincia = await vdProvinciaService.getAllVdsProvincia(req.query);
        objVdsProvincia = util.setRoot(objVdsProvincia,req.query.root);
        if (objVdsProvincia && objVdsProvincia.rows && objVdsProvincia.count) {
            util.setSuccess(200, 'VdsProvincia retrieved', objVdsProvincia.rows, objVdsProvincia.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.getAVdProvincia = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdProvincia = await vdProvinciaService.getAVdProvincia(Id, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.addVdProvincia = async (req, res) => {
    try {
        const objVdProvincia = await vdProvinciaService.addVdProvincia(req.body);
        util.setSuccess(201, 'VdProvincia Added!', objVdProvincia);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.updateVdProvincia = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdProvincia = await vdProvinciaService.updateVdProvincia(Id, req.body);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsProvinciaCtrl.deleteVdProvincia = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdProvincia = await vdProvinciaService.deleteVdProvincia(Id);
        if (objVdProvincia) {
            util.setSuccess(200, 'VdProvincia deleted', objVdProvincia);
        } else {
            util.setError(404, `VdProvincia with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsProvinciaCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByUid(Id, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneById(id, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByEstado = async (req, res) => {
    try {
        const { estado } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByEstado(estado, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByProvincia = async (req, res) => {
    try {
        const { provincia } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByProvincia(provincia, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByAbreviacion = async (req, res) => {
    try {
        const { abreviacion } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByAbreviacion(abreviacion, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByCreatedby(createdby, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByIdCiudad = async (req, res) => {
    try {
        const { idCiudad } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByIdCiudad(idCiudad, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByDueat(dueat, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByCreatedat(createdat, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdProvincia = await vdProvinciaService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdProvincia) {
            util.setError(404, `Cannot find vdProvincia with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdProvincia', objVdProvincia);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsProvinciaCtrl.updateVdProvinciaByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByUid(Id, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaById(id, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByEstado = async (req, res) => {
     const { estado } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByEstado(estado, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByProvincia = async (req, res) => {
     const { provincia } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByProvincia(provincia, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByAbreviacion = async (req, res) => {
     const { abreviacion } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByAbreviacion(abreviacion, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByCreatedby(createdby, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByUpdatedby(updatedby, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByIdCiudad = async (req, res) => {
     const { idCiudad } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByIdCiudad(idCiudad, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByDueat(dueat, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByCreatedat(createdat, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsProvinciaCtrl.updateVdProvinciaByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdProvincia = await vdProvinciaService.updateVdProvinciaByUpdatedat(updatedat, req.body);
            if (!objVdProvincia) {
                util.setError(404, `Cannot find vdProvincia with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdProvincia updated', objVdProvincia);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsProvinciaCtrl.findVdsCiudadCiudadWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsProvincia = await vdProvinciaService.findVdsCiudadCiudadWithEstado(select, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(Id, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(id, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { estado } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(estado, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { provincia } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(provincia, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { abreviacion } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(abreviacion, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { createdby } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(createdby, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(updatedby, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { idCiudad } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(idCiudad, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(dueat, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(createdat, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsProvinciaCtrl.filterVdsProvinciaByCiudad = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsProvincia = await vdProvinciaService.filterVdsProvinciaByCiudad(updatedat, req.query);
        if (vdsProvincia.length > 0) {
            util.setSuccess(200, 'VdsProvincia retrieved', vdsProvincia);
        } else {
            util.setSuccess(200, 'No vdProvincia found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsProvinciaCtrl;
//</es-section>
