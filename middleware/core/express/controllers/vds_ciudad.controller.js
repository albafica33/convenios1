/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Time: 0:53:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:53:58 GMT-0400 (GMT-04:00)
 * Last time updated: 0:53:58
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdCiudadService = require('../services/vds_ciudad.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsCiudadCtrl = {};
vdsCiudadCtrl.service = vdCiudadService;


vdsCiudadCtrl.getAllVdsCiudad = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsCiudad.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsCiudad.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsCiudad] = await vdCiudadService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsCiudad.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsCiudad = await vdCiudadService.getAllVdsCiudad(req.query);
        objVdsCiudad = util.setRoot(objVdsCiudad,req.query.root);
        if (objVdsCiudad && objVdsCiudad.rows && objVdsCiudad.count) {
            util.setSuccess(200, 'VdsCiudad retrieved', objVdsCiudad.rows, objVdsCiudad.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdCiudad found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.getAVdCiudad = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdCiudad = await vdCiudadService.getAVdCiudad(Id, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.addVdCiudad = async (req, res) => {
    try {
        const objVdCiudad = await vdCiudadService.addVdCiudad(req.body);
        util.setSuccess(201, 'VdCiudad Added!', objVdCiudad);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.updateVdCiudad = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdCiudad = await vdCiudadService.updateVdCiudad(Id, req.body);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsCiudadCtrl.deleteVdCiudad = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdCiudad = await vdCiudadService.deleteVdCiudad(Id);
        if (objVdCiudad) {
            util.setSuccess(200, 'VdCiudad deleted', objVdCiudad);
        } else {
            util.setError(404, `VdCiudad with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsCiudadCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByUid(Id, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdCiudad = await vdCiudadService.findOneById(id, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByEstado = async (req, res) => {
    try {
        const { estado } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByEstado(estado, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByCiudad = async (req, res) => {
    try {
        const { ciudad } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByCiudad(ciudad, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByAbrev = async (req, res) => {
    try {
        const { abrev } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByAbrev(abrev, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByCreatedby(createdby, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByIdPais = async (req, res) => {
    try {
        const { idPais } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByIdPais(idPais, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByDueat(dueat, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByCreatedat(createdat, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsCiudadCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdCiudad = await vdCiudadService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdCiudad) {
            util.setError(404, `Cannot find vdCiudad with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdCiudad', objVdCiudad);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsCiudadCtrl.updateVdCiudadByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByUid(Id, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadById(id, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByEstado = async (req, res) => {
     const { estado } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByEstado(estado, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByCiudad = async (req, res) => {
     const { ciudad } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByCiudad(ciudad, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByAbrev = async (req, res) => {
     const { abrev } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByAbrev(abrev, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByCreatedby(createdby, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByUpdatedby(updatedby, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByIdPais = async (req, res) => {
     const { idPais } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByIdPais(idPais, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByDueat(dueat, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByCreatedat(createdat, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsCiudadCtrl.updateVdCiudadByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdCiudad = await vdCiudadService.updateVdCiudadByUpdatedat(updatedat, req.body);
            if (!objVdCiudad) {
                util.setError(404, `Cannot find vdCiudad with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdCiudad updated', objVdCiudad);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}







//</es-section>

//<es-section>
module.exports = vdsCiudadCtrl;
//</es-section>
