/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:06 GMT-0400 (GMT-04:00)
 * Time: 0:54:6
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:06 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:6
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdParamService = require('../services/vds_params.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsParamsCtrl = {};
vdsParamsCtrl.service = vdParamService;


vdsParamsCtrl.getAllVdsParams = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsParams.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsParams.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsParams] = await vdParamService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsParams.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsParams = await vdParamService.getAllVdsParams(req.query);
        objVdsParams = util.setRoot(objVdsParams,req.query.root);
        if (objVdsParams && objVdsParams.rows && objVdsParams.count) {
            util.setSuccess(200, 'VdsParams retrieved', objVdsParams.rows, objVdsParams.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.getAVdParam = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdParam = await vdParamService.getAVdParam(Id, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.addVdParam = async (req, res) => {
    try {
        const objVdParam = await vdParamService.addVdParam(req.body);
        util.setSuccess(201, 'VdParam Added!', objVdParam);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.updateVdParam = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdParam = await vdParamService.updateVdParam(Id, req.body);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdParam updated', objVdParam);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsParamsCtrl.deleteVdParam = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdParam = await vdParamService.deleteVdParam(Id);
        if (objVdParam) {
            util.setSuccess(200, 'VdParam deleted', objVdParam);
        } else {
            util.setError(404, `VdParam with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsParamsCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdParam = await vdParamService.findOneByUid(Id, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdParam = await vdParamService.findOneById(id, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByParOrder = async (req, res) => {
    try {
        const { parOrder } = req.params;
        const objVdParam = await vdParamService.findOneByParOrder(parOrder, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByParCod = async (req, res) => {
    try {
        const { parCod } = req.params;
        const objVdParam = await vdParamService.findOneByParCod(parCod, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByParDescription = async (req, res) => {
    try {
        const { parDescription } = req.params;
        const objVdParam = await vdParamService.findOneByParDescription(parDescription, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByParAbbr = async (req, res) => {
    try {
        const { parAbbr } = req.params;
        const objVdParam = await vdParamService.findOneByParAbbr(parAbbr, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByParGroup = async (req, res) => {
    try {
        const { parGroup } = req.params;
        const objVdParam = await vdParamService.findOneByParGroup(parGroup, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdParam = await vdParamService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdParam = await vdParamService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByParDictionaryId = async (req, res) => {
    try {
        const { parDictionaryId } = req.params;
        const objVdParam = await vdParamService.findOneByParDictionaryId(parDictionaryId, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByParStatusId = async (req, res) => {
    try {
        const { parStatusId } = req.params;
        const objVdParam = await vdParamService.findOneByParStatusId(parStatusId, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdParam = await vdParamService.findOneByDueat(dueat, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdParam = await vdParamService.findOneByCreatedat(createdat, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdParam = await vdParamService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdParam) {
            util.setError(404, `Cannot find vdParam with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdParam', objVdParam);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsParamsCtrl.updateVdParamByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByUid(Id, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamById(id, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByParOrder = async (req, res) => {
     const { parOrder } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByParOrder(parOrder, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByParCod = async (req, res) => {
     const { parCod } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByParCod(parCod, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByParDescription = async (req, res) => {
     const { parDescription } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByParDescription(parDescription, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByParAbbr = async (req, res) => {
     const { parAbbr } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByParAbbr(parAbbr, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByParGroup = async (req, res) => {
     const { parGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByParGroup(parGroup, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByCreatedbyid(createdbyid, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByUpdatedbyid(updatedbyid, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByParDictionaryId = async (req, res) => {
     const { parDictionaryId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByParDictionaryId(parDictionaryId, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByParStatusId = async (req, res) => {
     const { parStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByParStatusId(parStatusId, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByDueat(dueat, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByCreatedat(createdat, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsParamsCtrl.updateVdParamByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdParam = await vdParamService.updateVdParamByUpdatedat(updatedat, req.body);
            if (!objVdParam) {
                util.setError(404, `Cannot find vdParam with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdParam updated', objVdParam);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsParamsCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsParams = await vdParamService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsParams = await vdParamService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findVdsDictionariesParDictionaryWithDicCode = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsParams = await vdParamService.findVdsDictionariesParDictionaryWithDicCode(select, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.findVdsParamsParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsParams = await vdParamService.findVdsParamsParStatusWithParOrder(select, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(Id, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(id, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { parOrder } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(parOrder, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { parCod } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(parCod, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { parDescription } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(parDescription, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { parAbbr } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(parAbbr, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { parGroup } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(parGroup, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(createdbyid, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByUpdatedby(updatedbyid, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByParDictionary = async (req, res) => {
    try {
        const { parDictionaryId } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByParDictionary(parDictionaryId, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByParStatus = async (req, res) => {
    try {
        const { parStatusId } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByParStatus(parStatusId, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(dueat, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(createdat, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsParamsCtrl.filterVdsParamsByCreatedby = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsParams = await vdParamService.filterVdsParamsByCreatedby(updatedat, req.query);
        if (vdsParams.length > 0) {
            util.setSuccess(200, 'VdsParams retrieved', vdsParams);
        } else {
            util.setSuccess(200, 'No vdParam found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsParamsCtrl;
//</es-section>
