/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:07 GMT-0400 (GMT-04:00)
 * Time: 0:54:7
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:07 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:7
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdPersonService = require('../services/vds_people.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsPeopleCtrl = {};
vdsPeopleCtrl.service = vdPersonService;


vdsPeopleCtrl.getAllVdsPeople = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsPeople.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsPeople.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsPeople] = await vdPersonService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsPeople.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsPeople = await vdPersonService.getAllVdsPeople(req.query);
        objVdsPeople = util.setRoot(objVdsPeople,req.query.root);
        if (objVdsPeople && objVdsPeople.rows && objVdsPeople.count) {
            util.setSuccess(200, 'VdsPeople retrieved', objVdsPeople.rows, objVdsPeople.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.getAVdPerson = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdPerson = await vdPersonService.getAVdPerson(Id, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.addVdPerson = async (req, res) => {
    try {
        const objVdPerson = await vdPersonService.addVdPerson(req.body);
        util.setSuccess(201, 'VdPerson Added!', objVdPerson);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.updateVdPerson = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdPerson = await vdPersonService.updateVdPerson(Id, req.body);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdPerson updated', objVdPerson);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsPeopleCtrl.deleteVdPerson = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdPerson = await vdPersonService.deleteVdPerson(Id);
        if (objVdPerson) {
            util.setSuccess(200, 'VdPerson deleted', objVdPerson);
        } else {
            util.setError(404, `VdPerson with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsPeopleCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdPerson = await vdPersonService.findOneByUid(Id, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdPerson = await vdPersonService.findOneById(id, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerFirstName = async (req, res) => {
    try {
        const { perFirstName } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerFirstName(perFirstName, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerSecondName = async (req, res) => {
    try {
        const { perSecondName } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerSecondName(perSecondName, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerFirstLastname = async (req, res) => {
    try {
        const { perFirstLastname } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerFirstLastname(perFirstLastname, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerSecondLastname = async (req, res) => {
    try {
        const { perSecondLastname } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerSecondLastname(perSecondLastname, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerLicense = async (req, res) => {
    try {
        const { perLicense } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerLicense(perLicense, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerLicenseComp = async (req, res) => {
    try {
        const { perLicenseComp } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerLicenseComp(perLicenseComp, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerHomeAddress = async (req, res) => {
    try {
        const { perHomeAddress } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerHomeAddress(perHomeAddress, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerMail = async (req, res) => {
    try {
        const { perMail } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerMail(perMail, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerHomePhone = async (req, res) => {
    try {
        const { perHomePhone } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerHomePhone(perHomePhone, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerCellphone = async (req, res) => {
    try {
        const { perCellphone } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerCellphone(perCellphone, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerGroup = async (req, res) => {
    try {
        const { perGroup } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerGroup(perGroup, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdPerson = await vdPersonService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdPerson = await vdPersonService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerParentId = async (req, res) => {
    try {
        const { perParentId } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerParentId(perParentId, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerParTypeDocId = async (req, res) => {
    try {
        const { perParTypeDocId } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerParTypeDocId(perParTypeDocId, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerParCityId = async (req, res) => {
    try {
        const { perParCityId } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerParCityId(perParCityId, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerParSexId = async (req, res) => {
    try {
        const { perParSexId } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerParSexId(perParSexId, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerParCountryId = async (req, res) => {
    try {
        const { perParCountryId } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerParCountryId(perParCountryId, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerParNacionalityId = async (req, res) => {
    try {
        const { perParNacionalityId } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerParNacionalityId(perParNacionalityId, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerParStatusId = async (req, res) => {
    try {
        const { perParStatusId } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerParStatusId(perParStatusId, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByPerBirthday = async (req, res) => {
    try {
        const { perBirthday } = req.params;
        const objVdPerson = await vdPersonService.findOneByPerBirthday(perBirthday, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdPerson = await vdPersonService.findOneByDueat(dueat, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdPerson = await vdPersonService.findOneByCreatedat(createdat, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdPerson = await vdPersonService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdPerson) {
            util.setError(404, `Cannot find vdPerson with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPerson', objVdPerson);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsPeopleCtrl.updateVdPersonByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByUid(Id, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonById(id, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerFirstName = async (req, res) => {
     const { perFirstName } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerFirstName(perFirstName, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerSecondName = async (req, res) => {
     const { perSecondName } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerSecondName(perSecondName, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerFirstLastname = async (req, res) => {
     const { perFirstLastname } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerFirstLastname(perFirstLastname, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerSecondLastname = async (req, res) => {
     const { perSecondLastname } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerSecondLastname(perSecondLastname, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerLicense = async (req, res) => {
     const { perLicense } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerLicense(perLicense, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerLicenseComp = async (req, res) => {
     const { perLicenseComp } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerLicenseComp(perLicenseComp, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerHomeAddress = async (req, res) => {
     const { perHomeAddress } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerHomeAddress(perHomeAddress, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerMail = async (req, res) => {
     const { perMail } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerMail(perMail, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerHomePhone = async (req, res) => {
     const { perHomePhone } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerHomePhone(perHomePhone, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerCellphone = async (req, res) => {
     const { perCellphone } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerCellphone(perCellphone, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerGroup = async (req, res) => {
     const { perGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerGroup(perGroup, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByCreatedbyid(createdbyid, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByUpdatedbyid(updatedbyid, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerParentId = async (req, res) => {
     const { perParentId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerParentId(perParentId, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerParTypeDocId = async (req, res) => {
     const { perParTypeDocId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerParTypeDocId(perParTypeDocId, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerParCityId = async (req, res) => {
     const { perParCityId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerParCityId(perParCityId, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerParSexId = async (req, res) => {
     const { perParSexId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerParSexId(perParSexId, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerParCountryId = async (req, res) => {
     const { perParCountryId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerParCountryId(perParCountryId, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerParNacionalityId = async (req, res) => {
     const { perParNacionalityId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerParNacionalityId(perParNacionalityId, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerParStatusId = async (req, res) => {
     const { perParStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerParStatusId(perParStatusId, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByPerBirthday = async (req, res) => {
     const { perBirthday } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByPerBirthday(perBirthday, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByDueat(dueat, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByCreatedat(createdat, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPeopleCtrl.updateVdPersonByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPerson = await vdPersonService.updateVdPersonByUpdatedat(updatedat, req.body);
            if (!objVdPerson) {
                util.setError(404, `Cannot find vdPerson with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPerson updated', objVdPerson);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsPeopleCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findVdsPeoplePerParentWithPerFirstName = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsPeoplePerParentWithPerFirstName(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findVdsParamsPerParTypeDocWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsParamsPerParTypeDocWithParOrder(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findVdsParamsPerParCityWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsParamsPerParCityWithParOrder(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findVdsParamsPerParSexWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsParamsPerParSexWithParOrder(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findVdsParamsPerParCountryWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsParamsPerParCountryWithParOrder(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findVdsParamsPerParNacionalityWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsParamsPerParNacionalityWithParOrder(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.findVdsParamsPerParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPeople = await vdPersonService.findVdsParamsPerParStatusWithParOrder(select, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(Id, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(id, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perFirstName } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perFirstName, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perSecondName } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perSecondName, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perFirstLastname } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perFirstLastname, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perSecondLastname } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perSecondLastname, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perLicense } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perLicense, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perLicenseComp } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perLicenseComp, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perHomeAddress } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perHomeAddress, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perMail } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perMail, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perHomePhone } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perHomePhone, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perCellphone } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perCellphone, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perGroup } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perGroup, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(createdbyid, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByUpdatedby(updatedbyid, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByPerParent = async (req, res) => {
    try {
        const { perParentId } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByPerParent(perParentId, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByPerParTypeDoc = async (req, res) => {
    try {
        const { perParTypeDocId } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByPerParTypeDoc(perParTypeDocId, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByPerParCity = async (req, res) => {
    try {
        const { perParCityId } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByPerParCity(perParCityId, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByPerParSex = async (req, res) => {
    try {
        const { perParSexId } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByPerParSex(perParSexId, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByPerParCountry = async (req, res) => {
    try {
        const { perParCountryId } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByPerParCountry(perParCountryId, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByPerParNacionality = async (req, res) => {
    try {
        const { perParNacionalityId } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByPerParNacionality(perParNacionalityId, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByPerParStatus = async (req, res) => {
    try {
        const { perParStatusId } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByPerParStatus(perParStatusId, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { perBirthday } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(perBirthday, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(dueat, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(createdat, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPeopleCtrl.filterVdsPeopleByCreatedby = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsPeople = await vdPersonService.filterVdsPeopleByCreatedby(updatedat, req.query);
        if (vdsPeople.length > 0) {
            util.setSuccess(200, 'VdsPeople retrieved', vdsPeople);
        } else {
            util.setSuccess(200, 'No vdPerson found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsPeopleCtrl;
//</es-section>
