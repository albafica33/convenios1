/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:09 GMT-0400 (GMT-04:00)
 * Time: 0:54:9
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:09 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:9
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdPersonaService = require('../services/vds_persona.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsPersonaCtrl = {};
vdsPersonaCtrl.service = vdPersonaService;


vdsPersonaCtrl.getAllVdsPersona = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsPersona.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsPersona.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsPersona] = await vdPersonaService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsPersona.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsPersona = await vdPersonaService.getAllVdsPersona(req.query);
        objVdsPersona = util.setRoot(objVdsPersona,req.query.root);
        if (objVdsPersona && objVdsPersona.rows && objVdsPersona.count) {
            util.setSuccess(200, 'VdsPersona retrieved', objVdsPersona.rows, objVdsPersona.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.getAVdPersona = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdPersona = await vdPersonaService.getAVdPersona(Id, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.addVdPersona = async (req, res) => {
    try {
        const objVdPersona = await vdPersonaService.addVdPersona(req.body);
        util.setSuccess(201, 'VdPersona Added!', objVdPersona);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.updateVdPersona = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdPersona = await vdPersonaService.updateVdPersona(Id, req.body);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdPersona updated', objVdPersona);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsPersonaCtrl.deleteVdPersona = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdPersona = await vdPersonaService.deleteVdPersona(Id);
        if (objVdPersona) {
            util.setSuccess(200, 'VdPersona deleted', objVdPersona);
        } else {
            util.setError(404, `VdPersona with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsPersonaCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdPersona = await vdPersonaService.findOneByUid(Id, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdPersona = await vdPersonaService.findOneById(id, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByEstado = async (req, res) => {
    try {
        const { estado } = req.params;
        const objVdPersona = await vdPersonaService.findOneByEstado(estado, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByTelefono = async (req, res) => {
    try {
        const { telefono } = req.params;
        const objVdPersona = await vdPersonaService.findOneByTelefono(telefono, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByCelular = async (req, res) => {
    try {
        const { celular } = req.params;
        const objVdPersona = await vdPersonaService.findOneByCelular(celular, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByNombres = async (req, res) => {
    try {
        const { nombres } = req.params;
        const objVdPersona = await vdPersonaService.findOneByNombres(nombres, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByPaterno = async (req, res) => {
    try {
        const { paterno } = req.params;
        const objVdPersona = await vdPersonaService.findOneByPaterno(paterno, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByMaterno = async (req, res) => {
    try {
        const { materno } = req.params;
        const objVdPersona = await vdPersonaService.findOneByMaterno(materno, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByCasada = async (req, res) => {
    try {
        const { casada } = req.params;
        const objVdPersona = await vdPersonaService.findOneByCasada(casada, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByCi = async (req, res) => {
    try {
        const { ci } = req.params;
        const objVdPersona = await vdPersonaService.findOneByCi(ci, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByCorreo = async (req, res) => {
    try {
        const { correo } = req.params;
        const objVdPersona = await vdPersonaService.findOneByCorreo(correo, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByDireccion = async (req, res) => {
    try {
        const { direccion } = req.params;
        const objVdPersona = await vdPersonaService.findOneByDireccion(direccion, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdPersona = await vdPersonaService.findOneByCreatedby(createdby, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdPersona = await vdPersonaService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByCiExpedido = async (req, res) => {
    try {
        const { ciExpedido } = req.params;
        const objVdPersona = await vdPersonaService.findOneByCiExpedido(ciExpedido, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByIdEstadoCivil = async (req, res) => {
    try {
        const { idEstadoCivil } = req.params;
        const objVdPersona = await vdPersonaService.findOneByIdEstadoCivil(idEstadoCivil, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByIdSexo = async (req, res) => {
    try {
        const { idSexo } = req.params;
        const objVdPersona = await vdPersonaService.findOneByIdSexo(idSexo, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByIdMunicipio = async (req, res) => {
    try {
        const { idMunicipio } = req.params;
        const objVdPersona = await vdPersonaService.findOneByIdMunicipio(idMunicipio, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByIdProvincia = async (req, res) => {
    try {
        const { idProvincia } = req.params;
        const objVdPersona = await vdPersonaService.findOneByIdProvincia(idProvincia, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByIdCiudad = async (req, res) => {
    try {
        const { idCiudad } = req.params;
        const objVdPersona = await vdPersonaService.findOneByIdCiudad(idCiudad, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByIdPais = async (req, res) => {
    try {
        const { idPais } = req.params;
        const objVdPersona = await vdPersonaService.findOneByIdPais(idPais, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdPersona = await vdPersonaService.findOneByDueat(dueat, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdPersona = await vdPersonaService.findOneByCreatedat(createdat, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdPersona = await vdPersonaService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdPersona) {
            util.setError(404, `Cannot find vdPersona with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdPersona', objVdPersona);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsPersonaCtrl.updateVdPersonaByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByUid(Id, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaById(id, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByEstado = async (req, res) => {
     const { estado } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByEstado(estado, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByTelefono = async (req, res) => {
     const { telefono } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByTelefono(telefono, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByCelular = async (req, res) => {
     const { celular } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByCelular(celular, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByNombres = async (req, res) => {
     const { nombres } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByNombres(nombres, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByPaterno = async (req, res) => {
     const { paterno } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByPaterno(paterno, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByMaterno = async (req, res) => {
     const { materno } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByMaterno(materno, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByCasada = async (req, res) => {
     const { casada } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByCasada(casada, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByCi = async (req, res) => {
     const { ci } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByCi(ci, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByCorreo = async (req, res) => {
     const { correo } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByCorreo(correo, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByDireccion = async (req, res) => {
     const { direccion } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByDireccion(direccion, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByCreatedby(createdby, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByUpdatedby(updatedby, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByCiExpedido = async (req, res) => {
     const { ciExpedido } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByCiExpedido(ciExpedido, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByIdEstadoCivil = async (req, res) => {
     const { idEstadoCivil } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByIdEstadoCivil(idEstadoCivil, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByIdSexo = async (req, res) => {
     const { idSexo } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByIdSexo(idSexo, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByIdMunicipio = async (req, res) => {
     const { idMunicipio } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByIdMunicipio(idMunicipio, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByIdProvincia = async (req, res) => {
     const { idProvincia } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByIdProvincia(idProvincia, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByIdCiudad = async (req, res) => {
     const { idCiudad } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByIdCiudad(idCiudad, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByIdPais = async (req, res) => {
     const { idPais } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByIdPais(idPais, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByDueat(dueat, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByCreatedat(createdat, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsPersonaCtrl.updateVdPersonaByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdPersona = await vdPersonaService.updateVdPersonaByUpdatedat(updatedat, req.body);
            if (!objVdPersona) {
                util.setError(404, `Cannot find vdPersona with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdPersona updated', objVdPersona);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsPersonaCtrl.findVdsCiudadCiExpedoWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPersona = await vdPersonaService.findVdsCiudadCiExpedoWithEstado(select, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findVdsEstadoCivilEstadoCivilWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPersona = await vdPersonaService.findVdsEstadoCivilEstadoCivilWithEstado(select, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findVdsSexoSexoWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPersona = await vdPersonaService.findVdsSexoSexoWithEstado(select, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findVdsMunicipioMunicipioWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPersona = await vdPersonaService.findVdsMunicipioMunicipioWithEstado(select, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findVdsProvinciaProvinciaWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPersona = await vdPersonaService.findVdsProvinciaProvinciaWithEstado(select, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findVdsCiudadCiudadWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPersona = await vdPersonaService.findVdsCiudadCiudadWithEstado(select, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.findVdsPaisPaisWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsPersona = await vdPersonaService.findVdsPaisPaisWithEstado(select, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(Id, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(id, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { estado } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(estado, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { telefono } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(telefono, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { celular } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(celular, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { nombres } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(nombres, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { paterno } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(paterno, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { materno } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(materno, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { casada } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(casada, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { ci } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(ci, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { correo } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(correo, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { direccion } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(direccion, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { createdby } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(createdby, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(updatedby, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { ciExpedido } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(ciExpedido, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByEstadoCivil = async (req, res) => {
    try {
        const { idEstadoCivil } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByEstadoCivil(idEstadoCivil, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaBySexo = async (req, res) => {
    try {
        const { idSexo } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaBySexo(idSexo, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByMunicipio = async (req, res) => {
    try {
        const { idMunicipio } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByMunicipio(idMunicipio, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByProvincia = async (req, res) => {
    try {
        const { idProvincia } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByProvincia(idProvincia, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiudad = async (req, res) => {
    try {
        const { idCiudad } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiudad(idCiudad, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByPais = async (req, res) => {
    try {
        const { idPais } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByPais(idPais, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(dueat, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(createdat, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsPersonaCtrl.filterVdsPersonaByCiExpedo = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsPersona = await vdPersonaService.filterVdsPersonaByCiExpedo(updatedat, req.query);
        if (vdsPersona.length > 0) {
            util.setSuccess(200, 'VdsPersona retrieved', vdsPersona);
        } else {
            util.setSuccess(200, 'No vdPersona found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsPersonaCtrl;
//</es-section>
