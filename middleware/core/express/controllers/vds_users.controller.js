/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Time: 0:54:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:13
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdUserService = require('../services/vds_users.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsUsersCtrl = {};
vdsUsersCtrl.service = vdUserService;


vdsUsersCtrl.getAllVdsUsers = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsUsers.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsUsers.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsUsers] = await vdUserService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsUsers.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsUsers = await vdUserService.getAllVdsUsers(req.query);
        objVdsUsers = util.setRoot(objVdsUsers,req.query.root);
        if (objVdsUsers && objVdsUsers.rows && objVdsUsers.count) {
            util.setSuccess(200, 'VdsUsers retrieved', objVdsUsers.rows, objVdsUsers.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.getAVdUser = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdUser = await vdUserService.getAVdUser(Id, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.addVdUser = async (req, res) => {
    try {
        const objVdUser = await vdUserService.addVdUser(req.body);
        util.setSuccess(201, 'VdUser Added!', objVdUser);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.updateVdUser = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdUser = await vdUserService.updateVdUser(Id, req.body);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdUser updated', objVdUser);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsUsersCtrl.deleteVdUser = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdUser = await vdUserService.deleteVdUser(Id);
        if (objVdUser) {
            util.setSuccess(200, 'VdUser deleted', objVdUser);
        } else {
            util.setError(404, `VdUser with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsUsersCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdUser = await vdUserService.findOneByUid(Id, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdUser = await vdUserService.findOneById(id, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByEstado = async (req, res) => {
    try {
        const { estado } = req.params;
        const objVdUser = await vdUserService.findOneByEstado(estado, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByUserName = async (req, res) => {
    try {
        const { userName } = req.params;
        const objVdUser = await vdUserService.findOneByUserName(userName, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByUserHash = async (req, res) => {
    try {
        const { userHash } = req.params;
        const objVdUser = await vdUserService.findOneByUserHash(userHash, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByCorreo = async (req, res) => {
    try {
        const { correo } = req.params;
        const objVdUser = await vdUserService.findOneByCorreo(correo, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByRecordatorio = async (req, res) => {
    try {
        const { recordatorio } = req.params;
        const objVdUser = await vdUserService.findOneByRecordatorio(recordatorio, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdUser = await vdUserService.findOneByCreatedby(createdby, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdUser = await vdUserService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByIdPersona = async (req, res) => {
    try {
        const { idPersona } = req.params;
        const objVdUser = await vdUserService.findOneByIdPersona(idPersona, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdUser = await vdUserService.findOneByDueat(dueat, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdUser = await vdUserService.findOneByCreatedat(createdat, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdUser = await vdUserService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdUser) {
            util.setError(404, `Cannot find vdUser with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUser', objVdUser);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsUsersCtrl.updateVdUserByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByUid(Id, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserById(id, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByEstado = async (req, res) => {
     const { estado } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByEstado(estado, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByUserName = async (req, res) => {
     const { userName } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByUserName(userName, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByUserHash = async (req, res) => {
     const { userHash } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByUserHash(userHash, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByCorreo = async (req, res) => {
     const { correo } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByCorreo(correo, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByRecordatorio = async (req, res) => {
     const { recordatorio } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByRecordatorio(recordatorio, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByCreatedby(createdby, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByUpdatedby(updatedby, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByIdPersona = async (req, res) => {
     const { idPersona } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByIdPersona(idPersona, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByDueat(dueat, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByCreatedat(createdat, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUsersCtrl.updateVdUserByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUser = await vdUserService.updateVdUserByUpdatedat(updatedat, req.body);
            if (!objVdUser) {
                util.setError(404, `Cannot find vdUser with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUser updated', objVdUser);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsUsersCtrl.findVdsPersonaPersonaWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsUsers = await vdUserService.findVdsPersonaPersonaWithEstado(select, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(Id, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(id, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { estado } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(estado, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { userName } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(userName, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { userHash } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(userHash, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { correo } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(correo, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { recordatorio } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(recordatorio, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { createdby } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(createdby, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(updatedby, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { idPersona } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(idPersona, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(dueat, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(createdat, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUsersCtrl.filterVdsUsersByPersona = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsUsers = await vdUserService.filterVdsUsersByPersona(updatedat, req.query);
        if (vdsUsers.length > 0) {
            util.setSuccess(200, 'VdsUsers retrieved', vdsUsers);
        } else {
            util.setSuccess(200, 'No vdUser found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsUsersCtrl;
//</es-section>
