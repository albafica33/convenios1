/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:14 GMT-0400 (GMT-04:00)
 * Time: 0:54:14
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:14 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:14
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdUserRoleService = require('../services/vds_user_roles.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsUserRolesCtrl = {};
vdsUserRolesCtrl.service = vdUserRoleService;


vdsUserRolesCtrl.getAllVdsUserRoles = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsUserRoles.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsUserRoles.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsUserRoles] = await vdUserRoleService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsUserRoles.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsUserRoles = await vdUserRoleService.getAllVdsUserRoles(req.query);
        objVdsUserRoles = util.setRoot(objVdsUserRoles,req.query.root);
        if (objVdsUserRoles && objVdsUserRoles.rows && objVdsUserRoles.count) {
            util.setSuccess(200, 'VdsUserRoles retrieved', objVdsUserRoles.rows, objVdsUserRoles.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.getAVdUserRole = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdUserRole = await vdUserRoleService.getAVdUserRole(Id, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.addVdUserRole = async (req, res) => {
    try {
        const objVdUserRole = await vdUserRoleService.addVdUserRole(req.body);
        util.setSuccess(201, 'VdUserRole Added!', objVdUserRole);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.updateVdUserRole = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdUserRole = await vdUserRoleService.updateVdUserRole(Id, req.body);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsUserRolesCtrl.deleteVdUserRole = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdUserRole = await vdUserRoleService.deleteVdUserRole(Id);
        if (objVdUserRole) {
            util.setSuccess(200, 'VdUserRole deleted', objVdUserRole);
        } else {
            util.setError(404, `VdUserRole with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsUserRolesCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByUid(Id, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneById(id, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByUsrRolGroup = async (req, res) => {
    try {
        const { usrRolGroup } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByUsrRolGroup(usrRolGroup, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByUsrId = async (req, res) => {
    try {
        const { usrId } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByUsrId(usrId, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByRolId = async (req, res) => {
    try {
        const { rolId } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByRolId(rolId, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByUsrRolParStatusId = async (req, res) => {
    try {
        const { usrRolParStatusId } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByUsrRolParStatusId(usrRolParStatusId, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByCreatedbyid = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByCreatedbyid(createdbyid, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByUpdatedbyid = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByUpdatedbyid(updatedbyid, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByDueat(dueat, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByCreatedat(createdat, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdUserRole = await vdUserRoleService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdUserRole) {
            util.setError(404, `Cannot find vdUserRole with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdUserRole', objVdUserRole);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsUserRolesCtrl.updateVdUserRoleByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByUid(Id, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleById(id, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByUsrRolGroup = async (req, res) => {
     const { usrRolGroup } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByUsrRolGroup(usrRolGroup, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByUsrId = async (req, res) => {
     const { usrId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByUsrId(usrId, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByRolId = async (req, res) => {
     const { rolId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByRolId(rolId, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByUsrRolParStatusId = async (req, res) => {
     const { usrRolParStatusId } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByUsrRolParStatusId(usrRolParStatusId, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByCreatedbyid = async (req, res) => {
     const { createdbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByCreatedbyid(createdbyid, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByUpdatedbyid = async (req, res) => {
     const { updatedbyid } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByUpdatedbyid(updatedbyid, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByDueat(dueat, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByCreatedat(createdat, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsUserRolesCtrl.updateVdUserRoleByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdUserRole = await vdUserRoleService.updateVdUserRoleByUpdatedat(updatedat, req.body);
            if (!objVdUserRole) {
                util.setError(404, `Cannot find vdUserRole with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdUserRole updated', objVdUserRole);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}



vdsUserRolesCtrl.findVdsUsersUsrWithEstado = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsUserRoles = await vdUserRoleService.findVdsUsersUsrWithEstado(select, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findVdsRolesRolWithRolCode = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsUserRoles = await vdUserRoleService.findVdsRolesRolWithRolCode(select, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findVdsParamsUsrRolParStatusWithParOrder = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsUserRoles = await vdUserRoleService.findVdsParamsUsrRolParStatusWithParOrder(select, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findVdsUserRolesCreatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsUserRoles = await vdUserRoleService.findVdsUserRolesCreatedbyWithUsrRolGroup(select, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.findVdsUserRolesUpdatedbyWithUsrRolGroup = async (req, res) => {
    try {
        const { select } = req.params;
        const vdsUserRoles = await vdUserRoleService.findVdsUserRolesUpdatedbyWithUsrRolGroup(select, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsUserRolesCtrl.filterVdsUserRolesByUsr = async (req, res) => {
    try {
        const { Id } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUsr(Id, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByUsr = async (req, res) => {
    try {
        const { id } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUsr(id, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByUsr = async (req, res) => {
    try {
        const { usrRolGroup } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUsr(usrRolGroup, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByUsr = async (req, res) => {
    try {
        const { usrId } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUsr(usrId, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByRol = async (req, res) => {
    try {
        const { rolId } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByRol(rolId, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByUsrRolParStatus = async (req, res) => {
    try {
        const { usrRolParStatusId } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUsrRolParStatus(usrRolParStatusId, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByCreatedby = async (req, res) => {
    try {
        const { createdbyid } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByCreatedby(createdbyid, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByUpdatedby = async (req, res) => {
    try {
        const { updatedbyid } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUpdatedby(updatedbyid, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByUsr = async (req, res) => {
    try {
        const { dueat } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUsr(dueat, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByUsr = async (req, res) => {
    try {
        const { createdat } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUsr(createdat, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsUserRolesCtrl.filterVdsUserRolesByUsr = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const vdsUserRoles = await vdUserRoleService.filterVdsUserRolesByUsr(updatedat, req.query);
        if (vdsUserRoles.length > 0) {
            util.setSuccess(200, 'VdsUserRoles retrieved', vdsUserRoles);
        } else {
            util.setSuccess(200, 'No vdUserRole found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



//</es-section>

//<es-section>
module.exports = vdsUserRolesCtrl;
//</es-section>
