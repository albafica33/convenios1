/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Time: 0:54:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 00:54:13 GMT-0400 (GMT-04:00)
 * Last time updated: 0:54:13
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../express');
const vdSexoService = require('../services/vds_sexo.service');
//</es-section>
require('../../../utils/Prototipes');
const Util = require('../../../utils/Utils');
const util = new Util();

// Controller for DB Mongoose

//<es-section>

//</es-section>

//<es-section>

const vdsSexoCtrl = {};
vdsSexoCtrl.service = vdSexoService;


vdsSexoCtrl.getAllVdsSexo = async (req, res) => {
    try {
        const { length } = req.body;
        const { limit } = req.body;
        const { start } = req.body;
        const { root } = req.body;
        const [column, dir] = util.getOrderByColumnDirection(models.sequelize.vdsSexo.rawAttributes,req.body);
        const [columns] = util.getDatatableColumns(models.sequelize.vdsSexo.rawAttributes,req.body);
        // const [searchPanes,searches,userVdsSexo] = await vdSexoService.setSearchPanes(req.body,req.query,columns);
		const [where] = util.getSearchableFields(models.sequelize.vdsSexo.rawAttributes,req.body,req.query, columns);
        req.query.where = where ? where : req.query.where;
        req.query.limit = length ? length : limit ? limit : req.query.limit;
        req.query.offset = start ? start : req.query.offset;
        req.query.root = root ? root : req.query.root;
        req.query.order = column && dir ? [[column,dir]] : req.query.order;

        let objVdsSexo = await vdSexoService.getAllVdsSexo(req.query);
        objVdsSexo = util.setRoot(objVdsSexo,req.query.root);
        if (objVdsSexo && objVdsSexo.rows && objVdsSexo.count) {
            util.setSuccess(200, 'VdsSexo retrieved', objVdsSexo.rows, objVdsSexo.count, req.query.limit, req.query.offset, columns);
        } else {
            util.setSuccess(200, 'No vdSexo found');
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.getAVdSexo = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdSexo = await vdSexoService.getAVdSexo(Id, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the id ${Id}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.addVdSexo = async (req, res) => {
    try {
        const objVdSexo = await vdSexoService.addVdSexo(req.body);
        util.setSuccess(201, 'VdSexo Added!', objVdSexo);
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.updateVdSexo = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please input a valid String value');
            return util.send(res);
        }
        const objVdSexo = await vdSexoService.updateVdSexo(Id, req.body);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
        } else {
            util.setSuccess(200, 'VdSexo updated', objVdSexo);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};

vdsSexoCtrl.deleteVdSexo = async (req, res) => {
    try {
        const { Id } = req.params;
        if (!util.isString(Id)) {
            util.setError(400, 'Please provide a String value');
            return util.send(res);
        }
        const objVdSexo = await vdSexoService.deleteVdSexo(Id);
        if (objVdSexo) {
            util.setSuccess(200, 'VdSexo deleted', objVdSexo);
        } else {
            util.setError(404, `VdSexo with the id ${Id} cannot be found`);
        }
        return util.send(res);
    } catch (e) {
        util.setError(400, e);
        return util.send(res);
    }
};



vdsSexoCtrl.findOneByUid = async (req, res) => {
    try {
        const { Id } = req.params;
        const objVdSexo = await vdSexoService.findOneByUid(Id, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.findOneById = async (req, res) => {
    try {
        const { id } = req.params;
        const objVdSexo = await vdSexoService.findOneById(id, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.findOneByEstado = async (req, res) => {
    try {
        const { estado } = req.params;
        const objVdSexo = await vdSexoService.findOneByEstado(estado, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.findOneByDescripcion = async (req, res) => {
    try {
        const { descripcion } = req.params;
        const objVdSexo = await vdSexoService.findOneByDescripcion(descripcion, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.findOneByCreatedby = async (req, res) => {
    try {
        const { createdby } = req.params;
        const objVdSexo = await vdSexoService.findOneByCreatedby(createdby, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.findOneByUpdatedby = async (req, res) => {
    try {
        const { updatedby } = req.params;
        const objVdSexo = await vdSexoService.findOneByUpdatedby(updatedby, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.findOneByDueat = async (req, res) => {
    try {
        const { dueat } = req.params;
        const objVdSexo = await vdSexoService.findOneByDueat(dueat, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.findOneByCreatedat = async (req, res) => {
    try {
        const { createdat } = req.params;
        const objVdSexo = await vdSexoService.findOneByCreatedat(createdat, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}

vdsSexoCtrl.findOneByUpdatedat = async (req, res) => {
    try {
        const { updatedat } = req.params;
        const objVdSexo = await vdSexoService.findOneByUpdatedat(updatedat, req.query);
        if (!objVdSexo) {
            util.setError(404, `Cannot find vdSexo with the lcObjLocalCommonFieldName ${lcObjLocalCommonFieldName}`);
        } else {
            util.setSuccess(200, 'Found vdSexo', objVdSexo);
        }
        return util.send(res);
    } catch(e) {
        util.setError(400, e);
        return util.send(res);
    }
}



vdsSexoCtrl.updateVdSexoByUid = async (req, res) => {
     const { Id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoByUid(Id, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsSexoCtrl.updateVdSexoById = async (req, res) => {
     const { id } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoById(id, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsSexoCtrl.updateVdSexoByEstado = async (req, res) => {
     const { estado } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoByEstado(estado, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsSexoCtrl.updateVdSexoByDescripcion = async (req, res) => {
     const { descripcion } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoByDescripcion(descripcion, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsSexoCtrl.updateVdSexoByCreatedby = async (req, res) => {
     const { createdby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoByCreatedby(createdby, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsSexoCtrl.updateVdSexoByUpdatedby = async (req, res) => {
     const { updatedby } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoByUpdatedby(updatedby, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsSexoCtrl.updateVdSexoByDueat = async (req, res) => {
     const { dueat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoByDueat(dueat, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsSexoCtrl.updateVdSexoByCreatedat = async (req, res) => {
     const { createdat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoByCreatedat(createdat, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}

vdsSexoCtrl.updateVdSexoByUpdatedat = async (req, res) => {
     const { updatedat } = req.params;
        try {
            if (!util.isString(Id)) {
                util.setError(400, 'Please input a valid String value');
                return util.send(res);
            }
            const objVdSexo = await vdSexoService.updateVdSexoByUpdatedat(updatedat, req.body);
            if (!objVdSexo) {
                util.setError(404, `Cannot find vdSexo with the id: ${Id}`);
            } else {
                util.setSuccess(200, 'VdSexo updated', objVdSexo);
            }
            return util.send(res);
        } catch (e) {
            util.setError(400, e);
            return util.send(res);
        }
}







//</es-section>

//<es-section>
module.exports = vdsSexoCtrl;
//</es-section>
