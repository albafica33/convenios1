/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:16 GMT-0400 (Bolivia Time)
 * Time: 1:54:16
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:16 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:16
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsPersona = sequelize.define('vdsPersona', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      estado: DataTypes.INTEGER,
      
      
      
      nombres: DataTypes.STRING,
      
      paterno: DataTypes.STRING,
      
      materno: DataTypes.STRING,
      
      casada: DataTypes.STRING,
      
      ci: DataTypes.STRING,
      
      correo: DataTypes.STRING,
      
      direccion: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      
      
      ci_expedido: DataTypes.STRING,
      
      id_estado_civil: DataTypes.STRING,
      
      id_sexo: DataTypes.STRING,
      
      id_municipio: DataTypes.STRING,
      
      id_provincia: DataTypes.STRING,
      
      id_ciudad: DataTypes.STRING,
      
      id_pais: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      telefono: DataTypes.DECIMAL,
      
      celular: DataTypes.DECIMAL,
      
      
      
    }, {
      tableName:'vds_persona',
      timestamps: false,
    });
    vdsPersona.associate = (models) => {
      
      models.vdsPersona.belongsTo(models.vdsCiudad,{foreignKey:'ci_expedido', targetKey: '_id',  as:'vdPersonaCiExpedo'});
      models.vdsCiudad.hasMany(models.vdsPersona,{foreignKey:'ci_expedido', sourceKey: '_id', as:'vdPersonaCiExpedo'});
      
      models.vdsPersona.belongsTo(models.vdsEstadoCivil,{foreignKey:'id_estado_civil', targetKey: '_id',  as:'vdPersonaEstadoCivil'});
      models.vdsEstadoCivil.hasMany(models.vdsPersona,{foreignKey:'id_estado_civil', sourceKey: '_id', as:'vdPersonaEstadoCivil'});
      
      models.vdsPersona.belongsTo(models.vdsSexo,{foreignKey:'id_sexo', targetKey: '_id',  as:'vdPersonaSexo'});
      models.vdsSexo.hasMany(models.vdsPersona,{foreignKey:'id_sexo', sourceKey: '_id', as:'vdPersonaSexo'});
      
      models.vdsPersona.belongsTo(models.vdsMunicipio,{foreignKey:'id_municipio', targetKey: '_id',  as:'vdPersonaMunicipio'});
      models.vdsMunicipio.hasMany(models.vdsPersona,{foreignKey:'id_municipio', sourceKey: '_id', as:'vdPersonaMunicipio'});
      
      models.vdsPersona.belongsTo(models.vdsProvincia,{foreignKey:'id_provincia', targetKey: '_id',  as:'vdPersonaProvincia'});
      models.vdsProvincia.hasMany(models.vdsPersona,{foreignKey:'id_provincia', sourceKey: '_id', as:'vdPersonaProvincia'});
      
      models.vdsPersona.belongsTo(models.vdsCiudad,{foreignKey:'id_ciudad', targetKey: '_id',  as:'vdPersonaCiudad'});
      models.vdsCiudad.hasMany(models.vdsPersona,{foreignKey:'id_ciudad', sourceKey: '_id', as:'vdPersonaCiudad'});
      
      models.vdsPersona.belongsTo(models.vdsPais,{foreignKey:'id_pais', targetKey: '_id',  as:'vdPersonaPais'});
      models.vdsPais.hasMany(models.vdsPersona,{foreignKey:'id_pais', sourceKey: '_id', as:'vdPersonaPais'});
      
    };
    return vdsPersona;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsPersona", new Schema({
    
    estado: {type: Number},
    
    
    id: {type: Number},
    
    
    nombres: {type: String},
    
    paterno: {type: String},
    
    materno: {type: String},
    
    casada: {type: String},
    
    ci: {type: String},
    
    correo: {type: String},
    
    direccion: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    
    
    
    ci_expedido: {type: mongoose.Types.ObjectId},
    
    id_estado_civil: {type: mongoose.Types.ObjectId},
    
    id_sexo: {type: mongoose.Types.ObjectId},
    
    id_municipio: {type: mongoose.Types.ObjectId},
    
    id_provincia: {type: mongoose.Types.ObjectId},
    
    id_ciudad: {type: mongoose.Types.ObjectId},
    
    id_pais: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    telefono: {type: mongoose.Types.Decimal128},
    
    celular: {type: mongoose.Types.Decimal128},
    
    
    
  }),'vds_persona');
  //</es-section>
