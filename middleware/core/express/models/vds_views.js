/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:26 GMT-0400 (Bolivia Time)
 * Time: 1:54:26
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:26 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:26
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsViews = sequelize.define('vdsViews', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      vie_code: DataTypes.STRING,
      
      vie_description: DataTypes.STRING,
      
      vie_route: DataTypes.STRING,
      
      vie_params: DataTypes.STRING,
      
      vie_icon: DataTypes.STRING,
      
      vie_group: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      
      
      vie_module_id: DataTypes.STRING,
      
      vie_return_id: DataTypes.STRING,
      
      vie_par_status_id: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_views',
      timestamps: false,
    });
    vdsViews.associate = (models) => {
      
      models.vdsViews.belongsTo(models.vdsModules,{foreignKey:'vie_module_id', targetKey: '_id',  as:'vdViewVieModule'});
      models.vdsModules.hasMany(models.vdsViews,{foreignKey:'vie_module_id', sourceKey: '_id', as:'vdViewVieModule'});
      
      models.vdsViews.belongsTo(models.vdsViews,{foreignKey:'vie_return_id', targetKey: '_id',  as:'vdViewVieReturn'});

      models.vdsViews.belongsTo(models.vdsParams,{foreignKey:'vie_par_status_id', targetKey: '_id',  as:'vdViewVieParStatus'});
      models.vdsParams.hasMany(models.vdsViews,{foreignKey:'vie_par_status_id', sourceKey: '_id', as:'vdViewVieParStatus'});
      
    };
    return vdsViews;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsViews", new Schema({
    
    
    id: {type: Number},
    
    
    vie_code: {type: String},
    
    vie_description: {type: String},
    
    vie_route: {type: String},
    
    vie_params: {type: String},
    
    vie_icon: {type: String},
    
    vie_group: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    
    
    
    vie_module_id: {type: mongoose.Types.ObjectId},
    
    vie_return_id: {type: mongoose.Types.ObjectId},
    
    vie_par_status_id: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_views');
  //</es-section>
