/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:03 GMT-0400 (Bolivia Time)
 * Time: 1:54:3
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:03 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:3
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsEstadoCivil = sequelize.define('vdsEstadoCivil', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      estado: DataTypes.INTEGER,
      
      
      
      descripcion: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_estado_civil',
      timestamps: false,
    });
    vdsEstadoCivil.associate = (models) => {
      
    };
    return vdsEstadoCivil;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsEstadoCivil", new Schema({
    
    estado: {type: Number},
    
    
    id: {type: Number},
    
    
    descripcion: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    
    
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_estado_civil');
  //</es-section>
