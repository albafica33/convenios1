/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:24 GMT-0400 (Bolivia Time)
 * Time: 1:54:24
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:24 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:24
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsUserRoles = sequelize.define('vdsUserRoles', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      usr_rol_group: DataTypes.STRING,
      
      
      
      usr_id: DataTypes.STRING,
      
      rol_id: DataTypes.STRING,
      
      usr_rol_par_status_id: DataTypes.STRING,
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_user_roles',
      timestamps: false,
    });
    vdsUserRoles.associate = (models) => {
      
      models.vdsUserRoles.belongsTo(models.vdsUsers,{foreignKey:'usr_id', targetKey: '_id',  as:'vdUserRoleUsr'});
      models.vdsUsers.hasMany(models.vdsUserRoles,{foreignKey:'usr_id', sourceKey: '_id', as:'vdUserRoleUsr'});
      
      models.vdsUserRoles.belongsTo(models.vdsRoles,{foreignKey:'rol_id', targetKey: '_id',  as:'vdUserRoleRol'});
      models.vdsRoles.hasMany(models.vdsUserRoles,{foreignKey:'rol_id', sourceKey: '_id', as:'vdUserRoleRol'});
      
      models.vdsUserRoles.belongsTo(models.vdsParams,{foreignKey:'usr_rol_par_status_id', targetKey: '_id',  as:'vdUserRoleUsrRolParStatus'});
      models.vdsParams.hasMany(models.vdsUserRoles,{foreignKey:'usr_rol_par_status_id', sourceKey: '_id', as:'vdUserRoleUsrRolParStatus'});
      
      models.vdsUserRoles.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdUserRoleCreatedby'});

      models.vdsUserRoles.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdUserRoleUpdatedby'});

    };
    return vdsUserRoles;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsUserRoles", new Schema({
    
    
    id: {type: Number},
    
    
    usr_rol_group: {type: String},
    
    
    
    
    usr_id: {type: mongoose.Types.ObjectId},
    
    rol_id: {type: mongoose.Types.ObjectId},
    
    usr_rol_par_status_id: {type: mongoose.Types.ObjectId},
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_user_roles');
  //</es-section>
