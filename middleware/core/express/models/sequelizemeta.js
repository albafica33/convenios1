/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:01 GMT-0400 (Bolivia Time)
 * Time: 1:54:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:01 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:1
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const sequelizemeta = sequelize.define('sequelizemeta', {
      
      name: { type: DataTypes.STRING, primaryKey: true },
      
      
      
      
      
      
      
      
      
      
      
      
      
      
    }, {
      tableName:'sequelizemeta',
      timestamps: false,
    });
    sequelizemeta.associate = (models) => {
      
    };
    return sequelizemeta;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("sequelizemeta", new Schema({
    
    
    
    
    
    
    
    
    
    
    
    
    
  }),'sequelizemeta');
  //</es-section>
