/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:13 GMT-0400 (Bolivia Time)
 * Time: 1:54:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:13 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:13
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsPeople = sequelize.define('vdsPeople', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      per_first_name: DataTypes.STRING,
      
      per_second_name: DataTypes.STRING,
      
      per_first_lastname: DataTypes.STRING,
      
      per_second_lastname: DataTypes.STRING,
      
      per_license: DataTypes.STRING,
      
      per_license_comp: DataTypes.STRING,
      
      per_home_address: DataTypes.STRING,
      
      per_mail: DataTypes.STRING,
      
      per_home_phone: DataTypes.STRING,
      
      per_cellphone: DataTypes.STRING,
      
      per_group: DataTypes.STRING,
      
      
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      per_parent_id: DataTypes.STRING,
      
      per_par_type_doc_id: DataTypes.STRING,
      
      per_par_city_id: DataTypes.STRING,
      
      per_par_sex_id: DataTypes.STRING,
      
      per_par_country_id: DataTypes.STRING,
      
      per_par_nacionality_id: DataTypes.STRING,
      
      per_par_status_id: DataTypes.STRING,
      
      
      per_birthday: DataTypes.DATE,
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_people',
      timestamps: false,
    });
    vdsPeople.associate = (models) => {
      
      models.vdsPeople.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdPersonCreatedby'});
      models.vdsUserRoles.hasMany(models.vdsPeople,{foreignKey:'createdById', sourceKey: '_id', as:'vdPersonCreatedby'});
      
      models.vdsPeople.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdPersonUpdatedby'});
      models.vdsUserRoles.hasMany(models.vdsPeople,{foreignKey:'updatedById', sourceKey: '_id', as:'vdPersonUpdatedby'});
      
      models.vdsPeople.belongsTo(models.vdsPeople,{foreignKey:'per_parent_id', targetKey: '_id',  as:'vdPersonPerParent'});

      models.vdsPeople.belongsTo(models.vdsParams,{foreignKey:'per_par_type_doc_id', targetKey: '_id',  as:'vdPersonPerParTypeDoc'});
      models.vdsParams.hasMany(models.vdsPeople,{foreignKey:'per_par_type_doc_id', sourceKey: '_id', as:'vdPersonPerParTypeDoc'});
      
      models.vdsPeople.belongsTo(models.vdsParams,{foreignKey:'per_par_city_id', targetKey: '_id',  as:'vdPersonPerParCity'});
      models.vdsParams.hasMany(models.vdsPeople,{foreignKey:'per_par_city_id', sourceKey: '_id', as:'vdPersonPerParCity'});
      
      models.vdsPeople.belongsTo(models.vdsParams,{foreignKey:'per_par_sex_id', targetKey: '_id',  as:'vdPersonPerParSex'});
      models.vdsParams.hasMany(models.vdsPeople,{foreignKey:'per_par_sex_id', sourceKey: '_id', as:'vdPersonPerParSex'});
      
      models.vdsPeople.belongsTo(models.vdsParams,{foreignKey:'per_par_country_id', targetKey: '_id',  as:'vdPersonPerParCountry'});
      models.vdsParams.hasMany(models.vdsPeople,{foreignKey:'per_par_country_id', sourceKey: '_id', as:'vdPersonPerParCountry'});
      
      models.vdsPeople.belongsTo(models.vdsParams,{foreignKey:'per_par_nacionality_id', targetKey: '_id',  as:'vdPersonPerParNacionality'});
      models.vdsParams.hasMany(models.vdsPeople,{foreignKey:'per_par_nacionality_id', sourceKey: '_id', as:'vdPersonPerParNacionality'});
      
      models.vdsPeople.belongsTo(models.vdsParams,{foreignKey:'per_par_status_id', targetKey: '_id',  as:'vdPersonPerParStatus'});
      models.vdsParams.hasMany(models.vdsPeople,{foreignKey:'per_par_status_id', sourceKey: '_id', as:'vdPersonPerParStatus'});
      
    };
    return vdsPeople;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsPeople", new Schema({
    
    
    id: {type: Number},
    
    
    per_first_name: {type: String},
    
    per_second_name: {type: String},
    
    per_first_lastname: {type: String},
    
    per_second_lastname: {type: String},
    
    per_license: {type: String},
    
    per_license_comp: {type: String},
    
    per_home_address: {type: String},
    
    per_mail: {type: String},
    
    per_home_phone: {type: String},
    
    per_cellphone: {type: String},
    
    per_group: {type: String},
    
    
    
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    per_parent_id: {type: mongoose.Types.ObjectId},
    
    per_par_type_doc_id: {type: mongoose.Types.ObjectId},
    
    per_par_city_id: {type: mongoose.Types.ObjectId},
    
    per_par_sex_id: {type: mongoose.Types.ObjectId},
    
    per_par_country_id: {type: mongoose.Types.ObjectId},
    
    per_par_nacionality_id: {type: mongoose.Types.ObjectId},
    
    per_par_status_id: {type: mongoose.Types.ObjectId},
    
    
    per_birthday: {type: Date},
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_people');
  //</es-section>
