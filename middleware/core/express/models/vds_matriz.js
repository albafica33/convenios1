/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:07 GMT-0400 (Bolivia Time)
 * Time: 1:54:7
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:07 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:7
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsMatriz = sequelize.define('vdsMatriz', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      estado_cumplimiento: DataTypes.INTEGER,
      
      
      
      
      compromiso: DataTypes.TEXT,
      
      entidad_responsable: DataTypes.TEXT,
      
      contacto: DataTypes.TEXT,
      
      observacion: DataTypes.TEXT,
      
      tema: DataTypes.TEXT,
      
      fecha_cumplimiento: DataTypes.TEXT,
      
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      
      fecha_modificacion: DataTypes.DATE,
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_matriz',
      timestamps: false,
    });
    vdsMatriz.associate = (models) => {
      
      models.vdsMatriz.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdMatrizCreatedby'});
      models.vdsUserRoles.hasMany(models.vdsMatriz,{foreignKey:'createdById', sourceKey: '_id', as:'vdMatrizCreatedby'});
      
      models.vdsMatriz.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdMatrizUpdatedby'});
      models.vdsUserRoles.hasMany(models.vdsMatriz,{foreignKey:'updatedById', sourceKey: '_id', as:'vdMatrizUpdatedby'});
      
    };
    return vdsMatriz;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsMatriz", new Schema({
    
    estado_cumplimiento: {type: Number},
    
    
    id: {type: Number},
    
    
    
    compromiso: {type: String},
    
    entidad_responsable: {type: String},
    
    contacto: {type: String},
    
    observacion: {type: String},
    
    tema: {type: String},
    
    fecha_cumplimiento: {type: String},
    
    
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    
    fecha_modificacion: {type: Date},
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_matriz');
  //</es-section>
