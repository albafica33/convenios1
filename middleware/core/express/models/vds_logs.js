/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:04 GMT-0400 (Bolivia Time)
 * Time: 1:54:4
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:04 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:4
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsLogs = sequelize.define('vdsLogs', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      log_obj_id: DataTypes.STRING,
      
      log_description: DataTypes.STRING,
      
      log_group: DataTypes.STRING,
      
      
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      log_par_status_id: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_logs',
      timestamps: false,
    });
    vdsLogs.associate = (models) => {
      
      models.vdsLogs.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdLogCreatedby'});
      models.vdsUserRoles.hasMany(models.vdsLogs,{foreignKey:'createdById', sourceKey: '_id', as:'vdLogCreatedby'});
      
      models.vdsLogs.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdLogUpdatedby'});
      models.vdsUserRoles.hasMany(models.vdsLogs,{foreignKey:'updatedById', sourceKey: '_id', as:'vdLogUpdatedby'});
      
      models.vdsLogs.belongsTo(models.vdsParams,{foreignKey:'log_par_status_id', targetKey: '_id',  as:'vdLogLogParStatus'});
      models.vdsParams.hasMany(models.vdsLogs,{foreignKey:'log_par_status_id', sourceKey: '_id', as:'vdLogLogParStatus'});
      
    };
    return vdsLogs;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsLogs", new Schema({
    
    
    id: {type: Number},
    
    
    log_obj_id: {type: String},
    
    log_description: {type: String},
    
    log_group: {type: String},
    
    
    
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    log_par_status_id: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_logs');
  //</es-section>
