/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:23 GMT-0400 (Bolivia Time)
 * Time: 1:54:23
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:23 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:23
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsUsers = sequelize.define('vdsUsers', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      estado: DataTypes.INTEGER,
      
      
      
      user_name: DataTypes.STRING,
      
      user_hash: DataTypes.STRING,
      
      correo: DataTypes.STRING,
      
      recordatorio: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      
      
      id_persona: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_users',
      timestamps: false,
    });
    vdsUsers.associate = (models) => {
      
      models.vdsUsers.belongsTo(models.vdsPersona,{foreignKey:'id_persona', targetKey: '_id',  as:'vdUserPersona'});
      models.vdsPersona.hasMany(models.vdsUsers,{foreignKey:'id_persona', sourceKey: '_id', as:'vdUserPersona'});
      
    };
    return vdsUsers;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsUsers", new Schema({
    
    estado: {type: Number},
    
    
    id: {type: Number},
    
    
    user_name: {type: String},
    
    user_hash: {type: String},
    
    correo: {type: String},
    
    recordatorio: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    
    
    
    id_persona: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_users');
  //</es-section>
