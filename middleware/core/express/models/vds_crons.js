/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:01 GMT-0400 (Bolivia Time)
 * Time: 1:54:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:01 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:1
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsCrons = sequelize.define('vdsCrons', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      cro_description: DataTypes.STRING,
      
      cro_expression: DataTypes.STRING,
      
      cro_group: DataTypes.STRING,
      
      cro_mai_id: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      cro_function: DataTypes.STRING,
      
      
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      cro_status: DataTypes.DECIMAL,
      
      
      
    }, {
      tableName:'vds_crons',
      timestamps: false,
    });
    vdsCrons.associate = (models) => {
      
    };
    return vdsCrons;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsCrons", new Schema({
    
    
    id: {type: Number},
    
    
    cro_description: {type: String},
    
    cro_expression: {type: String},
    
    cro_group: {type: String},
    
    cro_mai_id: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    cro_function: {type: String},
    
    
    
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    cro_status: {type: mongoose.Types.Decimal128},
    
    
    
  }),'vds_crons');
  //</es-section>
