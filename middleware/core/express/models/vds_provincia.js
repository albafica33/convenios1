/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:19 GMT-0400 (Bolivia Time)
 * Time: 1:54:19
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:19 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:19
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsProvincia = sequelize.define('vdsProvincia', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      estado: DataTypes.INTEGER,
      
      
      
      provincia: DataTypes.STRING,
      
      abreviacion: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      
      
      id_ciudad: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_provincia',
      timestamps: false,
    });
    vdsProvincia.associate = (models) => {
      
      models.vdsProvincia.belongsTo(models.vdsCiudad,{foreignKey:'id_ciudad', targetKey: '_id',  as:'vdProvinciaCiudad'});
      models.vdsCiudad.hasMany(models.vdsProvincia,{foreignKey:'id_ciudad', sourceKey: '_id', as:'vdProvinciaCiudad'});
      
    };
    return vdsProvincia;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsProvincia", new Schema({
    
    estado: {type: Number},
    
    
    id: {type: Number},
    
    
    provincia: {type: String},
    
    abreviacion: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    
    
    
    id_ciudad: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_provincia');
  //</es-section>
