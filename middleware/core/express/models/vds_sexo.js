/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:22 GMT-0400 (Bolivia Time)
 * Time: 1:54:22
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:22 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:22
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsSexo = sequelize.define('vdsSexo', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      estado: DataTypes.INTEGER,
      
      
      
      descripcion: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_sexo',
      timestamps: false,
    });
    vdsSexo.associate = (models) => {
      
    };
    return vdsSexo;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsSexo", new Schema({
    
    estado: {type: Number},
    
    
    id: {type: Number},
    
    
    descripcion: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    
    
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_sexo');
  //</es-section>
