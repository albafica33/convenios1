/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:21 GMT-0400 (Bolivia Time)
 * Time: 1:54:21
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:21 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:21
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsRoles = sequelize.define('vdsRoles', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      rol_code: DataTypes.STRING,
      
      rol_description: DataTypes.STRING,
      
      rol_abbr: DataTypes.STRING,
      
      rol_group: DataTypes.STRING,
      
      
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      rol_par_status_id: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_roles',
      timestamps: false,
    });
    vdsRoles.associate = (models) => {
      
      models.vdsRoles.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdRoleCreatedby'});
      models.vdsUserRoles.hasMany(models.vdsRoles,{foreignKey:'createdById', sourceKey: '_id', as:'vdRoleCreatedby'});
      
      models.vdsRoles.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdRoleUpdatedby'});
      models.vdsUserRoles.hasMany(models.vdsRoles,{foreignKey:'updatedById', sourceKey: '_id', as:'vdRoleUpdatedby'});
      
      models.vdsRoles.belongsTo(models.vdsParams,{foreignKey:'rol_par_status_id', targetKey: '_id',  as:'vdRoleRolParStatus'});
      models.vdsParams.hasMany(models.vdsRoles,{foreignKey:'rol_par_status_id', sourceKey: '_id', as:'vdRoleRolParStatus'});
      
    };
    return vdsRoles;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsRoles", new Schema({
    
    
    id: {type: Number},
    
    
    rol_code: {type: String},
    
    rol_description: {type: String},
    
    rol_abbr: {type: String},
    
    rol_group: {type: String},
    
    
    
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    rol_par_status_id: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_roles');
  //</es-section>
