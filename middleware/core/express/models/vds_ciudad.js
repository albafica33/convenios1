/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:01 GMT-0400 (Bolivia Time)
 * Time: 1:54:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:01 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:1
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsCiudad = sequelize.define('vdsCiudad', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      estado: DataTypes.INTEGER,
      
      
      
      ciudad: DataTypes.STRING,
      
      abrev: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      id_pais: DataTypes.STRING,
      
      
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_ciudad',
      timestamps: false,
    });
    vdsCiudad.associate = (models) => {
      
    };
    return vdsCiudad;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsCiudad", new Schema({
    
    estado: {type: Number},
    
    
    id: {type: Number},
    
    
    ciudad: {type: String},
    
    abrev: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    id_pais: {type: String},
    
    
    
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_ciudad');
  //</es-section>
