/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:02 GMT-0400 (Bolivia Time)
 * Time: 1:54:2
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:02 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:2
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsDictionaries = sequelize.define('vdsDictionaries', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      dic_code: DataTypes.STRING,
      
      dic_description: DataTypes.STRING,
      
      dic_group: DataTypes.STRING,
      
      
      
      dic_par_status_id: DataTypes.STRING,
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_dictionaries',
      timestamps: false,
    });
    vdsDictionaries.associate = (models) => {
      
      models.vdsDictionaries.belongsTo(models.vdsParams,{foreignKey:'dic_par_status_id', targetKey: '_id',  as:'vdDictionaryDicParStatus'});
      models.vdsParams.hasMany(models.vdsDictionaries,{foreignKey:'dic_par_status_id', sourceKey: '_id', as:'vdDictionaryDicParStatus'});
      
      models.vdsDictionaries.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdDictionaryCreatedby'});
      models.vdsUserRoles.hasMany(models.vdsDictionaries,{foreignKey:'createdById', sourceKey: '_id', as:'vdDictionaryCreatedby'});
      
      models.vdsDictionaries.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdDictionaryUpdatedby'});
      models.vdsUserRoles.hasMany(models.vdsDictionaries,{foreignKey:'updatedById', sourceKey: '_id', as:'vdDictionaryUpdatedby'});
      
    };
    return vdsDictionaries;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsDictionaries", new Schema({
    
    
    id: {type: Number},
    
    
    dic_code: {type: String},
    
    dic_description: {type: String},
    
    dic_group: {type: String},
    
    
    
    
    dic_par_status_id: {type: mongoose.Types.ObjectId},
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_dictionaries');
  //</es-section>
