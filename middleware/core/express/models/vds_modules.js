/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:08 GMT-0400 (Bolivia Time)
 * Time: 1:54:8
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:08 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:8
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsModules = sequelize.define('vdsModules', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      mod_code: DataTypes.STRING,
      
      mod_description: DataTypes.STRING,
      
      mod_abbr: DataTypes.STRING,
      
      mod_icon: DataTypes.STRING,
      
      mod_group: DataTypes.STRING,
      
      mod_parent_id: DataTypes.STRING,
      
      
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      mod_par_status_id: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_modules',
      timestamps: false,
    });
    vdsModules.associate = (models) => {
      
      models.vdsModules.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdModuleCreatedby'});
      models.vdsUserRoles.hasMany(models.vdsModules,{foreignKey:'createdById', sourceKey: '_id', as:'vdModuleCreatedby'});
      
      models.vdsModules.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdModuleUpdatedby'});
      models.vdsUserRoles.hasMany(models.vdsModules,{foreignKey:'updatedById', sourceKey: '_id', as:'vdModuleUpdatedby'});
      
      models.vdsModules.belongsTo(models.vdsParams,{foreignKey:'mod_par_status_id', targetKey: '_id',  as:'vdModuleModParStatus'});
      models.vdsParams.hasMany(models.vdsModules,{foreignKey:'mod_par_status_id', sourceKey: '_id', as:'vdModuleModParStatus'});
      
    };
    return vdsModules;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsModules", new Schema({
    
    
    id: {type: Number},
    
    
    mod_code: {type: String},
    
    mod_description: {type: String},
    
    mod_abbr: {type: String},
    
    mod_icon: {type: String},
    
    mod_group: {type: String},
    
    mod_parent_id: {type: String},
    
    
    
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    mod_par_status_id: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_modules');
  //</es-section>
