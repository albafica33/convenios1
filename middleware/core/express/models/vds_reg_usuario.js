/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:20 GMT-0400 (Bolivia Time)
 * Time: 1:54:20
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:20 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:20
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsRegUsuario = sequelize.define('vdsRegUsuario', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      
      
      
      
      id_usuario: DataTypes.STRING,
      
      id_reg: DataTypes.STRING,
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_reg_usuario',
      timestamps: false,
    });
    vdsRegUsuario.associate = (models) => {
      
      models.vdsRegUsuario.belongsTo(models.vdsUsers,{foreignKey:'id_usuario', targetKey: '_id',  as:'vdRegUsuarioUsuario'});
      models.vdsUsers.hasMany(models.vdsRegUsuario,{foreignKey:'id_usuario', sourceKey: '_id', as:'vdRegUsuarioUsuario'});
      
      models.vdsRegUsuario.belongsTo(models.vdsMatriz,{foreignKey:'id_reg', targetKey: '_id',  as:'vdRegUsuarioReg'});
      models.vdsMatriz.hasMany(models.vdsRegUsuario,{foreignKey:'id_reg', sourceKey: '_id', as:'vdRegUsuarioReg'});
      
      models.vdsRegUsuario.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdRegUsuarioCreatedby'});
      models.vdsUserRoles.hasMany(models.vdsRegUsuario,{foreignKey:'createdById', sourceKey: '_id', as:'vdRegUsuarioCreatedby'});
      
      models.vdsRegUsuario.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdRegUsuarioUpdatedby'});
      models.vdsUserRoles.hasMany(models.vdsRegUsuario,{foreignKey:'updatedById', sourceKey: '_id', as:'vdRegUsuarioUpdatedby'});
      
    };
    return vdsRegUsuario;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsRegUsuario", new Schema({
    
    
    id: {type: Number},
    
    
    
    
    
    id_usuario: {type: mongoose.Types.ObjectId},
    
    id_reg: {type: mongoose.Types.ObjectId},
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_reg_usuario');
  //</es-section>
