/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:09 GMT-0400 (Bolivia Time)
 * Time: 1:54:9
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:09 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:9
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsMunicipio = sequelize.define('vdsMunicipio', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      estado: DataTypes.INTEGER,
      
      
      
      municipio: DataTypes.STRING,
      
      createdBy: DataTypes.STRING,
      
      updatedBy: DataTypes.STRING,
      
      
      
      id_ciudad: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_municipio',
      timestamps: false,
    });
    vdsMunicipio.associate = (models) => {
      
      models.vdsMunicipio.belongsTo(models.vdsCiudad,{foreignKey:'id_ciudad', targetKey: '_id',  as:'vdMunicipioCiudad'});
      models.vdsCiudad.hasMany(models.vdsMunicipio,{foreignKey:'id_ciudad', sourceKey: '_id', as:'vdMunicipioCiudad'});
      
    };
    return vdsMunicipio;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsMunicipio", new Schema({
    
    estado: {type: Number},
    
    
    id: {type: Number},
    
    
    municipio: {type: String},
    
    createdBy: {type: String},
    
    updatedBy: {type: String},
    
    
    
    
    id_ciudad: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_municipio');
  //</es-section>
