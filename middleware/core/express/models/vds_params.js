/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 01:54:11 GMT-0400 (Bolivia Time)
 * Time: 1:54:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 01:54:11 GMT-0400 (Bolivia Time)
 * Last time updated: 1:54:11
 *
 * Caution: es-sections will be replaced by script execution
 */

const mongoose = require("mongoose");
const {Schema} = mongoose;

'use strict';

    //<es-section>

  module.exports.sequelize = (sequelize, DataTypes) => {
    const vdsParams = sequelize.define('vdsParams', {
      
      _id: { type: DataTypes.STRING, primaryKey: true },
      
      
      id: DataTypes.INTEGER,
      
      
      par_order: DataTypes.INTEGER,
      
      
      
      par_cod: DataTypes.STRING,
      
      par_description: DataTypes.STRING,
      
      par_abbr: DataTypes.STRING,
      
      par_group: DataTypes.STRING,
      
      
      
      createdById: DataTypes.STRING,
      
      updatedById: DataTypes.STRING,
      
      par_dictionary_id: DataTypes.STRING,
      
      par_status_id: DataTypes.STRING,
      
      
      dueAt: DataTypes.DATE,
      
      createdAt: DataTypes.DATE,
      
      updatedAt: DataTypes.DATE,
      
      
      
      
      
      
      
    }, {
      tableName:'vds_params',
      timestamps: false,
    });
    vdsParams.associate = (models) => {
      
      models.vdsParams.belongsTo(models.vdsUserRoles,{foreignKey:'createdById', targetKey: '_id',  as:'vdParamCreatedby'});
      models.vdsUserRoles.hasMany(models.vdsParams,{foreignKey:'createdById', sourceKey: '_id', as:'vdParamCreatedby'});
      
      models.vdsParams.belongsTo(models.vdsUserRoles,{foreignKey:'updatedById', targetKey: '_id',  as:'vdParamUpdatedby'});
      models.vdsUserRoles.hasMany(models.vdsParams,{foreignKey:'updatedById', sourceKey: '_id', as:'vdParamUpdatedby'});
      
      models.vdsParams.belongsTo(models.vdsDictionaries,{foreignKey:'par_dictionary_id', targetKey: '_id',  as:'vdParamParDictionary'});
      models.vdsDictionaries.hasMany(models.vdsParams,{foreignKey:'par_dictionary_id', sourceKey: '_id', as:'vdParamParDictionary'});
      
      models.vdsParams.belongsTo(models.vdsParams,{foreignKey:'par_status_id', targetKey: '_id',  as:'vdParamParStatus'});

    };
    return vdsParams;
    //</es-section>
  };

  //<es-section>
  module.exports.mongoose = mongoose.model("vdsParams", new Schema({
    
    par_order: {type: Number},
    
    
    id: {type: Number},
    
    
    par_cod: {type: String},
    
    par_description: {type: String},
    
    par_abbr: {type: String},
    
    par_group: {type: String},
    
    
    
    
    createdById: {type: mongoose.Types.ObjectId},
    
    updatedById: {type: mongoose.Types.ObjectId},
    
    par_dictionary_id: {type: mongoose.Types.ObjectId},
    
    par_status_id: {type: mongoose.Types.ObjectId},
    
    
    dueAt: {type: Date},
    
    createdAt: {type: Date},
    
    updatedAt: {type: Date},
    
    
    
    
    
    
    
  }),'vds_params');
  //</es-section>
