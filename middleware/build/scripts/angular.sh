#!/usr/bin/env bash

cd ../../../frontend
ng build --configuration test --watch --output-hashing none --base-href /lafuente/crm_pilat_dev/frontend/dist/frontend/ --build-optimizer
