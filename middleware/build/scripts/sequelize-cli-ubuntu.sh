#!/usr/bin/env bash
#npx sequelize-cli db:migrate:undo:all
#npx sequelize-cli db:migrate

# para deshacer una migracion con sequelize
#npx sequelize-cli db:migrate:undo --name 007-create-co-users.js

# para migrar una table con sequelize
# sequelize db:migrate --from 007-create-co-users.js --to 007-create-co-users.js
