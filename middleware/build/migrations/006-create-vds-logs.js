'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_logs', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      
      
      
      
      
      "log_obj_id": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "log_description": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "log_group": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "log_par_status_id": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_logs');
  }
};
