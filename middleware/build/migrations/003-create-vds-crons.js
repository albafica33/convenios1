'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_crons', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      
      
      "cro_status": {
        length: 10,
        decimals: 0,
        type: Sequelize.DECIMAL,
      },
      
      
      
      
      "cro_description": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "cro_expression": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "cro_group": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "cro_mai_id": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "cro_function": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_crons');
  }
};
