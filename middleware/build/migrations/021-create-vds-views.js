'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_views', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      
      
      
      
      
      "vie_code": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "vie_description": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "vie_route": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "vie_params": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "vie_icon": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "vie_group": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      "vie_module_id": {
        references: {
            model: {tableName:'vds_modules'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "vie_return_id": {
        references: {
            model: {tableName:'vds_views'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "vie_par_status_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_views');
  }
};
