'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_params', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      "par_order": {
        length: 11,
        type: Sequelize.INTEGER,
      },
      
      
      
      
      
      
      "par_cod": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "par_description": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "par_abbr": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "par_group": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      "par_dictionary_id": {
        references: {
            model: {tableName:'vds_dictionaries'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "par_status_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_params');
  }
};
