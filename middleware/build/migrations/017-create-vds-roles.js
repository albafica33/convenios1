'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_roles', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      
      
      
      
      
      "rol_code": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "rol_description": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "rol_abbr": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "rol_group": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      "rol_par_status_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_roles');
  }
};
