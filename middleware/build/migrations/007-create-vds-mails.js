'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_mails', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      "mai_port": {
        length: 11,
        type: Sequelize.INTEGER,
      },
      
      
      
      
      
      
      "mai_description": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_user_account": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_user_password": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_host": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_protocol": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_bus_id": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_par_status_id": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_group": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_subject": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mai_to": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      "mai_bcc": {
        type: Sequelize.TEXT,
      },
      
      "mai_cc": {
        type: Sequelize.TEXT,
      },
      
      "mai_text": {
        type: Sequelize.TEXT,
      },
      
      "mai_html": {
        type: Sequelize.TEXT,
      },
      
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_mails');
  }
};
