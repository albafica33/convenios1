'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_people', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      
      
      
      
      
      "per_first_name": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_second_name": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_first_lastname": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_second_lastname": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_license": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_license_comp": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_home_address": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_mail": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_home_phone": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_cellphone": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "per_group": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      "per_parent_id": {
        references: {
            model: {tableName:'vds_people'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "per_par_type_doc_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "per_par_city_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "per_par_sex_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "per_par_country_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "per_par_nacionality_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "per_par_status_id": {
        references: {
            model: {tableName:'vds_params'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      
      "per_birthday": {
        type: Sequelize.DATE,
      },
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_people');
  }
};
