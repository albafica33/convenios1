'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_estado_civil', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      "estado": {
        length: 11,
        type: Sequelize.INTEGER,
      },
      
      
      
      
      
      
      "descripcion": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_estado_civil');
  }
};
