'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_persona', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      "estado": {
        length: 11,
        type: Sequelize.INTEGER,
      },
      
      
      
      "telefono": {
        length: 10,
        decimals: 0,
        type: Sequelize.DECIMAL,
      },
      
      "celular": {
        length: 10,
        decimals: 0,
        type: Sequelize.DECIMAL,
      },
      
      
      
      
      "nombres": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "paterno": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "materno": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "casada": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "ci": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "correo": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "direccion": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedBy": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      "ci_expedido": {
        references: {
            model: {tableName:'vds_ciudad'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "id_estado_civil": {
        references: {
            model: {tableName:'vds_estado_civil'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "id_sexo": {
        references: {
            model: {tableName:'vds_sexo'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "id_municipio": {
        references: {
            model: {tableName:'vds_municipio'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "id_provincia": {
        references: {
            model: {tableName:'vds_provincia'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "id_ciudad": {
        references: {
            model: {tableName:'vds_ciudad'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      "id_pais": {
        references: {
            model: {tableName:'vds_pais'},
            key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        type: Sequelize.STRING,
      },
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_persona');
  }
};
