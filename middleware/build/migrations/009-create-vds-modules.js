'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_modules', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      
      
      
      
      
      "mod_code": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mod_description": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mod_abbr": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mod_icon": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mod_group": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "createdById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mod_parent_id": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "mod_par_status_id": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_modules');
  }
};
