'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vds_matriz', {


      
      "_id": {
        allowNull: false,
        primaryKey: true,
        autoIncrement: false,
        type: Sequelize.STRING,
      },
      
      
      "id": {
        type: Sequelize.BIGINT,
      },
      
      
      
      "estado_cumplimiento": {
        length: 11,
        type: Sequelize.INTEGER,
      },
      
      
      
      
      
      
      "createdById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      "updatedById": {
        length: 255,
        type: Sequelize.STRING,
      },
      
      
      "compromiso": {
        type: Sequelize.TEXT,
      },
      
      "entidad_responsable": {
        type: Sequelize.TEXT,
      },
      
      "contacto": {
        type: Sequelize.TEXT,
      },
      
      "observacion": {
        type: Sequelize.TEXT,
      },
      
      "tema": {
        type: Sequelize.TEXT,
      },
      
      "fecha_cumplimiento": {
        type: Sequelize.TEXT,
      },
      
      
      
      "fecha_modificacion": {
        type: Sequelize.DATE,
      },
      
      "dueAt": {
        type: Sequelize.DATE,
      },
      
      "createdAt": {
        type: Sequelize.DATE,
      },
      
      "updatedAt": {
        type: Sequelize.DATE,
      },
      
      
      

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vds_matriz');
  }
};
