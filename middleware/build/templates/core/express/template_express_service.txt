/**
 * Created by @ES Express Systems
 * User: #userCreated
 * Date: #dateCreated
 * Time: #timeCreated
 * Last User updated: #userUpdated
 * Last date updated: #dateUpdated
 * Last time updated: #timeUpdated
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const bcrypt = require('bcryptjs');

const models = require('../index');
const Util = require('../../../utils/Utils');
const util = new Util();

import esConfig from '../../../config/config';
const sql = esConfig.sql;

const { Op } = require("sequelize");

//<es-section>

//</es-section>

class UcObjSLocalTableNameService {

    //<es-section>

    //</es-section>

	//<es-section>
	<<<sequelizeDefaultServiceFunctions>>>
	static async getAllUcObjPLocalTableName(query) {
		try {
		    let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null;
			if(sql) {
			    let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
            	let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				return await models.sequelize.lcObjPLocalTableName.findAndCountAll({
				    attributes:query.select ? query.select.split(',') : null,
				    where: where && !where.where ? where : null,
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['local_primary_name','ASC']],
                });
			} else {
				return await models.mongoose.lcObjPLocalTableName.find().select(query.select).limit(parseInt(query.limit)).skip(parseInt(offset));
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSelectAllUcObjPLocalTableName(select = []) {
		try {
			if(sql) {
				return await models.sequelize.lcObjPLocalTableName.findAll({
				    attributes: select ? select : null
				});
			} else {
				return await models.mongoose.lcObjPLocalTableName.find().select(select.join(' '));
			}
		} catch (error) {
			throw error;
		}
	}

	static async addUcObjSLocalTableName(newUcObjSLocalTableName) {
		try {
			let objUcObjSLocalTableName;
			if(util.PrimaryKeyTypeIsString(models.sequelize.lcObjPLocalTableName.primaryKeys.local_primary_name.type.toString())) {
			    newUcObjSLocalTableName.local_primary_name = models.sequelize.objectId().toString();
		    }
			<id>
			if(!newUcObjSLocalTableName.local_second_primary_name) {
              let max = await models.sequelize.lcObjPLocalTableName.max('local_second_primary_name');
              newUcObjSLocalTableName.local_second_primary_name = newUcObjSLocalTableName.local_second_primary_name ? newUcObjSLocalTableName.local_second_primary_name : max ? max+1 : 1;
			}
			</id>
			<user-hash>
			newUcObjSLocalTableName.user_hash = await bcrypt.hash(newUcObjSLocalTableName.user_hash, bcrypt.genSaltSync(8)).then(encripted => {return encripted});
			</user-hash>
			<usr-id>
            newUcObjSLocalTableName.usr_id = newUcObjSLocalTableName.usr_id ? newUcObjSLocalTableName.usr_id : await Math.floor(Math.random() * 10000000000);
			</usr-id>
			if(sql) {
				objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.create(newUcObjSLocalTableName);
			} else {
				objUcObjSLocalTableName = new models.mongoose.lcObjPLocalTableName(newUcObjSLocalTableName);
				await objUcObjSLocalTableName.save();
			}
			return objUcObjSLocalTableName;
		} catch (error) {
			throw error;
		}
	}
	<user-name>
	static async findOneByUserNameAndUserHash(userName, userHash, query = {}) {
    	try {
    		let objUser;
    		if(sql) {
    			objUser = await models.sequelize[esConfig.esUser].findOne({
    				attributes:query.select ? query.select.split(',') : null,
    				where: { user_name: userName, user_hash: userHash},
    			});
    		} else {
    			objUser = await models.mongoose[esConfig.esUser].findOne({user_name: userName, user_hash: await bcrypt.hash(userHash)});
    		}
    		return objUser;
    	} catch (error) {
    		throw error;
    	}
    }
    </user-name>
	static async updateUcObjSLocalTableName(local_primary_name, updateUcObjSLocalTableName) {
		try {
			let objUcObjSLocalTableName;
			if(sql) {
				objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findOne({where: { local_primary_name: util.UcObjLocalPrimaryType(local_primary_name) }});
				if (objUcObjSLocalTableName) {
					await models.sequelize.lcObjPLocalTableName.update(updateUcObjSLocalTableName, { where: { local_primary_name: util.UcObjLocalPrimaryType(local_primary_name) } });
					objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findOne({where: { local_primary_name: util.UcObjLocalPrimaryType(local_primary_name) }});
				}
			} else {
				delete updateUcObjSLocalTableName._id;
				objUcObjSLocalTableName = await models.mongoose.lcObjPLocalTableName.findOneAndUpdate({local_primary_name:local_primary_name}, {$set: updateUcObjSLocalTableName}, {new: true});
			}
			return objUcObjSLocalTableName;
		} catch (error) {
			throw error;
		}
	}

	static async getAUcObjSLocalTableName(local_primary_name, query) {
		try {
			let objUcObjSLocalTableName;
			if(sql) {
			        let where = Object.keys(query).length ? query.where ? JSON.parse(query.where) : null : null;
					objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findOne({
					    where: where && !where.where ? where : { local_primary_name: util.UcObjLocalPrimaryType(local_primary_name) },
					    attributes:query.select ? query.select.split(',') : null,
                    });
			} else {
					objUcObjSLocalTableName = await models.mongoose.lcObjPLocalTableName.find({local_primary_name:util.UcObjLocalPrimaryType(local_primary_name)}).select(query.select);
			}
			return objUcObjSLocalTableName;
		} catch (error) {
			throw error;
		}
	}

	static async deleteUcObjSLocalTableName(local_primary_name) {
		try {
			let objUcObjSLocalTableName;
			if(sql) {
				objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findOne({ where: { local_primary_name: util.UcObjLocalPrimaryType(local_primary_name) } });
				if (objUcObjSLocalTableName) {
					await models.sequelize.lcObjPLocalTableName.destroy({where: { local_primary_name: util.UcObjLocalPrimaryType(local_primary_name) }});
				}
			} else {
				objUcObjSLocalTableName = await models.mongoose.lcObjPLocalTableName.deleteOne({local_primary_name:util.UcObjLocalPrimaryType(local_primary_name)});
			}
			return objUcObjSLocalTableName;
		} catch (error) {
			throw error;
		}
	}
	>>>sequelizeDefaultServiceFunctions<<<
	<<<sequelizeFindOneByFieldsServiceFunctions>>>
	static async findOneByUcObjLocalCommonFieldName(lcObjLocalFieldName, query = {}) {
    	try {
    		let objUcObjSLocalTableName;
    		if(sql) {
    			objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findOne({
    				attributes:query.select ? query.select.split(',') : null,
    			    where: { local_field_name: lcObjLocalFieldName },
    			});
    		} else {
    			objUcObjSLocalTableName = await models.mongoose.lcObjPLocalTableName.findOne({local_field_name: lcObjLocalFieldName});
    		}
    		return objUcObjSLocalTableName;
    	} catch (error) {
    		throw error;
    	}
    }
	>>>sequelizeFindOneByFieldsServiceFunctions<<<
	<<<sequelizeUpdateByFieldsServiceFunctions>>>
	static async updateUcObjSLocalTableNameByUcObjLocalCommonFieldName(lcObjLocalFieldName, updateUcObjSLocalTableName) {
    	try {
    		let objUcObjSLocalTableName;
    		if(sql) {
    			objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findOne({where: { local_field_name: lcObjLocalFieldName }});
    			if (objUcObjSLocalTableName) {
    				objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.update(updateUcObjSLocalTableName, { where: { local_field_name: lcObjLocalFieldName } });
    			}
    		} else {
    			objUcObjSLocalTableName = await models.mongoose.lcObjPLocalTableName.findOneAndUpdate({local_field_name: lcObjLocalFieldName}, {$set: updateUcObjSLocalTableName}, {new: true});
    		}
    		return objUcObjSLocalTableName;
    	} catch (error) {
    		throw error;
    	}
    }
	>>>sequelizeUpdateByFieldsServiceFunctions<<<
	<<<sequelizeStoreServiceFunctions>>>
	static async findUcObjPForeignTableNameUcObjlocalStoreFieldNameWithUcObjForeignStoreFieldNameWithLabel(select = ['foreign_store_field_name_value', 'foreign_store_field_name_label'], query = {}) {
    	try {
    		let offset = query.offset ? query.offset : query.start ? query.start : query.limit ? '0' : null;
    		if(sql) {
    		    return await models.sequelize.lcObjPForeignTableName.findAll({
                    attributes: select,
                    where: { local_group_field_name: {[Op.like]: '%grp_local_store_index_field_name%'}},
                    limit: query.limit ? parseInt(query.limit) : null,
                    offset: offset ? parseInt(offset) : 0,
                    order: query.order ? JSON.parse(query.order) : [['foreign_store_field_name_value','DESC']],
    		    });
    		} else {
    			return await models.mongoose.lcObjPForeignTableName.find({local_group_field_name: {$regex : ".*grp_local_store_index_field_name.*"}}).select(select.join(' '));
    		}
    	} catch (error) {
            throw error;
    	}
    }
	>>>sequelizeStoreServiceFunctions<<<
	<<<sequelizeStoreFilterServiceFunctions>>>
	static async filterUcObjPLocalTableNameByUcObjlocalStoreFieldName(ids, query = {select:'',level:0}) {
    	try {
    	    let status = await this.findUcObjPForeignStatusTableNameUcObjlocalStoreIndexStatusFieldNameWithUcObjForeignStoreStatusFieldNameWithLabel(['_id', 'par_abbr']);
        	let objUcObjSLocalTableName, project = {}, level, select;
        	level = Object.keys(query) && query.level ? query.level : 0;
        	select = Object.keys(query) && query.select ? query.select.split(',') : [];
        	select.forEach(sel => project[sel] = 1);
        	ids.forEach((id,i) => ids[i] = parseInt(id));
        	if(sql) {
    	        let enabled = status.find(param => param.dataValues.par_abbr == 'enabled');
    	        let idEnabled = enabled.dataValues._id;
        	    if ( level == 1 ) {
                    objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findAll(
                        arraySqlLevel1Include
                    );
        	    } else if( level == 2 ){
        	        objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findAll(
                        arraySqlLevel2Include
                    );
        	    } else {
        	        objUcObjSLocalTableName = await models.sequelize.lcObjPLocalTableName.findAll(
                        arraySqlLevel0Include
                    );
        	    }
    		} else {
    		    let enabled = status.find(param => param._doc.par_abbr == 'enabled');
    		    let idEnabled = enabled._doc._id;
    		    if ( level == 1 ) {
                    objUcObjSLocalTableName = await models.mongoose.lcObjPLocalTableName.aggregate(
                        arrayMongoLevel1Aggregate
                    );
    		    } else if( level == 2 ) {
    		        objUcObjSLocalTableName = await models.mongoose.lcObjPLocalTableName.aggregate(
                        arrayMongoLevel2Aggregate
                    );
    		    } else {
    		        objUcObjSLocalTableName = await models.mongoose.lcObjPLocalTableName.aggregate(
                        arrayMongoLevel0Aggregate
                    );
    		    }
    		}
    		return objUcObjSLocalTableName;
    	} catch (error) {
            throw error;
    	}
    }
	>>>sequelizeStoreFilterServiceFunctions<<<
	<<<sequelizeDatatableSearchPanes>>>
	static async setSearchPanes(body, query, dtColumns) {
    		try {
    			let { root } = query;
    			let { where } = query;
    			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
    			// let aData = await models.sequelize.lcObjPLocalTableName.findAll({where:objWhere});
    			let rawAttributes = models.sequelize.lcObjPLocalTableName.rawAttributes;
    			let aColumns = Object.values(rawAttributes);
    			let fields = Object.keys(rawAttributes);
    			let search = body['search[value]'];
    			let searchPanes = {};
    			let searches = [];
    			let dtOptions = {};
    			let userUcObjPLocalTableName = await models.sequelize.lcObjPLocalTableName.findAll({
    				where: objWhere
    			});

    			if (dtColumns) {
    				for (let i = 0 ; i < fields.length; i++ ) {
    					let field = fields[i];
    					dtOptions[`${root}.${field}`] = [];
    				}

    				let dtValues = [];
    					for (let k = 0 ; k < userUcObjPLocalTableName.length ; k++) {
    						let userUcObjSLocalTableName = userUcObjPLocalTableName[k].dataValues;
    						let aUserUcObjSLocalTableNameValues = Object.values(userUcObjSLocalTableName);
    						let aUserUcObjSLocalTableNameFields = Object.keys(userUcObjSLocalTableName);
    						for (let n = 0 ; n < aUserUcObjSLocalTableNameValues.length ; n++) {
    							let userUcObjSLocalTableNameField = aUserUcObjSLocalTableNameFields[n];
    							let userUcObjSLocalTableNameValue = aUserUcObjSLocalTableNameValues[n];
    							if (!dtValues.find(param => param.value == userUcObjSLocalTableNameValue && param.field == userUcObjSLocalTableNameField)) {
    								dtValues.push({value:userUcObjSLocalTableNameValue, count:1, label:userUcObjSLocalTableNameValue, field:userUcObjSLocalTableNameField});
    							} else {
    								for (let m = 0 ; m < dtValues.length ; m++) {
    									let dtValue = dtValues[m];
    									if (dtValue.value == userUcObjSLocalTableNameValue && dtValue.field == userUcObjSLocalTableNameField) {
    										dtValues[m].count++;
    									}
    								}
    							}
    						}
    					}

    				for (let l = 0 ; l < dtValues.length ; l++) {
    					let dtValue = dtValues[l];
    					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
    					dtOptions[`${root}.${dtValue.field}`].push({
    						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
    						total:dtValue.count,
    						value:optDate && optDate.getDate() ? optDate : dtValue.value,
    						count:dtValue.count
    					});
    				}

    				for (let j = 0 ; j < fields.length; j++ ) {
    					for (let z = 0 ; z < fields.length; z++ ) {
    						let field = fields[z];
    						if (root) {
    							if (body[`searchPanes[${root}.${field}][${j}]`]) {
    								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
    							}
    						}
    					}
    				}
    			}
    			dtOptions['tableLength'] = 100;
    			searchPanes['options'] = dtOptions;
    			return [searchPanes, searches, userUcObjPLocalTableName];
    		} catch (e) {
    			console.log(e);
    		}
    	}
	>>>sequelizeDatatableSearchPanes<<<
	
	//</es-section>
}

//<es-section>
module.exports = UcObjSLocalTableNameService;
//</es-section>
