<!-- /**
 * Created by @ES Express Systems
 * User: #userCreated
 * Date: #dateCreated
 * Time: #timeCreated
 * Last User updated: #userUpdated
 * Last date updated: #dateUpdated
 * Last time updated: #timeUpdated
 *
 * Caution: es-sections will be replaced by script execution
 */ -->

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>es</title>
    <link rel="stylesheet" type="text/css" href="resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css" href="shared/restful.css"/>

    <!-- GC -->

    <script type="text/javascript" src="ext-all.js"></script>
    <script type="text/javascript" src="bootstrap.js"></script>
    <script type="text/javascript" src="shared/common.js"></script>

    <link rel="stylesheet" type="text/css" href="layout-browser.css">
    <script>
			// use the scoped CSS stylesheet if the url has a scopecss parameter
			document.write('<link rel="stylesheet" type="text/css" href="resources/css/ext-all' +
				((window.location.search.indexOf('scopecss') === -1) ? '' : '-scoped') + '.css" />');
    </script>
    <link rel="stylesheet" type="text/css" href="shared/example.css" />
    <link rel="stylesheet" type="text/css" href="ux/css/CheckHeader.css" />

    <!-- GC -->

    <style type="text/css">
        p {
            margin: 5px;
        }

        .settings {
            background-image: url(shared/icons/fam/folder_wrench.png);
        }

        .nav {
            background-image: url(shared/icons/fam/folder_go.png);
        }

        .info {
            background-image: url(shared/icons/fam/information.png);
        }
    </style>


    <!-- page specific -->
    <script src="app.js"></script>

</head>
<body>

<div style="display:none;">
    <!-- es-section -->
    <<<extjsIndexFields>>>
    <div id="local-dbTable-name-details">
        <h2>local-dbTable-name</h2>
        <p>Comments</p>
    </div>
    >>>extjsIndexFields<<<
    <!-- /es-section-->
</div>


</body>
</html>
