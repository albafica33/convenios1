//import WhatsAppWeb from 'baileys';
import { WAConnection } from '@adiwajshing/baileys';
const QRCode = require('qrcode');
const fs = require('fs');
let qrCode, sessData, error;
let client;

module.exports.init = () => {

	client = new WAConnection();

	client.connectOptions = {
		/** fails the connection if no data is received for X seconds */
		maxIdleTimeMs: 604800000, // 7 dias
		/** maximum attempts to connect */
		maxRetries: 9999999,
		/** max time for the phone to respond to a connectivity test */
		phoneResponseTime: 15000,
		/** minimum time between new connections */
		connectCooldownMs: 4000,
		/** agent used for WS connections (could be a proxy agent) */
		// agent?: Agent = undefined,
		/** agent used for fetch requests -- uploading/downloading media */
		// fetchAgent?: Agent = undefined,
		/** always uses takeover for connecting */
		alwaysUseTakeover: true
	};
// client.onReadyForPhoneAuthentication((ref,publicKey,clientID) => {
// 	qrCode = ref + "," + publicKey + "," + clientID;
// });
	client.regenerateQRIntervalMs = 9000000; // QR regen every 20 seconds

	client.connect().then(async (data) => {
		sessData = data;
	}).catch(err => {
		sessData = {err};
	});

	client.on('qr', async qr => {
		qrCode = qr;
		console.log(qr);
	});

}

module.exports.connectApi = async (req, res) => {
	try {
		if (sessData && sessData.err) {
			res.jsonp({status:'ok',mensaje: 'Autenticación fallida',data:qrCode});
		} else if (sessData && sessData.user) {
			let authInfo = client.base64EncodedAuthInfo(); // get all the auth info we need to restore this session
			authInfo.user = sessData.user;
			authInfo.chats = sessData.chats;
			authInfo.contacts = sessData.contacts;
			authInfo.unread = sessData.unread;
			authInfo.user_name = client.user.name;
			authInfo.user_phone = client.user.phone;
			authInfo.user_jid = client.user.jid;
			res.jsonp({status:'ok',mensaje: 'Autenticación exitosa',data:authInfo});
		} else {
			res.jsonp({status:'ok',mensaje: 'Autenticación fallida',data:qrCode});
		}
	} catch (e) {
		console.log(e)
	}
};

module.exports.authStatus = async (req, res) => {
	if (sessData) {
		res.jsonp({status:'ok',mensaje: 'Autenticación exitosa',data:sessData});
	} else {
		res.jsonp({status:'ok',mensaje: 'Autenticación fallida',data:qrCode});
	}
};

module.exports.closeApi = async (req, res) => {
	client.close();
	res.jsonp({status:'ok',mensaje: 'Autenticación cerrada',data:qrCode});
};

module.exports.sendMessage = async (req, res) => {
	let options = {
		quoted: null,
		timestamp: new Date()
	}
	client.sendTextMessage(`${req.body.phone}@s.whatsapp.net`, req.body.message, options)
		.then(req.jsonp({messaje:'Notificacion enviada'}));
};


