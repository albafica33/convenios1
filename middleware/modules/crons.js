
require('../utils/Prototipes');
const nodeCron = require('node-cron');
const models = require('../core/express');
const nodecron = require('./nodecron');
const leadService = require('../api/src/crm/lead/lead.service');
const callService = require('../api/src/crm/call/call.service');
const nodeMailer = require("./nodemailer");
const fechas = require('fechas');
const moment = require('moment');
const crmService = require('../api/src/crm/crm.service');
const { Op } = require("sequelize");
const {loggerEmail} = require("./winston");

module.exports = async function() {

	// CRON OFICIAL

	let configCronJobs = await models.sequelize.vdsCrons.findAll({where:{cro_status:1}});
	let respCronJobs = [configCronJobs]
	// let respCronJobs = await models.sequelize.query(`
  //   select * from crm_prd_pilat.pilat_crons where cro_status = 1
	// `).catch(error => {
	// 	let resp = error;
	// });
	if (respCronJobs) {
		let cronJobs = respCronJobs[0];
		for (let i = 0; i < cronJobs.length; i++) {
			let cronJob = cronJobs[i];
			let expresion = cronJob.cro_expression ? cronJob.cro_expression : '* * * * * *';

			nodeCron.schedule(expresion, async () => {
				try {
					console.log("--------------------------");
					console.log("Iniciando Cron Job: " + cronJob.cro_description);
					let response = {status: "OK", message: "", data: {}};
					let respPilatCron = await models.sequelize.pilatCrons.findOne({where: {_id: cronJob._id, cro_status: 1}});
					let pilatCron = respPilatCron && respPilatCron.dataValues ? respPilatCron.dataValues : null;
					if (pilatCron && Object.keys(pilatCron).length) {
						response.data = pilatCron;
						let respFuncion;
						if (nodecron[pilatCron.cro_function]) {
							respFuncion = await nodecron[pilatCron.cro_function](pilatCron);
						}
						let fechaFinTarea = new Date();
						console.log("Cron finalizado: " + pilatCron.cro_description + ' - ' + pilatCron.cro_function + " - " + fechaFinTarea.toString(), respFuncion, response);
					}
				} catch (e) {
					console.log(e);
				}
			});
		}
	};

};
