const wbm = require('wbm');
const QRCode = require('qrcode');
const fs = require('fs');

module.exports.init = async () => {
	wbm.start({showBrowser: false, qrCodeData: true, session: true})
		.then(async qrCodeData => {
			console.log(qrCodeData); // show data used to generate QR Code
			fs.unlinkSync('./public/pilatsrl/qr/sales.png');
			await QRCode.toFile('./public/pilatsrl/qr/sales.png',qrCodeData);
			await wbm.waitQRCode();
			await wbm.end();
		} ).catch(err => { console.log(err); });
};

module.exports.sendMessage = async (req, res) => {
	const contacts = [
		{phone: '59169832432', name: 'Rafael'},
		{phone: '59169832532', name: 'Carolina'}
	];
	await wbm.send(contacts, 'Hey {{name}}, how are you!');
	await wbm.end();

	// Hey Bruno
	// Hey Will
	// await wbm.send(['5535988841854', '5535988841854'], 'Hey man');
	// Hey man
	// Hey man
};

module.exports.logout = async (req, res) => {
	await wbm.end();
};

