require('../utils/Prototipes');
require('dotenv').config();
const env = process.env.NODE_ENV;
import configJson from '../config/config';
const config = configJson[env];
const sql = configJson.sql;
const token = config.wt_token;
const apiUrl = config.wt_apiurl;
const app = require('express')();
const bodyParser = require('body-parser');
const fetch = require('node-fetch');

const qrcode = require('qrcode-terminal');
const { Client } = require('whatsapp-web.js');
const client = new Client();

module.exports = () => {

	client.on('qr', qr => {
		qrcode.generate(qr, {small: true});
	});

	client.on('ready', () => {
		console.log('Client is ready!');
	});

	client.on('auth_failure', msg => {
		// Fired if session restore was unsuccessfull
		console.error('AUTHENTICATION FAILURE', msg);
	});

	client.on('message_create', (msg) => {
		// Fired on all message creations, including your own
		if (msg.fromMe) {
			console.log(msg.fromMe)
			// do stuff here
		}
	});

	client.on('message', message => {
		console.log(message.body);
	});

	client.on('message', message => {
		if(message.body === '!ping') {
			message.reply('pong');
		}
	});
	client.on('message', message => {
		if(message.body === '!ping') {
			client.sendMessage(message.from, 'pong');
		}
	});

	client.initialize();
};
