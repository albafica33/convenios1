require('../utils/Prototipes');
require('dotenv').config();
const env = process.env.NODE_ENV;
import configJson from '../config/config';
const config = configJson[env];
const sql = configJson.sql;
const token = config.wt_token;
const apiUrl = config.wt_apiurl;
const app = require('express')();
const bodyParser = require('body-parser');
const fetch = require('node-fetch');

app.use(bodyParser.json());

class Whatsapp {

	static async apiChatApi(method, params){
		const options = {};
		options['method'] = "POST";
		options['body'] = JSON.stringify(params);
		options['headers'] = { 'Content-Type': 'application/json' };

		const url = `${apiUrl}/${method}?token=${token}`;

		const apiResponse = await fetch(url, options);
		const jsonResponse = await apiResponse.json();
		return jsonResponse;
	}
}

module.exports = Whatsapp;
