module.exports = async (app) => {

	const PORT = app.get("port");

	const sulla = require('sulla-hotfix');

	sulla.create('sales', {
		useChrome: true,
	}).then(client => start(client));

	function start(client) {
		client.onMessage(message => {
			if (message.body === 'Hi') {
				client.sendText(message.from, '👋 Hello!');
			}
		});
	}

};


