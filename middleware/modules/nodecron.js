require('../utils/Prototipes');
const nodeCron = require('node-cron');
const models = require('../core/express');
const leadService = require('../api/src/crm/lead/lead.service');
const callService = require('../api/src/crm/call/call.service');
const nodeMailer = require("./nodemailer");
const fechas = require('fechas');
const moment = require('moment');
const crmService = require('../api/src/crm/crm.service');
const { Op } = require("sequelize");
const {loggerEmail} = require("./winston");
const baileys = require('./baileys');

class NodeCron {

	static async initLeadsNotificationCalls(pilatCron, callback = null) {
		try {
			let today = new Date();
			let currentDay = new Date(today.getFullYear(),today.getMonth(),today.getDate());
			let dateToday = moment(currentDay).toDate();
			let dateTomorrow = moment(currentDay).add(1,'days').toDate();
			let respLeads = await models.sequelize.leads.findAll({
				include:[
					{
						model:models.sequelize.users, as:'leadUsersAssignedUser',
						include: [
							{ model:models.sequelize.emails, as:'userEmails' },
							{
								model:models.sequelize.emailAddrBeanRel, as:'userEmailAddrBeanRel',
								include: {
									model:models.sequelize.emailAddresses, as:'emailAddrBeanRelEmailAddresses'
								}
							}]
					},
					{
						model:models.sequelize.callsLeads, as:'leadCallsLeads',
						where:{call_id:{$ne: null}},
						include:{
							model:models.sequelize.calls, as:'callLeadCalls',
							where: { date_start: {
								$gte:dateToday, $lt:dateTomorrow
							}}
						}
					}]
			});
			if (respLeads && respLeads.length) {
				let respLeadsWithCall = respLeads.filter(param => param.leadCallsLeads != undefined);
				console.log("Leads Obtenidos, total: "+respLeadsWithCall.length);
				if (respLeadsWithCall && respLeadsWithCall.length) {
					for (let j = 0 ; j < respLeadsWithCall.length ; j++) {
						let respLeadWithCall = respLeadsWithCall[j];
						if (respLeadWithCall && respLeadWithCall.dataValues) {
							let leadWithCall = respLeadWithCall.dataValues;
							let callDateStart = leadWithCall.leadCallsLeads.callLeadCalls.date_start;
							// let callDateStartStr = crmService.minTwoDigits(callDateStart.getDate())+'/'+crmService.minTwoDigits((callDateStart.getMonth()+1))+'/'+callDateStart.getFullYear();
							// let callDateStartPlusFiveMinutesStr = fechas.add(dateStr,parseInt(parametros[index].valor));
							let callDateStartRestFiveMinutes = moment(callDateStart).add(-5, 'm').toDate();
							let expression = `0 ${callDateStartRestFiveMinutes.getMinutes()} ${callDateStartRestFiveMinutes.getHours()} ${callDateStartRestFiveMinutes.getDate()} ${callDateStartRestFiveMinutes.getMonth()+1} ${callDateStartRestFiveMinutes.getDay()}`;
							nodeCron.schedule(expression, async () => {
								try {
									let date = new Date();
									if (leadWithCall) {
										if (leadWithCall.leadUsersAssignedUser.userEmailAddrBeanRel && leadWithCall.leadUsersAssignedUser.userEmailAddrBeanRel.emailAddrBeanRelEmailAddresses) {
											let emailUser = leadWithCall.leadUsersAssignedUser.userEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.email_address;
											let user = leadWithCall.leadUsersAssignedUser;
											if (pilatCron.cro_mai_id) {
												let dateSentReceived;
												let callDateStart;
												if (typeof leadWithCall.leadCallsLeads.callLeadCalls.date_start == 'string') {
													callDateStart = new Date(leadWithCall.leadCallsLeads.callLeadCalls.date_start);
												} else {
													callDateStart = leadWithCall.leadCallsLeads.callLeadCalls.date_start;
												}
												if (typeof leadWithCall.leadUsersAssignedUser.userEmails.date_sent_received == 'string') {
													dateSentReceived = new Date(leadWithCall.leadUsersAssignedUser.userEmails.date_sent_received);
												} else {
													dateSentReceived = leadWithCall.leadUsersAssignedUser.userEmails.date_sent_received;
												}
												if (leadWithCall.leadUsersAssignedUser.userEmails &&
													leadWithCall.leadUsersAssignedUser.userEmails.status === 'sent' &&
													callDateStart.getHours() === dateSentReceived.getHours() &&
													callDateStart.getMinutes() === dateSentReceived.getMinutes()
												) {
												} else {
													let resp = crmService.sendMailRememberCall(pilatCron.cro_mai_id, leadWithCall,async (err, info) => {
														if (err) {
															loggerEmail.info('Error al enviar el correo: ' + date.toString(), {error:err, info:info});
														} {
															loggerEmail.info('Correo enviado: ' + date.toString(), {info:info});
															let maxUid = await models.sequelize.emails.max('uid');
															let max = parseInt(maxUid);
															let userEmails;
															if (leadWithCall.leadUsersAssignedUser.userEmails) {
																let userEmails = leadWithCall.leadUsersAssignedUser.userEmails;
																let userEmailId = userEmails.id;
																delete userEmails.id;
																userEmails.date_sent_received = new Date();
																userEmails.date_modified = new Date();
																let updatedUserEmails = await models.sequelize.emails.update(userEmails, {where:{id:userEmailId}});
																let respUserEmails = await models.sequelize.emails.findOne({where:{id:userEmailId}});
																userEmails = respUserEmails && respUserEmails.dataValues ? respUserEmails.dataValues : null;
															} else {
																let newUserEmail = {
																	id:models.sequelize.objectId().toString(),
																	name:user.first_name+' '+user.last_name,
																	date_entered:new Date(),
																	date_modified:new Date(),
																	modified_user_id:user.id,
																	created_by:user.id,
																	assigned_user_id:user.id,
																	date_sent_received:new Date(),
																	message_id:'',
																	type:'out',
																	status:err ? '' : 'sent',
																	// flagged:'',
																	// reply_to_status:'',
																	//intent:'pick',
																	// mailbox_id:'',
																	parent_type:'Users',
																	parent_id:user.id,
																	uid:max+1,
																	// category_id:''
																};
																userEmails = await models.sequelize.emails.create(newUserEmail);
															}
															if (typeof callback == 'function') {
																callback(err, info);
															}
														}
													})
												}
											}
										}
									}
								}	catch (e) {
									console.log(e)
								}
							});
						}
					}
				}
			}
		} catch (e) {
			console.log(e)
		}
	}

	static async initWhatsApp(callback = null) {
		try {
			let today = new Date();
			let currentDay = new Date(today.getFullYear(),today.getMonth(),today.getDate());
			let dateToday = moment(currentDay).toDate();
			let dateTomorrow = moment(currentDay).add(1,'days').toDate();

			baileys.init();

		} catch (e) {
			console.log(e);
		}
	}

}

module.exports = NodeCron;
