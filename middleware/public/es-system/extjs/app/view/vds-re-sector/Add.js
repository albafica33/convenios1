/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Fri Jun 04 2021 01:41:48 GMT-0400 (Bolivia Time)
 * Time: 1:41:48
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Fri Jun 04 2021 01:41:48 GMT-0400 (Bolivia Time)
 * Last time updated: 1:41:48
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-re-sector.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsReSectorAdd',
	id: 'vds-re-sector-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdReSector',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsCiudadCiudadWithCiudad = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-re-sector/findVdsCiudadCiudadWithCiudad',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'ciudad'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsProvinciaProvinciaWithProvincia = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-re-sector/findVdsProvinciaProvinciaWithProvincia',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'provincia'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsMunicipioMunicipioWithMunicipio = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-re-sector/findVdsMunicipioMunicipioWithMunicipio',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'municipio'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-re-sector-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						{
							fieldLabel: 'estado',
							name: 'estado',
						},
						
						
						{
							fieldLabel: 'sector',
							name: 'sector',
						},
						
						{
							fieldLabel: 'createdBy',
							name: 'createdBy',
						},
						
						{
							fieldLabel: 'updatedBy',
							name: 'updatedBy',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_ciudad',
								id: 'id_ciudad',
								fieldLabel: 'id_ciudad',
								store: storeVdsCiudadCiudadWithCiudad,
								valueField: "_id",
								displayField: "ciudad",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_provincia',
								id: 'id_provincia',
								fieldLabel: 'id_provincia',
								store: storeVdsProvinciaProvinciaWithProvincia,
								valueField: "_id",
								displayField: "provincia",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_municipio',
								id: 'id_municipio',
								fieldLabel: 'id_municipio',
								store: storeVdsMunicipioMunicipioWithMunicipio,
								valueField: "_id",
								displayField: "municipio",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'updatedAt',
							name: 'updatedAt',
							id:'updatedAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
