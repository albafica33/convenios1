/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Fri Jun 04 2021 01:41:48 GMT-0400 (Bolivia Time)
 * Time: 1:41:48
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Fri Jun 04 2021 01:41:48 GMT-0400 (Bolivia Time)
 * Last time updated: 1:41:48
 *
 * Caution: es-sections will be replaced by script execution
 */

// Set up a model to use in our Store
//<es-section>

var storeVdsCiudadCiudadWithCiudad = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-antidroga/vds-re-sector/findVdsCiudadCiudadWithCiudad',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'ciudad'],
	root: 'data',
	autoLoad: true
});

var storeVdsProvinciaProvinciaWithProvincia = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-antidroga/vds-re-sector/findVdsProvinciaProvinciaWithProvincia',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'provincia'],
	root: 'data',
	autoLoad: true
});

var storeVdsMunicipioMunicipioWithMunicipio = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-antidroga/vds-re-sector/findVdsMunicipioMunicipioWithMunicipio',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'municipio'],
	root: 'data',
	autoLoad: true
});

//</es-section>

Ext.define('es.view.vds-re-sector.List', {
	extend: 'Ext.grid.Panel',
	//<es-section>
	xtype: 'vdsReSectorList',
	//</es-section>
	title: 'Moduł użytkowników',

	viewConfig: {
		enableTextSelection: true,
		stripeRows: true
	},
	//<es-section>
	store: 'vds_re_sector',
	//</es-section>
	initComponent: function () {
		var me = this,
			rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
				clicksToEdit: 2,
				listeners:{
					dbclick: {
						element: 'body',
						fn: function(){ console.log('dblclick body'); }
					}
				}
			}),
			rowMenu = Ext.create('Ext.menu.Menu', {
				height: 58,
				width: 140,
				items: [{
					text: 'Edit',
					iconCls: 'button-edit'
				}, {
					text: 'Remove',
					iconCls: 'button-remove',
    				handler: function(){
					me.fireEvent('removeRow', this);
				}
			}]
		});
		this.listeners = {
			itemcontextmenu: function(view, record, item, index, e){
				e.stopEvent();
				rowMenu.showAt(e.getXY());
			}
		};

		this.plugins = [rowEditing];
		this.selType = 'rowmodel';

		this.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						text: 'Add',
						iconCls: 'icon-add',
						handler: () => {
							// Create a model instance
							// Create a model instance
							var date = new Date();
							//<es-section>
							var count = Ext.getStore('vdsReSector').data.length;
							var nextId = 0;
							for (var i = 0 ; i < count ; i++) {
								var id = Ext.getStore('vdsReSector').getAt(i).data.id;
								if(parseInt(id) > parseInt(nextId.toString())) {
									nextId = parseInt(id);
								}
							}
							Ext.getStore('vdsReSector').insert(count, {
								dueAt: date,
								createdAt: date,
								updatedAt: date,
							});
							Ext.getStore('vdsReSector').getAt(count).set('id',nextId+1);
							//</es-section>
						}
					}, '-' ,
					// {
					// 	text: 'Add Form',
					// 	itemId: 'add',
					// 	iconCls: 'icon-add',
					// },
					{
						xtype: 'container',
						flex: 1
					}
				]
			},
			{
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				width: 360,
				displayInfo: true,
				//<es-section>
				store: 'vdsReSector'
				//</es-section>
			}
		];

		this.columns = [

			//<es-section>
			
			{
				text: '_id',
				width: 160,
				dataIndex: '_id',
				hidden: false
			},
			
			
   			{
   			    text: 'id',
       			dataIndex: 'id',
       				width: 100,
       				editor: {
       				allowBlank: false
            	}
            },
            
			
			{
				text: 'sector',
				dataIndex: 'sector',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'createdBy',
				dataIndex: 'createdBy',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'updatedBy',
				dataIndex: 'updatedBy',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			
			
			
			
			
			
			
			
			{
				text: 'estado',
				dataIndex: 'estado',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			
			
			
			{
				text: 'id_ciudad',
				dataIndex: 'id_ciudad',
				renderer: (val, metaData, r) => {
                	let items = storeVdsCiudadCiudadWithCiudad.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.ciudad;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsCiudadCiudadWithCiudad,
					valueField: "_id",
					displayField: "ciudad",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'id_provincia',
				dataIndex: 'id_provincia',
				renderer: (val, metaData, r) => {
                	let items = storeVdsProvinciaProvinciaWithProvincia.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.provincia;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsProvinciaProvinciaWithProvincia,
					valueField: "_id",
					displayField: "provincia",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'id_municipio',
				dataIndex: 'id_municipio',
				renderer: (val, metaData, r) => {
                	let items = storeVdsMunicipioMunicipioWithMunicipio.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.municipio;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsMunicipioMunicipioWithMunicipio,
					valueField: "_id",
					displayField: "municipio",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			
			{
				text: 'dueAt',
				dataIndex: 'dueAt',
				width: 160,
				hidden:false,
				editor: {
					xtype: 'datefield',
					value: 'dueAt',
					editable: true,
					format: 'yy/m/d H:i:s',
					//minValue: new Date(),
				}
			},
			
			{
				text: 'createdAt',
				dataIndex: 'createdAt',
				width: 160,
				hidden:false,
				editor: {
					xtype: 'datefield',
					value: 'createdAt',
					editable: true,
					format: 'yy/m/d H:i:s',
					//minValue: new Date(),
				}
			},
			
			{
				text: 'updatedAt',
				dataIndex: 'updatedAt',
				width: 160,
				hidden:false,
				editor: {
					xtype: 'datefield',
					value: 'updatedAt',
					editable: true,
					format: 'yy/m/d H:i:s',
					//minValue: new Date(),
				}
			},
			
			//</es-section>
			{
				xtype: 'actioncolumn',
				width: 50,
				items: [
					{
						iconCls: 'button-add',
						tooltip: 'Add',
						icon: '/js/es/shared/icons/fam/add.gif',
						handler: function (grid, rowIndex, colIndex) {
							// Create a model instance
							// Create a model instance
							var date = new Date();
							//<es-section>
							var count = Ext.getStore('vdsReSector').data.length;
							var nextId = 0;
							for (var i = 0 ; i < count ; i++) {
								var id = Ext.getStore('vdsReSector').getAt(i).data.id;
								if(parseInt(id) > parseInt(nextId)) {
									nextId = parseInt(id);
								}
							}
							Ext.getStore('vdsReSector').insert(count, {
								dueAt: date,
								createdAt: date,
								updatedAt: date,
							});
							Ext.getStore('vdsReSector').getAt(count).set('id',nextId+1);
							rowEditing.startEdit(count,1);
						}
						//</es-section>
					},
					{
						iconCls: 'button-remove',
						tooltip: 'Remove',
						icon: '/js/es/shared/icons/fam/delete.gif',
						handler: function (grid, rowIndex, colIndex) {
							this.up('grid').fireEvent('removeRow', grid, rowIndex, colIndex);
						}
					}
				]
			}
		];

		//parent
		this.callParent(arguments);
	}
});
