/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:06:10 GMT-0400 (Bolivia Time)
 * Time: 0:6:10
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:06:10 GMT-0400 (Bolivia Time)
 * Last time updated: 0:6:10
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-user-roles.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsUserRolesAdd',
	id: 'vds-user-roles-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdUserRole',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsUsersUsrWithUserName = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-user-roles/findVdsUsersUsrWithUserName',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'user_name'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsRolesRolWithRolCode = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-user-roles/findVdsRolesRolWithRolCode',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'rol_code'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsUsrRolParStatusWithParOrder = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-user-roles/findVdsParamsUsrRolParStatusWithParOrder',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_order'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesCreatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-user-roles/findVdsUserRolesCreatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesUpdatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-user-roles/findVdsUserRolesUpdatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-user-roles-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						
						{
							fieldLabel: 'usr_rol_group',
							name: 'usr_rol_group',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'usr_id',
								id: 'usr_id',
								fieldLabel: 'usr_id',
								store: storeVdsUsersUsrWithUserName,
								valueField: "_id",
								displayField: "user_name",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'rol_id',
								id: 'rol_id',
								fieldLabel: 'rol_id',
								store: storeVdsRolesRolWithRolCode,
								valueField: "_id",
								displayField: "rol_code",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'usr_rol_par_status_id',
								id: 'usr_rol_par_status_id',
								fieldLabel: 'usr_rol_par_status_id',
								store: storeVdsParamsUsrRolParStatusWithParOrder,
								valueField: "_id",
								displayField: "par_order",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'createdById',
								id: 'createdById',
								fieldLabel: 'createdById',
								store: storeVdsUserRolesCreatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'updatedById',
								id: 'updatedById',
								fieldLabel: 'updatedById',
								store: storeVdsUserRolesUpdatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
