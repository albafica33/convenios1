/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:18 GMT-0400 (Bolivia Time)
 * Time: 1:25:18
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:18 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:18
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-page-components.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsPageComponentsAdd',
	id: 'vds-page-components-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdPageComponent',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsPagesPagWithPagCode = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-page-components/findVdsPagesPagWithPagCode',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'pag_code'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsComponentsComWithComCode = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-page-components/findVdsComponentsComWithComCode',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'com_code'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsPagComParStatusWithParCod = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-page-components/findVdsParamsPagComParStatusWithParCod',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_cod'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesCreatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-page-components/findVdsUserRolesCreatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesUpdatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-page-components/findVdsUserRolesUpdatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-page-components-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						
						{
							fieldLabel: 'pag_com_code',
							name: 'pag_com_code',
						},
						
						{
							fieldLabel: 'pag_com_group',
							name: 'pag_com_group',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'pag_id',
								id: 'pag_id',
								fieldLabel: 'pag_id',
								store: storeVdsPagesPagWithPagCode,
								valueField: "_id",
								displayField: "pag_code",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'com_id',
								id: 'com_id',
								fieldLabel: 'com_id',
								store: storeVdsComponentsComWithComCode,
								valueField: "_id",
								displayField: "com_code",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'pag_com_par_status_id',
								id: 'pag_com_par_status_id',
								fieldLabel: 'pag_com_par_status_id',
								store: storeVdsParamsPagComParStatusWithParCod,
								valueField: "_id",
								displayField: "par_cod",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'createdById',
								id: 'createdById',
								fieldLabel: 'createdById',
								store: storeVdsUserRolesCreatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'updatedById',
								id: 'updatedById',
								fieldLabel: 'updatedById',
								store: storeVdsUserRolesUpdatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
