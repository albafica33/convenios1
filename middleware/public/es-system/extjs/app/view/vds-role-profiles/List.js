/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Fri Jun 04 2021 01:41:56 GMT-0400 (Bolivia Time)
 * Time: 1:41:56
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Fri Jun 04 2021 01:41:56 GMT-0400 (Bolivia Time)
 * Last time updated: 1:41:56
 *
 * Caution: es-sections will be replaced by script execution
 */

// Set up a model to use in our Store
//<es-section>

var storeVdsRolesRolWithRolCode = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-antidroga/vds-role-profiles/findVdsRolesRolWithRolCode',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'rol_code'],
	root: 'data',
	autoLoad: true
});

var storeVdsProfilesProWithProCode = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-antidroga/vds-role-profiles/findVdsProfilesProWithProCode',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'pro_code'],
	root: 'data',
	autoLoad: true
});

var storeVdsParamsRolProParStatusWithParOrder = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-antidroga/vds-role-profiles/findVdsParamsRolProParStatusWithParOrder',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'par_order'],
	root: 'data',
	autoLoad: true
});

var storeVdsUserRolesCreatedbyWithUsrRolGroup = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-antidroga/vds-role-profiles/findVdsUserRolesCreatedbyWithUsrRolGroup',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'usr_rol_group'],
	root: 'data',
	autoLoad: true
});

var storeVdsUserRolesUpdatedbyWithUsrRolGroup = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-antidroga/vds-role-profiles/findVdsUserRolesUpdatedbyWithUsrRolGroup',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'usr_rol_group'],
	root: 'data',
	autoLoad: true
});

//</es-section>

Ext.define('es.view.vds-role-profiles.List', {
	extend: 'Ext.grid.Panel',
	//<es-section>
	xtype: 'vdsRoleProfilesList',
	//</es-section>
	title: 'Moduł użytkowników',

	viewConfig: {
		enableTextSelection: true,
		stripeRows: true
	},
	//<es-section>
	store: 'vds_role_profiles',
	//</es-section>
	initComponent: function () {
		var me = this,
			rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
				clicksToEdit: 2,
				listeners:{
					dbclick: {
						element: 'body',
						fn: function(){ console.log('dblclick body'); }
					}
				}
			}),
			rowMenu = Ext.create('Ext.menu.Menu', {
				height: 58,
				width: 140,
				items: [{
					text: 'Edit',
					iconCls: 'button-edit'
				}, {
					text: 'Remove',
					iconCls: 'button-remove',
    				handler: function(){
					me.fireEvent('removeRow', this);
				}
			}]
		});
		this.listeners = {
			itemcontextmenu: function(view, record, item, index, e){
				e.stopEvent();
				rowMenu.showAt(e.getXY());
			}
		};

		this.plugins = [rowEditing];
		this.selType = 'rowmodel';

		this.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						text: 'Add',
						iconCls: 'icon-add',
						handler: () => {
							// Create a model instance
							// Create a model instance
							var date = new Date();
							//<es-section>
							var count = Ext.getStore('vdsRoleProfiles').data.length;
							var nextId = 0;
							for (var i = 0 ; i < count ; i++) {
								var id = Ext.getStore('vdsRoleProfiles').getAt(i).data.id;
								if(parseInt(id) > parseInt(nextId.toString())) {
									nextId = parseInt(id);
								}
							}
							Ext.getStore('vdsRoleProfiles').insert(count, {
								dueAt: date,
								createdAt: date,
								updatedAt: date,
							});
							Ext.getStore('vdsRoleProfiles').getAt(count).set('id',nextId+1);
							//</es-section>
						}
					}, '-' ,
					// {
					// 	text: 'Add Form',
					// 	itemId: 'add',
					// 	iconCls: 'icon-add',
					// },
					{
						xtype: 'container',
						flex: 1
					}
				]
			},
			{
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				width: 360,
				displayInfo: true,
				//<es-section>
				store: 'vdsRoleProfiles'
				//</es-section>
			}
		];

		this.columns = [

			//<es-section>
			
			{
				text: '_id',
				width: 160,
				dataIndex: '_id',
				hidden: false
			},
			
			
   			{
   			    text: 'id',
       			dataIndex: 'id',
       				width: 100,
       				editor: {
       				allowBlank: false
            	}
            },
            
			
			{
				text: 'rol_pro_group',
				dataIndex: 'rol_pro_group',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			
			
			
			
			
			
			
			
			
			
			
			{
				text: 'rol_id',
				dataIndex: 'rol_id',
				renderer: (val, metaData, r) => {
                	let items = storeVdsRolesRolWithRolCode.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.rol_code;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsRolesRolWithRolCode,
					valueField: "_id",
					displayField: "rol_code",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'pro_id',
				dataIndex: 'pro_id',
				renderer: (val, metaData, r) => {
                	let items = storeVdsProfilesProWithProCode.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.pro_code;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsProfilesProWithProCode,
					valueField: "_id",
					displayField: "pro_code",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'rol_pro_par_status_id',
				dataIndex: 'rol_pro_par_status_id',
				renderer: (val, metaData, r) => {
                	let items = storeVdsParamsRolProParStatusWithParOrder.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.par_order;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsParamsRolProParStatusWithParOrder,
					valueField: "_id",
					displayField: "par_order",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'createdById',
				dataIndex: 'createdById',
				renderer: (val, metaData, r) => {
                	let items = storeVdsUserRolesCreatedbyWithUsrRolGroup.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.usr_rol_group;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsUserRolesCreatedbyWithUsrRolGroup,
					valueField: "_id",
					displayField: "usr_rol_group",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'updatedById',
				dataIndex: 'updatedById',
				renderer: (val, metaData, r) => {
                	let items = storeVdsUserRolesUpdatedbyWithUsrRolGroup.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.usr_rol_group;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsUserRolesUpdatedbyWithUsrRolGroup,
					valueField: "_id",
					displayField: "usr_rol_group",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			
			{
				text: 'dueAt',
				dataIndex: 'dueAt',
				width: 160,
				hidden:false,
				editor: {
					xtype: 'datefield',
					value: 'dueAt',
					editable: true,
					format: 'yy/m/d H:i:s',
					//minValue: new Date(),
				}
			},
			
			{
				text: 'createdAt',
				dataIndex: 'createdAt',
				width: 160,
				hidden:false,
				editor: {
					xtype: 'datefield',
					value: 'createdAt',
					editable: true,
					format: 'yy/m/d H:i:s',
					//minValue: new Date(),
				}
			},
			
			//</es-section>
			{
				xtype: 'actioncolumn',
				width: 50,
				items: [
					{
						iconCls: 'button-add',
						tooltip: 'Add',
						icon: '/js/es/shared/icons/fam/add.gif',
						handler: function (grid, rowIndex, colIndex) {
							// Create a model instance
							// Create a model instance
							var date = new Date();
							//<es-section>
							var count = Ext.getStore('vdsRoleProfiles').data.length;
							var nextId = 0;
							for (var i = 0 ; i < count ; i++) {
								var id = Ext.getStore('vdsRoleProfiles').getAt(i).data.id;
								if(parseInt(id) > parseInt(nextId)) {
									nextId = parseInt(id);
								}
							}
							Ext.getStore('vdsRoleProfiles').insert(count, {
								dueAt: date,
								createdAt: date,
								updatedAt: date,
							});
							Ext.getStore('vdsRoleProfiles').getAt(count).set('id',nextId+1);
							rowEditing.startEdit(count,1);
						}
						//</es-section>
					},
					{
						iconCls: 'button-remove',
						tooltip: 'Remove',
						icon: '/js/es/shared/icons/fam/delete.gif',
						handler: function (grid, rowIndex, colIndex) {
							this.up('grid').fireEvent('removeRow', grid, rowIndex, colIndex);
						}
					}
				]
			}
		];

		//parent
		this.callParent(arguments);
	}
});
