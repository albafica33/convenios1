/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:24:59 GMT-0400 (Bolivia Time)
 * Time: 1:24:59
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:24:59 GMT-0400 (Bolivia Time)
 * Last time updated: 1:24:59
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-object-attributes.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsObjectAttributesAdd',
	id: 'vds-object-attributes-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdObjectAttribute',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsObjectsObjWithObjDescription = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-object-attributes/findVdsObjectsObjWithObjDescription',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'obj_description'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsAttributesAttWithAttCode = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-object-attributes/findVdsAttributesAttWithAttCode',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'att_code'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsObjAttParEditableWithParCod = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-object-attributes/findVdsParamsObjAttParEditableWithParCod',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_cod'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsObjAttParInterfaceBehaviorWithParCod = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-object-attributes/findVdsParamsObjAttParInterfaceBehaviorWithParCod',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_cod'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsObjAttParStatusWithParCod = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-object-attributes/findVdsParamsObjAttParStatusWithParCod',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_cod'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesCreatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-object-attributes/findVdsUserRolesCreatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesUpdatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-object-attributes/findVdsUserRolesUpdatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-object-attributes-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						
						{
							fieldLabel: 'obj_att_default_value',
							name: 'obj_att_default_value',
						},
						
						{
							fieldLabel: 'obj_att_group',
							name: 'obj_att_group',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'obj_id',
								id: 'obj_id',
								fieldLabel: 'obj_id',
								store: storeVdsObjectsObjWithObjDescription,
								valueField: "_id",
								displayField: "obj_description",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'att_id',
								id: 'att_id',
								fieldLabel: 'att_id',
								store: storeVdsAttributesAttWithAttCode,
								valueField: "_id",
								displayField: "att_code",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'obj_att_par_editable_id',
								id: 'obj_att_par_editable_id',
								fieldLabel: 'obj_att_par_editable_id',
								store: storeVdsParamsObjAttParEditableWithParCod,
								valueField: "_id",
								displayField: "par_cod",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'obj_att_par_interface_behavior_id',
								id: 'obj_att_par_interface_behavior_id',
								fieldLabel: 'obj_att_par_interface_behavior_id',
								store: storeVdsParamsObjAttParInterfaceBehaviorWithParCod,
								valueField: "_id",
								displayField: "par_cod",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'obj_att_par_status_id',
								id: 'obj_att_par_status_id',
								fieldLabel: 'obj_att_par_status_id',
								store: storeVdsParamsObjAttParStatusWithParCod,
								valueField: "_id",
								displayField: "par_cod",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'createdById',
								id: 'createdById',
								fieldLabel: 'createdById',
								store: storeVdsUserRolesCreatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'updatedById',
								id: 'updatedById',
								fieldLabel: 'updatedById',
								store: storeVdsUserRolesUpdatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
