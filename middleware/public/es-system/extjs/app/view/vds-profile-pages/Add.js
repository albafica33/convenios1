/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:18 GMT-0400 (Bolivia Time)
 * Time: 1:25:18
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:18 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:18
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-profile-pages.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsProfilePagesAdd',
	id: 'vds-profile-pages-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdProfilePage',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsProfilesProWithProCode = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-profile-pages/findVdsProfilesProWithProCode',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'pro_code'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsPagesPagWithPagCode = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-profile-pages/findVdsPagesPagWithPagCode',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'pag_code'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsProPagParStatusWithParCod = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-profile-pages/findVdsParamsProPagParStatusWithParCod',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_cod'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesCreatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-profile-pages/findVdsUserRolesCreatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesUpdatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-profile-pages/findVdsUserRolesUpdatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-profile-pages-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						
						{
							fieldLabel: 'pro_pag_group',
							name: 'pro_pag_group',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'pro_id',
								id: 'pro_id',
								fieldLabel: 'pro_id',
								store: storeVdsProfilesProWithProCode,
								valueField: "_id",
								displayField: "pro_code",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'pag_id',
								id: 'pag_id',
								fieldLabel: 'pag_id',
								store: storeVdsPagesPagWithPagCode,
								valueField: "_id",
								displayField: "pag_code",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'pro_pag_par_status_id',
								id: 'pro_pag_par_status_id',
								fieldLabel: 'pro_pag_par_status_id',
								store: storeVdsParamsProPagParStatusWithParCod,
								valueField: "_id",
								displayField: "par_cod",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'createdById',
								id: 'createdById',
								fieldLabel: 'createdById',
								store: storeVdsUserRolesCreatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'updatedById',
								id: 'updatedById',
								fieldLabel: 'updatedById',
								store: storeVdsUserRolesUpdatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
