/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Fri Jun 04 2021 01:41:50 GMT-0400 (Bolivia Time)
 * Time: 1:41:50
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Fri Jun 04 2021 01:41:50 GMT-0400 (Bolivia Time)
 * Last time updated: 1:41:50
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-re-ftc-diario.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsReFtcDiarioAdd',
	id: 'vds-re-ftc-diario-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdReFtcDiario',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsReFtcSectorFtcSectorWithFtcSector = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-re-ftc-diario/findVdsReFtcSectorFtcSectorWithFtcSector',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'ftc_sector'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-re-ftc-diario-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
          				{
     		        		fieldLabel: 'cantidad',
    						name: 'cantidad',
     					},
                        
						
						
						{
							fieldLabel: 'estado',
							name: 'estado',
						},
						
						
						{
							fieldLabel: 'createdBy',
							name: 'createdBy',
						},
						
						{
							fieldLabel: 'updatedBy',
							name: 'updatedBy',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_ftc_sector',
								id: 'id_ftc_sector',
								fieldLabel: 'id_ftc_sector',
								store: storeVdsReFtcSectorFtcSectorWithFtcSector,
								valueField: "_id",
								displayField: "ftc_sector",
							})
						},
						
						
						{
							fieldLabel: 'fecha_ftc',
							name: 'fecha_ftc',
							id:'fecha_ftc',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'updatedAt',
							name: 'updatedAt',
							id:'updatedAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
