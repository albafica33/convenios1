/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:06:08 GMT-0400 (Bolivia Time)
 * Time: 0:6:8
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:06:08 GMT-0400 (Bolivia Time)
 * Last time updated: 0:6:8
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-persona.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsPersonaAdd',
	id: 'vds-persona-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdPersona',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsCiudadCiExpedoWithCiudad = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsCiudadCiExpedoWithCiudad',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'ciudad'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsEstadoCivilEstadoCivilWithDescripcion = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsEstadoCivilEstadoCivilWithDescripcion',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'descripcion'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsSexoSexoWithDescripcion = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsSexoSexoWithDescripcion',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'descripcion'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsMunicipioMunicipioWithMunicipio = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsMunicipioMunicipioWithMunicipio',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'municipio'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsProvinciaProvinciaWithProvincia = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsProvinciaProvinciaWithProvincia',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'provincia'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsCiudadCiudadWithCiudad = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsCiudadCiudadWithCiudad',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'ciudad'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsPaisPaisWithPais = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsPaisPaisWithPais',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'pais'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-persona-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						{
							fieldLabel: 'estado',
							name: 'estado',
						},
						
						
						{
							fieldLabel: 'nombres',
							name: 'nombres',
						},
						
						{
							fieldLabel: 'paterno',
							name: 'paterno',
						},
						
						{
							fieldLabel: 'materno',
							name: 'materno',
						},
						
						{
							fieldLabel: 'casada',
							name: 'casada',
						},
						
						{
							fieldLabel: 'ci',
							name: 'ci',
						},
						
						{
							fieldLabel: 'correo',
							name: 'correo',
						},
						
						{
							fieldLabel: 'direccion',
							name: 'direccion',
						},
						
						{
							fieldLabel: 'createdBy',
							name: 'createdBy',
						},
						
						{
							fieldLabel: 'updatedBy',
							name: 'updatedBy',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'ci_expedido',
								id: 'ci_expedido',
								fieldLabel: 'ci_expedido',
								store: storeVdsCiudadCiExpedoWithCiudad,
								valueField: "_id",
								displayField: "ciudad",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_estado_civil',
								id: 'id_estado_civil',
								fieldLabel: 'id_estado_civil',
								store: storeVdsEstadoCivilEstadoCivilWithDescripcion,
								valueField: "_id",
								displayField: "descripcion",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_sexo',
								id: 'id_sexo',
								fieldLabel: 'id_sexo',
								store: storeVdsSexoSexoWithDescripcion,
								valueField: "_id",
								displayField: "descripcion",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_municipio',
								id: 'id_municipio',
								fieldLabel: 'id_municipio',
								store: storeVdsMunicipioMunicipioWithMunicipio,
								valueField: "_id",
								displayField: "municipio",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_provincia',
								id: 'id_provincia',
								fieldLabel: 'id_provincia',
								store: storeVdsProvinciaProvinciaWithProvincia,
								valueField: "_id",
								displayField: "provincia",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_ciudad',
								id: 'id_ciudad',
								fieldLabel: 'id_ciudad',
								store: storeVdsCiudadCiudadWithCiudad,
								valueField: "_id",
								displayField: "ciudad",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_pais',
								id: 'id_pais',
								fieldLabel: 'id_pais',
								store: storeVdsPaisPaisWithPais,
								valueField: "_id",
								displayField: "pais",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'updatedAt',
							name: 'updatedAt',
							id:'updatedAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
