/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Fri Jun 04 2021 01:41:54 GMT-0400 (Bolivia Time)
 * Time: 1:41:54
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Fri Jun 04 2021 01:41:54 GMT-0400 (Bolivia Time)
 * Last time updated: 1:41:54
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-profile-components.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsProfileComponentsAdd',
	id: 'vds-profile-components-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdProfileComponent',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsParamsProComParStatusWithParOrder = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-profile-components/findVdsParamsProComParStatusWithParOrder',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_order'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesCreatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-profile-components/findVdsUserRolesCreatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesUpdatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-antidroga/vds-profile-components/findVdsUserRolesUpdatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-profile-components-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						
						{
							fieldLabel: 'pro_id',
							name: 'pro_id',
						},
						
						{
							fieldLabel: 'com_id',
							name: 'com_id',
						},
						
						{
							fieldLabel: 'pro_com_group',
							name: 'pro_com_group',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'pro_com_par_status_id',
								id: 'pro_com_par_status_id',
								fieldLabel: 'pro_com_par_status_id',
								store: storeVdsParamsProComParStatusWithParOrder,
								valueField: "_id",
								displayField: "par_order",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'createdById',
								id: 'createdById',
								fieldLabel: 'createdById',
								store: storeVdsUserRolesCreatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'updatedById',
								id: 'updatedById',
								fieldLabel: 'updatedById',
								store: storeVdsUserRolesUpdatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
