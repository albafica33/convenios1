/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Tue Jun 08 2021 23:47:50 GMT-0400 (GMT-04:00)
 * Time: 23:47:50
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Tue Jun 08 2021 23:47:50 GMT-0400 (GMT-04:00)
 * Last time updated: 23:47:50
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section> 
Ext.define('es.store.sequelizemeta', {
    extend: 'Ext.data.Store',
    model: 'es.model.sequelizemeta',
    autoLoad: true,
    autoSync: true,
    remoteFilter: true
});
//</es-section>
