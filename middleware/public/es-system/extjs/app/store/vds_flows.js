/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:10 GMT-0400 (Bolivia Time)
 * Time: 1:25:10
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:10 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:10
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section> 
Ext.define('es.store.vds_flows', {
    extend: 'Ext.data.Store',
    model: 'es.model.vds_flows',
    autoLoad: true,
    autoSync: true,
    remoteFilter: true
});
//</es-section>
