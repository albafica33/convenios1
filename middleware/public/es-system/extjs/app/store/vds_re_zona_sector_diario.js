/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 00:01:12 GMT-0400 (Bolivia Time)
 * Time: 0:1:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 00:01:12 GMT-0400 (Bolivia Time)
 * Last time updated: 0:1:12
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section> 
Ext.define('es.store.vds_re_zona_sector_diario', {
    extend: 'Ext.data.Store',
    model: 'es.model.vds_re_zona_sector_diario',
    autoLoad: true,
    autoSync: true,
    remoteFilter: true
});
//</es-section>
