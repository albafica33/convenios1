/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:09 GMT-0400 (Bolivia Time)
 * Time: 1:25:9
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:09 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:9
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section> 
Ext.define('es.store.vds_job_attributes', {
    extend: 'Ext.data.Store',
    model: 'es.model.vds_job_attributes',
    autoLoad: true,
    autoSync: true,
    remoteFilter: true
});
//</es-section>
