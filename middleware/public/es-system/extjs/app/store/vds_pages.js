/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:17 GMT-0400 (Bolivia Time)
 * Time: 1:25:17
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:17 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:17
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section> 
Ext.define('es.store.vds_pages', {
    extend: 'Ext.data.Store',
    model: 'es.model.vds_pages',
    autoLoad: true,
    autoSync: true,
    remoteFilter: true
});
//</es-section>
