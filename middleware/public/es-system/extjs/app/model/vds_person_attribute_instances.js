/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:16 GMT-0400 (Bolivia Time)
 * Time: 1:25:16
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:16 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:16
 *
 * Caution: es-sections will be replaced by script execution
 */

var types = Ext.data.Types; // allow shorthand type access
Ext.define('es.model.vdsPersonAttributeInstances', {
	extend: 'Ext.data.Model',

	proxy: {
		type: 'rest',
		//<es-section>
		url : '/api-antidroga/vds-person-attribute-instances/',
		//</es-section>
		reader: {
			type: 'json',
			root: 'data'
		}
	},

	fields: [
		//<es-section>
		
		{ name: '_id', type: 'text', defaultValue: null },
		
		{ name: 'id', type: 'number', defaultValue: null },
		
		{ name: 'per_att_id', type: 'text', defaultValue: null },
		
		{ name: 'per_att_value', type: 'text', defaultValue: null },
		
		{ name: 'per_att_ins_group', type: 'text', defaultValue: null },
		
		{ name: 'per_att_ins_par_status_id', type: 'text', defaultValue: null },
		
		{ name: 'createdById', type: 'text', defaultValue: null },
		
		{ name: 'updatedById', type: 'text', defaultValue: null },
		
		{ name: 'dueAt', type: 'date', defaultValue: null },
		
		{ name: 'createdAt', type: 'date', defaultValue: null },
		
		//</es-section>
	]
});
