/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:00 GMT-0400 (Bolivia Time)
 * Time: 1:25:0
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:00 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:0
 *
 * Caution: es-sections will be replaced by script execution
 */

var types = Ext.data.Types; // allow shorthand type access
Ext.define('es.model.vdsComponents', {
	extend: 'Ext.data.Model',

	proxy: {
		type: 'rest',
		//<es-section>
		url : '/api-antidroga/vds-components/',
		//</es-section>
		reader: {
			type: 'json',
			root: 'data'
		}
	},

	fields: [
		//<es-section>
		
		{ name: '_id', type: 'text', defaultValue: null },
		
		{ name: 'id', type: 'number', defaultValue: null },
		
		{ name: 'com_mod_id', type: 'text', defaultValue: null },
		
		{ name: 'com_code', type: 'text', defaultValue: null },
		
		{ name: 'com_tag', type: 'text', defaultValue: null },
		
		{ name: 'com_description', type: 'text', defaultValue: null },
		
		{ name: 'com_par_type_id', type: 'text', defaultValue: null },
		
		{ name: 'com_parent_id', type: 'text', defaultValue: null },
		
		{ name: 'com_group', type: 'text', defaultValue: null },
		
		{ name: 'com_par_status_id', type: 'text', defaultValue: null },
		
		{ name: 'createdById', type: 'text', defaultValue: null },
		
		{ name: 'updatedById', type: 'text', defaultValue: null },
		
		{ name: 'dueAt', type: 'date', defaultValue: null },
		
		{ name: 'createdAt', type: 'date', defaultValue: null },
		
		//</es-section>
	]
});
