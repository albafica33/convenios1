/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:24:58 GMT-0400 (Bolivia Time)
 * Time: 1:24:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:24:58 GMT-0400 (Bolivia Time)
 * Last time updated: 1:24:58
 *
 * Caution: es-sections will be replaced by script execution
 */

var types = Ext.data.Types; // allow shorthand type access
Ext.define('es.model.vdsPersonAttributes', {
	extend: 'Ext.data.Model',

	proxy: {
		type: 'rest',
		//<es-section>
		url : '/api-antidroga/vds-person-attributes/',
		//</es-section>
		reader: {
			type: 'json',
			root: 'data'
		}
	},

	fields: [
		//<es-section>
		
		{ name: '_id', type: 'text', defaultValue: null },
		
		{ name: 'id', type: 'number', defaultValue: null },
		
		{ name: 'per_id', type: 'text', defaultValue: null },
		
		{ name: 'per_att_name', type: 'text', defaultValue: null },
		
		{ name: 'per_att_value', type: 'text', defaultValue: null },
		
		{ name: 'per_att_group', type: 'text', defaultValue: null },
		
		{ name: 'per_att_par_status_id', type: 'text', defaultValue: null },
		
		{ name: 'createdById', type: 'text', defaultValue: null },
		
		{ name: 'updatedById', type: 'text', defaultValue: null },
		
		{ name: 'dueAt', type: 'date', defaultValue: null },
		
		{ name: 'createdAt', type: 'date', defaultValue: null },
		
		//</es-section>
	]
});
