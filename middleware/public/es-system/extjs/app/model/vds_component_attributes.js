/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:13 GMT-0400 (Bolivia Time)
 * Time: 1:25:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:13 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:13
 *
 * Caution: es-sections will be replaced by script execution
 */

var types = Ext.data.Types; // allow shorthand type access
Ext.define('es.model.vdsComponentAttributes', {
	extend: 'Ext.data.Model',

	proxy: {
		type: 'rest',
		//<es-section>
		url : '/api-antidroga/vds-component-attributes/',
		//</es-section>
		reader: {
			type: 'json',
			root: 'data'
		}
	},

	fields: [
		//<es-section>
		
		{ name: '_id', type: 'text', defaultValue: null },
		
		{ name: 'id', type: 'number', defaultValue: null },
		
		{ name: 'com_id', type: 'text', defaultValue: null },
		
		{ name: 'com_att_name', type: 'text', defaultValue: null },
		
		{ name: 'com_att_description', type: 'text', defaultValue: null },
		
		{ name: 'com_att_group', type: 'text', defaultValue: null },
		
		{ name: 'com_att_par_status_id', type: 'text', defaultValue: null },
		
		{ name: 'createdById', type: 'text', defaultValue: null },
		
		{ name: 'updatedById', type: 'text', defaultValue: null },
		
		{ name: 'dueAt', type: 'date', defaultValue: null },
		
		{ name: 'createdAt', type: 'date', defaultValue: null },
		
		//</es-section>
	]
});
