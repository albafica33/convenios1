/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 00:01:12 GMT-0400 (Bolivia Time)
 * Time: 0:1:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 00:01:12 GMT-0400 (Bolivia Time)
 * Last time updated: 0:1:12
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsReFtcSector', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-re-ftc-sector.List',
		'vds-re-ftc-sector.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_re_ftc_sector'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsReFtcSectorList',
			selector: 'vdsReFtcSectorList'
		},
		{
			ref: 'vdsReFtcSectorAdd',
			selector: 'vdsReFtcSectorAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsReFtcSectorList > toolbar > button#add': {
				click: me.onVdsReFtcSectorAddClick
			},
			'vdsReFtcSectorList':{
				removeRow: me.removeRow
			},
			'vdsReFtcSectorAdd > form > button#save': {
				click: me.onVdsReFtcSectorAddSaveClick
			},
			'vdsReFtcSectorAdd > form > button#cancel': {
				click: me.onVdsReFtcSectorAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsReFtcSectorAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsReFtcSectorAdd().destroy();
		//</es-section>
	},
	onVdsReFtcSectorAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsReFtcSectorAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsReFtcSectorList().getStore().add(rec);

			me.getVdsReFtcSectorAdd().destroy();
		}
		//</es-section>
	},
	onVdsReFtcSectorAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsReFtcSectorAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsReFtcSector());
		//</es-section>
	}
});
