/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 00:01:11 GMT-0400 (Bolivia Time)
 * Time: 0:1:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 00:01:11 GMT-0400 (Bolivia Time)
 * Last time updated: 0:1:11
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsReSector', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-re-sector.List',
		'vds-re-sector.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_re_sector'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsReSectorList',
			selector: 'vdsReSectorList'
		},
		{
			ref: 'vdsReSectorAdd',
			selector: 'vdsReSectorAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsReSectorList > toolbar > button#add': {
				click: me.onVdsReSectorAddClick
			},
			'vdsReSectorList':{
				removeRow: me.removeRow
			},
			'vdsReSectorAdd > form > button#save': {
				click: me.onVdsReSectorAddSaveClick
			},
			'vdsReSectorAdd > form > button#cancel': {
				click: me.onVdsReSectorAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsReSectorAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsReSectorAdd().destroy();
		//</es-section>
	},
	onVdsReSectorAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsReSectorAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsReSectorList().getStore().add(rec);

			me.getVdsReSectorAdd().destroy();
		}
		//</es-section>
	},
	onVdsReSectorAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsReSectorAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsReSector());
		//</es-section>
	}
});
