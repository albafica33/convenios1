/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:07 GMT-0400 (Bolivia Time)
 * Time: 1:25:7
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:07 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:7
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsComponentFieldAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-component-field-attributes.List',
		'vds-component-field-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_component_field_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsComponentFieldAttributesList',
			selector: 'vdsComponentFieldAttributesList'
		},
		{
			ref: 'vdsComponentFieldAttributesAdd',
			selector: 'vdsComponentFieldAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsComponentFieldAttributesList > toolbar > button#add': {
				click: me.onVdsComponentFieldAttributesAddClick
			},
			'vdsComponentFieldAttributesList':{
				removeRow: me.removeRow
			},
			'vdsComponentFieldAttributesAdd > form > button#save': {
				click: me.onVdsComponentFieldAttributesAddSaveClick
			},
			'vdsComponentFieldAttributesAdd > form > button#cancel': {
				click: me.onVdsComponentFieldAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsComponentFieldAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsComponentFieldAttributesAdd().destroy();
		//</es-section>
	},
	onVdsComponentFieldAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsComponentFieldAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsComponentFieldAttributesList().getStore().add(rec);

			me.getVdsComponentFieldAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsComponentFieldAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsComponentFieldAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsComponentFieldAttributes());
		//</es-section>
	}
});
