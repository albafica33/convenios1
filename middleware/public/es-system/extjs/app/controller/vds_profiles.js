/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:01 GMT-0400 (Bolivia Time)
 * Time: 1:25:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:01 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:1
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsProfiles', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-profiles.List',
		'vds-profiles.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_profiles'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsProfilesList',
			selector: 'vdsProfilesList'
		},
		{
			ref: 'vdsProfilesAdd',
			selector: 'vdsProfilesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsProfilesList > toolbar > button#add': {
				click: me.onVdsProfilesAddClick
			},
			'vdsProfilesList':{
				removeRow: me.removeRow
			},
			'vdsProfilesAdd > form > button#save': {
				click: me.onVdsProfilesAddSaveClick
			},
			'vdsProfilesAdd > form > button#cancel': {
				click: me.onVdsProfilesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsProfilesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsProfilesAdd().destroy();
		//</es-section>
	},
	onVdsProfilesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsProfilesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsProfilesList().getStore().add(rec);

			me.getVdsProfilesAdd().destroy();
		}
		//</es-section>
	},
	onVdsProfilesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsProfilesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsProfiles());
		//</es-section>
	}
});
