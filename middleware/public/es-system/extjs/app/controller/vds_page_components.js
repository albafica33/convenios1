/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:18 GMT-0400 (Bolivia Time)
 * Time: 1:25:18
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:18 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:18
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsPageComponents', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-page-components.List',
		'vds-page-components.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_page_components'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsPageComponentsList',
			selector: 'vdsPageComponentsList'
		},
		{
			ref: 'vdsPageComponentsAdd',
			selector: 'vdsPageComponentsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsPageComponentsList > toolbar > button#add': {
				click: me.onVdsPageComponentsAddClick
			},
			'vdsPageComponentsList':{
				removeRow: me.removeRow
			},
			'vdsPageComponentsAdd > form > button#save': {
				click: me.onVdsPageComponentsAddSaveClick
			},
			'vdsPageComponentsAdd > form > button#cancel': {
				click: me.onVdsPageComponentsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsPageComponentsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsPageComponentsAdd().destroy();
		//</es-section>
	},
	onVdsPageComponentsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsPageComponentsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsPageComponentsList().getStore().add(rec);

			me.getVdsPageComponentsAdd().destroy();
		}
		//</es-section>
	},
	onVdsPageComponentsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsPageComponentsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsPageComponents());
		//</es-section>
	}
});
