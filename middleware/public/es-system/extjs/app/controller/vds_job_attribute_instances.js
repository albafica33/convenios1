/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:10 GMT-0400 (Bolivia Time)
 * Time: 1:25:10
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:10 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:10
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsJobAttributeInstances', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-job-attribute-instances.List',
		'vds-job-attribute-instances.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_job_attribute_instances'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsJobAttributeInstancesList',
			selector: 'vdsJobAttributeInstancesList'
		},
		{
			ref: 'vdsJobAttributeInstancesAdd',
			selector: 'vdsJobAttributeInstancesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsJobAttributeInstancesList > toolbar > button#add': {
				click: me.onVdsJobAttributeInstancesAddClick
			},
			'vdsJobAttributeInstancesList':{
				removeRow: me.removeRow
			},
			'vdsJobAttributeInstancesAdd > form > button#save': {
				click: me.onVdsJobAttributeInstancesAddSaveClick
			},
			'vdsJobAttributeInstancesAdd > form > button#cancel': {
				click: me.onVdsJobAttributeInstancesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsJobAttributeInstancesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsJobAttributeInstancesAdd().destroy();
		//</es-section>
	},
	onVdsJobAttributeInstancesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsJobAttributeInstancesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsJobAttributeInstancesList().getStore().add(rec);

			me.getVdsJobAttributeInstancesAdd().destroy();
		}
		//</es-section>
	},
	onVdsJobAttributeInstancesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsJobAttributeInstancesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsJobAttributeInstances());
		//</es-section>
	}
});
