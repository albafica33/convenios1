/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:09 GMT-0400 (Bolivia Time)
 * Time: 1:25:9
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:09 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:9
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsCronAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-cron-attributes.List',
		'vds-cron-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_cron_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsCronAttributesList',
			selector: 'vdsCronAttributesList'
		},
		{
			ref: 'vdsCronAttributesAdd',
			selector: 'vdsCronAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsCronAttributesList > toolbar > button#add': {
				click: me.onVdsCronAttributesAddClick
			},
			'vdsCronAttributesList':{
				removeRow: me.removeRow
			},
			'vdsCronAttributesAdd > form > button#save': {
				click: me.onVdsCronAttributesAddSaveClick
			},
			'vdsCronAttributesAdd > form > button#cancel': {
				click: me.onVdsCronAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsCronAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsCronAttributesAdd().destroy();
		//</es-section>
	},
	onVdsCronAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsCronAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsCronAttributesList().getStore().add(rec);

			me.getVdsCronAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsCronAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsCronAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsCronAttributes());
		//</es-section>
	}
});
