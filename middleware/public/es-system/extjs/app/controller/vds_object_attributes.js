/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:24:59 GMT-0400 (Bolivia Time)
 * Time: 1:24:59
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:24:59 GMT-0400 (Bolivia Time)
 * Last time updated: 1:24:59
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsObjectAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-object-attributes.List',
		'vds-object-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_object_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsObjectAttributesList',
			selector: 'vdsObjectAttributesList'
		},
		{
			ref: 'vdsObjectAttributesAdd',
			selector: 'vdsObjectAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsObjectAttributesList > toolbar > button#add': {
				click: me.onVdsObjectAttributesAddClick
			},
			'vdsObjectAttributesList':{
				removeRow: me.removeRow
			},
			'vdsObjectAttributesAdd > form > button#save': {
				click: me.onVdsObjectAttributesAddSaveClick
			},
			'vdsObjectAttributesAdd > form > button#cancel': {
				click: me.onVdsObjectAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsObjectAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsObjectAttributesAdd().destroy();
		//</es-section>
	},
	onVdsObjectAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsObjectAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsObjectAttributesList().getStore().add(rec);

			me.getVdsObjectAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsObjectAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsObjectAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsObjectAttributes());
		//</es-section>
	}
});
