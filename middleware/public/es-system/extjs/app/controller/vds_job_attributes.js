/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:09 GMT-0400 (Bolivia Time)
 * Time: 1:25:9
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:09 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:9
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsJobAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-job-attributes.List',
		'vds-job-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_job_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsJobAttributesList',
			selector: 'vdsJobAttributesList'
		},
		{
			ref: 'vdsJobAttributesAdd',
			selector: 'vdsJobAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsJobAttributesList > toolbar > button#add': {
				click: me.onVdsJobAttributesAddClick
			},
			'vdsJobAttributesList':{
				removeRow: me.removeRow
			},
			'vdsJobAttributesAdd > form > button#save': {
				click: me.onVdsJobAttributesAddSaveClick
			},
			'vdsJobAttributesAdd > form > button#cancel': {
				click: me.onVdsJobAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsJobAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsJobAttributesAdd().destroy();
		//</es-section>
	},
	onVdsJobAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsJobAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsJobAttributesList().getStore().add(rec);

			me.getVdsJobAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsJobAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsJobAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsJobAttributes());
		//</es-section>
	}
});
