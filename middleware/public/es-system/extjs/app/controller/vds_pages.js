/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:17 GMT-0400 (Bolivia Time)
 * Time: 1:25:17
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:17 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:17
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsPages', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-pages.List',
		'vds-pages.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_pages'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsPagesList',
			selector: 'vdsPagesList'
		},
		{
			ref: 'vdsPagesAdd',
			selector: 'vdsPagesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsPagesList > toolbar > button#add': {
				click: me.onVdsPagesAddClick
			},
			'vdsPagesList':{
				removeRow: me.removeRow
			},
			'vdsPagesAdd > form > button#save': {
				click: me.onVdsPagesAddSaveClick
			},
			'vdsPagesAdd > form > button#cancel': {
				click: me.onVdsPagesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsPagesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsPagesAdd().destroy();
		//</es-section>
	},
	onVdsPagesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsPagesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsPagesList().getStore().add(rec);

			me.getVdsPagesAdd().destroy();
		}
		//</es-section>
	},
	onVdsPagesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsPagesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsPages());
		//</es-section>
	}
});
