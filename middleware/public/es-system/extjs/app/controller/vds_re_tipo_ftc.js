/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 00:01:12 GMT-0400 (Bolivia Time)
 * Time: 0:1:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 00:01:12 GMT-0400 (Bolivia Time)
 * Last time updated: 0:1:12
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsReTipoFtc', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-re-tipo-ftc.List',
		'vds-re-tipo-ftc.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_re_tipo_ftc'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsReTipoFtcList',
			selector: 'vdsReTipoFtcList'
		},
		{
			ref: 'vdsReTipoFtcAdd',
			selector: 'vdsReTipoFtcAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsReTipoFtcList > toolbar > button#add': {
				click: me.onVdsReTipoFtcAddClick
			},
			'vdsReTipoFtcList':{
				removeRow: me.removeRow
			},
			'vdsReTipoFtcAdd > form > button#save': {
				click: me.onVdsReTipoFtcAddSaveClick
			},
			'vdsReTipoFtcAdd > form > button#cancel': {
				click: me.onVdsReTipoFtcAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsReTipoFtcAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsReTipoFtcAdd().destroy();
		//</es-section>
	},
	onVdsReTipoFtcAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsReTipoFtcAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsReTipoFtcList().getStore().add(rec);

			me.getVdsReTipoFtcAdd().destroy();
		}
		//</es-section>
	},
	onVdsReTipoFtcAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsReTipoFtcAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsReTipoFtc());
		//</es-section>
	}
});
