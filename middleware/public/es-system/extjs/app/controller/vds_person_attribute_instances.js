/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:16 GMT-0400 (Bolivia Time)
 * Time: 1:25:16
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:16 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:16
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsPersonAttributeInstances', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-person-attribute-instances.List',
		'vds-person-attribute-instances.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_person_attribute_instances'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsPersonAttributeInstancesList',
			selector: 'vdsPersonAttributeInstancesList'
		},
		{
			ref: 'vdsPersonAttributeInstancesAdd',
			selector: 'vdsPersonAttributeInstancesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsPersonAttributeInstancesList > toolbar > button#add': {
				click: me.onVdsPersonAttributeInstancesAddClick
			},
			'vdsPersonAttributeInstancesList':{
				removeRow: me.removeRow
			},
			'vdsPersonAttributeInstancesAdd > form > button#save': {
				click: me.onVdsPersonAttributeInstancesAddSaveClick
			},
			'vdsPersonAttributeInstancesAdd > form > button#cancel': {
				click: me.onVdsPersonAttributeInstancesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsPersonAttributeInstancesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsPersonAttributeInstancesAdd().destroy();
		//</es-section>
	},
	onVdsPersonAttributeInstancesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsPersonAttributeInstancesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsPersonAttributeInstancesList().getStore().add(rec);

			me.getVdsPersonAttributeInstancesAdd().destroy();
		}
		//</es-section>
	},
	onVdsPersonAttributeInstancesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsPersonAttributeInstancesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsPersonAttributeInstances());
		//</es-section>
	}
});
