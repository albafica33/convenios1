/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:00 GMT-0400 (Bolivia Time)
 * Time: 1:25:0
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:00 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:0
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsComponents', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-components.List',
		'vds-components.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_components'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsComponentsList',
			selector: 'vdsComponentsList'
		},
		{
			ref: 'vdsComponentsAdd',
			selector: 'vdsComponentsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsComponentsList > toolbar > button#add': {
				click: me.onVdsComponentsAddClick
			},
			'vdsComponentsList':{
				removeRow: me.removeRow
			},
			'vdsComponentsAdd > form > button#save': {
				click: me.onVdsComponentsAddSaveClick
			},
			'vdsComponentsAdd > form > button#cancel': {
				click: me.onVdsComponentsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsComponentsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsComponentsAdd().destroy();
		//</es-section>
	},
	onVdsComponentsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsComponentsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsComponentsList().getStore().add(rec);

			me.getVdsComponentsAdd().destroy();
		}
		//</es-section>
	},
	onVdsComponentsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsComponentsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsComponents());
		//</es-section>
	}
});
