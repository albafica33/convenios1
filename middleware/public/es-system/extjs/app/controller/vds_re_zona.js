/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 00:01:11 GMT-0400 (Bolivia Time)
 * Time: 0:1:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 00:01:11 GMT-0400 (Bolivia Time)
 * Last time updated: 0:1:11
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsReZona', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-re-zona.List',
		'vds-re-zona.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_re_zona'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsReZonaList',
			selector: 'vdsReZonaList'
		},
		{
			ref: 'vdsReZonaAdd',
			selector: 'vdsReZonaAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsReZonaList > toolbar > button#add': {
				click: me.onVdsReZonaAddClick
			},
			'vdsReZonaList':{
				removeRow: me.removeRow
			},
			'vdsReZonaAdd > form > button#save': {
				click: me.onVdsReZonaAddSaveClick
			},
			'vdsReZonaAdd > form > button#cancel': {
				click: me.onVdsReZonaAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsReZonaAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsReZonaAdd().destroy();
		//</es-section>
	},
	onVdsReZonaAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsReZonaAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsReZonaList().getStore().add(rec);

			me.getVdsReZonaAdd().destroy();
		}
		//</es-section>
	},
	onVdsReZonaAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsReZonaAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsReZona());
		//</es-section>
	}
});
