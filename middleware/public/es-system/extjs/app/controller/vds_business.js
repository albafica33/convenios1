/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:14 GMT-0400 (Bolivia Time)
 * Time: 1:25:14
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:14 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:14
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsBusiness', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-business.List',
		'vds-business.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_business'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsBusinessList',
			selector: 'vdsBusinessList'
		},
		{
			ref: 'vdsBusinessAdd',
			selector: 'vdsBusinessAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsBusinessList > toolbar > button#add': {
				click: me.onVdsBusinessAddClick
			},
			'vdsBusinessList':{
				removeRow: me.removeRow
			},
			'vdsBusinessAdd > form > button#save': {
				click: me.onVdsBusinessAddSaveClick
			},
			'vdsBusinessAdd > form > button#cancel': {
				click: me.onVdsBusinessAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsBusinessAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsBusinessAdd().destroy();
		//</es-section>
	},
	onVdsBusinessAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsBusinessAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsBusinessList().getStore().add(rec);

			me.getVdsBusinessAdd().destroy();
		}
		//</es-section>
	},
	onVdsBusinessAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsBusinessAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsBusiness());
		//</es-section>
	}
});
