/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:02 GMT-0400 (Bolivia Time)
 * Time: 1:25:2
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:02 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:2
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsProfileViews', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-profile-views.List',
		'vds-profile-views.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_profile_views'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsProfileViewsList',
			selector: 'vdsProfileViewsList'
		},
		{
			ref: 'vdsProfileViewsAdd',
			selector: 'vdsProfileViewsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsProfileViewsList > toolbar > button#add': {
				click: me.onVdsProfileViewsAddClick
			},
			'vdsProfileViewsList':{
				removeRow: me.removeRow
			},
			'vdsProfileViewsAdd > form > button#save': {
				click: me.onVdsProfileViewsAddSaveClick
			},
			'vdsProfileViewsAdd > form > button#cancel': {
				click: me.onVdsProfileViewsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsProfileViewsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsProfileViewsAdd().destroy();
		//</es-section>
	},
	onVdsProfileViewsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsProfileViewsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsProfileViewsList().getStore().add(rec);

			me.getVdsProfileViewsAdd().destroy();
		}
		//</es-section>
	},
	onVdsProfileViewsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsProfileViewsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsProfileViews());
		//</es-section>
	}
});
