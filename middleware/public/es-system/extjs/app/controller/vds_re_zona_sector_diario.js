/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 00:01:12 GMT-0400 (Bolivia Time)
 * Time: 0:1:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 00:01:12 GMT-0400 (Bolivia Time)
 * Last time updated: 0:1:12
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsReZonaSectorDiario', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-re-zona-sector-diario.List',
		'vds-re-zona-sector-diario.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_re_zona_sector_diario'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsReZonaSectorDiarioList',
			selector: 'vdsReZonaSectorDiarioList'
		},
		{
			ref: 'vdsReZonaSectorDiarioAdd',
			selector: 'vdsReZonaSectorDiarioAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsReZonaSectorDiarioList > toolbar > button#add': {
				click: me.onVdsReZonaSectorDiarioAddClick
			},
			'vdsReZonaSectorDiarioList':{
				removeRow: me.removeRow
			},
			'vdsReZonaSectorDiarioAdd > form > button#save': {
				click: me.onVdsReZonaSectorDiarioAddSaveClick
			},
			'vdsReZonaSectorDiarioAdd > form > button#cancel': {
				click: me.onVdsReZonaSectorDiarioAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsReZonaSectorDiarioAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsReZonaSectorDiarioAdd().destroy();
		//</es-section>
	},
	onVdsReZonaSectorDiarioAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsReZonaSectorDiarioAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsReZonaSectorDiarioList().getStore().add(rec);

			me.getVdsReZonaSectorDiarioAdd().destroy();
		}
		//</es-section>
	},
	onVdsReZonaSectorDiarioAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsReZonaSectorDiarioAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsReZonaSectorDiario());
		//</es-section>
	}
});
