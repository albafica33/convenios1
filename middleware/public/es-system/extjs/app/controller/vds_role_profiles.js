/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:03 GMT-0400 (Bolivia Time)
 * Time: 1:25:3
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:03 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:3
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsRoleProfiles', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-role-profiles.List',
		'vds-role-profiles.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_role_profiles'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsRoleProfilesList',
			selector: 'vdsRoleProfilesList'
		},
		{
			ref: 'vdsRoleProfilesAdd',
			selector: 'vdsRoleProfilesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsRoleProfilesList > toolbar > button#add': {
				click: me.onVdsRoleProfilesAddClick
			},
			'vdsRoleProfilesList':{
				removeRow: me.removeRow
			},
			'vdsRoleProfilesAdd > form > button#save': {
				click: me.onVdsRoleProfilesAddSaveClick
			},
			'vdsRoleProfilesAdd > form > button#cancel': {
				click: me.onVdsRoleProfilesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsRoleProfilesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsRoleProfilesAdd().destroy();
		//</es-section>
	},
	onVdsRoleProfilesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsRoleProfilesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsRoleProfilesList().getStore().add(rec);

			me.getVdsRoleProfilesAdd().destroy();
		}
		//</es-section>
	},
	onVdsRoleProfilesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsRoleProfilesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsRoleProfiles());
		//</es-section>
	}
});
