/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:24:58 GMT-0400 (Bolivia Time)
 * Time: 1:24:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:24:58 GMT-0400 (Bolivia Time)
 * Last time updated: 1:24:58
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsPersonAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-person-attributes.List',
		'vds-person-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_person_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsPersonAttributesList',
			selector: 'vdsPersonAttributesList'
		},
		{
			ref: 'vdsPersonAttributesAdd',
			selector: 'vdsPersonAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsPersonAttributesList > toolbar > button#add': {
				click: me.onVdsPersonAttributesAddClick
			},
			'vdsPersonAttributesList':{
				removeRow: me.removeRow
			},
			'vdsPersonAttributesAdd > form > button#save': {
				click: me.onVdsPersonAttributesAddSaveClick
			},
			'vdsPersonAttributesAdd > form > button#cancel': {
				click: me.onVdsPersonAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsPersonAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsPersonAttributesAdd().destroy();
		//</es-section>
	},
	onVdsPersonAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsPersonAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsPersonAttributesList().getStore().add(rec);

			me.getVdsPersonAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsPersonAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsPersonAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsPersonAttributes());
		//</es-section>
	}
});
