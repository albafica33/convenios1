/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:10 GMT-0400 (Bolivia Time)
 * Time: 1:25:10
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:10 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:10
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsFlows', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-flows.List',
		'vds-flows.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_flows'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsFlowsList',
			selector: 'vdsFlowsList'
		},
		{
			ref: 'vdsFlowsAdd',
			selector: 'vdsFlowsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsFlowsList > toolbar > button#add': {
				click: me.onVdsFlowsAddClick
			},
			'vdsFlowsList':{
				removeRow: me.removeRow
			},
			'vdsFlowsAdd > form > button#save': {
				click: me.onVdsFlowsAddSaveClick
			},
			'vdsFlowsAdd > form > button#cancel': {
				click: me.onVdsFlowsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsFlowsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsFlowsAdd().destroy();
		//</es-section>
	},
	onVdsFlowsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsFlowsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsFlowsList().getStore().add(rec);

			me.getVdsFlowsAdd().destroy();
		}
		//</es-section>
	},
	onVdsFlowsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsFlowsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsFlows());
		//</es-section>
	}
});
