/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:11 GMT-0400 (Bolivia Time)
 * Time: 1:25:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:11 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:11
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsCronAttributeInstances', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-cron-attribute-instances.List',
		'vds-cron-attribute-instances.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_cron_attribute_instances'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsCronAttributeInstancesList',
			selector: 'vdsCronAttributeInstancesList'
		},
		{
			ref: 'vdsCronAttributeInstancesAdd',
			selector: 'vdsCronAttributeInstancesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsCronAttributeInstancesList > toolbar > button#add': {
				click: me.onVdsCronAttributeInstancesAddClick
			},
			'vdsCronAttributeInstancesList':{
				removeRow: me.removeRow
			},
			'vdsCronAttributeInstancesAdd > form > button#save': {
				click: me.onVdsCronAttributeInstancesAddSaveClick
			},
			'vdsCronAttributeInstancesAdd > form > button#cancel': {
				click: me.onVdsCronAttributeInstancesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsCronAttributeInstancesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsCronAttributeInstancesAdd().destroy();
		//</es-section>
	},
	onVdsCronAttributeInstancesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsCronAttributeInstancesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsCronAttributeInstancesList().getStore().add(rec);

			me.getVdsCronAttributeInstancesAdd().destroy();
		}
		//</es-section>
	},
	onVdsCronAttributeInstancesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsCronAttributeInstancesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsCronAttributeInstances());
		//</es-section>
	}
});
