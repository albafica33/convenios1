/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:08 GMT-0400 (Bolivia Time)
 * Time: 1:25:8
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:08 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:8
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsJobs', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-jobs.List',
		'vds-jobs.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_jobs'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsJobsList',
			selector: 'vdsJobsList'
		},
		{
			ref: 'vdsJobsAdd',
			selector: 'vdsJobsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsJobsList > toolbar > button#add': {
				click: me.onVdsJobsAddClick
			},
			'vdsJobsList':{
				removeRow: me.removeRow
			},
			'vdsJobsAdd > form > button#save': {
				click: me.onVdsJobsAddSaveClick
			},
			'vdsJobsAdd > form > button#cancel': {
				click: me.onVdsJobsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsJobsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsJobsAdd().destroy();
		//</es-section>
	},
	onVdsJobsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsJobsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsJobsList().getStore().add(rec);

			me.getVdsJobsAdd().destroy();
		}
		//</es-section>
	},
	onVdsJobsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsJobsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsJobs());
		//</es-section>
	}
});
