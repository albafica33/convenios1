/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:11 GMT-0400 (Bolivia Time)
 * Time: 1:25:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:11 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:11
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsLogAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-log-attributes.List',
		'vds-log-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_log_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsLogAttributesList',
			selector: 'vdsLogAttributesList'
		},
		{
			ref: 'vdsLogAttributesAdd',
			selector: 'vdsLogAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsLogAttributesList > toolbar > button#add': {
				click: me.onVdsLogAttributesAddClick
			},
			'vdsLogAttributesList':{
				removeRow: me.removeRow
			},
			'vdsLogAttributesAdd > form > button#save': {
				click: me.onVdsLogAttributesAddSaveClick
			},
			'vdsLogAttributesAdd > form > button#cancel': {
				click: me.onVdsLogAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsLogAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsLogAttributesAdd().destroy();
		//</es-section>
	},
	onVdsLogAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsLogAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsLogAttributesList().getStore().add(rec);

			me.getVdsLogAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsLogAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsLogAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsLogAttributes());
		//</es-section>
	}
});
