/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:16 GMT-0400 (Bolivia Time)
 * Time: 1:25:16
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:16 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:16
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsMailAttributeInstances', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-mail-attribute-instances.List',
		'vds-mail-attribute-instances.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_mail_attribute_instances'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsMailAttributeInstancesList',
			selector: 'vdsMailAttributeInstancesList'
		},
		{
			ref: 'vdsMailAttributeInstancesAdd',
			selector: 'vdsMailAttributeInstancesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsMailAttributeInstancesList > toolbar > button#add': {
				click: me.onVdsMailAttributeInstancesAddClick
			},
			'vdsMailAttributeInstancesList':{
				removeRow: me.removeRow
			},
			'vdsMailAttributeInstancesAdd > form > button#save': {
				click: me.onVdsMailAttributeInstancesAddSaveClick
			},
			'vdsMailAttributeInstancesAdd > form > button#cancel': {
				click: me.onVdsMailAttributeInstancesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsMailAttributeInstancesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsMailAttributeInstancesAdd().destroy();
		//</es-section>
	},
	onVdsMailAttributeInstancesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsMailAttributeInstancesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsMailAttributeInstancesList().getStore().add(rec);

			me.getVdsMailAttributeInstancesAdd().destroy();
		}
		//</es-section>
	},
	onVdsMailAttributeInstancesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsMailAttributeInstancesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsMailAttributeInstances());
		//</es-section>
	}
});
