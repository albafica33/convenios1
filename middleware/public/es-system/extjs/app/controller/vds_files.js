/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:24:56 GMT-0400 (Bolivia Time)
 * Time: 1:24:56
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:24:56 GMT-0400 (Bolivia Time)
 * Last time updated: 1:24:56
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsFiles', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-files.List',
		'vds-files.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_files'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsFilesList',
			selector: 'vdsFilesList'
		},
		{
			ref: 'vdsFilesAdd',
			selector: 'vdsFilesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsFilesList > toolbar > button#add': {
				click: me.onVdsFilesAddClick
			},
			'vdsFilesList':{
				removeRow: me.removeRow
			},
			'vdsFilesAdd > form > button#save': {
				click: me.onVdsFilesAddSaveClick
			},
			'vdsFilesAdd > form > button#cancel': {
				click: me.onVdsFilesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsFilesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsFilesAdd().destroy();
		//</es-section>
	},
	onVdsFilesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsFilesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsFilesList().getStore().add(rec);

			me.getVdsFilesAdd().destroy();
		}
		//</es-section>
	},
	onVdsFilesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsFilesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsFiles());
		//</es-section>
	}
});
