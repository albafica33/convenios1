/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:10 GMT-0400 (Bolivia Time)
 * Time: 1:25:10
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:10 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:10
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsFlowAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-flow-attributes.List',
		'vds-flow-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_flow_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsFlowAttributesList',
			selector: 'vdsFlowAttributesList'
		},
		{
			ref: 'vdsFlowAttributesAdd',
			selector: 'vdsFlowAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsFlowAttributesList > toolbar > button#add': {
				click: me.onVdsFlowAttributesAddClick
			},
			'vdsFlowAttributesList':{
				removeRow: me.removeRow
			},
			'vdsFlowAttributesAdd > form > button#save': {
				click: me.onVdsFlowAttributesAddSaveClick
			},
			'vdsFlowAttributesAdd > form > button#cancel': {
				click: me.onVdsFlowAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsFlowAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsFlowAttributesAdd().destroy();
		//</es-section>
	},
	onVdsFlowAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsFlowAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsFlowAttributesList().getStore().add(rec);

			me.getVdsFlowAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsFlowAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsFlowAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsFlowAttributes());
		//</es-section>
	}
});
