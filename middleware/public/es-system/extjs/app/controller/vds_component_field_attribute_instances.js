/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:13 GMT-0400 (Bolivia Time)
 * Time: 1:25:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:13 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:13
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsComponentFieldAttributeInstances', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-component-field-attribute-instances.List',
		'vds-component-field-attribute-instances.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_component_field_attribute_instances'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsComponentFieldAttributeInstancesList',
			selector: 'vdsComponentFieldAttributeInstancesList'
		},
		{
			ref: 'vdsComponentFieldAttributeInstancesAdd',
			selector: 'vdsComponentFieldAttributeInstancesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsComponentFieldAttributeInstancesList > toolbar > button#add': {
				click: me.onVdsComponentFieldAttributeInstancesAddClick
			},
			'vdsComponentFieldAttributeInstancesList':{
				removeRow: me.removeRow
			},
			'vdsComponentFieldAttributeInstancesAdd > form > button#save': {
				click: me.onVdsComponentFieldAttributeInstancesAddSaveClick
			},
			'vdsComponentFieldAttributeInstancesAdd > form > button#cancel': {
				click: me.onVdsComponentFieldAttributeInstancesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsComponentFieldAttributeInstancesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsComponentFieldAttributeInstancesAdd().destroy();
		//</es-section>
	},
	onVdsComponentFieldAttributeInstancesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsComponentFieldAttributeInstancesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsComponentFieldAttributeInstancesList().getStore().add(rec);

			me.getVdsComponentFieldAttributeInstancesAdd().destroy();
		}
		//</es-section>
	},
	onVdsComponentFieldAttributeInstancesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsComponentFieldAttributeInstancesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsComponentFieldAttributeInstances());
		//</es-section>
	}
});
