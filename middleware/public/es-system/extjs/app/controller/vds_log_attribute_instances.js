/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:11 GMT-0400 (Bolivia Time)
 * Time: 1:25:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:11 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:11
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsLogAttributeInstances', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-log-attribute-instances.List',
		'vds-log-attribute-instances.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_log_attribute_instances'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsLogAttributeInstancesList',
			selector: 'vdsLogAttributeInstancesList'
		},
		{
			ref: 'vdsLogAttributeInstancesAdd',
			selector: 'vdsLogAttributeInstancesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsLogAttributeInstancesList > toolbar > button#add': {
				click: me.onVdsLogAttributeInstancesAddClick
			},
			'vdsLogAttributeInstancesList':{
				removeRow: me.removeRow
			},
			'vdsLogAttributeInstancesAdd > form > button#save': {
				click: me.onVdsLogAttributeInstancesAddSaveClick
			},
			'vdsLogAttributeInstancesAdd > form > button#cancel': {
				click: me.onVdsLogAttributeInstancesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsLogAttributeInstancesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsLogAttributeInstancesAdd().destroy();
		//</es-section>
	},
	onVdsLogAttributeInstancesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsLogAttributeInstancesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsLogAttributeInstancesList().getStore().add(rec);

			me.getVdsLogAttributeInstancesAdd().destroy();
		}
		//</es-section>
	},
	onVdsLogAttributeInstancesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsLogAttributeInstancesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsLogAttributeInstances());
		//</es-section>
	}
});
