/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 00:01:13 GMT-0400 (Bolivia Time)
 * Time: 0:1:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 00:01:13 GMT-0400 (Bolivia Time)
 * Last time updated: 0:1:13
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsReFtcDiario', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-re-ftc-diario.List',
		'vds-re-ftc-diario.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_re_ftc_diario'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsReFtcDiarioList',
			selector: 'vdsReFtcDiarioList'
		},
		{
			ref: 'vdsReFtcDiarioAdd',
			selector: 'vdsReFtcDiarioAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsReFtcDiarioList > toolbar > button#add': {
				click: me.onVdsReFtcDiarioAddClick
			},
			'vdsReFtcDiarioList':{
				removeRow: me.removeRow
			},
			'vdsReFtcDiarioAdd > form > button#save': {
				click: me.onVdsReFtcDiarioAddSaveClick
			},
			'vdsReFtcDiarioAdd > form > button#cancel': {
				click: me.onVdsReFtcDiarioAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsReFtcDiarioAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsReFtcDiarioAdd().destroy();
		//</es-section>
	},
	onVdsReFtcDiarioAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsReFtcDiarioAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsReFtcDiarioList().getStore().add(rec);

			me.getVdsReFtcDiarioAdd().destroy();
		}
		//</es-section>
	},
	onVdsReFtcDiarioAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsReFtcDiarioAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsReFtcDiario());
		//</es-section>
	}
});
