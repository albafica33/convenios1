/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:24:59 GMT-0400 (Bolivia Time)
 * Time: 1:24:59
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:24:59 GMT-0400 (Bolivia Time)
 * Last time updated: 1:24:59
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsFileAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-file-attributes.List',
		'vds-file-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_file_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsFileAttributesList',
			selector: 'vdsFileAttributesList'
		},
		{
			ref: 'vdsFileAttributesAdd',
			selector: 'vdsFileAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsFileAttributesList > toolbar > button#add': {
				click: me.onVdsFileAttributesAddClick
			},
			'vdsFileAttributesList':{
				removeRow: me.removeRow
			},
			'vdsFileAttributesAdd > form > button#save': {
				click: me.onVdsFileAttributesAddSaveClick
			},
			'vdsFileAttributesAdd > form > button#cancel': {
				click: me.onVdsFileAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsFileAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsFileAttributesAdd().destroy();
		//</es-section>
	},
	onVdsFileAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsFileAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsFileAttributesList().getStore().add(rec);

			me.getVdsFileAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsFileAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsFileAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsFileAttributes());
		//</es-section>
	}
});
