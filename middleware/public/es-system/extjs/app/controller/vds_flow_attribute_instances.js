/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:12 GMT-0400 (Bolivia Time)
 * Time: 1:25:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:12 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:12
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsFlowAttributeInstances', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-flow-attribute-instances.List',
		'vds-flow-attribute-instances.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_flow_attribute_instances'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsFlowAttributeInstancesList',
			selector: 'vdsFlowAttributeInstancesList'
		},
		{
			ref: 'vdsFlowAttributeInstancesAdd',
			selector: 'vdsFlowAttributeInstancesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsFlowAttributeInstancesList > toolbar > button#add': {
				click: me.onVdsFlowAttributeInstancesAddClick
			},
			'vdsFlowAttributeInstancesList':{
				removeRow: me.removeRow
			},
			'vdsFlowAttributeInstancesAdd > form > button#save': {
				click: me.onVdsFlowAttributeInstancesAddSaveClick
			},
			'vdsFlowAttributeInstancesAdd > form > button#cancel': {
				click: me.onVdsFlowAttributeInstancesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsFlowAttributeInstancesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsFlowAttributeInstancesAdd().destroy();
		//</es-section>
	},
	onVdsFlowAttributeInstancesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsFlowAttributeInstancesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsFlowAttributeInstancesList().getStore().add(rec);

			me.getVdsFlowAttributeInstancesAdd().destroy();
		}
		//</es-section>
	},
	onVdsFlowAttributeInstancesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsFlowAttributeInstancesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsFlowAttributeInstances());
		//</es-section>
	}
});
