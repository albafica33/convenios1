/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:24:59 GMT-0400 (Bolivia Time)
 * Time: 1:24:59
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:24:59 GMT-0400 (Bolivia Time)
 * Last time updated: 1:24:59
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsObjects', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-objects.List',
		'vds-objects.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_objects'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsObjectsList',
			selector: 'vdsObjectsList'
		},
		{
			ref: 'vdsObjectsAdd',
			selector: 'vdsObjectsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsObjectsList > toolbar > button#add': {
				click: me.onVdsObjectsAddClick
			},
			'vdsObjectsList':{
				removeRow: me.removeRow
			},
			'vdsObjectsAdd > form > button#save': {
				click: me.onVdsObjectsAddSaveClick
			},
			'vdsObjectsAdd > form > button#cancel': {
				click: me.onVdsObjectsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsObjectsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsObjectsAdd().destroy();
		//</es-section>
	},
	onVdsObjectsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsObjectsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsObjectsList().getStore().add(rec);

			me.getVdsObjectsAdd().destroy();
		}
		//</es-section>
	},
	onVdsObjectsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsObjectsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsObjects());
		//</es-section>
	}
});
