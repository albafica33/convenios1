/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:06 GMT-0400 (Bolivia Time)
 * Time: 1:25:6
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:06 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:6
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsFields', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-fields.List',
		'vds-fields.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_fields'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsFieldsList',
			selector: 'vdsFieldsList'
		},
		{
			ref: 'vdsFieldsAdd',
			selector: 'vdsFieldsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsFieldsList > toolbar > button#add': {
				click: me.onVdsFieldsAddClick
			},
			'vdsFieldsList':{
				removeRow: me.removeRow
			},
			'vdsFieldsAdd > form > button#save': {
				click: me.onVdsFieldsAddSaveClick
			},
			'vdsFieldsAdd > form > button#cancel': {
				click: me.onVdsFieldsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsFieldsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsFieldsAdd().destroy();
		//</es-section>
	},
	onVdsFieldsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsFieldsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsFieldsList().getStore().add(rec);

			me.getVdsFieldsAdd().destroy();
		}
		//</es-section>
	},
	onVdsFieldsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsFieldsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsFields());
		//</es-section>
	}
});
