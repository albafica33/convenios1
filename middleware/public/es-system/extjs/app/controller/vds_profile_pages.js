/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:18 GMT-0400 (Bolivia Time)
 * Time: 1:25:18
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:18 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:18
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsProfilePages', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-profile-pages.List',
		'vds-profile-pages.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_profile_pages'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsProfilePagesList',
			selector: 'vdsProfilePagesList'
		},
		{
			ref: 'vdsProfilePagesAdd',
			selector: 'vdsProfilePagesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsProfilePagesList > toolbar > button#add': {
				click: me.onVdsProfilePagesAddClick
			},
			'vdsProfilePagesList':{
				removeRow: me.removeRow
			},
			'vdsProfilePagesAdd > form > button#save': {
				click: me.onVdsProfilePagesAddSaveClick
			},
			'vdsProfilePagesAdd > form > button#cancel': {
				click: me.onVdsProfilePagesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsProfilePagesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsProfilePagesAdd().destroy();
		//</es-section>
	},
	onVdsProfilePagesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsProfilePagesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsProfilePagesList().getStore().add(rec);

			me.getVdsProfilePagesAdd().destroy();
		}
		//</es-section>
	},
	onVdsProfilePagesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsProfilePagesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsProfilePages());
		//</es-section>
	}
});
