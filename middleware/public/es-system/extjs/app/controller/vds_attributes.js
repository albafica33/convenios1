/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:24:58 GMT-0400 (Bolivia Time)
 * Time: 1:24:58
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:24:58 GMT-0400 (Bolivia Time)
 * Last time updated: 1:24:58
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-attributes.List',
		'vds-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsAttributesList',
			selector: 'vdsAttributesList'
		},
		{
			ref: 'vdsAttributesAdd',
			selector: 'vdsAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsAttributesList > toolbar > button#add': {
				click: me.onVdsAttributesAddClick
			},
			'vdsAttributesList':{
				removeRow: me.removeRow
			},
			'vdsAttributesAdd > form > button#save': {
				click: me.onVdsAttributesAddSaveClick
			},
			'vdsAttributesAdd > form > button#cancel': {
				click: me.onVdsAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsAttributesAdd().destroy();
		//</es-section>
	},
	onVdsAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsAttributesList().getStore().add(rec);

			me.getVdsAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsAttributes());
		//</es-section>
	}
});
