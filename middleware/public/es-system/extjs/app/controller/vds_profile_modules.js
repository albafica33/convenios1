/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:02 GMT-0400 (Bolivia Time)
 * Time: 1:25:2
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:02 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:2
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsProfileModules', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-profile-modules.List',
		'vds-profile-modules.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_profile_modules'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsProfileModulesList',
			selector: 'vdsProfileModulesList'
		},
		{
			ref: 'vdsProfileModulesAdd',
			selector: 'vdsProfileModulesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsProfileModulesList > toolbar > button#add': {
				click: me.onVdsProfileModulesAddClick
			},
			'vdsProfileModulesList':{
				removeRow: me.removeRow
			},
			'vdsProfileModulesAdd > form > button#save': {
				click: me.onVdsProfileModulesAddSaveClick
			},
			'vdsProfileModulesAdd > form > button#cancel': {
				click: me.onVdsProfileModulesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsProfileModulesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsProfileModulesAdd().destroy();
		//</es-section>
	},
	onVdsProfileModulesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsProfileModulesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsProfileModulesList().getStore().add(rec);

			me.getVdsProfileModulesAdd().destroy();
		}
		//</es-section>
	},
	onVdsProfileModulesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsProfileModulesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsProfileModules());
		//</es-section>
	}
});
