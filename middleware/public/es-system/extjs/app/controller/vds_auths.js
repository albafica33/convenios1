/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:05 GMT-0400 (Bolivia Time)
 * Time: 1:25:5
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:05 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:5
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsAuths', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-auths.List',
		'vds-auths.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_auths'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsAuthsList',
			selector: 'vdsAuthsList'
		},
		{
			ref: 'vdsAuthsAdd',
			selector: 'vdsAuthsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsAuthsList > toolbar > button#add': {
				click: me.onVdsAuthsAddClick
			},
			'vdsAuthsList':{
				removeRow: me.removeRow
			},
			'vdsAuthsAdd > form > button#save': {
				click: me.onVdsAuthsAddSaveClick
			},
			'vdsAuthsAdd > form > button#cancel': {
				click: me.onVdsAuthsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsAuthsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsAuthsAdd().destroy();
		//</es-section>
	},
	onVdsAuthsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsAuthsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsAuthsList().getStore().add(rec);

			me.getVdsAuthsAdd().destroy();
		}
		//</es-section>
	},
	onVdsAuthsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsAuthsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsAuths());
		//</es-section>
	}
});
