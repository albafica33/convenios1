/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 00:01:11 GMT-0400 (Bolivia Time)
 * Time: 0:1:11
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 00:01:11 GMT-0400 (Bolivia Time)
 * Last time updated: 0:1:11
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsReTipoZona', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-re-tipo-zona.List',
		'vds-re-tipo-zona.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_re_tipo_zona'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsReTipoZonaList',
			selector: 'vdsReTipoZonaList'
		},
		{
			ref: 'vdsReTipoZonaAdd',
			selector: 'vdsReTipoZonaAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsReTipoZonaList > toolbar > button#add': {
				click: me.onVdsReTipoZonaAddClick
			},
			'vdsReTipoZonaList':{
				removeRow: me.removeRow
			},
			'vdsReTipoZonaAdd > form > button#save': {
				click: me.onVdsReTipoZonaAddSaveClick
			},
			'vdsReTipoZonaAdd > form > button#cancel': {
				click: me.onVdsReTipoZonaAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsReTipoZonaAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsReTipoZonaAdd().destroy();
		//</es-section>
	},
	onVdsReTipoZonaAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsReTipoZonaAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsReTipoZonaList().getStore().add(rec);

			me.getVdsReTipoZonaAdd().destroy();
		}
		//</es-section>
	},
	onVdsReTipoZonaAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsReTipoZonaAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsReTipoZona());
		//</es-section>
	}
});
