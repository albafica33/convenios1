/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:12 GMT-0400 (Bolivia Time)
 * Time: 1:25:12
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:12 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:12
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsObjectAttributeInstances', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-object-attribute-instances.List',
		'vds-object-attribute-instances.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_object_attribute_instances'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsObjectAttributeInstancesList',
			selector: 'vdsObjectAttributeInstancesList'
		},
		{
			ref: 'vdsObjectAttributeInstancesAdd',
			selector: 'vdsObjectAttributeInstancesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsObjectAttributeInstancesList > toolbar > button#add': {
				click: me.onVdsObjectAttributeInstancesAddClick
			},
			'vdsObjectAttributeInstancesList':{
				removeRow: me.removeRow
			},
			'vdsObjectAttributeInstancesAdd > form > button#save': {
				click: me.onVdsObjectAttributeInstancesAddSaveClick
			},
			'vdsObjectAttributeInstancesAdd > form > button#cancel': {
				click: me.onVdsObjectAttributeInstancesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsObjectAttributeInstancesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsObjectAttributeInstancesAdd().destroy();
		//</es-section>
	},
	onVdsObjectAttributeInstancesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsObjectAttributeInstancesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsObjectAttributeInstancesList().getStore().add(rec);

			me.getVdsObjectAttributeInstancesAdd().destroy();
		}
		//</es-section>
	},
	onVdsObjectAttributeInstancesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsObjectAttributeInstancesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsObjectAttributeInstances());
		//</es-section>
	}
});
