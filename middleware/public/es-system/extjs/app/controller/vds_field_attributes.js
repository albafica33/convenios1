/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:08 GMT-0400 (Bolivia Time)
 * Time: 1:25:8
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:08 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:8
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsFieldAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-field-attributes.List',
		'vds-field-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_field_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsFieldAttributesList',
			selector: 'vdsFieldAttributesList'
		},
		{
			ref: 'vdsFieldAttributesAdd',
			selector: 'vdsFieldAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsFieldAttributesList > toolbar > button#add': {
				click: me.onVdsFieldAttributesAddClick
			},
			'vdsFieldAttributesList':{
				removeRow: me.removeRow
			},
			'vdsFieldAttributesAdd > form > button#save': {
				click: me.onVdsFieldAttributesAddSaveClick
			},
			'vdsFieldAttributesAdd > form > button#cancel': {
				click: me.onVdsFieldAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsFieldAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsFieldAttributesAdd().destroy();
		//</es-section>
	},
	onVdsFieldAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsFieldAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsFieldAttributesList().getStore().add(rec);

			me.getVdsFieldAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsFieldAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsFieldAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsFieldAttributes());
		//</es-section>
	}
});
