/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:15 GMT-0400 (Bolivia Time)
 * Time: 1:25:15
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:15 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:15
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsMailAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-mail-attributes.List',
		'vds-mail-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_mail_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsMailAttributesList',
			selector: 'vdsMailAttributesList'
		},
		{
			ref: 'vdsMailAttributesAdd',
			selector: 'vdsMailAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsMailAttributesList > toolbar > button#add': {
				click: me.onVdsMailAttributesAddClick
			},
			'vdsMailAttributesList':{
				removeRow: me.removeRow
			},
			'vdsMailAttributesAdd > form > button#save': {
				click: me.onVdsMailAttributesAddSaveClick
			},
			'vdsMailAttributesAdd > form > button#cancel': {
				click: me.onVdsMailAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsMailAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsMailAttributesAdd().destroy();
		//</es-section>
	},
	onVdsMailAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsMailAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsMailAttributesList().getStore().add(rec);

			me.getVdsMailAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsMailAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsMailAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsMailAttributes());
		//</es-section>
	}
});
