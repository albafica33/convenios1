/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:01 GMT-0400 (Bolivia Time)
 * Time: 1:25:1
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:01 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:1
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsProfileComponents', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-profile-components.List',
		'vds-profile-components.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_profile_components'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsProfileComponentsList',
			selector: 'vdsProfileComponentsList'
		},
		{
			ref: 'vdsProfileComponentsAdd',
			selector: 'vdsProfileComponentsAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsProfileComponentsList > toolbar > button#add': {
				click: me.onVdsProfileComponentsAddClick
			},
			'vdsProfileComponentsList':{
				removeRow: me.removeRow
			},
			'vdsProfileComponentsAdd > form > button#save': {
				click: me.onVdsProfileComponentsAddSaveClick
			},
			'vdsProfileComponentsAdd > form > button#cancel': {
				click: me.onVdsProfileComponentsAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsProfileComponentsAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsProfileComponentsAdd().destroy();
		//</es-section>
	},
	onVdsProfileComponentsAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsProfileComponentsAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsProfileComponentsList().getStore().add(rec);

			me.getVdsProfileComponentsAdd().destroy();
		}
		//</es-section>
	},
	onVdsProfileComponentsAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsProfileComponentsAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsProfileComponents());
		//</es-section>
	}
});
