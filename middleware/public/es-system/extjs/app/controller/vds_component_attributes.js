/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Wed Jun 02 2021 01:25:13 GMT-0400 (Bolivia Time)
 * Time: 1:25:13
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Wed Jun 02 2021 01:25:13 GMT-0400 (Bolivia Time)
 * Last time updated: 1:25:13
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.controller.vdsComponentAttributes', {
	extend: 'Ext.app.Controller',

	views:[
		//<es-section>
		'vds-component-attributes.List',
		'vds-component-attributes.Add'
		//</es-section>
	],

	stores:[
		//<es-section>
		'vds_component_attributes'
		//</es-section>
	],

	refs: [
		//<es-section>
		{
			ref: 'vdsComponentAttributesList',
			selector: 'vdsComponentAttributesList'
		},
		{
			ref: 'vdsComponentAttributesAdd',
			selector: 'vdsComponentAttributesAdd'
		}
		//</es-section>
	],

	init: function () {
		var me = this;

		this.control({
			//<es-section>
			'vdsComponentAttributesList > toolbar > button#add': {
				click: me.onVdsComponentAttributesAddClick
			},
			'vdsComponentAttributesList':{
				removeRow: me.removeRow
			},
			'vdsComponentAttributesAdd > form > button#save': {
				click: me.onVdsComponentAttributesAddSaveClick
			},
			'vdsComponentAttributesAdd > form > button#cancel': {
				click: me.onVdsComponentAttributesAddCancelClick
			}
			//</es-section>
		});
	},
	removeRow: function(grid, rowIndex, colIndex){
		//<es-section>
		Ext.Msg.confirm('Confirm', 'Remove?', function(button) {
			if (button === 'yes') {
				grid.getStore().removeAt(rowIndex);
			}
		});
		//</es-section>
	},
	onVdsComponentAttributesAddCancelClick: function(button, e, eOpts) {
		//<es-section>
		this.getVdsComponentAttributesAdd().destroy();
		//</es-section>
	},
	onVdsComponentAttributesAddSaveClick: function(){
		//<es-section>
		var me = this, form = me.getVdsComponentAttributesAdd().down('form').getForm(), rec;
		if(form.isValid())
		{
			form.updateRecord();
			rec = form.getRecord();
			me.getVdsComponentAttributesList().getStore().add(rec);

			me.getVdsComponentAttributesAdd().destroy();
		}
		//</es-section>
	},
	onVdsComponentAttributesAddClick: function(){
		//<es-section>
		var me = this, window = Ext.widget('vdsComponentAttributesAdd');
		window.show();
		window.down('form').getForm().loadRecord(new es.model.vdsComponentAttributes());
		//</es-section>
	}
});
