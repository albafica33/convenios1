/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jan 25 2021 02:44:28 GMT-0400 (Bolivia Time)
 * Time: 2:44:28
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jan 25 2021 02:44:28 GMT-0400 (Bolivia Time)
 * Last time updated: 2:44:28
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.application({
	name: 'es',

	views: [
		'Viewport',
		//<es-section>
	
		'vds_dictionaries.List',
		'vds_dictionaries.Add',
	
		'vds_params.List',
		'vds_params.Add',
	
		'vds_roles.List',
		'vds_roles.Add',
	
		'vds_people.List',
		'vds_people.Add',
	
		'vds_mails.List',
		'vds_mails.Add',
	
		'vds_logs.List',
		'vds_logs.Add',
	
		'vds_pais.List',
		'vds_pais.Add',
	
		'vds_ciudad.List',
		'vds_ciudad.Add',
	
		'vds_estado_civil.List',
		'vds_estado_civil.Add',
	
		'vds_municipio.List',
		'vds_municipio.Add',
	
		'vds_sexo.List',
		'vds_sexo.Add',
	
		'vds_provincia.List',
		'vds_provincia.Add',
	
		'vds_persona.List',
		'vds_persona.Add',
	
		'vds_users.List',
		'vds_users.Add',
	
		'vds_crons.List',
		'vds_crons.Add',
	
		'vds_modules.List',
		'vds_modules.Add',
	
		'vds_views.List',
		'vds_views.Add',
	
		'vds_user_roles.List',
		'vds_user_roles.Add',
	
		'vds_matriz.List',
		'vds_matriz.Add',
	
		'vds_reg_usuario.List',
		'vds_reg_usuario.Add',
	
		//</es-section>
	],

	controllers: [
		'Main',
		//<es-section>
		
		'vds_dictionaries',
		
		'vds_params',
		
		'vds_roles',
		
		'vds_people',
		
		'vds_mails',
		
		'vds_logs',
		
		'vds_pais',
		
		'vds_ciudad',
		
		'vds_estado_civil',
		
		'vds_municipio',
		
		'vds_sexo',
		
		'vds_provincia',
		
		'vds_persona',
		
		'vds_users',
		
		'vds_crons',
		
		'vds_modules',
		
		'vds_views',
		
		'vds_user_roles',
		
		'vds_matriz',
		
		'vds_reg_usuario',
		
		//</es-section>
	],

	stores: [
		//<es-section>
		
		'vds_dictionaries',
		
		'vds_params',
		
		'vds_roles',
		
		'vds_people',
		
		'vds_mails',
		
		'vds_logs',
		
		'vds_pais',
		
		'vds_ciudad',
		
		'vds_estado_civil',
		
		'vds_municipio',
		
		'vds_sexo',
		
		'vds_provincia',
		
		'vds_persona',
		
		'vds_users',
		
		'vds_crons',
		
		'vds_modules',
		
		'vds_views',
		
		'vds_user_roles',
		
		'vds_matriz',
		
		'vds_reg_usuario',
		
		//</es-section>
	],

	autoCreateViewport: true
});

function initTable(table){
	let tab;
	let newTab;
	let tabs = Ext.getCmp('content-panel');
	if(tab = tabs.items.items.find(element => element.id == table)){
		Ext.getCmp('content-panel').setActiveTab(tab);
	} else {
		switch (table) {
			//<es-section>
			
			case 'vds-dictionaries':
				newTab = {
					xtype: 'vdsDictionariesList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-params':
				newTab = {
					xtype: 'vdsParamsList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-roles':
				newTab = {
					xtype: 'vdsRolesList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-people':
				newTab = {
					xtype: 'vdsPeopleList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-mails':
				newTab = {
					xtype: 'vdsMailsList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-logs':
				newTab = {
					xtype: 'vdsLogsList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-pais':
				newTab = {
					xtype: 'vdsPaisList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-ciudad':
				newTab = {
					xtype: 'vdsCiudadList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-estado-civil':
				newTab = {
					xtype: 'vdsEstadoCivilList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-municipio':
				newTab = {
					xtype: 'vdsMunicipioList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-sexo':
				newTab = {
					xtype: 'vdsSexoList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-provincia':
				newTab = {
					xtype: 'vdsProvinciaList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-persona':
				newTab = {
					xtype: 'vdsPersonaList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-users':
				newTab = {
					xtype: 'vdsUsersList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-crons':
				newTab = {
					xtype: 'vdsCronsList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-modules':
				newTab = {
					xtype: 'vdsModulesList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-views':
				newTab = {
					xtype: 'vdsViewsList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-user-roles':
				newTab = {
					xtype: 'vdsUserRolesList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-matriz':
				newTab = {
					xtype: 'vdsMatrizList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			case 'vds-reg-usuario':
				newTab = {
					xtype: 'vdsRegUsuarioList',
					title: table,
					html: table,
					id: table,
					closable: true,
				};
				tab = Ext.getCmp('content-panel').add(newTab);
				Ext.getCmp('content-panel').setActiveTab(tab);
				break;
			
			//</es-section>
		}
	}
}
