/**
 * Created by @ES Express Systems
 * User: #userCreated
 * Date: #dateCreated
 * Time: #timeCreated
 * Last User updated: #userUpdated
 * Last date updated: #dateUpdated
 * Last time updated: #timeUpdated
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section> 
Ext.define('es.store.lcObjPLocalTableName', {
    extend: 'Ext.data.Store',
    model: 'es.model.lcObjPLocalTableName',
    autoLoad: true,
    autoSync: true,
    remoteFilter: true
});
//</es-section>
