let html = `
<!-- es-section -->

<div id="es-dictionaries-details">
    <h2>es-dictionaries</h2>
    <p>Comments</p>
</div>

<div id="es-params-details">
    <h2>es-params</h2>
    <p>Comments</p>
</div>

<div id="es-roles-details">
    <h2>es-roles</h2>
    <p>Comments</p>
</div>

<div id="es-people-details">
    <h2>es-people</h2>
    <p>Comments</p>
</div>

<div id="es-files-details">
    <h2>es-files</h2>
    <p>Comments</p>
</div>

<div id="es-modules-details">
    <h2>es-modules</h2>
    <p>Comments</p>
</div>

<div id="es-users-details">
    <h2>es-users</h2>
    <p>Comments</p>
</div>

<div id="es-user-roles-details">
    <h2>es-user-roles</h2>
    <p>Comments</p>
</div>

<div id="es-attributes-details">
    <h2>es-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-person-attributes-details">
    <h2>es-person-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-file-attributes-details">
    <h2>es-file-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-objects-details">
    <h2>es-objects</h2>
    <p>Comments</p>
</div>

<div id="es-object-attributes-details">
    <h2>es-object-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-views-details">
    <h2>es-views</h2>
    <p>Comments</p>
</div>

<div id="es-components-details">
    <h2>es-components</h2>
    <p>Comments</p>
</div>

<div id="es-profiles-details">
    <h2>es-profiles</h2>
    <p>Comments</p>
</div>

<div id="es-profile-components-details">
    <h2>es-profile-components</h2>
    <p>Comments</p>
</div>

<div id="es-profile-modules-details">
    <h2>es-profile-modules</h2>
    <p>Comments</p>
</div>

<div id="es-profile-views-details">
    <h2>es-profile-views</h2>
    <p>Comments</p>
</div>

<div id="es-role-profiles-details">
    <h2>es-role-profiles</h2>
    <p>Comments</p>
</div>

<div id="es-employees-details">
    <h2>es-employees</h2>
    <p>Comments</p>
</div>

<div id="es-logs-details">
    <h2>es-logs</h2>
    <p>Comments</p>
</div>

<div id="es-auths-details">
    <h2>es-auths</h2>
    <p>Comments</p>
</div>

<div id="es-fields-details">
    <h2>es-fields</h2>
    <p>Comments</p>
</div>

<div id="es-component-field-attributes-details">
    <h2>es-component-field-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-field-attributes-details">
    <h2>es-field-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-jobs-details">
    <h2>es-jobs</h2>
    <p>Comments</p>
</div>

<div id="es-job-attributes-details">
    <h2>es-job-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-crons-details">
    <h2>es-crons</h2>
    <p>Comments</p>
</div>

<div id="es-cron-attributes-details">
    <h2>es-cron-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-flows-details">
    <h2>es-flows</h2>
    <p>Comments</p>
</div>

<div id="es-flow-attributes-details">
    <h2>es-flow-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-job-attribute-instances-details">
    <h2>es-job-attribute-instances</h2>
    <p>Comments</p>
</div>

<div id="es-cron-attribute-instances-details">
    <h2>es-cron-attribute-instances</h2>
    <p>Comments</p>
</div>

<div id="es-log-attributes-details">
    <h2>es-log-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-log-attribute-instances-details">
    <h2>es-log-attribute-instances</h2>
    <p>Comments</p>
</div>

<div id="es-object-attribute-instances-details">
    <h2>es-object-attribute-instances</h2>
    <p>Comments</p>
</div>

<div id="es-flow-attribute-instances-details">
    <h2>es-flow-attribute-instances</h2>
    <p>Comments</p>
</div>

<div id="es-component-field-attribute-instances-details">
    <h2>es-component-field-attribute-instances</h2>
    <p>Comments</p>
</div>

<div id="es-component-attributes-details">
    <h2>es-component-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-business-details">
    <h2>es-business</h2>
    <p>Comments</p>
</div>

<div id="es-mails-details">
    <h2>es-mails</h2>
    <p>Comments</p>
</div>

<div id="es-mail-attributes-details">
    <h2>es-mail-attributes</h2>
    <p>Comments</p>
</div>

<div id="es-mail-attribute-instances-details">
    <h2>es-mail-attribute-instances</h2>
    <p>Comments</p>
</div>

<div id="es-person-attribute-instances-details">
    <h2>es-person-attribute-instances</h2>
    <p>Comments</p>
</div>

<div id="es-pages-details">
    <h2>es-pages</h2>
    <p>Comments</p>
</div>

<div id="es-profile-pages-details">
    <h2>es-profile-pages</h2>
    <p>Comments</p>
</div>

<div id="es-page-components-details">
    <h2>es-page-components</h2>
    <p>Comments</p>
</div>

<!-- /es-section-->
</div>`;

let es = document.getElementById("es");
es.innerHTML = html;
