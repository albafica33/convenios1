/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 03:37:42 GMT-0400 (Bolivia Time)
 * Time: 3:37:42
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 03:37:42 GMT-0400 (Bolivia Time)
 * Last time updated: 3:37:42
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-persona.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsPersonaAdd',
	id: 'vds-persona-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdPersona',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsCiudadCiExpedoWithEstado = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsCiudadCiExpedoWithEstado',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsEstadoCivilEstadoCivilWithEstado = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsEstadoCivilEstadoCivilWithEstado',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsSexoSexoWithEstado = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsSexoSexoWithEstado',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsMunicipioMunicipioWithEstado = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsMunicipioMunicipioWithEstado',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsProvinciaProvinciaWithEstado = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsProvinciaProvinciaWithEstado',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsCiudadCiudadWithEstado = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsCiudadCiudadWithEstado',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsPaisPaisWithEstado = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-persona/findVdsPaisPaisWithEstado',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-persona-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						{
							fieldLabel: 'estado',
							name: 'estado',
						},
						
						
						{
							fieldLabel: 'nombres',
							name: 'nombres',
						},
						
						{
							fieldLabel: 'paterno',
							name: 'paterno',
						},
						
						{
							fieldLabel: 'materno',
							name: 'materno',
						},
						
						{
							fieldLabel: 'casada',
							name: 'casada',
						},
						
						{
							fieldLabel: 'ci',
							name: 'ci',
						},
						
						{
							fieldLabel: 'correo',
							name: 'correo',
						},
						
						{
							fieldLabel: 'direccion',
							name: 'direccion',
						},
						
						{
							fieldLabel: 'createdBy',
							name: 'createdBy',
						},
						
						{
							fieldLabel: 'updatedBy',
							name: 'updatedBy',
						},
						
						
						
						
						
						
						{
							fieldLabel: 'telefono',
							name: 'telefono',
						},
						
						{
							fieldLabel: 'celular',
							name: 'celular',
						},
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'ci_expedido',
								id: 'ci_expedido',
								fieldLabel: 'ci_expedido',
								store: storeVdsCiudadCiExpedoWithEstado,
								valueField: "_id",
								displayField: "estado",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_estado_civil',
								id: 'id_estado_civil',
								fieldLabel: 'id_estado_civil',
								store: storeVdsEstadoCivilEstadoCivilWithEstado,
								valueField: "_id",
								displayField: "estado",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_sexo',
								id: 'id_sexo',
								fieldLabel: 'id_sexo',
								store: storeVdsSexoSexoWithEstado,
								valueField: "_id",
								displayField: "estado",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_municipio',
								id: 'id_municipio',
								fieldLabel: 'id_municipio',
								store: storeVdsMunicipioMunicipioWithEstado,
								valueField: "_id",
								displayField: "estado",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_provincia',
								id: 'id_provincia',
								fieldLabel: 'id_provincia',
								store: storeVdsProvinciaProvinciaWithEstado,
								valueField: "_id",
								displayField: "estado",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_ciudad',
								id: 'id_ciudad',
								fieldLabel: 'id_ciudad',
								store: storeVdsCiudadCiudadWithEstado,
								valueField: "_id",
								displayField: "estado",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_pais',
								id: 'id_pais',
								fieldLabel: 'id_pais',
								store: storeVdsPaisPaisWithEstado,
								valueField: "_id",
								displayField: "estado",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'updatedAt',
							name: 'updatedAt',
							id:'updatedAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
