/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 03:37:42 GMT-0400 (Bolivia Time)
 * Time: 3:37:42
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 03:37:42 GMT-0400 (Bolivia Time)
 * Last time updated: 3:37:42
 *
 * Caution: es-sections will be replaced by script execution
 */

// Set up a model to use in our Store
//<es-section>

var storeVdsCiudadCiExpedoWithEstado = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-convenios/vds-persona/findVdsCiudadCiExpedoWithEstado',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'estado'],
	root: 'data',
	autoLoad: true
});

var storeVdsEstadoCivilEstadoCivilWithEstado = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-convenios/vds-persona/findVdsEstadoCivilEstadoCivilWithEstado',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'estado'],
	root: 'data',
	autoLoad: true
});

var storeVdsSexoSexoWithEstado = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-convenios/vds-persona/findVdsSexoSexoWithEstado',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'estado'],
	root: 'data',
	autoLoad: true
});

var storeVdsMunicipioMunicipioWithEstado = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-convenios/vds-persona/findVdsMunicipioMunicipioWithEstado',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'estado'],
	root: 'data',
	autoLoad: true
});

var storeVdsProvinciaProvinciaWithEstado = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-convenios/vds-persona/findVdsProvinciaProvinciaWithEstado',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'estado'],
	root: 'data',
	autoLoad: true
});

var storeVdsCiudadCiudadWithEstado = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-convenios/vds-persona/findVdsCiudadCiudadWithEstado',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'estado'],
	root: 'data',
	autoLoad: true
});

var storeVdsPaisPaisWithEstado = new Ext.data.JsonStore({
	proxy: {
		type: 'ajax',
		url : '/api-convenios/vds-persona/findVdsPaisPaisWithEstado',
		reader: {
			type: 'json',
			root: 'data'
		},
	},
	fields: ['_id', 'estado'],
	root: 'data',
	autoLoad: true
});

//</es-section>

Ext.define('es.view.vds-persona.List', {
	extend: 'Ext.grid.Panel',
	//<es-section>
	xtype: 'vdsPersonaList',
	//</es-section>
	title: 'Moduł użytkowników',

	viewConfig: {
		enableTextSelection: true,
		stripeRows: true
	},
	//<es-section>
	store: 'vds_persona',
	//</es-section>
	initComponent: function () {
		var me = this,
			rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
				clicksToEdit: 2,
				listeners:{
					dbclick: {
						element: 'body',
						fn: function(){ console.log('dblclick body'); }
					}
				}
			}),
			rowMenu = Ext.create('Ext.menu.Menu', {
				height: 58,
				width: 140,
				items: [{
					text: 'Edit',
					iconCls: 'button-edit'
				}, {
					text: 'Remove',
					iconCls: 'button-remove',
    				handler: function(){
					me.fireEvent('removeRow', this);
				}
			}]
		});
		this.listeners = {
			itemcontextmenu: function(view, record, item, index, e){
				e.stopEvent();
				rowMenu.showAt(e.getXY());
			}
		};

		this.plugins = [rowEditing];
		this.selType = 'rowmodel';

		this.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						text: 'Add',
						iconCls: 'icon-add',
						handler: () => {
							// Create a model instance
							// Create a model instance
							var date = new Date();
							//<es-section>
							var count = Ext.getStore('vdsPersona').data.length;
							var nextId = 0;
							for (var i = 0 ; i < count ; i++) {
								var id = Ext.getStore('vdsPersona').getAt(i).data.id;
								if(parseInt(id) > parseInt(nextId.toString())) {
									nextId = parseInt(id);
								}
							}
							Ext.getStore('vdsPersona').insert(count, {
								dueAt: date,
								createdAt: date,
								updatedAt: date,
							});
							Ext.getStore('vdsPersona').getAt(count).set('id',nextId+1);
							//</es-section>
						}
					}, '-' ,
					// {
					// 	text: 'Add Form',
					// 	itemId: 'add',
					// 	iconCls: 'icon-add',
					// },
					{
						xtype: 'container',
						flex: 1
					}
				]
			},
			{
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				width: 360,
				displayInfo: true,
				//<es-section>
				store: 'vdsPersona'
				//</es-section>
			}
		];

		this.columns = [

			//<es-section>
			
			{
				text: '_id',
				width: 160,
				dataIndex: '_id',
				hidden: false
			},
			
			
   			{
   			    text: 'id',
       			dataIndex: 'id',
       				width: 100,
       				editor: {
       				allowBlank: false
            	}
            },
            
			
			{
				text: 'nombres',
				dataIndex: 'nombres',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'paterno',
				dataIndex: 'paterno',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'materno',
				dataIndex: 'materno',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'casada',
				dataIndex: 'casada',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'ci',
				dataIndex: 'ci',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'correo',
				dataIndex: 'correo',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'direccion',
				dataIndex: 'direccion',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'createdBy',
				dataIndex: 'createdBy',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'updatedBy',
				dataIndex: 'updatedBy',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			
			
			
			
			
			{
				text: 'telefono',
				dataIndex: 'telefono',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			{
				text: 'celular',
				dataIndex: 'celular',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			
			
			
			{
				text: 'estado',
				dataIndex: 'estado',
				width: 160,
				editor: {
					allowBlank: false
				}
			},
			
			
			
			
			{
				text: 'ci_expedido',
				dataIndex: 'ci_expedido',
				renderer: (val, metaData, r) => {
                	let items = storeVdsCiudadCiExpedoWithEstado.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.estado;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsCiudadCiExpedoWithEstado,
					valueField: "_id",
					displayField: "estado",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'id_estado_civil',
				dataIndex: 'id_estado_civil',
				renderer: (val, metaData, r) => {
                	let items = storeVdsEstadoCivilEstadoCivilWithEstado.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.estado;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsEstadoCivilEstadoCivilWithEstado,
					valueField: "_id",
					displayField: "estado",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'id_sexo',
				dataIndex: 'id_sexo',
				renderer: (val, metaData, r) => {
                	let items = storeVdsSexoSexoWithEstado.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.estado;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsSexoSexoWithEstado,
					valueField: "_id",
					displayField: "estado",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'id_municipio',
				dataIndex: 'id_municipio',
				renderer: (val, metaData, r) => {
                	let items = storeVdsMunicipioMunicipioWithEstado.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.estado;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsMunicipioMunicipioWithEstado,
					valueField: "_id",
					displayField: "estado",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'id_provincia',
				dataIndex: 'id_provincia',
				renderer: (val, metaData, r) => {
                	let items = storeVdsProvinciaProvinciaWithEstado.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.estado;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsProvinciaProvinciaWithEstado,
					valueField: "_id",
					displayField: "estado",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'id_ciudad',
				dataIndex: 'id_ciudad',
				renderer: (val, metaData, r) => {
                	let items = storeVdsCiudadCiudadWithEstado.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.estado;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsCiudadCiudadWithEstado,
					valueField: "_id",
					displayField: "estado",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			{
				text: 'id_pais',
				dataIndex: 'id_pais',
				renderer: (val, metaData, r) => {
                	let items = storeVdsPaisPaisWithEstado.data.items;
                	let label;
                	for (let i = 0 ; i < items.length ; i++) {
                		let item = items[i];
                		if(item.raw._id == val) {
                			label = item.raw.estado;
                		}
                	}
                	return label;
                },
				width: 160,
				editor:  new Ext.form.field.ComboBox({
					typeAhead: true,
					triggerAction: 'all',
					selectOnTab: true,
					store: storeVdsPaisPaisWithEstado,
					valueField: "_id",
					displayField: "estado",
					lazyRender: true,
					listClass: 'x-combo-list-small'
				})
			},
			
			
			{
				text: 'dueAt',
				dataIndex: 'dueAt',
				width: 160,
				hidden:false,
				editor: {
					xtype: 'datefield',
					value: 'dueAt',
					editable: true,
					format: 'yy/m/d H:i:s',
					//minValue: new Date(),
				}
			},
			
			{
				text: 'createdAt',
				dataIndex: 'createdAt',
				width: 160,
				hidden:false,
				editor: {
					xtype: 'datefield',
					value: 'createdAt',
					editable: true,
					format: 'yy/m/d H:i:s',
					//minValue: new Date(),
				}
			},
			
			{
				text: 'updatedAt',
				dataIndex: 'updatedAt',
				width: 160,
				hidden:false,
				editor: {
					xtype: 'datefield',
					value: 'updatedAt',
					editable: true,
					format: 'yy/m/d H:i:s',
					//minValue: new Date(),
				}
			},
			
			//</es-section>
			{
				xtype: 'actioncolumn',
				width: 50,
				items: [
					{
						iconCls: 'button-add',
						tooltip: 'Add',
						icon: '/js/es/shared/icons/fam/add.gif',
						handler: function (grid, rowIndex, colIndex) {
							// Create a model instance
							// Create a model instance
							var date = new Date();
							//<es-section>
							var count = Ext.getStore('vdsPersona').data.length;
							var nextId = 0;
							for (var i = 0 ; i < count ; i++) {
								var id = Ext.getStore('vdsPersona').getAt(i).data.id;
								if(parseInt(id) > parseInt(nextId)) {
									nextId = parseInt(id);
								}
							}
							Ext.getStore('vdsPersona').insert(count, {
								dueAt: date,
								createdAt: date,
								updatedAt: date,
							});
							Ext.getStore('vdsPersona').getAt(count).set('id',nextId+1);
							rowEditing.startEdit(count,1);
						}
						//</es-section>
					},
					{
						iconCls: 'button-remove',
						tooltip: 'Remove',
						icon: '/js/es/shared/icons/fam/delete.gif',
						handler: function (grid, rowIndex, colIndex) {
							this.up('grid').fireEvent('removeRow', grid, rowIndex, colIndex);
						}
					}
				]
			}
		];

		//parent
		this.callParent(arguments);
	}
});
