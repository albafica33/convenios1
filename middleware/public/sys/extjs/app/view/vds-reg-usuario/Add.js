/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 03:37:43 GMT-0400 (Bolivia Time)
 * Time: 3:37:43
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 03:37:43 GMT-0400 (Bolivia Time)
 * Last time updated: 3:37:43
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-reg-usuario.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsRegUsuarioAdd',
	id: 'vds-reg-usuario-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdRegUsuario',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsUsersUsuarioWithEstado = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-reg-usuario/findVdsUsersUsuarioWithEstado',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsMatrizRegWithEstadoCumplimiento = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-reg-usuario/findVdsMatrizRegWithEstadoCumplimiento',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'estado_cumplimiento'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesCreatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-reg-usuario/findVdsUserRolesCreatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesUpdatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-reg-usuario/findVdsUserRolesUpdatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-reg-usuario-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_usuario',
								id: 'id_usuario',
								fieldLabel: 'id_usuario',
								store: storeVdsUsersUsuarioWithEstado,
								valueField: "_id",
								displayField: "estado",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'id_reg',
								id: 'id_reg',
								fieldLabel: 'id_reg',
								store: storeVdsMatrizRegWithEstadoCumplimiento,
								valueField: "_id",
								displayField: "estado_cumplimiento",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'createdById',
								id: 'createdById',
								fieldLabel: 'createdById',
								store: storeVdsUserRolesCreatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'updatedById',
								id: 'updatedById',
								fieldLabel: 'updatedById',
								store: storeVdsUserRolesUpdatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'updatedAt',
							name: 'updatedAt',
							id:'updatedAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
