/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 03:37:35 GMT-0400 (Bolivia Time)
 * Time: 3:37:35
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 03:37:35 GMT-0400 (Bolivia Time)
 * Last time updated: 3:37:35
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.sequelizemeta.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.sequelizemetaAdd',
	id: 'sequelizemeta-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add Sequelizemeta',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'sequelizemeta-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: 'name',
							name: 'name',
						},
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
