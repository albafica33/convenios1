/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Jun 14 2021 03:37:41 GMT-0400 (Bolivia Time)
 * Time: 3:37:41
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Jun 14 2021 03:37:41 GMT-0400 (Bolivia Time)
 * Last time updated: 3:37:41
 *
 * Caution: es-sections will be replaced by script execution
 */

Ext.define('es.view.vds-people.Add', {
	extend: 'Ext.window.Window',
	alias: 'widget.vdsPeopleAdd',
	id: 'vds-people-add',
	height: 300,
	width: 369,
	resizable: true,
	autoScroll: true,
	title: 'Add VdPerson',
	modal: true,

	initComponent: function () {
		var me = this;

		setTimeout(()=> {
			Ext.getCmp('createdAt').setValue(new Date());
			Ext.getCmp('updatedAt').setValue(new Date());
			Ext.getCmp('dueAt').setValue(new Date());
		}, 50);

		//<es-section>
		
		var storeVdsUserRolesCreatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsUserRolesCreatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsUserRolesUpdatedbyWithUsrRolGroup = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsUserRolesUpdatedbyWithUsrRolGroup',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'usr_rol_group'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsPeoplePerParentWithPerFirstName = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsPeoplePerParentWithPerFirstName',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'per_first_name'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsPerParTypeDocWithParOrder = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsParamsPerParTypeDocWithParOrder',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_order'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsPerParCityWithParOrder = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsParamsPerParCityWithParOrder',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_order'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsPerParSexWithParOrder = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsParamsPerParSexWithParOrder',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_order'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsPerParCountryWithParOrder = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsParamsPerParCountryWithParOrder',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_order'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsPerParNacionalityWithParOrder = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsParamsPerParNacionalityWithParOrder',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_order'],
			root: 'data',
			autoLoad: true
		});
		
		var storeVdsParamsPerParStatusWithParOrder = new Ext.data.JsonStore({
			proxy: {
				type: 'ajax',
				url : '/api-convenios/vds-people/findVdsParamsPerParStatusWithParOrder',
				reader: {
					type: 'json',
					root: 'data'
				},
			},
			fields: ['_id', 'par_order'],
			root: 'data',
			autoLoad: true
		});
		
		//</es-section>
		Ext.applyIf(me, {
			items: [
				{
					xtype: 'form',
					id:'vds-people-form',
					bodyPadding: 20,
					title: '',
					defaults: { // defaults are applied to items, not the container
						allowBlank: false,
						allowOnlyWhitespace: false,
						msgTarget: 'side',
						xtype: 'textfield',
						anchor: '100%'
					},
					items: [
						//<es-section>
						
						{
							fieldLabel: '_id',
							name: '_id',
						},
						
						
          				{
     		        		fieldLabel: 'id',
    						name: 'id',
     					},
                        
						
						
						
						{
							fieldLabel: 'per_first_name',
							name: 'per_first_name',
						},
						
						{
							fieldLabel: 'per_second_name',
							name: 'per_second_name',
						},
						
						{
							fieldLabel: 'per_first_lastname',
							name: 'per_first_lastname',
						},
						
						{
							fieldLabel: 'per_second_lastname',
							name: 'per_second_lastname',
						},
						
						{
							fieldLabel: 'per_license',
							name: 'per_license',
						},
						
						{
							fieldLabel: 'per_license_comp',
							name: 'per_license_comp',
						},
						
						{
							fieldLabel: 'per_home_address',
							name: 'per_home_address',
						},
						
						{
							fieldLabel: 'per_mail',
							name: 'per_mail',
						},
						
						{
							fieldLabel: 'per_home_phone',
							name: 'per_home_phone',
						},
						
						{
							fieldLabel: 'per_cellphone',
							name: 'per_cellphone',
						},
						
						{
							fieldLabel: 'per_group',
							name: 'per_group',
						},
						
						
						
						
						
						
						
						
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'createdById',
								id: 'createdById',
								fieldLabel: 'createdById',
								store: storeVdsUserRolesCreatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'updatedById',
								id: 'updatedById',
								fieldLabel: 'updatedById',
								store: storeVdsUserRolesUpdatedbyWithUsrRolGroup,
								valueField: "_id",
								displayField: "usr_rol_group",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'per_parent_id',
								id: 'per_parent_id',
								fieldLabel: 'per_parent_id',
								store: storeVdsPeoplePerParentWithPerFirstName,
								valueField: "_id",
								displayField: "per_first_name",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'per_par_type_doc_id',
								id: 'per_par_type_doc_id',
								fieldLabel: 'per_par_type_doc_id',
								store: storeVdsParamsPerParTypeDocWithParOrder,
								valueField: "_id",
								displayField: "par_order",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'per_par_city_id',
								id: 'per_par_city_id',
								fieldLabel: 'per_par_city_id',
								store: storeVdsParamsPerParCityWithParOrder,
								valueField: "_id",
								displayField: "par_order",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'per_par_sex_id',
								id: 'per_par_sex_id',
								fieldLabel: 'per_par_sex_id',
								store: storeVdsParamsPerParSexWithParOrder,
								valueField: "_id",
								displayField: "par_order",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'per_par_country_id',
								id: 'per_par_country_id',
								fieldLabel: 'per_par_country_id',
								store: storeVdsParamsPerParCountryWithParOrder,
								valueField: "_id",
								displayField: "par_order",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'per_par_nacionality_id',
								id: 'per_par_nacionality_id',
								fieldLabel: 'per_par_nacionality_id',
								store: storeVdsParamsPerParNacionalityWithParOrder,
								valueField: "_id",
								displayField: "par_order",
							})
						},
						
						{
							xtype: new Ext.form.field.ComboBox({
								name: 'per_par_status_id',
								id: 'per_par_status_id',
								fieldLabel: 'per_par_status_id',
								store: storeVdsParamsPerParStatusWithParOrder,
								valueField: "_id",
								displayField: "par_order",
							})
						},
						
						
						{
							fieldLabel: 'per_birthday',
							name: 'per_birthday',
							id:'per_birthday',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'dueAt',
							name: 'dueAt',
							id:'dueAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'createdAt',
							name: 'createdAt',
							id:'createdAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						{
							fieldLabel: 'updatedAt',
							name: 'updatedAt',
							id:'updatedAt',
							xtype: 'datefield',
							format: 'yy/m/d H:i:s',
							minValue: new Date(),
						},
						
						//</es-section>
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'save',
							text: 'Save'
						},
						{
							xtype: 'button',
							anchor: 0,
							itemId: 'cancel',
							text: 'Cancel'
						}
					]
				}
			]
		});

		me.callParent(arguments);
	},
});
