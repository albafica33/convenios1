/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:15:41 GMT-0400 (Bolivia Time)
 * Time: 0:15:41
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:15:41 GMT-0400 (Bolivia Time)
 * Last time updated: 0:15:41
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section> 
Ext.define('es.store.vds_sexo', {
    extend: 'Ext.data.Store',
    model: 'es.model.vds_sexo',
    autoLoad: true,
    autoSync: true,
    remoteFilter: true
});
//</es-section>
