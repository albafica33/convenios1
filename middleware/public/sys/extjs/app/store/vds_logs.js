/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:15:39 GMT-0400 (Bolivia Time)
 * Time: 0:15:39
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:15:39 GMT-0400 (Bolivia Time)
 * Last time updated: 0:15:39
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section> 
Ext.define('es.store.vds_logs', {
    extend: 'Ext.data.Store',
    model: 'es.model.vds_logs',
    autoLoad: true,
    autoSync: true,
    remoteFilter: true
});
//</es-section>
