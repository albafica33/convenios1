require('dotenv').config();
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;

module.exports = {

  // If using onine database
  // development: {
  //   use_env_variable: 'DATABASE_URL'
  // },

  system: process.env.SYSTEM,
  sql: process.env.SQL,

  apiStatus: [
    {id: 1, status: 'info'},
    {id: 2, status: 'warn'},
    {id: 3, status: 'error'},
  ],

  esUser:process.env.ES_USER,

  local: {
    database: process.env.LOCAL_DB_NAME,
    port: process.env.LOCAL_DB_PORT,
    username: process.env.LOCAL_DB_USER,
    password: process.env.LOCAL_DB_PASS,
    host: process.env.LOCAL_DB_HOST,
    dialect: process.env.LOCAL_DB_DIALECT,
    secret_token: process.env.LOCAL_SECRET_TOKEN,
    gate: process.env.LOCAL_PORT,
    sql: process.env.LOCAL_SQL,
    access: process.env.LOCAL_ACCESS,
    wt_apiurl: process.env.LOCAL_WT_APIURL,
    wt_token: process.env.LOCAL_WT_TOKEN,
    domain: process.env.LOCAL_HOST,
    protocol: process.env.LOCAL_PROTOCOL,
  },

  development: {
    database: process.env.DEV_DB_NAME,
    port: process.env.DEV_DB_PORT,
    username: process.env.DEV_DB_USER,
    password: process.env.DEV_DB_PASS,
    host: process.env.DEV_DB_HOST,
    dialect: process.env.DEV_DB_DIALECT,
    secret_token: process.env.DEV_SECRET_TOKEN,
    gate: process.env.DEV_PORT,
    sql: process.env.DEV_SQL,
    access: process.env.DEV_ACCESS,
    wt_apiurl: process.env.DEV_WT_APIURL,
    wt_token: process.env.DEV_WT_TOKEN,
    domain: process.env.DEV_HOST,
    protocol: process.env.DEV_PROTOCOL,
  },

  test: {
    database: process.env.TEST_DB_NAME,
    port: process.env.TEST_DB_PORT,
    username: process.env.TEST_DB_USER,
    password: process.env.TEST_DB_PASS,
    host: process.env.TEST_DB_HOST,
    dialect: process.env.TEST_DB_DIALECT,
    secret_token: process.env.TEST_SECRET_TOKEN,
    gate: process.env.TEST_PORT,
    sql: process.env.TEST_SQL,
    access: process.env.TEST_ACCESS,
    wt_apiurl: process.env.TEST_WT_APIURL,
    wt_token: process.env.TEST_WT_TOKEN,
    domain: process.env.TEST_HOST,
    protocol: process.env.TEST_PROTOCOL,
  },

  production: {
    database: process.env.PROD_DB_NAME,
    port: process.env.PROD_DB_PORT,
    username: process.env.PROD_DB_USER,
    password: process.env.PROD_DB_PASS,
    host: process.env.PROD_DB_HOST,
    dialect: process.env.PROD_DB_DIALECT,
    secret_token: process.env.PROD_SECRET_TOKEN,
    gate: process.env.PROD_PORT,
    sql: process.env.PROD_SQL,
    access: process.env.PROD_ACCESS,
    wt_apiurl: process.env.PROD_WT_APIURL,
    wt_token: process.env.PROD_WT_TOKEN,
    domain: process.env.PROD_HOST,
    protocol: process.env.PROD_PROTOCOL,
  },
};
