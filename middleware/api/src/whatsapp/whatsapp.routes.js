/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Sat Dec 19 2020 19:44:07 GMT-0400 (Bolivia Time)
 * Time: 19:44:7users
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Sat Dec 19 2020 19:44:07 GMT-0400 (Bolivia Time)
 * Last time updated: 19:44:7
 *
 * Caution: es-sections will be replaced by script execution
 */

import configJson from '../../../config/config';
const sys = configJson.system;
const express = require("express");
// const wbm = require("../../../modules/wbm");
const baileys = require("../../../modules/baileys");
const router = express.Router();
//const authenticateToken = require("../../../../config/token");

//<es-section>
const whatsappCtrl = require("./whatsapp.controller");

// router.post(`/api-${sys}/whatsapp/sendMessage`, (req, res) => wbm.sendMessage(req, res));
// router.post(`/api-${sys}/whatsapp/logout`, (req, res) => wbm.logout(req, res));

router.post(`/api-${sys}/whatsapp/closeApi`, (req, res) => baileys.closeApi(req, res));
router.post(`/api-${sys}/whatsapp/connectApi`, (req, res) => baileys.connectApi(req, res));
router.post(`/api-${sys}/whatsapp/sendMessage`, (req, res) => baileys.sendMessage(req, res));

module.exports = router;
