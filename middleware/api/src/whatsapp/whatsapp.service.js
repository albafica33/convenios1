/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Nov 12 2020 02:25:36 GMT-0400 (Bolivia Time)
 * Time: 2:25:36
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Nov 12 2020 02:25:36 GMT-0400 (Bolivia Time)
 * Last time updated: 2:25:36
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const models = require('../../relations');
import configJson from '../../../config/config';
const sql = configJson.sql;
const Util = require('../../../utils/Utils');
const util = new Util();

const { Op } = require("sequelize");
//import WhatsAppWeb from 'baileys';
import { WAConnection } from '@adiwajshing/baileys'
const client = new WAConnection();

class WhatsappService {

	static async getQr(req,res) {
		try {
			return await client.on('qr', qr => {
				console.log(qr);
				return qr
			});
		} catch (error) {
			throw error;
		}
	}

	static async connect(req,res) {
		try {
			return await client.connect().then();
		} catch (error) {
			throw error;
		}
	}

	static async sendMessage(req,res) {
		try {
			let options = {
				quoted: null,
				timestamp: new Date()
			}
			client.sendTextMessage(`${req.body.name}@s.whatsapp.net`, req.body.body, options)
				.then(req.jsonp({messaje:'Notificacion enviada'}));

		} catch (error) {
			throw error;
		}
	}

}

//<es-section>
module.exports = WhatsappService;
//</es-section>
