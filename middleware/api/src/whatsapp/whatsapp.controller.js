/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Mon Sep 21 2020 22:55:51 GMT-0400 (Bolivia Time)
 * Time: 22:55:51
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Mon Sep 21 2020 22:55:51 GMT-0400 (Bolivia Time)
 * Last time updated: 22:55:51
 *
 * Caution: es-sections will be replaced by script execution
 */
 
//<es-section>
const models = require('../../../core/express');
const whatsappService = require('./whatsapp.service');
//</es-section>
const Util = require('../../../utils/Utils');
const util = new Util();
const passport = require('passport');

// Controller for DB Mongoose

const whatsappCtrl = {};
whatsappCtrl.service = whatsappService;

whatsappCtrl.connect = async (req, res) => {
	try {
		let objQr = await whatsappService.getQr(req, res);
		let objConnect = await whatsappService.connect(req, res);
		if (objQr) {
			util.setSuccess(200, 'QR code retrieved', {qr:objQr, connect: objConnect});
		} else {
			util.setSuccess(200, 'No QR code found');
		}
		return util.send(res);
	} catch(e) {
		util.setError(400, e);
		return util.send(res);
	}
}

module.exports = whatsappCtrl;
