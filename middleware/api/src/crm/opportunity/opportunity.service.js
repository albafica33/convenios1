import fs from "fs";

/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Nov 12 2020 02:25:36 GMT-0400 (Bolivia Time)
 * Time: 2:25:36
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Nov 12 2020 02:25:36 GMT-0400 (Bolivia Time)
 * Last time updated: 2:25:36
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../../utils/Prototipes');
const helpers = require('../../../../utils/helpers');
const models = require('../../../relations');
import configJson from '../../../../config/config';
const sql = configJson.sql;
const Util = require('../../../../utils/Utils');
const util = new Util();
const htmlPdf = require('../../../../modules/html-pdf');
const nodeMailer = require("../../../../modules/nodemailer");
const {loggerEmail, loggerPdf} = require("../../../../modules/winston");
const crmService = require('../crm.service');

const path = require('path');
const { Op } = require("sequelize");

class OpportunityService {
	constructor() {

	}

	static async getAllOpportunities(query) {
		try {
			if(sql) {
				let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
				let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				let where3,where2,where1;
				if (where && where.where && where.where.where && Object.keys(where.where.where).length) {
					where3 = where.where.where; delete where.where.where;
				}
				if (where && where.where && Object.keys(where.where).length) {
					where2 = where.where; delete where.where;
				}
				if (where && Object.keys(where).length) {
					where1 = where;
				}
				return await models.sequelize.opportunities.findAndCountAll({
					attributes:query.select ? query.select.split(',') : null,
					where: where1,
					limit: query.limit ? parseInt(query.limit) : null,
					offset: offset ? parseInt(offset) : 0,
					order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['id','ASC']],
					include: [
						{
							model:models.sequelize.users, as:'opportunityUsersAssignedUser',
						},
						{
							model:models.sequelize.opportunitiesCstm, as:'opportunityOpportunitiesCstm',
							where: where2,
						},
						{model:models.sequelize.opportunitiesContacts, as:'opportunityOpportunitiesContacts',
							include:{
								model:models.sequelize.contacts, as:'opportunityContactContacts',
								include:[
									{model:models.sequelize.emails, as:'contactEmails'},
									{model:models.sequelize.contactsCstm, as:'contactContactsCstm'},
									{model:models.sequelize.emailAddrBeanRel, as:'contactEmailAddrBeanRel',
										include:{
											model:models.sequelize.emailAddresses, as:'emailAddrBeanRelEmailAddresses'
										}
									}
								]
							}
						},
						{model:models.sequelize.accountsOpportunities, as:'opportunityAccountsOpportunities',
							include:{
								model:models.sequelize.accounts, as:'accountOpportunityAccounts',
								include:{
									model:models.sequelize.accountsCstm, as:'accountAccountsCstm'
								}
							}
						},
						{model:models.sequelize.aosQuotes, as:'opportunityAosQuotes',
							include:{
								model:models.sequelize.aosQuotesCstm, as:'aoQuoteAosQuotesCstm'
							}
						},
					]
				});
			}
		} catch (error) {
			console.log(error);
			throw error;
		}
	}

	static async getAOpportunity(id, query) {
		try {
			if(sql) {
				return await models.sequelize.opportunities.findOne({
					attributes:query.select ? query.select.split(',') : null,
					where:{id:id},
					include: [
						{model:models.sequelize.opportunitiesCstm, as:'opportunityOpportunitiesCstm'},
						{model:models.sequelize.opportunitiesContacts, as:'opportunityOpportunitiesContacts',
							include:{
								model:models.sequelize.contacts, as:'opportunityContactContacts',
								include:[
									{model:models.sequelize.emails, as:'contactEmails'},
									{model:models.sequelize.contactsCstm, as:'contactContactsCstm'},
									{model:models.sequelize.emailAddrBeanRel, as:'contactEmailAddrBeanRel',
										include:{
											model:models.sequelize.emailAddresses, as:'emailAddrBeanRelEmailAddresses'
										}
									}
								]
							}
						},
						{model:models.sequelize.accountsOpportunities, as:'opportunityAccountsOpportunities',
							include:{
								model:models.sequelize.accounts, as:'accountOpportunityAccounts',
								include:{
									model:models.sequelize.accountsCstm, as:'accountAccountsCstm'
								}
							}
						},
						{model:models.sequelize.aosQuotes, as:'opportunityAosQuotes',
							include:{
								model:models.sequelize.aosQuotesCstm, as:'aoQuoteAosQuotesCstm'
							}
						},
						{model:models.sequelize.opportunitiesAudit, as:'opportunityOpportunitiesAudit'},
						{model:models.sequelize.sugarfeed, as:'opportunitySugarfeed'},
						{model:models.sequelize.aodIndexevent, as:'opportunityAodIndexevent'},
						{model:models.sequelize.tracker, as:'opportunityTracker'},
					]
				});
			}
		} catch (error) {
			throw error;
		}
	}

	// static async updateOpportunity(id, updateOpportunity) {
	// 	try {
	// 		let objOpportunity;
	// 		if(sql) {
	// 			objOpportunity = await models.sequelize.opportunities.findOne({where: { id: util.Char(id) }});
	// 			objOpportunity.opportunityCstm = await models.sequelize.opportunitiesCstm.findOne({where: { id_c: util.Char(id) }});
	// 			if (objOpportunity) {
	// 				await models.sequelize.opportunities.update(updateOpportunity, { where: { id: util.Char(id) } });
	// 				objOpportunity = await models.sequelize.opportunities.findOne({where: { id: util.Char(id) }});
	// 				await models.sequelize.opportunitiesCstm.update(updateOpportunity.opportunityCstm, { where: { id_c: util.Char(id) } });
	// 				objOpportunity.opportunityCstm = await models.sequelize.opportunitiesCstm.findOne({where: { id_c: util.Char(id) }});
	// 			}
	// 		} else {
	// 			delete updateOpportunity._id;
	// 			objOpportunity = await models.mongoose.opportunities.findOneAndUpdate({id:id}, {$set: updateOpportunity}, {new: true});
	// 		}
	// 		return objOpportunity;
	// 	} catch (error) {
	// 		throw error;
	// 	}
	// }


	static async addOpportunity(newOpportunity) {		try {

			let objOpportunity, respContacts, pilatLog;

			if(sql) {

				if (newOpportunity) {
					newOpportunity.amount = crmService.setNumberToSave(newOpportunity.amount);
					let respOpportunity, respOpportunitiesCstm;
					let newOpportunityId;
					if (newOpportunity.id) {
						newOpportunityId = newOpportunity.id;
						delete newOpportunity.id;
						newOpportunity.date_modified = new Date();
						await models.sequelize.opportunities.update(newOpportunity, {where:{id:newOpportunityId}});
						respOpportunity = await models.sequelize.opportunities.findOne({where: { id: newOpportunityId }});
						objOpportunity = respOpportunity && respOpportunity.dataValues ? respOpportunity.dataValues : null;
						pilatLog = await crmService.setPilatLog('update', 'opportunities',objOpportunity.name, objOpportunity.id, objOpportunity.id, objOpportunity.assigned_user_id);
					} else {
						newOpportunity.id = models.sequelize.objectId().toString();
						newOpportunity.date_entered = new Date();
						newOpportunity.date_modified = new Date();
						newOpportunity.date_closed = new Date();
						respOpportunity = await models.sequelize.opportunities.create(newOpportunity);
						objOpportunity = respOpportunity && respOpportunity.dataValues ? respOpportunity.dataValues : null;
						pilatLog = await crmService.setPilatLog('create', 'opportunities',objOpportunity.name, objOpportunity.id, objOpportunity.id, objOpportunity.assigned_user_id);
					}
					if (newOpportunity.opportunityOpportunitiesCstm) {
						if (newOpportunity.opportunityOpportunitiesCstm.id_c){
							await models.sequelize.opportunitiesCstm.update(newOpportunity.opportunityOpportunitiesCstm, {where:{id_c:newOpportunity.opportunityOpportunitiesCstm.id_c}});
							respOpportunitiesCstm = await models.sequelize.opportunitiesCstm.findOne({where: { id_c: newOpportunity.opportunityOpportunitiesCstm.id_c }});
						} else {
							newOpportunity.opportunityOpportunitiesCstm.id_c = newOpportunity.id;
							respOpportunitiesCstm = await models.sequelize.opportunitiesCstm.create(newOpportunity.opportunityOpportunitiesCstm);
						}
					}
					objOpportunity = respOpportunity.dataValues;
					objOpportunity.opportunityOpportunitiesCstm = respOpportunitiesCstm.dataValues;

					// BEGIN CONTACTS

					let respContactsCstm, objContacts, respOpportunitiesContacts, objOpportunitiesContacts;
					let opportunityContactContactsId;
					if (newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts) {
						if (newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.id) {
							opportunityContactContactsId = newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.id;
							delete newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.id;
							newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.date_modified = new Date();
							await models.sequelize.contacts.update(newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts, {where:{id:opportunityContactContactsId}});
							respContacts = await models.sequelize.contacts.findOne({where: { id: opportunityContactContactsId }});
							objContacts = respContacts && respContacts.dataValues ? respContacts.dataValues : null;
							pilatLog = await crmService.setPilatLog('update', 'contacts',objContacts.first_name+' '+objContacts.last_name, objContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
						} else {
							newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.id = models.sequelize.objectId().toString();
							newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.date_entered = new Date();
							newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.date_modified = new Date();
							respContacts = await models.sequelize.contacts.create(newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts);
							objContacts = respContacts && respContacts.dataValues ? respContacts.dataValues : null;
							pilatLog = await crmService.setPilatLog('create', 'contacts',objContacts.first_name+' '+objContacts.last_name, objContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
						}

						if (newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm) {
							if (newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c) {
								await models.sequelize.contactsCstm.update(newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm, {where:{id_c:newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c}});
								respContactsCstm = await models.sequelize.contactsCstm.findOne({where: { id_c: newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c }});
							} else {
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c = respContacts.dataValues.id;
								respContactsCstm = await models.sequelize.contactsCstm.create(newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm);
							}
						}

						let opportunityOpportunitiesContactsId;
						if (newOpportunity.opportunityOpportunitiesContacts) {
							if (newOpportunity.opportunityOpportunitiesContacts.id) {
								opportunityOpportunitiesContactsId = newOpportunity.opportunityOpportunitiesContacts.id;
								delete newOpportunity.opportunityOpportunitiesContacts.id;
								newOpportunity.opportunityOpportunitiesContacts.date_modified = new Date();
								newOpportunity.opportunityOpportunitiesContacts.opportunity_id = respOpportunity.dataValues.id;
								newOpportunity.opportunityOpportunitiesContacts.contact_id = respContacts.dataValues.id;
								await models.sequelize.opportunitiesContacts.update(newOpportunity.opportunityOpportunitiesContacts, {where:{opportunity_id:newOpportunity.opportunityOpportunitiesContacts.opportunity_id}});
								respOpportunitiesContacts = await models.sequelize.opportunitiesContacts.findOne({where: { opportunity_id: newOpportunity.opportunityOpportunitiesContacts.opportunity_id }});
								objOpportunitiesContacts = respOpportunitiesContacts && respOpportunitiesContacts.dataValues ? respOpportunitiesContacts.dataValues : null;
								pilatLog = await crmService.setPilatLog('update', 'contacts',{description:'opportunities_contacts',from:objOpportunity.name,to:objContacts.first_name+' '+objContacts.last_name}, objOpportunitiesContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
							} else {
								newOpportunity.opportunityOpportunitiesContacts.id = models.sequelize.objectId().toString();
								newOpportunity.opportunityOpportunitiesContacts.opportunity_id = respOpportunity.dataValues.id;
								newOpportunity.opportunityOpportunitiesContacts.contact_id = respContacts.dataValues.id;
								newOpportunity.opportunityOpportunitiesContacts.date_modified = new Date();
								respOpportunitiesContacts = await models.sequelize.opportunitiesContacts.create(newOpportunity.opportunityOpportunitiesContacts);
								objOpportunitiesContacts = respOpportunitiesContacts && respOpportunitiesContacts.dataValues ? respOpportunitiesContacts.dataValues : null;
								pilatLog = await crmService.setPilatLog('create', 'contacts',{description:'opportunities_contacts',from:objOpportunity.name,to:objContacts.first_name+' '+objContacts.last_name}, objOpportunitiesContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
							}
						}
						objOpportunity.opportunityOpportunitiesContacts = respOpportunitiesContacts.dataValues;
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts = respContacts.dataValues;
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm = respContactsCstm.dataValues;
					}

					// END CONTACTS
					//
					// BEGIN EMAIL ADDRESSES

					let respEmailAddresses, respEmailAddrBeanRel, objEmailAddresses, objEmailAddrBeanRel;
					let emailAddrBeanRelEmailAddressesId;
					if (newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses) {
						if (newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id) {
							emailAddrBeanRelEmailAddressesId = newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id;
							delete newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id;
							newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.date_modified = new Date();
							await models.sequelize.emailAddresses.update(newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses, {where:{id:emailAddrBeanRelEmailAddressesId}});
							respEmailAddresses = await models.sequelize.emailAddresses.findOne({where: { id: emailAddrBeanRelEmailAddressesId }});
							objEmailAddresses = respEmailAddresses && respEmailAddresses.dataValues ? respEmailAddresses.dataValues : null;
							pilatLog = await crmService.setPilatLog('update', 'emails',objEmailAddresses.email_address, objEmailAddresses.id, objOpportunity.id, objContacts.assigned_user_id);
						} else {
							newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id = models.sequelize.objectId().toString();
							newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.date_entered = new Date();
							newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.date_modified = new Date();
							respEmailAddresses = await models.sequelize.emailAddresses.create(newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses);
							objEmailAddresses = respEmailAddresses && respEmailAddresses.dataValues ? respEmailAddresses.dataValues : null;
							pilatLog = await crmService.setPilatLog('create', 'emails',objEmailAddresses.email_address, objEmailAddresses.id, objOpportunity.id, objContacts.assigned_user_id);
						}

						let contactEmailAddrBeanRelId;
						if (newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel) {
							if (newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.id) {
								contactEmailAddrBeanRelId = newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.id;
								delete newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.id;
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.date_modified = new Date();
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.bean_id = respContacts.id;
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.email_address_id = respEmailAddresses.id;
								await models.sequelize.emailAddrBeanRel.update(newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel, {where:{bean_id:newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.bean_id}});
								respEmailAddrBeanRel = await models.sequelize.emailAddrBeanRel.findOne({where: { bean_id: newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.bean_id}});
								objEmailAddrBeanRel = respEmailAddrBeanRel && respEmailAddrBeanRel.dataValues ? respEmailAddrBeanRel.dataValues : null;
								pilatLog = await crmService.setPilatLog('update', 'emails',{description:'email_addr_bean_rel',from:objEmailAddresses.email_address,to:objContacts.first_name+' '+objContacts.last_name}, objEmailAddrBeanRel.id, objOpportunity.id, objOpportunity.assigned_user_id);
							} else {
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.id = models.sequelize.objectId().toString();
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.bean_id = respContacts.id;
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.email_address_id = respEmailAddresses.id;
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.date_modified = new Date();
								newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.date_created = new Date();
								respEmailAddrBeanRel = await models.sequelize.emailAddrBeanRel.create(newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel);
								objEmailAddrBeanRel = respEmailAddrBeanRel && respEmailAddrBeanRel.dataValues ? respEmailAddrBeanRel.dataValues : null;
								pilatLog = await crmService.setPilatLog('create', 'emails',{description:'email_addr_bean_rel',from:objEmailAddresses.email_address,to:objContacts.first_name+' '+objContacts.last_name}, objEmailAddrBeanRel.id, objOpportunity.id, objOpportunity.assigned_user_id);
							}
						}
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel = respEmailAddrBeanRel.dataValues;
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses = respEmailAddresses.dataValues;
					}

					// END EMAIL ADDRESSES
					//
					// BEGIN ACCOUNTS

					let respAccounts, respAccountsCstm, respAccountsOpportunities, objAccounts, objAccountsOpportunities;
					let accountOpportunityAccountsId;
					if (newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts) {
						if (newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.id) {
							accountOpportunityAccountsId = newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.id;
							delete newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.id;
							newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.date_modified = new Date();
							await models.sequelize.accounts.update(newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts, {where:{id:accountOpportunityAccountsId}});
							respAccounts = await models.sequelize.accounts.findOne({where: { id: accountOpportunityAccountsId }});
							objAccounts = respAccounts && respAccounts.dataValues ? respAccounts.dataValues : null;
							pilatLog = await crmService.setPilatLog('update', 'accounts',objAccounts.name, objAccounts.id, objOpportunity.id, objOpportunity.assigned_user_id);
						} else {
							newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.id = models.sequelize.objectId().toString();
							newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.date_entered = new Date();
							newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.date_modified = new Date();
							respAccounts = await models.sequelize.accounts.create(newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts);
							objAccounts = respAccounts && respAccounts.dataValues ? respAccounts.dataValues : null;
							pilatLog = await crmService.setPilatLog('create', 'accounts',objAccounts.name, objAccounts.id, objOpportunity.id, objOpportunity.assigned_user_id);
						}

						if (newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm) {
							if (newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm.id_c) {
								await models.sequelize.accountsCstm.update(newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm, {where:{id_c:newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm.id_c}});
								respAccountsCstm = await models.sequelize.accountsCstm.findOne({where: { id_c: newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c }});
							} else {
								newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm.id_c = respAccounts.dataValues.id;
								respAccountsCstm = await models.sequelize.accountsCstm.create(newOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm);
							}
						}

						let opportunityAccountsOpportunitiesId;
						if (newOpportunity.opportunityAccountsOpportunities) {
							if (newOpportunity.opportunityAccountsOpportunities.id) {
								opportunityAccountsOpportunitiesId = newOpportunity.opportunityAccountsOpportunities.id;
								delete newOpportunity.opportunityAccountsOpportunities.id;
								newOpportunity.opportunityAccountsOpportunities.date_modified = new Date();
								newOpportunity.opportunityAccountsOpportunities.opportunity_id = respOpportunity.dataValues.id;
								newOpportunity.opportunityAccountsOpportunities.account_id = respAccounts.dataValues.id;
								await models.sequelize.accountsOpportunities.update(newOpportunity.opportunityAccountsOpportunities, {where:{opportunity_id:newOpportunity.opportunityAccountsOpportunities.opportunity_id}});
								respAccountsOpportunities = await models.sequelize.accountsOpportunities.findOne({where: { opportunity_id: newOpportunity.opportunityAccountsOpportunities.opportunity_id }});
								objAccountsOpportunities = respAccountsOpportunities && respAccountsOpportunities.dataValues ? respAccountsOpportunities.dataValues : null;
								pilatLog = await crmService.setPilatLog('update', 'accounts',{description:'accounts_opportunities',from:objAccounts.name,to:objOpportunity.name}, objAccountsOpportunities.id, objOpportunity.id, objOpportunity.assigned_user_id);
							} else {
								newOpportunity.opportunityAccountsOpportunities.id = models.sequelize.objectId().toString();
								newOpportunity.opportunityAccountsOpportunities.opportunity_id = respOpportunity.dataValues.id;
								newOpportunity.opportunityAccountsOpportunities.account_id = respAccounts.dataValues.id;
								newOpportunity.opportunityAccountsOpportunities.date_modified = new Date();
								respAccountsOpportunities = await models.sequelize.accountsOpportunities.create(newOpportunity.opportunityAccountsOpportunities);
								objAccountsOpportunities = respAccountsOpportunities && respAccountsOpportunities.dataValues ? respAccountsOpportunities.dataValues : null;
								pilatLog = await crmService.setPilatLog('create', 'accounts',{description:'accounts_opportunities',from:objAccounts.name,to:objOpportunity.name}, objAccountsOpportunities.id, objOpportunity.id, objOpportunity.assigned_user_id);
							}
						}
						objOpportunity.opportunityAccountsOpportunities = respAccountsOpportunities.dataValues;
						objOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts = respAccounts.dataValues;
						objOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm = respAccountsCstm.dataValues;
					}

					// END ACCOUNTS
					//
					// BEGIN AOS QUOTES

					let nextNumber = await models.sequelize.aosQuotes.max('number');
					let respAosQuotes, respAosQuotesCstm, objAosQuotes;
					if (newOpportunity.opportunityAosQuotes) {

						newOpportunity.opportunityAosQuotes.total_amount = crmService.setNumberToSave(newOpportunity.opportunityAosQuotes.total_amount);
						newOpportunity.opportunityAosQuotes.discount_amount = crmService.setNumberToSave(newOpportunity.opportunityAosQuotes.discount_amount);
						newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c = crmService.setNumberToSave(newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c);
						newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c = crmService.setNumberToSave(newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c);
						newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.saldo_c = crmService.setNumberToSave(newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.saldo_c);

						let opportunityAosQuotesId;
						if (newOpportunity.opportunityAosQuotes.id) {
							opportunityAosQuotesId = newOpportunity.opportunityAosQuotes.id;
							delete newOpportunity.opportunityAosQuotes.id;
							newOpportunity.opportunityAosQuotes.date_modified = new Date();
							newOpportunity.opportunityAosQuotes.opportunity_id = respOpportunity.dataValues.id;
							newOpportunity.opportunityAosQuotes.number = newOpportunity.opportunityAosQuotes.number ? newOpportunity.opportunityAosQuotes.number : nextNumber+1;
							await models.sequelize.aosQuotes.update(newOpportunity.opportunityAosQuotes, {where:{id:opportunityAosQuotesId}});
							respAosQuotes = await models.sequelize.aosQuotes.findOne({where: { id: opportunityAosQuotesId }});
							objAosQuotes = respAosQuotes && respAosQuotes.dataValues ? respAosQuotes.dataValues : null;
							pilatLog = await crmService.setPilatLog('update', 'AOS_Quotes',objAosQuotes.name, objAosQuotes.id, objOpportunity.id, objOpportunity.assigned_user_id);
						} else {
							newOpportunity.opportunityAosQuotes.id = models.sequelize.objectId().toString();
							newOpportunity.opportunityAosQuotes.opportunity_id = respOpportunity.dataValues.id;
							newOpportunity.opportunityAosQuotes.date_entered = new Date();
							newOpportunity.opportunityAosQuotes.date_modified = new Date();
							newOpportunity.opportunityAosQuotes.number = newOpportunity.opportunityAosQuotes.number ? newOpportunity.opportunityAosQuotes.number : nextNumber+1;
							respAosQuotes = await models.sequelize.aosQuotes.create(newOpportunity.opportunityAosQuotes);
							objAosQuotes = respAosQuotes && respAosQuotes.dataValues ? respAosQuotes.dataValues : null;
							pilatLog = await crmService.setPilatLog('create', 'AOS_Quotes',objAosQuotes.name, objAosQuotes.id, objOpportunity.id, objOpportunity.assigned_user_id);
						}

						if (newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm) {
							if (newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.id_c) {
								await models.sequelize.aosQuotesCstm.update(newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm, {where:{id_c:newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.id_c}});
								respAosQuotesCstm = await models.sequelize.aosQuotesCstm.findOne({where: { id_c: newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.id_c }});
							} else {
								newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.id_c = respAosQuotes.dataValues.id;
								respAosQuotesCstm = await models.sequelize.aosQuotesCstm.create(newOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm);
							}
						}
						objOpportunity.opportunityAosQuotes = respAosQuotes.dataValues;
						objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm = respAosQuotesCstm.dataValues;
					}

					// END AOS QUOTES

					let opportunityOpportunitiesAuditId;
					if (newOpportunity.opportunityOpportunitiesAudit) {
						if (newOpportunity.opportunityOpportunitiesAudit.id) {
							opportunityOpportunitiesAuditId = newOpportunity.opportunityOpportunitiesAudit.id;
							delete newOpportunity.opportunityOpportunitiesAudit.id;
							await models.sequelize.opportunitiesAudit.update(newOpportunity.opportunityOpportunitiesAudit,{where:{parent_id:newOpportunity.opportunityOpportunitiesAudit.parent_id}});
							let respOpportunitiesAudit = await models.sequelize.opportunitiesAudit.findOne({where: { parent_id: newOpportunity.opportunityOpportunitiesAudit.parent_id }});
							objOpportunity.opportunityOpportunitiesAudit = respOpportunitiesAudit.dataValues;
						} else {
							newOpportunity.opportunityOpportunitiesAudit.id = models.sequelize.objectId().toString();
							newOpportunity.opportunityOpportunitiesAudit.parent_id = newOpportunity.id;
							newOpportunity.opportunityOpportunitiesAudit.date_created = new Date();
							let respOpportunitiesAudit = await models.sequelize.opportunitiesAudit.create(newOpportunity.opportunityOpportunitiesAudit);
							objOpportunity.opportunityOpportunitiesAudit = respOpportunitiesAudit.dataValues;
						}
					}

					let opportunitySugarfeedId;
					if (newOpportunity.opportunitySugarfeed) {
						if (newOpportunity.opportunitySugarfeed.id) {
							opportunitySugarfeedId = newOpportunity.opportunitySugarfeed.id;
							delete newOpportunity.opportunitySugarfeed.id;
							newOpportunity.opportunitySugarfeed.date_modified = new Date();
							await models.sequelize.sugarfeed.update(newOpportunity.opportunitySugarfeed, {where:{related_id:newOpportunity.opportunitySugarfeed.related_id}});
							let respSugarfeed = await models.sequelize.sugarfeed.findOne({where: { related_id: newOpportunity.opportunitySugarfeed.related_id }});
							objOpportunity.opportunitySugarfeed = respSugarfeed.dataValues;
						} else {
							newOpportunity.opportunitySugarfeed.id = models.sequelize.objectId().toString();
							newOpportunity.opportunitySugarfeed.related_id = newOpportunity.id;
							newOpportunity.opportunitySugarfeed.date_entered = new Date();
							newOpportunity.opportunitySugarfeed.date_modified = new Date();
							let respSugarfeed = await models.sequelize.sugarfeed.create(newOpportunity.opportunitySugarfeed);
							objOpportunity.opportunitySugarfeed = respSugarfeed.dataValues;
						}
					}

					let opportunityAodIndexeventId;
					if (newOpportunity.opportunityAodIndexevent) {
						if (newOpportunity.opportunityAodIndexevent.id) {
							opportunityAodIndexeventId = newOpportunity.opportunityAodIndexevent.id;
							delete newOpportunity.opportunityAodIndexevent.id;
							newOpportunity.opportunityAodIndexevent.date_modified = new Date();
							await models.sequelize.aodIndexevent.update(newOpportunity.opportunityAodIndexevent, {where:{record_id:newOpportunity.opportunityAodIndexevent.record_id}});
							let respAodIndexevent = await models.sequelize.aodIndexevent.findOne({where: { record_id: newOpportunity.opportunityAodIndexevent.record_id }});
							objOpportunity.opportunityAodIndexevent = respAodIndexevent.dataValues;
						} else {
							newOpportunity.opportunityAodIndexevent.id = models.sequelize.objectId().toString();
							newOpportunity.opportunityAodIndexevent.record_id = newOpportunity.id;
							newOpportunity.opportunityAodIndexevent.date_entered = new Date();
							newOpportunity.opportunityAodIndexevent.date_modified = new Date();
							let respAodIndexevent = await models.sequelize.aodIndexevent.create(newOpportunity.opportunityAodIndexevent);
							objOpportunity.opportunityAodIndexevent = respAodIndexevent.dataValues;
						}
					}

					let opportunityTrackerId;
					if (newOpportunity.opportunityTracker) {
						if (newOpportunity.opportunityTracker.id) {
							opportunityTrackerId = newOpportunity.opportunityTracker.id;
							delete newOpportunity.opportunityTracker.id;
							newOpportunity.opportunityTracker.date_modified = new Date();
							await models.sequelize.tracker.update(newOpportunity.opportunityTracker, {where:{item_id:newOpportunity.opportunityTracker.item_id}});
							let respTracker = await models.sequelize.tracker.findOne({where: { item_id: newOpportunity.opportunityTracker.item_id }});
							objOpportunity.opportunityTracker = respTracker.dataValues;
						} else {
							let max = await models.sequelize.tracker.max('id');
							newOpportunity.opportunityTracker.id = newOpportunity.opportunityTracker.id ? newOpportunity.opportunityTracker.id : max+1;
							newOpportunity.opportunityTracker.monitor_id = models.sequelize.objectId().toString();
							newOpportunity.opportunityTracker.item_id = newOpportunity.id;
							newOpportunity.opportunityTracker.date_modified = new Date();
							let respTracker = await models.sequelize.tracker.create(newOpportunity.opportunityTracker);
							objOpportunity.opportunityTracker = respTracker.dataValues;
						}
					}
				}

				if (newOpportunity.opportunityOpportunitiesContacts && newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts && newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails) {
					objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails = newOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails;
				}
				await crmService.setEmailOpportunity(objOpportunity, respContacts);
			} else {
				objOpportunity = new models.mongoose.opportunities(newOpportunity);
				await objOpportunity.save();
			}
			return objOpportunity;
		} catch (error) {
			throw error;
		}
	}

	static async updateOpportunity(id ,updateOpportunity, userLoggedIn) {
		try {
			let objOpportunity, pilatLog;

			if(sql) {
				let respOpportunity, respContacts, respOpportunitiesCstm;
				let updateOpportunityId;
				if (updateOpportunity) {
					updateOpportunity.amount = crmService.setNumberToSave(updateOpportunity.amount);
					if (updateOpportunity.id) {
						updateOpportunityId = updateOpportunity.id;
						delete updateOpportunity.id;
						updateOpportunity.date_modified = new Date();
						await models.sequelize.opportunities.update(updateOpportunity, {where:{id:id}});
						respOpportunity = await models.sequelize.opportunities.findOne({where: { id: id }});
						objOpportunity = respOpportunity && respOpportunity.dataValues ? respOpportunity.dataValues : null;
						pilatLog = await crmService.setPilatLog('update', 'opportunities',objOpportunity.name, objOpportunity.id, objOpportunity.id, objOpportunity.assigned_user_id);
					} else {
						let oldOpportunity = await models.sequelize.opportunities.findOne({where:{id:id}});
						if (oldOpportunity && oldOpportunity.dataValues) {
							oldOpportunity = oldOpportunity.dataValues;
							updateOpportunityId = oldOpportunity.id;
							delete oldOpportunity.id;
							updateOpportunity.date_modified = new Date();
							await models.sequelize.opportunities.update(updateOpportunity, {where:{id:updateOpportunityId}});
							respOpportunity = await models.sequelize.opportunities.findOne({where: { id: updateOpportunityId }});
							objOpportunity = respOpportunity && respOpportunity.dataValues ? respOpportunity.dataValues : null;
							pilatLog = await crmService.setPilatLog('update', 'opportunities', objOpportunity.name, objOpportunity.id, objOpportunity.id, objOpportunity.assigned_user_id);
						} else {
							updateOpportunity.id = models.sequelize.objectId().toString();
							updateOpportunity.date_entered = new Date();
							updateOpportunity.date_modified = new Date();
							updateOpportunity.date_closed = new Date();
							respOpportunity = await models.sequelize.opportunities.create(updateOpportunity);
							objOpportunity = respOpportunity && respOpportunity.dataValues ? respOpportunity.dataValues : null;
							pilatLog = await crmService.setPilatLog('create', 'opportunities', objOpportunity.name, objOpportunity.id, objOpportunity.id, objOpportunity.assigned_user_id);
						}
					}
					if (updateOpportunity.opportunityOpportunitiesCstm) {
						if (updateOpportunity.opportunityOpportunitiesCstm.id_c){
							await models.sequelize.opportunitiesCstm.update(updateOpportunity.opportunityOpportunitiesCstm, {where:{id_c:updateOpportunity.opportunityOpportunitiesCstm.id_c}});
							respOpportunitiesCstm = await models.sequelize.opportunitiesCstm.findOne({where: { id_c: updateOpportunity.opportunityOpportunitiesCstm.id_c}});
						} else {
							let oldOpportunityOpportunitiesCstm = await models.sequelize.opportunitiesCstm.findOne({where:{id_c:id}});
							if (oldOpportunityOpportunitiesCstm && oldOpportunityOpportunitiesCstm.dataValues) {
								oldOpportunityOpportunitiesCstm = oldOpportunityOpportunitiesCstm.dataValues;
								await models.sequelize.opportunitiesCstm.update(updateOpportunity.opportunityOpportunitiesCstm, {where:{id_c:oldOpportunityOpportunitiesCstm.id_c}});
								respOpportunitiesCstm = await models.sequelize.opportunitiesCstm.findOne({where: { id_c: oldOpportunityOpportunitiesCstm.id_c}});
							} else {
								updateOpportunity.opportunityOpportunitiesCstm.id_c = respOpportunity.dataValues.id;
								respOpportunitiesCstm = await models.sequelize.opportunitiesCstm.create(updateOpportunity.opportunityOpportunitiesCstm);
							}
						}
					}
					objOpportunity = respOpportunity && respOpportunity.dataValues ? respOpportunity.dataValues : null;
					objOpportunity.opportunityOpportunitiesCstm = respOpportunitiesCstm && respOpportunitiesCstm.dataValues ? respOpportunitiesCstm.dataValues : null;

					// Begin Contacts

					let respContactsCstm, objContacts;
					let opportunityContactContactsId;
					if (updateOpportunity.opportunityOpportunitiesContacts) {
						if (updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts) {
							if (updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.id) {
								let opportunityContactContactsId = updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.id;
								delete updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.id;
								updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.date_modified = new Date();
								await models.sequelize.contacts.update(updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts, {where:{id:opportunityContactContactsId}});
								respContacts = await models.sequelize.contacts.findOne({where: { id: opportunityContactContactsId }});
								objContacts = respContacts && respContacts.dataValues ? respContacts.dataValues : null;
								pilatLog = await crmService.setPilatLog('update', 'contacts', objContacts.first_name+' '+objContacts.last_name, objContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
							} else {
								updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.id = models.sequelize.objectId().toString();
								updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.date_entered = new Date();
								updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.date_reviewed = new Date();
								updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.date_modified = new Date();
								respContacts = await models.sequelize.contacts.create(updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts);
								objContacts = respContacts && respContacts.dataValues ? respContacts.dataValues : null;
								pilatLog = await crmService.setPilatLog('create', 'contacts', objContacts.first_name+' '+objContacts.last_name, objContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
							}

							if (updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm) {
								if (updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c) {
									await models.sequelize.contactsCstm.update(updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm, {where:{id_c:updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c}});
									respContactsCstm = await models.sequelize.contactsCstm.findOne({where: { id_c: updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c }});
								} else {
									updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm.id_c = respContacts.dataValues.id;
									respContactsCstm = await models.sequelize.contactsCstm.create(updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm);
								}
							}

							let respOpportunitiesContacts, objOpportunitiesContacts;
							let opportunityOpportunitiesContactsId;
							if (updateOpportunity.opportunityOpportunitiesContacts) {
								if (updateOpportunity.opportunityOpportunitiesContacts.id) {
									opportunityOpportunitiesContactsId = updateOpportunity.opportunityOpportunitiesContacts.id;
									delete updateOpportunity.opportunityOpportunitiesContacts.id;
									updateOpportunity.opportunityOpportunitiesContacts.date_modified = new Date();
									updateOpportunity.opportunityOpportunitiesContacts.opportunity_id = respOpportunity.dataValues.id;
									updateOpportunity.opportunityOpportunitiesContacts.contact_id = respContacts.dataValues.id;
									await models.sequelize.opportunitiesContacts.update(updateOpportunity.opportunityOpportunitiesContacts, {where:{opportunity_id:id}});
									respOpportunitiesContacts = await models.sequelize.opportunitiesContacts.findOne({where: { opportunity_id: id }});
									objOpportunitiesContacts = respOpportunitiesContacts && respOpportunitiesContacts.dataValues ? respOpportunitiesContacts.dataValues : null;
									pilatLog = await crmService.setPilatLog('update', 'contacts', {description:'opportunities_contacts',from:objOpportunity.name,to:objContacts.first_name+' '+objContacts.last_name}, objOpportunitiesContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
								} else {
									let oldOpportunityOpportunitiesContacts = await models.sequelize.opportunitiesContacts.findOne({where:{opportunity_id:id}});
									if (oldOpportunityOpportunitiesContacts && oldOpportunityOpportunitiesContacts.dataValues) {
										oldOpportunityOpportunitiesContacts = oldOpportunityOpportunitiesContacts.dataValues;
										opportunityOpportunitiesContactsId = oldOpportunityOpportunitiesContacts.id;
										delete oldOpportunityOpportunitiesContacts.id;
										updateOpportunity.opportunityOpportunitiesContacts.date_modified = new Date();
										updateOpportunity.opportunityOpportunitiesContacts.opportunity_id = respOpportunity.dataValues.id;
										updateOpportunity.opportunityOpportunitiesContacts.contact_id = respContacts.dataValues.id;
										await models.sequelize.opportunitiesContacts.update(updateOpportunity.opportunityOpportunitiesContacts, {where:{id:opportunityOpportunitiesContactsId}});
										respOpportunitiesContacts = await models.sequelize.opportunitiesContacts.findOne({where: { id: opportunityOpportunitiesContactsId }});
										objOpportunitiesContacts = respOpportunitiesContacts && respOpportunitiesContacts.dataValues ? respOpportunitiesContacts.dataValues : null;
										pilatLog = await crmService.setPilatLog('update', 'contacts', {description:'opportunities_contacts',from:objOpportunity.name,to:objContacts.first_name+' '+objContacts.last_name}, objOpportunitiesContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
									} else {
										updateOpportunity.opportunityOpportunitiesContacts.id = models.sequelize.objectId().toString();
										updateOpportunity.opportunityOpportunitiesContacts.opportunity_id = respOpportunity.dataValues.id;
										updateOpportunity.opportunityOpportunitiesContacts.contact_id = respContacts.dataValues.id;
										updateOpportunity.opportunityOpportunitiesContacts.date_modified = new Date();
										respOpportunitiesContacts = await models.sequelize.opportunitiesContacts.create(updateOpportunity.opportunityOpportunitiesContacts);
										objOpportunitiesContacts = respOpportunitiesContacts && respOpportunitiesContacts.dataValues ? respOpportunitiesContacts.dataValues : null;
										pilatLog = await crmService.setPilatLog('create', 'contacts', {description:'opportunities_contacts',from:objOpportunity.name,to:objContacts.first_name+' '+objContacts.last_name}, objOpportunitiesContacts.id, objOpportunity.id, objOpportunity.assigned_user_id);
									}
								}
							}
							objOpportunity.opportunityOpportunitiesContacts = respOpportunitiesContacts && respOpportunitiesContacts.dataValues ? respOpportunitiesContacts.dataValues : null;
							objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts = respContacts && respContacts.dataValues ? respContacts.dataValues : null;
							objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactContactsCstm = respContactsCstm && respContactsCstm.dataValues ? respContactsCstm.dataValues : null;

							// End Contacts
							//
							// Begin Email Addresses

							let respEmailAddresses, respEmailAddrBeanRel, objEmailAddresses, objEmailAddrBeanRel;
							let emailAddrBeanRelEmailAddressesId;
							if (updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses) {
								if (updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id) {
									emailAddrBeanRelEmailAddressesId = updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id;
									delete updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id;
									updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.date_modified = new Date();
									await models.sequelize.emailAddresses.update(updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses, {where:{id:emailAddrBeanRelEmailAddressesId}});
									respEmailAddresses = await models.sequelize.emailAddresses.findOne({where: { id: emailAddrBeanRelEmailAddressesId }});
									objEmailAddresses = respEmailAddresses && respEmailAddresses.dataValues ? respEmailAddresses.dataValues : null;
									pilatLog = await crmService.setPilatLog('update', 'emails', objEmailAddresses.email_address, objEmailAddresses.id, objOpportunity.id, objOpportunity.assigned_user_id);
								} else {
									updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id = models.sequelize.objectId().toString();
									updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.date_entered = new Date();
									updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.date_reviewed = new Date();
									updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.date_modified = new Date();
									respEmailAddresses = await models.sequelize.emailAddresses.create(updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses);
									objEmailAddresses = respEmailAddresses && respEmailAddresses.dataValues ? respEmailAddresses.dataValues : null;
									pilatLog = await crmService.setPilatLog('create', 'emails', objEmailAddresses.email_address, objEmailAddresses.id, objOpportunity.id, objOpportunity.assigned_user_id);
								}

								let contactEmailAddrBeanRelId;
								if (updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel) {
									if (updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.id) {
										contactEmailAddrBeanRelId = updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.id;
										delete updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.id;
										updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.date_modified = new Date();
										updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.bean_id = respContacts.dataValues.id;
										updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.email_address_id = respEmailAddresses.dataValues.id;
										await models.sequelize.emailAddrBeanRel.update(updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel, {where: {bean_id: updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.bean_id}});
										respEmailAddrBeanRel = await models.sequelize.emailAddrBeanRel.findOne({where: {bean_id: updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.bean_id}});
										objEmailAddrBeanRel = respEmailAddrBeanRel && respEmailAddrBeanRel.dataValues ? respEmailAddrBeanRel.dataValues : null;
										pilatLog = await crmService.setPilatLog('update', 'emails', {description:'email_addr_bean_rel',from:objEmailAddresses.email_address, to:objContacts.first_name+' '+objContacts.last_name}, objEmailAddresses.id, objOpportunity.id, objOpportunity.assigned_user_id);
									} else {
										updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.id = models.sequelize.objectId().toString();
										updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.bean_id = respContacts.dataValues.id;
										updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.email_address_id = respEmailAddresses.dataValues.id;
										updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.date_created = new Date();
										updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.date_modified = new Date();
										respEmailAddrBeanRel = await models.sequelize.emailAddrBeanRel.create(updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel);
										objEmailAddrBeanRel = respEmailAddrBeanRel && respEmailAddrBeanRel.dataValues ? respEmailAddrBeanRel.dataValues : null;
										pilatLog = await crmService.setPilatLog('create', 'emails', {description:'email_addr_bean_rel',from:objEmailAddresses.email_address, to:objContacts.first_name+' '+objContacts.last_name}, objEmailAddresses.id, objOpportunity.id, objOpportunity.assigned_user_id);
									}
									objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel = respEmailAddrBeanRel && respEmailAddrBeanRel.dataValues ? respEmailAddrBeanRel.dataValues : null;
									objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses = respEmailAddresses && respEmailAddresses.dataValues ? respEmailAddresses.dataValues : null;
								}
							}
						}

					}

					// End Email Addresses
					//
					// Begin Accounts

					let respAccounts, respAccountsCstm, respAccountsOpportunities, objAccounts, objAccountsOpportunities;
					if (updateOpportunity.opportunityAccountsOpportunities) {

						let accountOpportunityAccountsId;
						if (updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts) {
							if (updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.id) {
								accountOpportunityAccountsId = updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.id;
								delete updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.id;
								updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.date_modified = new Date();
								await models.sequelize.accounts.update(updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts, {where:{id:accountOpportunityAccountsId}});
								respAccounts = await models.sequelize.accounts.findOne({where: { id: accountOpportunityAccountsId }});
								objAccounts = respAccounts && respAccounts.dataValues ? respAccounts.dataValues : null;
								pilatLog = await crmService.setPilatLog('update', 'accounts', objAccounts.name, objAccounts.id, objOpportunity.id, objOpportunity.assigned_user_id);
							} else {
								updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.id = models.sequelize.objectId().toString();
								updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.date_entered = new Date();
								updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.date_reviewed = new Date();
								updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.date_modified = new Date();
								respAccounts = await models.sequelize.accounts.create(updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts);
								objAccounts = respAccounts && respAccounts.dataValues ? respAccounts.dataValues : null;
								pilatLog = await crmService.setPilatLog('create', 'accounts', objAccounts.name, objAccounts.id, objOpportunity.id, objOpportunity.assigned_user_id);
							}

							if (updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm) {
								if (updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm.id_c) {
									await models.sequelize.accountsCstm.update(updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm, {where:{id_c:updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm.id_c}});
									respAccountsCstm = await models.sequelize.accountsCstm.findOne({where: { id_c: updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm.id_c }});
								} else {
									updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm.id_c = respAccounts.dataValues.id;
									respAccountsCstm = await models.sequelize.accountsCstm.create(updateOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm);
								}
							}

							let opportunityAccountsOpportunitiesId;
							if (updateOpportunity.opportunityAccountsOpportunities) {
								if (updateOpportunity.opportunityAccountsOpportunities.id) {
									opportunityAccountsOpportunitiesId = updateOpportunity.opportunityAccountsOpportunities.id;
									updateOpportunity.opportunityAccountsOpportunities.date_modified = new Date();
									updateOpportunity.opportunityAccountsOpportunities.opportunity_id = respOpportunity.dataValues.id;
									updateOpportunity.opportunityAccountsOpportunities.account_id = respAccounts.dataValues.id;
									delete updateOpportunity.opportunityAccountsOpportunities.id;
									await models.sequelize.accountsOpportunities.update(updateOpportunity.opportunityAccountsOpportunities, {where: {opportunity_id: updateOpportunity.opportunityAccountsOpportunities.opportunity_id}});
									respAccountsOpportunities = await models.sequelize.accountsOpportunities.findOne({where: {opportunity_id: updateOpportunity.opportunityAccountsOpportunities.opportunity_id}});
									objAccountsOpportunities = respAccountsOpportunities && respAccountsOpportunities.dataValues ? respAccounts.dataValues : null;
									pilatLog = await crmService.setPilatLog('update', 'accounts', {description:'accounts_opportunities',from:objAccounts.name,to:objOpportunity.name}, objAccountsOpportunities.id, objOpportunity.id, objOpportunity.assigned_user_id);
								} else {
									updateOpportunity.opportunityAccountsOpportunities.id = models.sequelize.objectId().toString();
									updateOpportunity.opportunityAccountsOpportunities.date_modified = new Date();
									updateOpportunity.opportunityAccountsOpportunities.opportunity_id = respOpportunity.dataValues.id;
									updateOpportunity.opportunityAccountsOpportunities.account_id = respAccounts.dataValues.id;
									respAccountsOpportunities = await models.sequelize.accountsOpportunities.create(updateOpportunity.opportunityAccountsOpportunities);
									objAccountsOpportunities = respAccountsOpportunities && respAccountsOpportunities.dataValues ? respAccounts.dataValues : null;
									pilatLog = await crmService.setPilatLog('create', 'accounts', {description:'accounts_opportunities',from:objAccounts.name,to:objOpportunity.name}, objAccountsOpportunities.id, objOpportunity.id, objOpportunity.assigned_user_id);
								}
								objOpportunity.opportunityAccountsOpportunities = respAccountsOpportunities && respAccountsOpportunities.dataValues ? respAccountsOpportunities.dataValues : null;
								objOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts = respAccounts && respAccounts.dataValues ? respAccounts.dataValues : null;
								objOpportunity.opportunityAccountsOpportunities.accountOpportunityAccounts.accountAccountsCstm = respAccountsCstm && respAccountsCstm.dataValues ? respAccountsCstm.dataValues : null;
							}
						}
					}

					// End Accounts
					//
					// Begin Aos Quotes

					let nextNumber = await models.sequelize.aosQuotes.max('number');
					let respAosQuotes, respAosQuotesCstm;
					if (updateOpportunity.opportunityAosQuotes) {

						updateOpportunity.opportunityAosQuotes.total_amount = crmService.setNumberToSave(updateOpportunity.opportunityAosQuotes.total_amount);
						updateOpportunity.opportunityAosQuotes.discount_amount = crmService.setNumberToSave(updateOpportunity.opportunityAosQuotes.discount_amount);
						updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c = crmService.setNumberToSave(updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c);
						updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c = crmService.setNumberToSave(updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c);
						updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.saldo_c = crmService.setNumberToSave(updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.saldo_c);

						let opportunityAosQuotesId;
						if (updateOpportunity.opportunityAosQuotes.id) {
							opportunityAosQuotesId = updateOpportunity.opportunityAosQuotes.id;
							delete updateOpportunity.opportunityAosQuotes.id;
							updateOpportunity.opportunityAosQuotes.date_modified = new Date();
							updateOpportunity.opportunityAosQuotes.opportunity_id = respOpportunity.dataValues.id;
							updateOpportunity.opportunityAosQuotes.billing_account_id = respAccounts.dataValues.id;
							updateOpportunity.opportunityAosQuotes.billing_contact_id = respContacts.dataValues.id;
							updateOpportunity.opportunityAosQuotes.number = updateOpportunity.opportunityAosQuotes.number ? updateOpportunity.opportunityAosQuotes.number : nextNumber+1;
							await models.sequelize.aosQuotes.update(updateOpportunity.opportunityAosQuotes, {where:{id:opportunityAosQuotesId}});
							respAosQuotes = await models.sequelize.aosQuotes.findOne({where: { id: opportunityAosQuotesId }});
						} else {
							updateOpportunity.opportunityAosQuotes.id = models.sequelize.objectId().toString();
							updateOpportunity.opportunityAosQuotes.date_entered = new Date();
							updateOpportunity.opportunityAosQuotes.date_reviewed = new Date();
							updateOpportunity.opportunityAosQuotes.date_modified = new Date();
							updateOpportunity.opportunityAosQuotes.opportunity_id = respOpportunity.dataValues.id;
							updateOpportunity.opportunityAosQuotes.billing_account_id = respAccounts.dataValues.id;
							updateOpportunity.opportunityAosQuotes.billing_contact_id = respContacts.dataValues.id;
							updateOpportunity.opportunityAosQuotes.number = updateOpportunity.opportunityAosQuotes.number ? updateOpportunity.opportunityAosQuotes.number : nextNumber+1;
							respAosQuotes = await models.sequelize.aosQuotes.create(updateOpportunity.opportunityAosQuotes);
						}

						if (updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm) {
							if (updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.id_c) {
								await models.sequelize.aosQuotesCstm.update(updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm, {where:{id_c:updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.id_c}});
								respAosQuotesCstm = await models.sequelize.aosQuotesCstm.findOne({where: { id_c: updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.id_c }});
							} else {
								updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.id_c = respAosQuotes.dataValues.id;
								respAosQuotesCstm = await models.sequelize.aosQuotesCstm.create(updateOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm);
							}
						}
						objOpportunity.opportunityAosQuotes = respAosQuotes && respAosQuotes.dataValues ? respAosQuotes.dataValues : null;
						objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm = respAosQuotesCstm && respAosQuotesCstm.dataValues ? respAosQuotesCstm.dataValues : null;
					}

					// End Aos Quotes
					let opportunityOpportunitiesAuditId;
					if (updateOpportunity.opportunityOpportunitiesAudit) {
						if (updateOpportunity.opportunityOpportunitiesAudit.id) {
							opportunityOpportunitiesAuditId = updateOpportunity.opportunityOpportunitiesAudit.id;
							delete updateOpportunity.opportunityOpportunitiesAudit.id;
							await models.sequelize.opportunitiesAudit.update(updateOpportunity.opportunityOpportunitiesAudit,{where:{id:opportunityOpportunitiesAuditId}});
							let respOpportunitiesAudit = await models.sequelize.opportunitiesAudit.findOne({where: { id: opportunityOpportunitiesAuditId }});
							objOpportunity.opportunityOpportunitiesAudit = respOpportunitiesAudit.dataValues;
						} else {
							let oldOpportunityOpportunitiesAudit = await models.sequelize.opportunitiesAudit.findOne({where:{parent_id:id}});
							if (oldOpportunityOpportunitiesAudit && oldOpportunityOpportunitiesAudit.dataValues) {
								oldOpportunityOpportunitiesAudit = oldOpportunityOpportunitiesAudit.dataValues;
								opportunityOpportunitiesAuditId = oldOpportunityOpportunitiesAudit.id;
								delete oldOpportunityOpportunitiesAudit.id;
								await models.sequelize.opportunitiesAudit.update(updateOpportunity.opportunityOpportunitiesAudit,{where:{id:opportunityOpportunitiesAuditId}});
								let respOpportunitiesAudit = await models.sequelize.opportunitiesAudit.findOne({where: { id: opportunityOpportunitiesAuditId }});
								objOpportunity.opportunityOpportunitiesAudit = respOpportunitiesAudit.dataValues;
							} else {
								updateOpportunity.opportunityOpportunitiesAudit.id = models.sequelize.objectId().toString();
								updateOpportunity.opportunityOpportunitiesAudit.parent_id = respOpportunity.dataValues.id;
								updateOpportunity.opportunityOpportunitiesAudit.date_created = new Date();
								let respOpportunitiesAudit = await models.sequelize.opportunitiesAudit.create(updateOpportunity.opportunityOpportunitiesAudit);
								objOpportunity.opportunityOpportunitiesAudit = respOpportunitiesAudit && respOpportunitiesAudit.dataValues ? respOpportunitiesAudit.dataValues : null;
							}
						}
					}

					let opportunitySugarfeedId;
					if (updateOpportunity.opportunitySugarfeed) {
						if (updateOpportunity.opportunitySugarfeed.id) {
							opportunitySugarfeedId = updateOpportunity.opportunitySugarfeed.id;
							delete updateOpportunity.opportunitySugarfeed.id;
							updateOpportunity.opportunitySugarfeed.date_modified = new Date();
							await models.sequelize.sugarfeed.update(updateOpportunity.opportunitySugarfeed, {where:{id:opportunitySugarfeedId}});
							let respSugarfeed = await models.sequelize.sugarfeed.findOne({where: { id: opportunitySugarfeedId}});
							objOpportunity.opportunitySugarfeed = respSugarfeed.dataValues;
						} else {
							let oldOpportunitySugarfeed = await models.sequelize.sugarfeed.findOne({where:{related_id:id}});
							if (oldOpportunitySugarfeed && oldOpportunitySugarfeed.dataValues) {
								oldOpportunitySugarfeed = oldOpportunitySugarfeed.dataValues;
								opportunitySugarfeedId = oldOpportunitySugarfeed.id;
								delete oldOpportunitySugarfeed.id;
								updateOpportunity.opportunitySugarfeed.date_modified = new Date();
								await models.sequelize.sugarfeed.update(updateOpportunity.opportunitySugarfeed, {where:{id:opportunitySugarfeedId}});
								let respSugarfeed = await models.sequelize.sugarfeed.findOne({where: { id: opportunitySugarfeedId }});
								objOpportunity.opportunitySugarfeed = respSugarfeed.dataValues;
							} else {
								updateOpportunity.opportunitySugarfeed.id = models.sequelize.objectId().toString();
								updateOpportunity.opportunitySugarfeed.related_id = respOpportunity.dataValues.id;
								updateOpportunity.opportunitySugarfeed.date_entered = new Date();
								updateOpportunity.opportunitySugarfeed.date_modified = new Date();
								let respSugarfeed = await models.sequelize.sugarfeed.create(updateOpportunity.opportunitySugarfeed);
								objOpportunity.opportunitySugarfeed = respSugarfeed && respSugarfeed.dataValues ? respSugarfeed.dataValues : null;
							}
						}
					}

					let opportunityAodIndexeventId;
					if (updateOpportunity.opportunityAodIndexevent) {
						if (updateOpportunity.opportunityAodIndexevent.id) {
							opportunityAodIndexeventId = updateOpportunity.opportunityAodIndexevent.id;
							delete updateOpportunity.opportunityAodIndexevent.id;
							updateOpportunity.opportunityAodIndexevent.date_modified = new Date();
							await models.sequelize.aodIndexevent.update(updateOpportunity.opportunityAodIndexevent, {where:{id:opportunityAodIndexeventId}});
							let respAodIndexevent = await models.sequelize.aodIndexevent.findOne({where: { id: opportunityAodIndexeventId }});
							objOpportunity.opportunityAodIndexevent = respAodIndexevent.dataValues;
						} else {
							let oldOpportunityAodIndexevent = await models.sequelize.aodIndexevent.findOne({where: { record_id: id }});
							if (oldOpportunityAodIndexevent && oldOpportunityAodIndexevent.dataValues) {
								oldOpportunityAodIndexevent = oldOpportunityAodIndexevent.dataValues;
								opportunityAodIndexeventId = oldOpportunityAodIndexevent.id;
								delete oldOpportunityAodIndexevent.id;
								updateOpportunity.opportunityAodIndexevent.date_modified = new Date();
								await models.sequelize.aodIndexevent.update(updateOpportunity.opportunityAodIndexevent, {where:{id:opportunityAodIndexeventId}});
								let respAodIndexevent = await models.sequelize.aodIndexevent.findOne({where: { id: opportunityAodIndexeventId }});
								objOpportunity.opportunityAodIndexevent = respAodIndexevent.dataValues;
							} else {
								updateOpportunity.opportunityAodIndexevent.id = models.sequelize.objectId().toString();
								updateOpportunity.opportunityAodIndexevent.record_id = respOpportunity.dataValues.id;
								updateOpportunity.opportunityAodIndexevent.date_entered = new Date();
								updateOpportunity.opportunityAodIndexevent.date_modified = new Date();
								let respAodIndexevent = await models.sequelize.aodIndexevent.create(updateOpportunity.opportunityAodIndexevent);
								objOpportunity.opportunityAodIndexevent = respAodIndexevent && respAodIndexevent.dataValues ? respAodIndexevent.dataValues : null;
							}
						}
					}

					let opportunityTrackerId;
					if (updateOpportunity.opportunityTracker) {
						if (updateOpportunity.opportunityTracker.id) {
							opportunityTrackerId = updateOpportunity.opportunityTracker.id;
							delete updateOpportunity.opportunityTracker.id;
							updateOpportunity.opportunityTracker.date_modified = new Date();
							await models.sequelize.tracker.update(updateOpportunity.opportunityTracker, {where:{id:opportunityTrackerId}});
							let respTracker = await models.sequelize.tracker.findOne({where: { id: opportunityTrackerId }});
							objOpportunity.opportunityTracker = respTracker.dataValues;
						} else {
							let oldOpportunityTracker = await models.sequelize.tracker.findOne({where: { item_id: id }});
							if (oldOpportunityTracker && oldOpportunityTracker.dataValues) {
								oldOpportunityTracker = oldOpportunityTracker.dataValues;
								opportunityTrackerId = oldOpportunityTracker.id;
								delete oldOpportunityTracker.id;
								updateOpportunity.opportunityTracker.date_modified = new Date();
								await models.sequelize.tracker.update(updateOpportunity.opportunityTracker, {where:{id:opportunityTrackerId}});
								let respTracker = await models.sequelize.tracker.findOne({where: { id: opportunityTrackerId }});
								objOpportunity.opportunityTracker = respTracker.dataValues;
							} else {
								let max = await models.sequelize.tracker.max('id');
								updateOpportunity.opportunityTracker.id = max+1;
								updateOpportunity.opportunityTracker.monitor_id = models.sequelize.objectId().toString();
								updateOpportunity.opportunityTracker.item_id = respOpportunity.dataValues.id;
								updateOpportunity.opportunityTracker.date_modified = new Date();
								let respTracker = await models.sequelize.tracker.create(updateOpportunity.opportunityTracker);
								objOpportunity.opportunityTracker = respTracker && respTracker.dataValues ? respTracker.dataValues : null;
							}
						}
					}
				}

				if (updateOpportunity.opportunityOpportunitiesContacts && updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts && updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails) {
					objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails = updateOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails;
				}

				await crmService.setEmailOpportunity(objOpportunity, respContacts);

			} else {
				objOpportunity = new models.mongoose.opportunities(updateOpportunity);
				await objOpportunity.save();
			}
			return objOpportunity;
		} catch (error) {
			throw error;
		}
	}

	// static async setEmailOpportunity(idOpportunity, respContacts) {
	// 	try {
	// 		let respOpportunity = await this.getAOpportunity(idOpportunity,{});
	// 		let objOpportunity = respOpportunity.dataValues;
	// 		if (objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel) {
	// 			if (!objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails) {
	// 				await OpportunityService.createAndSendPdf(objOpportunity, async (err,file,info) => {
	// 					if (err) {
	// 						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.invalid_email = 1;
	// 						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.confirm_opt_in_fail_date = new Date();
	// 					} else {
	// 						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.invalid_email = 0;
	// 						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.confirm_opt_in_date = new Date();
	// 						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.confirm_opt_in_sent_date = new Date();
	// 					}
	// 					let id = objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id;
	// 					let respEmailAddresses = await models.sequelize.emailAddresses.update(objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses, {where:{id:id}});
	//
	// 					let respContactEmails, respOldContactEmails = await models.sequelize.emails.findOne({where:{parent_id:objOpportunity.opportunityOpportunitiesContacts.contact_id}});
	// 					if (respOldContactEmails) {
	// 						let updateContactEmail = respOldContactEmails.dataValues;
	// 						updateContactEmail.date_modified = new Date();
	// 						updateContactEmail.modified_user_id = respContacts.dataValues.assigned_user_id;
	// 						let respEmails = await models.sequelize.emails.update(updateContactEmail,{where:{parent_id:objOpportunity.opportunityOpportunitiesContacts.contact_id}});
	// 						respContactEmails = await models.sequelize.emails.findOne({where:{parent_id:objOpportunity.opportunityOpportunitiesContacts.contact_id}});
	// 					} else {
	// 						let max = await models.sequelize.emails.max('uid');
	// 						let newContactEmail = {
	// 							id:models.sequelize.objectId().toString(),
	// 							name:respContacts.dataValues.first_name+' '+respContacts.dataValues.last_name,
	// 							date_entered:new Date(),
	// 							date_modified:new Date(),
	// 							modified_user_id:respContacts.dataValues.id,
	// 							created_by:respContacts.dataValues.id,
	// 							assigned_user_id:respContacts.dataValues.id,
	// 							date_sent_received:new Date(),
	// 							message_id:'',
	// 							type:'out',
	// 							status:'sent',
	// 							// flagged:'',
	// 							// reply_to_status:'',
	// 							//intent:'pick',
	// 							// mailbox_id:'',
	// 							//parent_type:'Contacts',
	// 							parent_id:respContacts.dataValues.id,
	// 							uid:max+1,
	// 							// category_id:''
	// 						};
	// 						respContactEmails = await models.sequelize.emails.create(newContactEmail);
	// 					}
	// 					objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails = respContactEmails.dataValues;
	// 					objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses = respEmailAddresses.dataValues;
	// 				});
	// 			}
	// 		}
	// 	} catch (e) {
	// 		console.log(e);
	// 	}
	// }
	// static async createAndSendPdf(objOpportunity, callback = null) {
	// 	let date = new Date();
	// 	await this.createPdf(objOpportunity, (err, res, file) => {
	// 		if (res) {
	// 			loggerPdf.info('Archivo pdf creado: ' + date.toString(), {file:res});
	// 			this.sendMail(1, objOpportunity,file, (err, info) => {
	// 				if (err) {
	// 					loggerEmail.info('Error al enviar el correo: ' + date.toString(), {error:err, info:info});
	// 				} {
	// 					loggerEmail.info('Correo enviado: ' + date.toString(), {file:file, info:info});
	//
	// 					if (typeof callback == 'function') {
	// 						callback(err, file, info);
	// 					}
	// 				}
	// 			})
	// 		} else {
	// 			loggerPdf.info('Hubo un error al crear el archivo pdf: ' + date.toString(), {error:err, respuesta:res});
	// 		}
	// 	})
	// }

	// static async setContent(content, objOpportunity) {
	// 	if (content) {
	// 		content = content.replaceAll('@numTerrenos','1');
	// 		content = content.replaceAll('@superficie',parseInt(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_superficie_c+""));
	// 		content = content.replaceAll('@unidadIndustrial',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.unidad_industrial_c);
	// 		content = content.replaceAll('@manzano',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.manzano_c);
	// 		content = content.replaceAll('@lote',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.ubicacion_c);
	// 		content = content.replaceAll('@frente',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.frente_metros_c);
	// 		content = content.replaceAll('@fondo',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.fondo_metros_c);
	// 		content = content.replaceAll('@product','terreno pilat');
	// 		content = content.replaceAll('@ucProduct','TERRENO PILAT');
	// 		content = content.replaceAll('@client',objOpportunity.opportunityAosQuotes.name);
	//
	// 		content = content.replaceAll('@precioMetroCuadrado',formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c));
	// 		content = content.replaceAll('@precioTotal',formatNumber(objOpportunity.opportunityAosQuotes.total_amount));
	// 		content = content.replaceAll('@cuotaInicial',formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c));
	// 		content = content.replaceAll('@saldo',formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.saldo_c));
	//
	// 		if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.tipo_pago_c == 'PLAZOS') {
	// 			if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.term_years_c == '5') {
	// 				content = content.replaceAll('@precioMetroCuadradoPlazoCinco',formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c));
	// 				content = content.replaceAll('@cuotaInicialPlazoCinco',fomatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c));
	// 				content = content.replaceAll('@cuotaMensualPlazoCinco',formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuota_mensual_c));
	// 				content = content.replaceAll('@precioTotalPlazoCinco',formatNumber(objOpportunity.opportunityAosQuotes.total_amount));
	// 			} else if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.term_years_c == '10') {
	// 				content = content.replaceAll('@precioMetroCuadradoPlazoDiez',formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c));
	// 				content = content.replaceAll('@cuotaInicialPlazoDiez',formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c));
	// 				content = content.replaceAll('@cuotaMensualPlazoDiez',formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuota_mensual_c));
	// 				content = content.replaceAll('@precioTotalPlazoDiez',formatNumber(objOpportunity.opportunityAosQuotes.total_amount));
	// 			}
	// 		}
	// 	}
	// 	return content
	// }

	// static async createPdf(objOpportunity, callback = null) {
	// 	try {
	// 		let id = objOpportunity.id;
	// 		let localDir = await path.join(__dirname, '../../../../public/pilatsrl/pdfs/quotes/');
	// 		let localDirTemplatesQuotes = await path.join(__dirname, '../../../../public/pilatsrl/templates/quotes/');
	// 		let localTemplatesQuote;
	// 		if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.tipo_pago_c == 'PLAZOS') {
	// 			if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.term_years_c == '5') {
	// 				localTemplatesQuote = 'cotizacion_plazo_cinco.html';
	// 			} else if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.term_years_c == '10') {
	// 				localTemplatesQuote = 'cotizacion_plazo_diez.html';
	// 			}
	// 		} else if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.tipo_pago_c == 'CONTADO') {
	// 			localTemplatesQuote = 'cotizacion_contado.html';
	// 		} else if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.tipo_pago_c == 'BANCO') {
	// 			localTemplatesQuote = 'cotizacion_banco.html';
	// 		}
	// 		let files = await fs.readdirSync(localDir);
	// 		let file = files.find(param => param == 'quote_'+id+'.pdf');
	//
	// 		let respHtmlPdf, content;
	// 		if (file) {
	// 			await fs.unlinkSync(localDir+file);
	// 			file = null;
	// 		}
	//
	// 		if (!file) {
	// 			content = await fs.readFileSync(localDirTemplatesQuotes+localTemplatesQuote).toString();
	// 			content = await this.setContent(content,objOpportunity);
	// 			respHtmlPdf = await htmlPdf.createPdf(content,localDir+'quote_'+id+'.pdf', callback);
	// 			file = 'quote_'+id+'.pdf';
	// 		}
	// 		return [localDir,file]
	// 	} catch (e) {
	// 		console.log(e)
	// 	}
	// }

	// static async sendMail(idCredential, objOpportunity, dirFile, callback = null) {
	// 	let resp;
	// 	let subDirs = dirFile.split('/');
	// 	let file = subDirs[subDirs.length-1];
	// 	if (objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel) {
	// 		if (file && objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.email_address) {
	// 			let respCredential = await models.sequelize.pilatMails.findOne({where:{id:idCredential}});
	// 			let credential = respCredential.dataValues;
	// 			credential.mai_text = await this.setContent(credential.mai_text, objOpportunity);
	// 			credential.mai_html = await this.setContent(credential.mai_html, objOpportunity);
	// 			credential.mai_subject = await this.setContent(credential.mai_subject, objOpportunity);
	// 			if (credential) {
	// 				let mailOptions = {
	// 					from: '"' + credential.mai_user_account + '" <' + credential.mai_user_account + '>', // sender address
	// 					html: credential.mai_html,
	// 					to: objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.email_address,
	// 					cc: credential.mai_cc,
	// 					bcc: credential.mai_bcc,
	// 					attachments: [
	// 						{
	// 							filename: file,
	// 							path: dirFile
	// 						}
	// 					],
	// 					subject: credential.mai_subject,
	// 					text: credential.mai_text
	// 				};
	//
	// 				resp = nodeMailer.sendMail(mailOptions,credential, callback);
	// 			}
	// 		}
	// 	}
	// 	return resp;
	// }

	static async deleteOpportunity(id) {
		try {
			let objOpportunity;
			if(sql) {

				let opportunities = await models.sequelize.opportunities.findOne({ where: { id: util.Char(id) } });
				if (opportunities) await models.sequelize.opportunities.destroy({where: { id: util.Char(id) }});

				let opportunityOpportunitiesCstm = await models.sequelize.opportunitiesCstm.findOne({ where: { id_c: util.Char(id) } });
				if (opportunityOpportunitiesCstm) await models.sequelize.opportunitiesCstm.destroy({where: { id_c: util.Char(id) }});

				let opportunityOpportunitiesAudit = await models.sequelize.opportunitiesAudit.findOne({ where: { parent_id: util.Char(id) } });
				if (opportunityOpportunitiesAudit) await models.sequelize.opportunitiesAudit.destroy({where: { parent_id: util.Char(id) }});

				let opportunitiesContacts = await models.sequelize.opportunitiesContacts.findOne({ where: { opportunity_id: util.Char(id) } });
				if (opportunitiesContacts) await models.sequelize.opportunitiesContacts.destroy({where: { opportunity_id: util.Char(id) }});

				let opportunitySugarfeed = await models.sequelize.sugarfeed.findOne({ where: { related_id: util.Char(id) } });
				if (opportunitySugarfeed) await models.sequelize.sugarfeed.destroy({where: { related_id: util.Char(id) }});

				let opportunityAodIndexevent = await models.sequelize.aodIndexevent.findOne({ where: { record_id: util.Char(id) } });
				if (opportunityAodIndexevent) await models.sequelize.aodIndexevent.destroy({where: { record_id: util.Char(id) }});

				let opportunityTracker = await models.sequelize.tracker.findOne({ where: { item_id: util.Char(id) } });
				if (opportunityTracker) await models.sequelize.tracker.destroy({where: { item_id: util.Char(id) }});

				objOpportunity = opportunities;
				objOpportunity.opportunityOpportunitiesCstm = opportunityOpportunitiesCstm;
				objOpportunity.opportunityOpportunitiesAudit = opportunityOpportunitiesAudit;
				objOpportunity.opportunitiesContacts = opportunitiesContacts;
				objOpportunity.opportunitySugarfeed = opportunitySugarfeed;
				objOpportunity.opportunityAodIndexevent = opportunityAodIndexevent;
				objOpportunity.opportunityTracker = opportunityTracker;

			} else {
				objOpportunity = await models.mongoose.opportunities.deleteOne({id:util.Char(id)});
			}
			return objOpportunity;
		} catch (error) {
			throw error;
		}
	}

	static async setSearchPanes(body, query, dtColumns) {
		try {
			let { root } = query;
			let { where } = query;
			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
			// let aData = await models.sequelize.opportunities.findAll({where:objWhere});
			let rawAttributes = models.sequelize.opportunities.rawAttributes;
			let aColumns = Object.values(rawAttributes);
			let fields = Object.keys(rawAttributes);
			let search = body['search[value]'];
			let searchPanes = {};
			let searches = [];
			let dtOptions = {};
			let userOpportunities = await models.sequelize.opportunities.findAll({
				where: objWhere
			});

			if (dtColumns) {
				for (let i = 0 ; i < fields.length; i++ ) {
					let field = fields[i];
					dtOptions[`${root}.${field}`] = [];
				}

				let dtValues = [];
				for (let k = 0 ; k < userOpportunities.length ; k++) {
					let userOpportunity = userOpportunities[k].dataValues;
					let aUserOpportunityValues = Object.values(userOpportunity);
					let aUserOpportunityFields = Object.keys(userOpportunity);
					for (let n = 0 ; n < aUserOpportunityValues.length ; n++) {
						let userOpportunityField = aUserOpportunityFields[n];
						let userOpportunityValue = aUserOpportunityValues[n];
						if (!dtValues.find(param => param.value == userOpportunityValue && param.field == userOpportunityField)) {
							dtValues.push({value:userOpportunityValue, count:1, label:userOpportunityValue, field:userOpportunityField});
						} else {
							for (let m = 0 ; m < dtValues.length ; m++) {
								let dtValue = dtValues[m];
								if (dtValue.value == userOpportunityValue && dtValue.field == userOpportunityField) {
									dtValues[m].count++;
								}
							}
						}
					}
				}

				for (let l = 0 ; l < dtValues.length ; l++) {
					let dtValue = dtValues[l];
					let [optNumber,optDate] = util.setDataValueTypes(dtValue.value);
					dtOptions[`${root}.${dtValue.field}`].push({
						label:optDate && optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
						total:dtValue.count,
						value:optDate && optDate.getDate() ? optDate : dtValue.value,
						count:dtValue.count
					});
				}

				for (let j = 0 ; j < fields.length; j++ ) {
					for (let z = 0 ; z < fields.length; z++ ) {
						let field = fields[z];
						if (root) {
							if (body[`searchPanes[${root}.${field}][${j}]`]) {
								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
							}
						}
					}
				}
			}
			dtOptions['tableLength'] = 100;
			searchPanes['options'] = dtOptions;
			return [searchPanes, searches, userOpportunities];
		} catch (e) {
			console.log(e);
		}
	}

	static async getAosQuotesNextNumber() {
		let maxNumber = await models.sequelize.aosQuotes.max('number');
		return maxNumber+1;
	}
}

// function formatNumber (num, decimals = 2) {
// 	let stringFloat = num + "";
// 	let arraySplitFloat = stringFloat.split(".");
// 	let decimalsValue = "0";
// 	if (arraySplitFloat.length > 1) {
// 		decimalsValue = arraySplitFloat[1].slice(0, decimals);
// 	}
// 	let integerValue = arraySplitFloat[0];
// 	let arrayFullStringValue = [integerValue, decimalsValue];
// 	let FullStringValue = arrayFullStringValue.join(".");
// 	let floatFullValue = parseFloat(FullStringValue);
// 	let formatFloatFullValue = new Intl.NumberFormat("de-DE", { minimumFractionDigits: decimals }).format(floatFullValue);
// 	return solveFeatureFormatFloatNumberSpanish(formatFloatFullValue);
// }
//
// function solveFeatureFormatFloatNumberSpanish(num) {
// 	let indexComma = num.indexOf(',');
// 	let indexPoint = num.indexOf('.');
// 	let aParts = num.split('.');
// 	let decimal = aParts[aParts.length-1];
// 	let entero = aParts[0];
// 	let parts;
// 	if (indexComma < indexPoint) {
// 		parts = entero.split(',');
// 		num = parts.join('.');
// 		num = num +','+decimal;
// 	}
// 	return num;
// }

//<es-section>
module.exports = OpportunityService;
//</es-section>
