import fs from "fs";

/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Nov 12 2020 02:25:36 GMT-0400 (Bolivia Time)
 * Time: 2:25:36
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Nov 12 2020 02:25:36 GMT-0400 (Bolivia Time)
 * Last time updated: 2:25:36
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../utils/Prototipes');
const helpers = require('../../../utils/helpers');
const models = require('../../../api/relations');
import configJson from '../../../config/config';
const sql = configJson.sql;
const Util = require('../../../utils/Utils');
const util = new Util();
const htmlPdf = require('../../../modules/html-pdf');
const nodeMailer = require("../../../modules/nodemailer");
const {loggerEmail, loggerPdf} = require("../../../modules/winston");
const moment = require('moment');
const path = require('path');
const { Op } = require("sequelize");

let DIC_CURRENCIES = '6007aadcb4fefa3640d86b7c';
let DIC_ACCTION_RELATE = '6048f227c7cf6507f63bfaae';
let DIC_ESTADOS_EMAIL = '603dd76636a4741d6003d9e1';
let DIC_MODULOS = '60088baf3682cc5720f557b4';
let DIC_ACCION_ACTUALIZAR = '6047e3bc65effb63aa94676b';
let DIC_ACCION_CREACION = '6047ddfe65effb63aa946769';
let DIC_RELACION = '6047dda665effb63aa946768';
let DIC_ACCION_BD = '6047dd7d65effb63aa946767';

let pilatParams;

// let parActionCreate = '';
// const parActionUpdate = '';
//
// const parModuleLeads = '';
// const parModuleOpportunities = '';
// const parModuleCalls = '';
// const parModuleEmails = '';
// const parModuleAoSQuotes = '';
// const parModuleContacts = '';
// const parModuleAccounts = '';
//
// const parDescriptionLeadsCreated = '';
// const parDescriptionOpportunitiesCreated = '';
// const parDescriptionCallsCreated = '';
// const parDescriptionEmailsCreated = '';
// const parDescriptionAoSQuotesCreated = '';
// const parDescriptionContactsCreated = '';
// const parDescriptionAccountsCreated = '';
//
// const parDescriptionLeadsUpdated = '';
// const parDescriptionOpportunitiesUpdated = '';
// const parDescriptionCallsUpdated = '';
// const parDescriptionEmailsUpdated = '';
// const parDescriptionAoSQuotesUpdated = '';
// const parDescriptionContactsUpdated = '';
// const parDescriptionAccountsUpdated = '';

class CrmService {

	static async setParams(pilatDictionaries = []) {
		let toReturn = {};
		if (pilatDictionaries.length) {
			let params = await models.sequelize.pilatParams.findAll({where:{par_dictionary_id:pilatDictionaries}});
			if (params && params.length) {

				toReturn.parCurrencies = [];

				for (let i = 0 ; i < params.length ; i++) {

					let param = params[i].dataValues;

					switch (param.id) {

						case 114: toReturn.parCreate = param; break;
						case 115: toReturn.parUpdate = param; break;

						case 27: toReturn.parLeads = param; break;
						case 31: toReturn.parOpportunities = param; break;
						case 30: toReturn.parCalls = param; break;
						case 33: toReturn.parEmails = param; break;
						case 32: toReturn.parAOS_Quotes = param; break;
						case 28: toReturn.parContacts = param; break;
						case 29: toReturn.parAccounts = param; break;

						case 41: toReturn.parCurrencyBob = param; break;
						case 42: toReturn.parCurrencyUsd = param; break;

						case 123: toReturn.parDescriptionLeadsCreated = param; break;
						case 124: toReturn.parDescriptionOpportunitiesCreated = param; break;
						case 125: toReturn.parDescriptionCallsCreated = param; break;
						case 126: toReturn.parDescriptionEmailsCreated = param; break;
						case 127: toReturn.parDescriptionAoSQuotesCreated = param; break;
						case 128: toReturn.parDescriptionContactsCreated = param; break;
						case 129: toReturn.parDescriptionAccountsCreated = param; break;
						case 130: toReturn.parDescriptionLeadsUpdated = param; break;
						case 131: toReturn.parDescriptionOpportunitiesUpdated = param; break;
						case 132: toReturn.parDescriptionCallsUpdated = param; break;
						case 133: toReturn.parDescriptionEmailsUpdated = param; break;
						case 134: toReturn.parDescriptionAoSQuotesUpdated = param; break;
						case 135: toReturn.parDescriptionContactsUpdated = param; break;
						case 136: toReturn.parDescriptionAccountsUpdated = param; break;

						case 116: toReturn.parRelationCallsLeads = param; break;
						case 117: toReturn.parRelationMeetingsLeads = param; break;
						case 118: toReturn.parRelationCallsContacts = param; break;
						case 119: toReturn.parRelationCallsUsers = param; break;
						case 120: toReturn.parRelationAccountsContacts = param; break;
						case 121: toReturn.parRelationAccountsOpportunities = param; break;
						case 122: toReturn.parRelationContactsUsers = param; break;
						case 137: toReturn.parRelationEmailAddrBeanRel = param; break;
						case 138: toReturn.parRelationOpportunitiesContacts = param; break;
						case 141: toReturn.parRelationMeetingsContacts = param; break;

						case 142: toReturn.parDescriptionRelateCallsLeads = param; break;
						case 143: toReturn.parDescriptionRelateMeetingsLeads = param; break;
						case 144: toReturn.parDescriptionRelateCallsContacts = param; break;
						case 145: toReturn.parDescriptionRelateCallsUsers = param; break;
						case 146: toReturn.parDescriptionRelateAccountsContacts = param; break;
						case 147: toReturn.parDescriptionRelateAccountsOpportunities = param; break;
						case 148: toReturn.parDescriptionRelateContactsUsers = param; break;
						case 149: toReturn.parDescriptionRelateEmailAddrBeanRel = param; break;
						case 150: toReturn.parDescriptionRelateOpportunitiesContacts = param; break;
						case 151: toReturn.parDescriptionRelateMeetingsContacts = param; break;

						case 108: toReturn.parEstadoEmailEnviado = param; break;

					}

					switch (param.par_dictionary_id) {

						case DIC_CURRENCIES: toReturn.parCurrencies.push(param);break;

					}
				}
			}
		}

		return toReturn;
	}

	static async setEmailOpportunity(objOpportunity, respContacts) {
		try {
			if (objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel) {

				await CrmService.createAndSendPdfOpportunity(objOpportunity, async (err, file, info) => {
					if (err) {
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.invalid_email = 1;
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.confirm_opt_in_fail_date = new Date();
					} else {
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.invalid_email = 0;
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.confirm_opt_in_date = new Date();
						objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.confirm_opt_in_sent_date = new Date();
					}
					let id = objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.id;
					let respEmailAddresses = await models.sequelize.emailAddresses.update(objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses, {where:{id:id}});
					let respOldContactEmails = await models.sequelize.emails.findOne({where:{parent_id:objOpportunity.opportunityOpportunitiesContacts.contact_id}});
					let respContactEmails;

					if (respOldContactEmails) {
						let updateContactEmail = respOldContactEmails.dataValues;
						updateContactEmail.date_modified = new Date();
						updateContactEmail.modified_user_id = respContacts.dataValues.assigned_user_id;
						let respEmails = await models.sequelize.emails.update(updateContactEmail,{where:{parent_id:objOpportunity.opportunityOpportunitiesContacts.contact_id}});
						respContactEmails = await models.sequelize.emails.findOne({where:{parent_id:objOpportunity.opportunityOpportunitiesContacts.contact_id}});
					} else {
						let maxUid = await models.sequelize.emails.max('uid');
						let max = parseInt(maxUid);
						let newContactEmail = {
							id:models.sequelize.objectId().toString(),
							name:respContacts.dataValues.first_name+' '+respContacts.dataValues.last_name,
							date_entered:new Date(),
							date_modified:new Date(),
							modified_user_id:objOpportunity.assigned_user_id,
							created_by:objOpportunity.assigned_user_id,

							// arreglo  verificar

							assigned_user_id:objOpportunity.assigned_user_id,

							// -----

							date_sent_received:new Date(),
							message_id:'',
							type:'out',
							status:err ? '' : 'sent',
							// flagged:'',
							// reply_to_status:'',
							//intent:'pick',
							// mailbox_id:'',
							parent_type:'Contacts',
							parent_id:respContacts.dataValues.id,
							uid:max+1,
							// category_id:''
						};
						respContactEmails = await models.sequelize.emails.create(newContactEmail);
					}
					objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails = respContactEmails.dataValues;
					objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses = respEmailAddresses.dataValues;
				});
			}
		} catch (e) {
			console.log(e);
		}
	}

	static async createAndSendPdfOpportunity(objOpportunity, callback = null) {
		pilatParams = await this.setParams([
			DIC_ACCION_BD,
			DIC_RELACION,
			DIC_ACCION_CREACION,
			DIC_ACCION_ACTUALIZAR,
			DIC_MODULOS,
			DIC_ESTADOS_EMAIL,
			DIC_ACCTION_RELATE,
			DIC_CURRENCIES
	]);
		let date = new Date();
		let hasEmailHistory;

		await this.createPdfOpportunity(objOpportunity, (err, res, file) => {
			if (res) {
				loggerPdf.info('Archivo pdf creado: ' + date.toString(), {file:res});
				if (objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails && objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmails.status === pilatParams.parEstadoEmailEnviado.par_cod) {
					if (typeof callback == 'function') {
						callback(null, file, null);
					}
				} else {
					this.sendMailOpportunity(1, objOpportunity,file, (err, info) => {
						if (err) {
							loggerEmail.info('Error al enviar el correo: ' + date.toString(), {error:err, info:info});
						} {
							loggerEmail.info('Correo enviado: ' + date.toString(), {file:file, info:info});

							if (typeof callback == 'function') {
								callback(err, file, info);
							}
						}
					})
				}
			} else {
				loggerPdf.info('Hubo un error al crear el archivo pdf: ' + date.toString(), {error:err, respuesta:res});
			}
		})

	}

	static setContent(content, objOpportunity = null, objLead = null) {
		if (content) {
			if (objLead) {
				let dateStart = objLead.leadCallsLeads && objLead.leadCallsLeads.callLeadCalls ? new Date(objLead.leadCallsLeads.callLeadCalls.date_start) : null;
				let dateStartStr = `${dateStart.getDate()}-${dateStart.getMonth()+1}-${dateStart.getFullYear()} ${dateStart.getHours()}:${dateStart.getMinutes()}`;
				content = content.replaceAll('@leadFirstName',objLead.first_name ? objLead.first_name : '');
				content = content.replaceAll('@leadLastName',objLead.last_name ? objLead.last_name : '');
				content = content.replaceAll('@userName',objLead.leadUsersAssignedUser && objLead.leadUsersAssignedUser.user_name ? objLead.leadUsersAssignedUser.user_name : '');
				content = content.replaceAll('@userFirstName',objLead.leadUsersAssignedUser && objLead.leadUsersAssignedUser.user_name ? objLead.leadUsersAssignedUser.user_name : '');
				content = content.replaceAll('@userLastName',objLead.leadUsersAssignedUser && objLead.leadUsersAssignedUser.user_name ? objLead.leadUsersAssignedUser.user_name : '');
				content = content.replaceAll('@userCargo', objLead.leadUsersAssignedUser && objLead.leadUsersAssignedUser.title ? objLead.leadUsersAssignedUser.title : '');
				content = content.replaceAll('@dateStart',dateStartStr ? dateStartStr : '');
			}
			if (objOpportunity) {
				let currencyId = objOpportunity.currency_id == '-99' ? 'USD' : objOpportunity.currency_id;
				let currency = pilatParams.parCurrencies.find(param => param.par_cod == currencyId);
				content = content.replaceAll('@numTerrenos','1');
				content = content.replaceAll('@superficie',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_superficie_c ? parseInt(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_superficie_c+"") : '');
				content = content.replaceAll('@unidadIndustrial',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.unidad_industrial_c ? objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.unidad_industrial_c : '');
				content = content.replaceAll('@manzano',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.manzano_c ? objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.manzano_c : '');
				content = content.replaceAll('@lote',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.ubicacion_c ? objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.ubicacion_c : '');
				content = content.replaceAll('@frente',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.frente_metros_c ? objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.frente_metros_c : '');
				content = content.replaceAll('@fondo',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.fondo_metros_c ? objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.fondo_metros_c : '');
				content = content.replaceAll('@product','terreno pilat');
				content = content.replaceAll('@ucProduct','TERRENO PILAT');
				content = content.replaceAll('@currency',currency && currency.par_abbr ? currency.par_abbr : '');
				content = content.replaceAll('@client',objOpportunity.opportunityAosQuotes.name ? objOpportunity.opportunityAosQuotes.name : '');
				content = content.replaceAll('@urlProduct',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.link_terreno_c ? objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.link_terreno_c : '');
				content = content.replaceAll('@userFirstName', objOpportunity.opportunityUsersAssignedUser && objOpportunity.opportunityUsersAssignedUser.first_name ? objOpportunity.opportunityUsersAssignedUser.first_name : '');
				content = content.replaceAll('@userLastName', objOpportunity.opportunityUsersAssignedUser && objOpportunity.opportunityUsersAssignedUser.last_name ? objOpportunity.opportunityUsersAssignedUser.last_name : '');
				content = content.replaceAll('@userCargo', objOpportunity.opportunityUsersAssignedUser && objOpportunity.opportunityUsersAssignedUser.title ? objOpportunity.opportunityUsersAssignedUser.title : '');
				content = content.replaceAll('@years',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.term_years_c ? objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.term_years_c : '');
				content = content.replaceAll('@precioMetroCuadradoYears',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_years_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_years_c) : '');
				content = content.replaceAll('@precioCuotaInicialPrefereenciaYears',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuotaini_preferencia_years_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuotaini_preferencia_years_c) : '');
				content = content.replaceAll('@precioCuotaMensualPreferencialYears',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuotamen_preferencia_years_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuotamen_preferencia_years_c) : '');
				content = content.replaceAll('@precioTotalYears',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_total_years_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_total_years_c) : '');
				content = content.replaceAll('@cuotaMensual',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuota_mensual_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuota_mensual_c) : '');
				content = content.replaceAll('@precioMetroCuadrado',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c) : '');
				content = content.replaceAll('@precioTotal',objOpportunity.opportunityAosQuotes.total_amount ? this.formatNumber(objOpportunity.opportunityAosQuotes.total_amount) : '');
				content = content.replaceAll('@cuotaInicial',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c) : '');
				content = content.replaceAll('@saldo',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.saldo_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.saldo_c) : '');
				content = content.replaceAll('@precioMetroCuadradoPlazoCinco',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.precio_mcuadrado_c) : '');
				content = content.replaceAll('@cuotaInicialPlazoCinco',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.lbl_cuotainicial_c) : '');
				content = content.replaceAll('@cuotaMensualPlazoCinco',objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuota_mensual_c ? this.formatNumber(objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.cuota_mensual_c) : '');
				content = content.replaceAll('@precioTotalPlazoCinco',objOpportunity.opportunityAosQuotes.total_amount ? this.formatNumber(objOpportunity.opportunityAosQuotes.total_amount) : '');
			}
		}
		return content
	}

	static async createPdfOpportunity(objOpportunity, callback = null) {
		try {
			let id = objOpportunity.id;
			let localDir = await path.join(__dirname, '../../../../public/pilatsrl/pdfs/quotes/');
			let localDirTemplatesQuotes = await path.join(__dirname, '../../../../public/pilatsrl/templates/quotes/');
			let localTemplatesQuote, files, file;
			if (objOpportunity.opportunityAosQuotes) {
				if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.tipo_pago_c == 'PLAZOS') {
					localTemplatesQuote = 'cotizacion_plazos.html';
				} else if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.tipo_pago_c == 'CONTADO') {
					localTemplatesQuote = 'cotizacion_contado.html';
				} else if (objOpportunity.opportunityAosQuotes.aoQuoteAosQuotesCstm.tipo_pago_c == 'BANCO') {
					localTemplatesQuote = 'cotizacion_banco.html';
				}
				files = await fs.readdirSync(localDir);
				file = files.find(param => param == 'quote_'+id+'.pdf');

				let respHtmlPdf, content;
				if (file) {
					await fs.unlinkSync(localDir+file);
					file = null;
				}

				if (!file) {
					content = await fs.readFileSync(localDirTemplatesQuotes+localTemplatesQuote).toString();
					content = this.setContent(content,objOpportunity);
					respHtmlPdf = await htmlPdf.createPdf(content,localDir+'quote_'+id+'.pdf', callback);
					file = 'quote_'+id+'.pdf';
				}
			}
			return [localDir,file]
		} catch (e) {
			console.log(e)
		}
	}

	static async sendMailOpportunity(idCredential, objOpportunity, dirFile, callback = null) {
		let resp;
		let subDirs = dirFile.split('/');
		let file = subDirs[subDirs.length-1];
		let respUser = await models.sequelize.users.findOne({
			where:{id:objOpportunity.assigned_user_id},
			include: [
				{ model:models.sequelize.emails, as:'userEmails' },
				{
					model:models.sequelize.emailAddrBeanRel, as:'userEmailAddrBeanRel',
					include: {
						model:models.sequelize.emailAddresses, as:'emailAddrBeanRelEmailAddresses'
					}
				}
			]
		});
		let user = respUser && respUser.dataValues ? respUser.dataValues : null;
		if (objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel) {
			if (file && objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.email_address) {
				let respCredential = await models.sequelize.pilatMails.findOne({where:{id:idCredential}});
				let credential = respCredential.dataValues;
				credential.mai_text = this.setContent(credential.mai_text, objOpportunity);
				credential.mai_html = this.setContent(credential.mai_html, objOpportunity);
				credential.mai_subject = this.setContent(credential.mai_subject, objOpportunity);
				if (credential) {
					let emailUser = user && user.userEmailAddrBeanRel && user.userEmailAddrBeanRel.emailAddrBeanRelEmailAddresses ? user.userEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.email_address : null;
					let mailOptions = {
						from: '"' + credential.mai_user_account + '" <' + credential.mai_user_account + '>', // sender address
						html: credential.mai_html,
						to: objOpportunity.opportunityOpportunitiesContacts.opportunityContactContacts.contactEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.email_address,
						cc: emailUser ? emailUser : credential.mai_cc,
						bcc: credential.mai_bcc,
						attachments: [
							{
								filename: file,
								path: dirFile
							}
						],
						subject: credential.mai_subject,
						text: credential.mai_text
					};

					resp = nodeMailer.sendMail(mailOptions,credential, callback);
				}
			}
		}
		return resp;
	}

	static async sendMailRememberCall(idCredential, lead, callback = null) {
		let resp, toSend = true;
		// if (lead.leadUsersAssignedUser.userEmails && lead.leadUsersAssignedUser.userEmails.status === 'sent') {
		// 	toSend = false;
		// }
		if (toSend) {
			if (lead.leadUsersAssignedUser.userEmailAddrBeanRel && lead.leadUsersAssignedUser.userEmailAddrBeanRel.emailAddrBeanRelEmailAddresses && lead.leadUsersAssignedUser.userEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.email_address) {
				let userEmail = lead.leadUsersAssignedUser.userEmailAddrBeanRel.emailAddrBeanRelEmailAddresses.email_address;
				let respCredential = await models.sequelize.pilatMails.findOne({where:{_id:idCredential}});
				let credential = respCredential && respCredential.dataValues ? respCredential.dataValues : null;
				if (credential) {
					credential.mai_text = this.setContent(credential.mai_text, null,lead);
					credential.mai_html = this.setContent(credential.mai_html, null, lead);
					credential.mai_subject = this.setContent(credential.mai_subject, null, lead);
					if (credential) {
						let mailOptions = {
							from: '"' + credential.mai_user_account + '" <' + credential.mai_user_account + '>', // sender address
							html: credential.mai_html,
							to: userEmail,
							cc: credential.mai_cc,
							bcc: credential.mai_bcc,
							subject: credential.mai_subject,
							text: credential.mai_text
						};
						resp = nodeMailer.sendMail(mailOptions,credential, callback);
					}
				}
			}
		}
		return resp;
	}

	static formatNumber (num, decimals = 2) {
		let stringFloat = num + "";
		let arraySplitFloat = stringFloat.split(".");
		let decimalsValue = "0";
		if (arraySplitFloat.length > 1) {
			decimalsValue = arraySplitFloat[1].slice(0, decimals);
		}
		let integerValue = arraySplitFloat[0];
		let arrayFullStringValue = [integerValue, decimalsValue];
		let FullStringValue = arrayFullStringValue.join(".");
		let floatFullValue = parseFloat(FullStringValue);
		let formatFloatFullValue = new Intl.NumberFormat("de-DE", { minimumFractionDigits: decimals }).format(floatFullValue);
		return this.solveFeatureFormatFloatNumberSpanish(formatFloatFullValue);
	}

	static solveFeatureFormatFloatNumberSpanish(num) {
		let indexComma = num.indexOf(',');
		let indexPoint = num.indexOf('.');
		let aParts = num.split('.');
		let decimal = aParts[aParts.length-1];
		let entero = aParts[0];
		let parts;
		if (indexComma < indexPoint) {
			parts = entero.split(',');
			num = parts.join('.');
			num = num +','+decimal;
		}
		return num;
	}

	static setNumberToSave(amount) {
		if (amount) {
			amount = amount+'';
			if (typeof amount == 'string') {
				let numParts = amount.split(',');
				let integer = numParts[0] ? numParts[0] : '';
				let decimal = numParts[1] ? numParts[1] : '';
				integer = integer ? integer.replaceAll('.','') : integer;
				decimal = decimal ? decimal.replaceAll(',','') : decimal;
				let numToReturn = integer;
				return numToReturn;
			}
		}
		return amount;
	}
	static minTwoDigits(n) {
		return (n < 10 ? '0' : '') + n;
	}

	static async setPilatLog(action,module,relate,sourceId,moduleId,userId) {
		let pilatParams = await this.setParams([
			'6047dd7d65effb63aa946767',
			'6047dda665effb63aa946768',
			'6047ddfe65effb63aa946769',
			'6047e3bc65effb63aa94676b',
			'60088baf3682cc5720f557b4',
			'603dd76636a4741d6003d9e1',
			'6048f227c7cf6507f63bfaae'
		]);

		let setDescription;
		let maxPilatLogId = await models.sequelize.pilatLogs.max('id');
		let nextId = maxPilatLogId ? maxPilatLogId+1 : 1;
		let pilatLog, respPilatLog, oldPilatLog, respOldPilatLog;
		let respUser = await models.sequelize.users.findOne({where:{id:userId}});
		let user = respUser && respUser.dataValues ? respUser.dataValues : null;
		let descriptionFrom, descriptionTo, relation;
		let parAction, parModule;

		switch (action) {
			case pilatParams.parCreate.par_cod.toLowerCase():
				parAction = pilatParams.parCreate;
				switch (module.toLowerCase()) {
					case pilatParams.parLeads.par_cod.toLowerCase():
						parModule = pilatParams.parLeads;
						setDescription = pilatParams.parDescriptionLeadsCreated.par_description;
						break;
					case pilatParams.parOpportunities.par_cod.toLowerCase():
						parModule = pilatParams.parOpportunities;
						setDescription = pilatParams.parDescriptionOpportunitiesCreated.par_description;
						break;
					case pilatParams.parAOS_Quotes.par_cod.toLowerCase():
						parModule = pilatParams.parAOS_Quotes;
						setDescription = pilatParams.parDescriptionAoSQuotesCreated.par_description;
						break;
					case pilatParams.parContacts.par_cod.toLowerCase():
						parModule = pilatParams.parContacts;
						setDescription = pilatParams.parDescriptionContactsCreated.par_description;
						break;
					case pilatParams.parAccounts.par_cod.toLowerCase():
						parModule = pilatParams.parAccounts;
						setDescription = pilatParams.parDescriptionAccountsCreated.par_description;
						break;
					case pilatParams.parCalls.par_cod.toLowerCase():
						parModule = pilatParams.parCalls;
						setDescription = pilatParams.parDescriptionCallsCreated.par_description;
						break;
					case pilatParams.parEmails.par_cod.toLowerCase():
						parModule = pilatParams.parEmails;
						setDescription = pilatParams.parDescriptionEmailsCreated.par_description;
						break;
				}
				break;
			case pilatParams.parUpdate.par_cod.toLowerCase():
				parAction = pilatParams.parUpdate;
				switch (module.toLowerCase()) {
					case pilatParams.parLeads.par_cod.toLowerCase():
						parModule = pilatParams.parLeads;
						setDescription = pilatParams.parDescriptionLeadsUpdated.par_description;
						break;
					case pilatParams.parOpportunities.par_cod.toLowerCase():
						parModule = pilatParams.parOpportunities;
						setDescription = pilatParams.parDescriptionOpportunitiesUpdated.par_description;
						break;
					case pilatParams.parAOS_Quotes.par_cod.toLowerCase():
						parModule = pilatParams.parAOS_Quotes;
						setDescription = pilatParams.parDescriptionAoSQuotesUpdated.par_description;
						break;
					case pilatParams.parContacts.par_cod.toLowerCase():
						parModule = pilatParams.parContacts;
						setDescription = pilatParams.parDescriptionContactsUpdated.par_description;
						break;
					case pilatParams.parAccounts.par_cod.toLowerCase():
						parModule = pilatParams.parAccounts;
						setDescription = pilatParams.parDescriptionAccountsUpdated.par_description;
						break;
					case pilatParams.parCalls.par_cod.toLowerCase():
						parModule = pilatParams.parCalls;
						setDescription = pilatParams.parDescriptionCallsUpdated.par_description;
						break;
					case pilatParams.parEmails.par_cod.toLowerCase():
						parModule = pilatParams.parEmails;
						setDescription = pilatParams.parDescriptionEmailsUpdated.par_description;
						break;
				}
				break;
		}

		let descriptionRelated = '', description = '';

		if (relate && Object.keys(relate).length) {
			relation = relate.description;
			descriptionFrom = relate.from ? relate.from : 'Sin Especificar';
			descriptionTo = relate.to ? relate.to : 'Sin especificar';
			if (relation) {
				switch (relation) {
					case pilatParams.parRelationCallsLeads.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateCallsLeads.par_description.replaceAll('@descriptionCalls',descriptionFrom).replaceAll('@descriptionLeads',descriptionTo); break;
					case pilatParams.parRelationMeetingsLeads.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateMeetingsLeads.par_description.replaceAll('@descriptionMeetings',descriptionFrom).replaceAll('@descriptionLeads',descriptionTo); break;
					case pilatParams.parRelationCallsContacts.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateCallsContacts.par_description.replaceAll('@descriptionCalls',descriptionFrom).replaceAll('@descriptionContacts',descriptionTo); break;
					case pilatParams.parRelationCallsUsers.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateCallsUsers.par_description.replaceAll('@descriptionCalls',descriptionFrom).replaceAll('@descriptionUsers',descriptionTo); break;
					case pilatParams.parRelationAccountsContacts.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateAccountsContacts.par_description.replaceAll('@descriptionAccounts',descriptionFrom).replaceAll('@descriptionContacts',descriptionTo); break;
					case pilatParams.parRelationAccountsOpportunities.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateAccountsOpportunities.par_description.replaceAll('@descriptionAccounts',descriptionFrom).replaceAll('@descriptionOpportunities',descriptionTo); break;
					case pilatParams.parRelationContactsUsers.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateContactsUsers.par_description.replaceAll('@descriptionContacts',descriptionFrom).replaceAll('@descriptionUsers',descriptionTo); break;
					case pilatParams.parRelationEmailAddrBeanRel.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateEmailAddrBeanRel.par_description.replaceAll('@descriptionEmails',descriptionFrom).replaceAll('@descriptionModule',descriptionTo); break;
					case pilatParams.parRelationOpportunitiesContacts.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateOpportunitiesContacts.par_description.replaceAll('@descriptionOpportunities',descriptionFrom).replaceAll('@descriptionContacts',descriptionTo); break;
					case pilatParams.parRelationMeetingsContacts.par_cod.toLowerCase(): descriptionRelated = pilatParams.parDescriptionRelateMeetingsContacts.par_description.replaceAll('@descriptionMeetings',descriptionFrom).replaceAll('@descriptionContacts',descriptionTo); break;
				}
			} else {
				description = typeof relate == 'string' ? relate : '';
			}
		} else {
			description = typeof relate == 'string' ? relate : '';
		}

		if (descriptionRelated) {
			description = description ? description+', '+descriptionRelated : descriptionRelated;
		} else if (setDescription) {
			description = description ? setDescription+', '+description : setDescription;
		}

		respOldPilatLog = await models.sequelize.pilatLogs.findOne({where:{source_id:sourceId}});
		if (respOldPilatLog && respOldPilatLog.dataValues) {
			oldPilatLog = respOldPilatLog && respOldPilatLog.dataValues ? respOldPilatLog.dataValues : null;
			oldPilatLog.action = parAction.par_description;
			oldPilatLog.module = parModule.par_description;
			oldPilatLog.description = description;
			oldPilatLog.user = user.user_name;
			oldPilatLog.module_id = moduleId;
			oldPilatLog.updatedBy = userId;
			oldPilatLog.updatedAt = new Date();
			respPilatLog = await models.sequelize.pilatLogs.update(oldPilatLog,{where:{source_id:sourceId}});
			pilatLog = respPilatLog && respPilatLog.dataValues ? respPilatLog.dataValues : null;
		} else {
			let newPilatLog = {
				_id:models.sequelize.objectId().toString(),
				id: nextId,
				action:parAction.par_description,
				description: description,
				module:parModule.par_description,
				user:user.user_name,
				source_id:sourceId,
				module_id:moduleId,
				createdBy:userId,
				updatedBy:userId,
				createdAt: new Date(),
				updatedAt: new Date()
			};
			respPilatLog = await models.sequelize.pilatLogs.create(newPilatLog);
			pilatLog = respPilatLog && respPilatLog.dataValues ? respPilatLog.dataValues : null;
		}
		return pilatLog;
	}
}

//<es-section>
module.exports = CrmService;
//</es-section>
