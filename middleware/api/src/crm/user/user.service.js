/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Nov 12 2020 02:25:36 GMT-0400 (Bolivia Time)
 * Time: 2:25:36
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Nov 12 2020 02:25:36 GMT-0400 (Bolivia Time)
 * Last time updated: 2:25:36
 *
 * Caution: es-sections will be replaced by script execution
 */

require('../../../../utils/Prototipes');
const helpers = require('../../../../utils/helpers');
const models = require('../../../relations');
import configJson from '../../../../config/config';
const sql = configJson.sql;
const Util = require('../../../../utils/Utils');
const util = new Util();

const { Op } = require("sequelize");
class UserService {

	static async getAllUsers(query) {
		try {
			if(sql) {
				let offset = Object.keys(query).length ? query.offset ? query.offset : query.start ? query.start : query.limit ? 0 : null : null;
				let where = Object.keys(query).length ? query.where ? util.isJson(query.where) ? query.where : JSON.parse(query.where) : null : null;

				let where3,where2,where1;
				if (where && where.where && where.where.where && Object.keys(where.where.where).length) {
            where3 = where.where.where; delete where.where.where;
				}
				if (where && where.where && Object.keys(where.where).length) {
            where2 = where.where; delete where.where;
				}
				if (where && Object.keys(where).length) {
            where1 = where;
				}
				return await models.sequelize.users.findAndCountAll({
					attributes:query.select ? query.select.split(',') : null,
					where: where1,
					limit: query.limit ? parseInt(query.limit) : null,
					offset: offset ? parseInt(offset) : 0,
					order: query.order ? Array.isArray(query.order) ? query.order : JSON.parse(query.order) : [['id','ASC']],
					include: [
						{
							model:models.sequelize.usersCstm, as: 'userUsersCstm',
							where: where2,
						},
					]
				});
			}
		} catch (error) {
			throw error;
		}
	}

	static async getAUser(id, query) {
		try {
			if(sql) {
				return await models.sequelize.users.findOne({
					attributes:query.select ? query.select.split(',') : null,
					where:{id:id},
					include: [
						{model:models.sequelize.usersCstm, as: 'userUsersCstm'},
						{model:models.sequelize.sugarfeed, as: 'userSugarfeed'},
						{model:models.sequelize.aodIndexevent, as: 'userAodIndexevent'},
						{model:models.sequelize.tracker, as: 'trackerItem'},
					]
				});
			}
		} catch (error) {
			throw error;
		}
	}

	static async getSecurityGroups(id, query) {
		try {
			if(sql) {
				return await models.sequelize.users.findOne({
					attributes:query.select ? query.select.split(',') : null,
					where:{id:id},
					include: [
						{model:models.sequelize.usersCstm, as: 'userUsersCstm'},
						{model:models.sequelize.aclRolesUsers, as:'aclRolUserUsers',
							include:{model:models.sequelize.aclRoles, as:'aclRolUserAclRoles'}
						},
						{model:models.sequelize.securitygroups, as: 'userSecuritygroups',
							include: [
								{model:models.sequelize.users, as:'securitygroupUsers'},
							]
						},
					]
				});
			}
		} catch (error) {
			throw error;
		}
	}

	static async addUser(newUser) {
		try {
			let objUser;

			if(sql) {

				if (newUser) {
					newUser.id = models.sequelize.objectId().toString();
					newUser.date_entered = new Date();
					newUser.date_modified = new Date();
					newUser.date_reviewed = new Date();
					let respUser = await models.sequelize.users.create(newUser);
					objUser = respUser.dataValues;
				}

				if (newUser.userUsersCstm) {
					newUser.userUsersCstm.id_c = newUser.id;
					let respUsersCstm = await models.sequelize.usersCstm.create(newUser.userUsersCstm);
					objUser.userUsersCstm = respUsersCstm.dataValues;
				}

				if (newUser.userSugarfeed) {
					newUser.userSugarfeed.id = models.sequelize.objectId().toString();
					newUser.userSugarfeed.related_id = newUser.id;
					newUser.userSugarfeed.date_entered = new Date();
					newUser.userSugarfeed.date_modified = new Date();
					let respSugarfeed = await models.sequelize.sugarfeed.create(newUser.userSugarfeed);
					objUser.userSugarfeed = respSugarfeed.dataValues;
				}

				if (newUser.userAodIndexevent) {
					newUser.userAodIndexevent.id = models.sequelize.objectId().toString();
					newUser.userAodIndexevent.record_id = newUser.id;
					newUser.userAodIndexevent.date_entered = new Date();
					newUser.userAodIndexevent.date_modified = new Date();
					let respAodIndexevent = await models.sequelize.aodIndexevent.create(newUser.userAodIndexevent);
					objUser.userAodIndexevent = respAodIndexevent.dataValues;
				}

				if (newUser.userTracker) {
					let max = await models.sequelize.tracker.max('id');
					newUser.userTracker.id = newUser.userTracker.id ? newUser.userTracker.id : max+1;
					newUser.userTracker.monitor_id = models.sequelize.objectId().toString();
					newUser.userTracker.item_id = newUser.id;
					newUser.userTracker.date_modified = new Date();
					let respTracker = await models.sequelize.tracker.create(newUser.userTracker);
					objUser.userTracker = respTracker.dataValues;
				}

			} else {
				objUser = new models.mongoose.users(newUser);
				await objUser.save();
			}
			return objUser;
		} catch (error) {
			throw error;
		}
	}

	static async updateUser(id, updateUser) {
		try {
			let objUser;

			if(sql) {

				if (updateUser) {
					if (updateUser.id) {
						updateUser.date_modified = new Date();
						await models.sequelize.users.update(updateUser, {where:{id:id}});
						let respUser = await models.sequelize.users.findOne({where: { id: id }});
						objUser = respUser.dataValues;
					} else {
						let oldUser = await models.sequelize.users.findOne({where: { id: id }});
						if (oldUser && oldUser.dataValues) {
							oldUser = oldUser.dataValues;
							updateUser.date_modified = new Date();
							await models.sequelize.users.update(updateUser, {where:{id:oldUser.id}});
							let respUser = await models.sequelize.users.findOne({where: { id: oldUser.id }});
							objUser = respUser.dataValues;
						} else {
							let newUser = updateUser;
							newUser.id = models.sequelize.objectId().toString();
							newUser.date_entered = new Date();
							newUser.date_modified = new Date();
							newUser.date_reviewed = new Date();
							let respUser = await models.sequelize.users.create(newUser);
							objUser = respUser.dataValues;
						}
					}
				}

				if (updateUser.userUsersCstm) {
					if (updateUser.userUsersCstm.id_c) {
						await models.sequelize.usersCstm.update(updateUser.userUsersCstm, {where:{id_c:id}});
						let respUsersCstm = await models.sequelize.usersCstm.findOne({where: { id_c: id }});
						objUser.userUsersCstm = respUsersCstm.dataValues;
					} else {
						let oldUserUsersCstm = await models.sequelize.usersCstm.findOne({where: { id_c: id }});
						if (oldUserUsersCstm && oldUserUsersCstm.dataValues) {
							oldUserUsersCstm = oldUserUsersCstm.dataValues;
							await models.sequelize.usersCstm.update(updateUser.userUsersCstm, {where:{id_c:oldUserUsersCstm.id_c}});
							let respUsersCstm = await models.sequelize.usersCstm.findOne({where: { id_c: oldUserUsersCstm.id_c }});
							objUser.userUsersCstm = respUsersCstm.dataValues;
						} else {
							let newUser = updateUser;
							newUser.userUsersCstm.id_c = newUser.id;
							let respUsersCstm = await models.sequelize.usersCstm.create(newUser.userUsersCstm);
							objUser.userUsersCstm = respUsersCstm.dataValues;
						}
					}
				}

				if (updateUser.userSugarfeed) {
					if (updateUser.userSugarfeed.id) {
						updateUser.userSugarfeed.date_modified = new Date();
						await models.sequelize.sugarfeed.update(updateUser.userSugarfeed, {where:{related_id:id}});
						let respSugarfeed = await models.sequelize.sugarfeed.findOne({where: { related_id: id }});
						objUser.userSugarfeed = respSugarfeed.dataValues;
					} else {
						let oldUserSugarfeed = await models.sequelize.sugarfeed.findOne({where: { related_id: id }});
						if (oldUserSugarfeed && oldUserSugarfeed.dataValues) {
							oldUserSugarfeed = oldUserSugarfeed.dataValues;
							updateUser.userSugarfeed.date_modified = new Date();
							await models.sequelize.sugarfeed.update(updateUser.userSugarfeed, {where:{id:oldUserSugarfeed.id}});
							let respSugarfeed = await models.sequelize.sugarfeed.findOne({where: { id: oldUserSugarfeed.id }});
							objUser.userSugarfeed = respSugarfeed.dataValues;
						} else {
							let newUser = updateUser;
							newUser.userSugarfeed.id = models.sequelize.objectId().toString();
							newUser.userSugarfeed.related_id = newUser.id;
							newUser.userSugarfeed.date_entered = new Date();
							newUser.userSugarfeed.date_modified = new Date();
							let respSugarfeed = await models.sequelize.sugarfeed.create(newUser.userSugarfeed);
							objUser.userSugarfeed = respSugarfeed.dataValues;
						}
					}
				}

				if (updateUser.userAodIndexevent) {
					if (updateUser.userAodIndexevent.id) {
						updateUser.userAodIndexevent.date_modified = new Date();
						await models.sequelize.aodIndexevent.update(updateUser.userAodIndexevent, {where:{record_id:id}});
						let respAodIndexevent = await models.sequelize.aodIndexevent.findOne({where: { record_id: id }});
						objUser.userAodIndexevent = respAodIndexevent.dataValues;
					} else {
						let oldUserAodIndexevent = await models.sequelize.aodIndexevent.findOne({where: { record_id: id }});
						if (oldUserAodIndexevent && oldUserAodIndexevent.dataValues) {
							oldUserAodIndexevent = oldUserAodIndexevent.dataValues;
							updateUser.userAodIndexevent.date_modified = new Date();
							await models.sequelize.aodIndexevent.update(updateUser.userAodIndexevent, {where:{id:oldUserAodIndexevent.id}});
							let respAodIndexevent = await models.sequelize.aodIndexevent.findOne({where: { id: oldUserAodIndexevent.id }});
							objUser.userAodIndexevent = respAodIndexevent.dataValues;
						} else {
							let newUser = updateUser;
							newUser.userAodIndexevent.id = models.sequelize.objectId().toString();
							newUser.userAodIndexevent.record_id = newUser.id;
							newUser.userAodIndexevent.date_entered = new Date();
							newUser.userAodIndexevent.date_modified = new Date();
							let respAodIndexevent = await models.sequelize.aodIndexevent.create(newUser.userAodIndexevent);
							objUser.userAodIndexevent = respAodIndexevent.dataValues;
						}
					}
				}

				if (updateUser.userTracker) {
					if (updateUser.userTracker.id) {
						updateUser.userTracker.date_modified = new Date();
						await models.sequelize.tracker.update(updateUser.userTracker, {where:{item_id:id}});
						let respTracker = await models.sequelize.tracker.findOne({where: { item_id: id }});
						objUser.userTracker = respTracker.dataValues;
					} else {
						let oldUserTracker = await models.sequelize.tracker.findOne({where: { item_id: id }});
						if (oldUserTracker && oldUserTracker.dataValues) {
							oldUserTracker = oldUserTracker.dataValues;
							updateUser.userTracker.date_modified = new Date();
							await models.sequelize.tracker.update(updateUser.userTracker, {where:{id:oldUserTracker.id}});
							let respTracker = await models.sequelize.tracker.findOne({where: { id: oldUserTracker.id }});
							objUser.userTracker = respTracker.dataValues;
						} else {
							let newUser = updateUser;
							let max = await models.sequelize.tracker.max('id');
							newUser.userTracker.id = newUser.userTracker.id ? newUser.userTracker.id : max+1;
							newUser.userTracker.monitor_id = models.sequelize.objectId().toString();
							newUser.userTracker.item_id = newUser.id;
							newUser.userTracker.date_modified = new Date();
							let respTracker = await models.sequelize.tracker.create(newUser.userTracker);
							objUser.userTracker = respTracker.dataValues;
						}
					}
				}

			} else {
				objUser = new models.mongoose.users(updateUser);
				await objUser.save();
			}
			return objUser;
		} catch (error) {
			throw error;
		}
	}

	static async deleteUser(id) {
		try {

			let objUser;

			if(sql) {

				let users = await models.sequelize.users.findOne({ where: { id: util.Char(id) } });
				if (users) await models.sequelize.users.destroy({where: { id: util.Char(id) }});

				let userUsersCstm = await models.sequelize.usersCstm.findOne({ where: { id_c: util.Char(id) } });
				if (userUsersCstm) await models.sequelize.usersCstm.destroy({where: { id_c: util.Char(id) }});

				let userSugarfeed = await models.sequelize.sugarfeed.findOne({ where: { related_id: util.Char(id) } });
				if (userSugarfeed) await models.sequelize.sugarfeed.destroy({where: { related_id: util.Char(id) }});

				let userAodIndexevent = await models.sequelize.aodIndexevent.findOne({ where: { record_id: util.Char(id) } });
				if (userAodIndexevent) await models.sequelize.aodIndexevent.destroy({where: { record_id: util.Char(id) }});

				let userTracker = await models.sequelize.tracker.findOne({ where: { item_id: util.Char(id) } });
				if (userTracker) await models.sequelize.tracker.destroy({where: { item_id: util.Char(id) }});

				objUser = users;
				objUser.userUsersCstm = userUsersCstm;
				objUser.userSugarfeed = userSugarfeed;
				objUser.userAodIndexevent = userAodIndexevent;
				objUser.userTracker = userTracker;

			} else {
				objUser = await models.mongoose.users.deleteOne({id:util.Char(id)});
			}
			return objUser;
		} catch (error) {
			throw error;
		}
	}

	static async findOneByUserNameAndUserHash(userName, userHash, query = {}) {
		try {
			let objUser;
			if(sql) {
				objUser = await models.sequelize.users.findOne({
					attributes:query.select ? query.select.split(',') : null,
					where: { user_name: userName, user_hash: userHash},
				});
			} else {
				objUser = await models.mongoose.users.findOne({user_name: userName, user_hash: userHash});
			}
			return objUser;
		} catch (error) {
			throw error;
		}
	}

	static async setSearchPanes(body, query, dtColumns) {
		try {
			let { root } = query;
			let { where } = query;
			let objWhere = where ? util.isJson(where) ? where : JSON.parse(where) : {};
			// let aData = await models.sequelize.users.findAll({where:objWhere});
			let rawAttributes = models.sequelize.users.rawAttributes;
			let aColumns = Object.values(rawAttributes);
			let fields = Object.keys(rawAttributes);
			let search = body['search[value]'];
			let searchPanes = {};
			let searches = [];
			let dtOptions = {};
			let userUsers = await models.sequelize.users.findAll({
				where: objWhere
			});

			if (dtColumns) {
				for (let i = 0 ; i < fields.length; i++ ) {
					let field = fields[i];
					dtOptions[`${root}.${field}`] = [];
				}

				let dtValues = [];
				for (let k = 0 ; k < userUsers.length ; k++) {
					let userUser = userUsers[k].dataValues;
					let aUserUserValues = Object.values(userUser);
					let aUserUserFields = Object.keys(userUser);
					for (let n = 0 ; n < aUserUserValues.length ; n++) {
						let userUserField = aUserUserFields[n];
						let userUserValue = aUserUserValues[n];
						if (!dtValues.find(param => param.value == userUserValue && param.field == userUserField)) {
							dtValues.push({value:userUserValue, count:1, label:userUserValue, field:userUserField});
						} else {
							for (let m = 0 ; m < dtValues.length ; m++) {
								let dtValue = dtValues[m];
								if (dtValue.value == userUserValue && dtValue.field == userUserField) {
									dtValues[m].count++;
								}
							}
						}
					}
				}

				for (let l = 0 ; l < dtValues.length ; l++) {
					let dtValue = dtValues[l];
					let optDate = new Date(dtValue.value);
					dtOptions[`${root}.${dtValue.field}`].push({
						label:optDate.getDate() ? optDate.getDate().pad(2)+'/'+(optDate.getMonth()+1).pad(2)+'/'+optDate.getFullYear() : dtValue.value,
						total:dtValue.count,
						value:optDate.getDate() ? optDate : dtValue.value,
						count:dtValue.count
					});
				}

				for (let j = 0 ; j < fields.length; j++ ) {
					for (let z = 0 ; z < fields.length; z++ ) {
						let field = fields[z];
						if (root) {
							if (body[`searchPanes[${root}.${field}][${j}]`]) {
								searches.push({field: field, value: body[`searchPanes[${root}.${field}][${j}]`]});
							}
						}
					}
				}
			}
			dtOptions['tableLength'] = 100;
			searchPanes['options'] = dtOptions;
			return [searchPanes, searches, userUsers];
		} catch (e) {
			console.log(e);
		}
	}
}

//<es-section>
module.exports = UserService;
//</es-section>
