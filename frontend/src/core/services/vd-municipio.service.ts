/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:29 GMT-0400 (Bolivia Time)
 * Time: 0:27:29
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:29 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:29
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsMunicipio} from "../models/vdsMunicipio";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdMunicipioService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-municipio`;
  dataChange: BehaviorSubject<VdsMunicipio[]> = new BehaviorSubject<VdsMunicipio[]>([]);
  vdMunicipioData: VdsMunicipio = new VdsMunicipio();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsMunicipio[] {
    return this.dataChange.value;
  }

  getDataVdsMunicipio(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsMunicipio(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsMunicipio[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsMunicipio(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdMunicipio(vdMunicipio:VdsMunicipio) {
    return this.http.post(this.basePath, vdMunicipio);
  }
  getVdMunicipio(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdMunicipio(id:any, vdMunicipio:VdsMunicipio) {
    return this.http.put(this.basePath + '/' + id, vdMunicipio);
  }
  deleteVdMunicipio(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstado(estado:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstado/' + estado + '?' + attributes);
  }
  
  findOneByMunicipio(municipio:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMunicipio/' + municipio + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByIdCiudad(idCiudad:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdCiudad/' + idCiudad + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdMunicipioByUid(Id:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByUid?Id=' + Id, vdMunicipio);
  }
  
  updateVdMunicipioById(id:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioById?id=' + id, vdMunicipio);
  }
  
  updateVdMunicipioByEstado(estado:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByEstado?estado=' + estado, vdMunicipio);
  }
  
  updateVdMunicipioByMunicipio(municipio:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByMunicipio?municipio=' + municipio, vdMunicipio);
  }
  
  updateVdMunicipioByCreatedby(createdby:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByCreatedby?createdby=' + createdby, vdMunicipio);
  }
  
  updateVdMunicipioByUpdatedby(updatedby:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByUpdatedby?updatedby=' + updatedby, vdMunicipio);
  }
  
  updateVdMunicipioByIdCiudad(idCiudad:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByIdCiudad?idCiudad=' + idCiudad, vdMunicipio);
  }
  
  updateVdMunicipioByDueat(dueat:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByDueat?dueat=' + dueat, vdMunicipio);
  }
  
  updateVdMunicipioByCreatedat(createdat:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByCreatedat?createdat=' + createdat, vdMunicipio);
  }
  
  updateVdMunicipioByUpdatedat(updatedat:any, vdMunicipio:VdsMunicipio) {
      return this.http.post(this.basePath + '/updateVdMunicipioByUpdatedat?updatedat=' + updatedat, vdMunicipio);
  }
  
  
  findVdsCiudadCiudadWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsCiudadCiudadWithEstado' + '?' + attributes);
  }
  
  
  filterVdsMunicipioByCiudad(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsMunicipioByCiudad/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
