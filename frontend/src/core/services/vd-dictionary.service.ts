/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:23 GMT-0400 (Bolivia Time)
 * Time: 0:27:23
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:23 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:23
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsDictionaries} from "../models/vdsDictionaries";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdDictionaryService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-dictionaries`;
  dataChange: BehaviorSubject<VdsDictionaries[]> = new BehaviorSubject<VdsDictionaries[]>([]);
  vdDictionaryData: VdsDictionaries = new VdsDictionaries();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsDictionaries[] {
    return this.dataChange.value;
  }

  getDataVdsDictionaries(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsDictionaries(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsDictionaries[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsDictionaries(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdDictionary(vdDictionary:VdsDictionaries) {
    return this.http.post(this.basePath, vdDictionary);
  }
  getVdDictionary(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdDictionary(id:any, vdDictionary:VdsDictionaries) {
    return this.http.put(this.basePath + '/' + id, vdDictionary);
  }
  deleteVdDictionary(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByDicCode(dicCode:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDicCode/' + dicCode + '?' + attributes);
  }
  
  findOneByDicDescription(dicDescription:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDicDescription/' + dicDescription + '?' + attributes);
  }
  
  findOneByDicGroup(dicGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDicGroup/' + dicGroup + '?' + attributes);
  }
  
  findOneByDicParStatusId(dicParStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDicParStatusId/' + dicParStatusId + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdDictionaryByUid(Id:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByUid?Id=' + Id, vdDictionary);
  }
  
  updateVdDictionaryById(id:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryById?id=' + id, vdDictionary);
  }
  
  updateVdDictionaryByDicCode(dicCode:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByDicCode?dicCode=' + dicCode, vdDictionary);
  }
  
  updateVdDictionaryByDicDescription(dicDescription:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByDicDescription?dicDescription=' + dicDescription, vdDictionary);
  }
  
  updateVdDictionaryByDicGroup(dicGroup:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByDicGroup?dicGroup=' + dicGroup, vdDictionary);
  }
  
  updateVdDictionaryByDicParStatusId(dicParStatusId:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByDicParStatusId?dicParStatusId=' + dicParStatusId, vdDictionary);
  }
  
  updateVdDictionaryByCreatedbyid(createdbyid:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByCreatedbyid?createdbyid=' + createdbyid, vdDictionary);
  }
  
  updateVdDictionaryByUpdatedbyid(updatedbyid:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByUpdatedbyid?updatedbyid=' + updatedbyid, vdDictionary);
  }
  
  updateVdDictionaryByDueat(dueat:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByDueat?dueat=' + dueat, vdDictionary);
  }
  
  updateVdDictionaryByCreatedat(createdat:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByCreatedat?createdat=' + createdat, vdDictionary);
  }
  
  updateVdDictionaryByUpdatedat(updatedat:any, vdDictionary:VdsDictionaries) {
      return this.http.post(this.basePath + '/updateVdDictionaryByUpdatedat?updatedat=' + updatedat, vdDictionary);
  }
  
  
  findVdsParamsDicParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsDicParStatusWithParOrder' + '?' + attributes);
  }
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  
  filterVdsDictionariesByDicParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsDictionariesByDicParStatus/' + ids + '?' + attributes);
  }
  
  filterVdsDictionariesByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsDictionariesByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsDictionariesByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsDictionariesByUpdatedby/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
