/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:33 GMT-0400 (Bolivia Time)
 * Time: 0:27:33
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:33 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:33
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsUserRoles} from "../models/vdsUserRoles";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdUserRoleService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-user-roles`;
  dataChange: BehaviorSubject<VdsUserRoles[]> = new BehaviorSubject<VdsUserRoles[]>([]);
  vdUserRoleData: VdsUserRoles = new VdsUserRoles();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsUserRoles[] {
    return this.dataChange.value;
  }

  getDataVdsUserRoles(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsUserRoles(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsUserRoles[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsUserRoles(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdUserRole(vdUserRole:VdsUserRoles) {
    return this.http.post(this.basePath, vdUserRole);
  }
  getVdUserRole(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdUserRole(id:any, vdUserRole:VdsUserRoles) {
    return this.http.put(this.basePath + '/' + id, vdUserRole);
  }
  deleteVdUserRole(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByUsrRolGroup(usrRolGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUsrRolGroup/' + usrRolGroup + '?' + attributes);
  }
  
  findOneByUsrId(usrId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUsrId/' + usrId + '?' + attributes);
  }
  
  findOneByRolId(rolId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByRolId/' + rolId + '?' + attributes);
  }
  
  findOneByUsrRolParStatusId(usrRolParStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUsrRolParStatusId/' + usrRolParStatusId + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdUserRoleByUid(Id:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByUid?Id=' + Id, vdUserRole);
  }
  
  updateVdUserRoleById(id:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleById?id=' + id, vdUserRole);
  }
  
  updateVdUserRoleByUsrRolGroup(usrRolGroup:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByUsrRolGroup?usrRolGroup=' + usrRolGroup, vdUserRole);
  }
  
  updateVdUserRoleByUsrId(usrId:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByUsrId?usrId=' + usrId, vdUserRole);
  }
  
  updateVdUserRoleByRolId(rolId:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByRolId?rolId=' + rolId, vdUserRole);
  }
  
  updateVdUserRoleByUsrRolParStatusId(usrRolParStatusId:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByUsrRolParStatusId?usrRolParStatusId=' + usrRolParStatusId, vdUserRole);
  }
  
  updateVdUserRoleByCreatedbyid(createdbyid:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByCreatedbyid?createdbyid=' + createdbyid, vdUserRole);
  }
  
  updateVdUserRoleByUpdatedbyid(updatedbyid:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByUpdatedbyid?updatedbyid=' + updatedbyid, vdUserRole);
  }
  
  updateVdUserRoleByDueat(dueat:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByDueat?dueat=' + dueat, vdUserRole);
  }
  
  updateVdUserRoleByCreatedat(createdat:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByCreatedat?createdat=' + createdat, vdUserRole);
  }
  
  updateVdUserRoleByUpdatedat(updatedat:any, vdUserRole:VdsUserRoles) {
      return this.http.post(this.basePath + '/updateVdUserRoleByUpdatedat?updatedat=' + updatedat, vdUserRole);
  }
  
  
  findVdsUsersUsrWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUsersUsrWithEstado' + '?' + attributes);
  }
  
  findVdsRolesRolWithRolCode(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsRolesRolWithRolCode' + '?' + attributes);
  }
  
  findVdsParamsUsrRolParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsUsrRolParStatusWithParOrder' + '?' + attributes);
  }
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  
  filterVdsUserRolesByUsr(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsUserRolesByUsr/' + ids + '?' + attributes);
  }
  
  filterVdsUserRolesByRol(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsUserRolesByRol/' + ids + '?' + attributes);
  }
  
  filterVdsUserRolesByUsrRolParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsUserRolesByUsrRolParStatus/' + ids + '?' + attributes);
  }
  
  filterVdsUserRolesByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsUserRolesByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsUserRolesByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsUserRolesByUpdatedby/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
