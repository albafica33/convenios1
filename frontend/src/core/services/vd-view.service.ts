/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:32 GMT-0400 (Bolivia Time)
 * Time: 0:27:32
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:32 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:32
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsViews} from "../models/vdsViews";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdViewService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-views`;
  dataChange: BehaviorSubject<VdsViews[]> = new BehaviorSubject<VdsViews[]>([]);
  vdViewData: VdsViews = new VdsViews();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsViews[] {
    return this.dataChange.value;
  }

  getDataVdsViews(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsViews(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsViews[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsViews(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdView(vdView:VdsViews) {
    return this.http.post(this.basePath, vdView);
  }
  getVdView(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdView(id:any, vdView:VdsViews) {
    return this.http.put(this.basePath + '/' + id, vdView);
  }
  deleteVdView(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByVieCode(vieCode:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieCode/' + vieCode + '?' + attributes);
  }
  
  findOneByVieDescription(vieDescription:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieDescription/' + vieDescription + '?' + attributes);
  }
  
  findOneByVieRoute(vieRoute:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieRoute/' + vieRoute + '?' + attributes);
  }
  
  findOneByVieParams(vieParams:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieParams/' + vieParams + '?' + attributes);
  }
  
  findOneByVieIcon(vieIcon:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieIcon/' + vieIcon + '?' + attributes);
  }
  
  findOneByVieGroup(vieGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieGroup/' + vieGroup + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByVieModuleId(vieModuleId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieModuleId/' + vieModuleId + '?' + attributes);
  }
  
  findOneByVieReturnId(vieReturnId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieReturnId/' + vieReturnId + '?' + attributes);
  }
  
  findOneByVieParStatusId(vieParStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByVieParStatusId/' + vieParStatusId + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdViewByUid(Id:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByUid?Id=' + Id, vdView);
  }
  
  updateVdViewById(id:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewById?id=' + id, vdView);
  }
  
  updateVdViewByVieCode(vieCode:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieCode?vieCode=' + vieCode, vdView);
  }
  
  updateVdViewByVieDescription(vieDescription:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieDescription?vieDescription=' + vieDescription, vdView);
  }
  
  updateVdViewByVieRoute(vieRoute:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieRoute?vieRoute=' + vieRoute, vdView);
  }
  
  updateVdViewByVieParams(vieParams:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieParams?vieParams=' + vieParams, vdView);
  }
  
  updateVdViewByVieIcon(vieIcon:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieIcon?vieIcon=' + vieIcon, vdView);
  }
  
  updateVdViewByVieGroup(vieGroup:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieGroup?vieGroup=' + vieGroup, vdView);
  }
  
  updateVdViewByCreatedby(createdby:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByCreatedby?createdby=' + createdby, vdView);
  }
  
  updateVdViewByUpdatedby(updatedby:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByUpdatedby?updatedby=' + updatedby, vdView);
  }
  
  updateVdViewByVieModuleId(vieModuleId:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieModuleId?vieModuleId=' + vieModuleId, vdView);
  }
  
  updateVdViewByVieReturnId(vieReturnId:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieReturnId?vieReturnId=' + vieReturnId, vdView);
  }
  
  updateVdViewByVieParStatusId(vieParStatusId:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByVieParStatusId?vieParStatusId=' + vieParStatusId, vdView);
  }
  
  updateVdViewByDueat(dueat:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByDueat?dueat=' + dueat, vdView);
  }
  
  updateVdViewByCreatedat(createdat:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByCreatedat?createdat=' + createdat, vdView);
  }
  
  updateVdViewByUpdatedat(updatedat:any, vdView:VdsViews) {
      return this.http.post(this.basePath + '/updateVdViewByUpdatedat?updatedat=' + updatedat, vdView);
  }
  
  
  findVdsModulesVieModuleWithModCode(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsModulesVieModuleWithModCode' + '?' + attributes);
  }
  
  findVdsViewsVieReturnWithVieCode(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsViewsVieReturnWithVieCode' + '?' + attributes);
  }
  
  findVdsParamsVieParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsVieParStatusWithParOrder' + '?' + attributes);
  }
  
  
  filterVdsViewsByVieModule(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsViewsByVieModule/' + ids + '?' + attributes);
  }
  
  filterVdsViewsByVieReturn(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsViewsByVieReturn/' + ids + '?' + attributes);
  }
  
  filterVdsViewsByVieParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsViewsByVieParStatus/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
