/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:31 GMT-0400 (Bolivia Time)
 * Time: 0:27:31
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:31 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:31
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsCrons} from "../models/vdsCrons";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdCronService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-crons`;
  dataChange: BehaviorSubject<VdsCrons[]> = new BehaviorSubject<VdsCrons[]>([]);
  vdCronData: VdsCrons = new VdsCrons();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsCrons[] {
    return this.dataChange.value;
  }

  getDataVdsCrons(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsCrons(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsCrons[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsCrons(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdCron(vdCron:VdsCrons) {
    return this.http.post(this.basePath, vdCron);
  }
  getVdCron(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdCron(id:any, vdCron:VdsCrons) {
    return this.http.put(this.basePath + '/' + id, vdCron);
  }
  deleteVdCron(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByCroStatus(croStatus:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCroStatus/' + croStatus + '?' + attributes);
  }
  
  findOneByCroDescription(croDescription:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCroDescription/' + croDescription + '?' + attributes);
  }
  
  findOneByCroExpression(croExpression:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCroExpression/' + croExpression + '?' + attributes);
  }
  
  findOneByCroGroup(croGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCroGroup/' + croGroup + '?' + attributes);
  }
  
  findOneByCroMaiId(croMaiId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCroMaiId/' + croMaiId + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByCroFunction(croFunction:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCroFunction/' + croFunction + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdCronByUid(Id:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByUid?Id=' + Id, vdCron);
  }
  
  updateVdCronById(id:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronById?id=' + id, vdCron);
  }
  
  updateVdCronByCroStatus(croStatus:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByCroStatus?croStatus=' + croStatus, vdCron);
  }
  
  updateVdCronByCroDescription(croDescription:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByCroDescription?croDescription=' + croDescription, vdCron);
  }
  
  updateVdCronByCroExpression(croExpression:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByCroExpression?croExpression=' + croExpression, vdCron);
  }
  
  updateVdCronByCroGroup(croGroup:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByCroGroup?croGroup=' + croGroup, vdCron);
  }
  
  updateVdCronByCroMaiId(croMaiId:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByCroMaiId?croMaiId=' + croMaiId, vdCron);
  }
  
  updateVdCronByCreatedby(createdby:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByCreatedby?createdby=' + createdby, vdCron);
  }
  
  updateVdCronByUpdatedby(updatedby:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByUpdatedby?updatedby=' + updatedby, vdCron);
  }
  
  updateVdCronByCroFunction(croFunction:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByCroFunction?croFunction=' + croFunction, vdCron);
  }
  
  updateVdCronByDueat(dueat:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByDueat?dueat=' + dueat, vdCron);
  }
  
  updateVdCronByCreatedat(createdat:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByCreatedat?createdat=' + createdat, vdCron);
  }
  
  updateVdCronByUpdatedat(updatedat:any, vdCron:VdsCrons) {
      return this.http.post(this.basePath + '/updateVdCronByUpdatedat?updatedat=' + updatedat, vdCron);
  }
  
  
  
  //</es-section>
}
