/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:32:45 GMT-0400 (Bolivia Time)
 * Time: 0:32:45
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:32:45 GMT-0400 (Bolivia Time)
 * Last time updated: 0:32:45
 *
 * Caution: es-sections will be replaced by script execution
 */

import { TestBed } from '@angular/core/testing';
//<es-section>

import { SequelizemetaService } from './sequelizemeta.service';

describe('SequelizemetaService', () => {
  let service: SequelizemetaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SequelizemetaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

//</es-section>
