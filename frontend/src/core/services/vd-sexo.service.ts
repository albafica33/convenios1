/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:29 GMT-0400 (Bolivia Time)
 * Time: 0:27:29
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:29 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:29
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsSexo} from "../models/vdsSexo";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdSexoService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-sexo`;
  dataChange: BehaviorSubject<VdsSexo[]> = new BehaviorSubject<VdsSexo[]>([]);
  vdSexoData: VdsSexo = new VdsSexo();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsSexo[] {
    return this.dataChange.value;
  }

  getDataVdsSexo(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsSexo(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsSexo[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsSexo(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdSexo(vdSexo:VdsSexo) {
    return this.http.post(this.basePath, vdSexo);
  }
  getVdSexo(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdSexo(id:any, vdSexo:VdsSexo) {
    return this.http.put(this.basePath + '/' + id, vdSexo);
  }
  deleteVdSexo(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstado(estado:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstado/' + estado + '?' + attributes);
  }
  
  findOneByDescripcion(descripcion:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDescripcion/' + descripcion + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdSexoByUid(Id:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoByUid?Id=' + Id, vdSexo);
  }
  
  updateVdSexoById(id:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoById?id=' + id, vdSexo);
  }
  
  updateVdSexoByEstado(estado:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoByEstado?estado=' + estado, vdSexo);
  }
  
  updateVdSexoByDescripcion(descripcion:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoByDescripcion?descripcion=' + descripcion, vdSexo);
  }
  
  updateVdSexoByCreatedby(createdby:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoByCreatedby?createdby=' + createdby, vdSexo);
  }
  
  updateVdSexoByUpdatedby(updatedby:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoByUpdatedby?updatedby=' + updatedby, vdSexo);
  }
  
  updateVdSexoByDueat(dueat:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoByDueat?dueat=' + dueat, vdSexo);
  }
  
  updateVdSexoByCreatedat(createdat:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoByCreatedat?createdat=' + createdat, vdSexo);
  }
  
  updateVdSexoByUpdatedat(updatedat:any, vdSexo:VdsSexo) {
      return this.http.post(this.basePath + '/updateVdSexoByUpdatedat?updatedat=' + updatedat, vdSexo);
  }
  
  
  
  //</es-section>
}
