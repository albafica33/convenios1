/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:25 GMT-0400 (Bolivia Time)
 * Time: 0:27:25
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:25 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:25
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsPeople} from "../models/vdsPeople";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdPersonService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-people`;
  dataChange: BehaviorSubject<VdsPeople[]> = new BehaviorSubject<VdsPeople[]>([]);
  vdPersonData: VdsPeople = new VdsPeople();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsPeople[] {
    return this.dataChange.value;
  }

  getDataVdsPeople(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsPeople(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsPeople[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsPeople(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdPerson(vdPerson:VdsPeople) {
    return this.http.post(this.basePath, vdPerson);
  }
  getVdPerson(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdPerson(id:any, vdPerson:VdsPeople) {
    return this.http.put(this.basePath + '/' + id, vdPerson);
  }
  deleteVdPerson(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByPerFirstName(perFirstName:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerFirstName/' + perFirstName + '?' + attributes);
  }
  
  findOneByPerSecondName(perSecondName:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerSecondName/' + perSecondName + '?' + attributes);
  }
  
  findOneByPerFirstLastname(perFirstLastname:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerFirstLastname/' + perFirstLastname + '?' + attributes);
  }
  
  findOneByPerSecondLastname(perSecondLastname:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerSecondLastname/' + perSecondLastname + '?' + attributes);
  }
  
  findOneByPerLicense(perLicense:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerLicense/' + perLicense + '?' + attributes);
  }
  
  findOneByPerLicenseComp(perLicenseComp:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerLicenseComp/' + perLicenseComp + '?' + attributes);
  }
  
  findOneByPerHomeAddress(perHomeAddress:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerHomeAddress/' + perHomeAddress + '?' + attributes);
  }
  
  findOneByPerMail(perMail:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerMail/' + perMail + '?' + attributes);
  }
  
  findOneByPerHomePhone(perHomePhone:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerHomePhone/' + perHomePhone + '?' + attributes);
  }
  
  findOneByPerCellphone(perCellphone:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerCellphone/' + perCellphone + '?' + attributes);
  }
  
  findOneByPerGroup(perGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerGroup/' + perGroup + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByPerParentId(perParentId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerParentId/' + perParentId + '?' + attributes);
  }
  
  findOneByPerParTypeDocId(perParTypeDocId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerParTypeDocId/' + perParTypeDocId + '?' + attributes);
  }
  
  findOneByPerParCityId(perParCityId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerParCityId/' + perParCityId + '?' + attributes);
  }
  
  findOneByPerParSexId(perParSexId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerParSexId/' + perParSexId + '?' + attributes);
  }
  
  findOneByPerParCountryId(perParCountryId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerParCountryId/' + perParCountryId + '?' + attributes);
  }
  
  findOneByPerParNacionalityId(perParNacionalityId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerParNacionalityId/' + perParNacionalityId + '?' + attributes);
  }
  
  findOneByPerParStatusId(perParStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerParStatusId/' + perParStatusId + '?' + attributes);
  }
  
  findOneByPerBirthday(perBirthday:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPerBirthday/' + perBirthday + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdPersonByUid(Id:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByUid?Id=' + Id, vdPerson);
  }
  
  updateVdPersonById(id:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonById?id=' + id, vdPerson);
  }
  
  updateVdPersonByPerFirstName(perFirstName:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerFirstName?perFirstName=' + perFirstName, vdPerson);
  }
  
  updateVdPersonByPerSecondName(perSecondName:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerSecondName?perSecondName=' + perSecondName, vdPerson);
  }
  
  updateVdPersonByPerFirstLastname(perFirstLastname:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerFirstLastname?perFirstLastname=' + perFirstLastname, vdPerson);
  }
  
  updateVdPersonByPerSecondLastname(perSecondLastname:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerSecondLastname?perSecondLastname=' + perSecondLastname, vdPerson);
  }
  
  updateVdPersonByPerLicense(perLicense:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerLicense?perLicense=' + perLicense, vdPerson);
  }
  
  updateVdPersonByPerLicenseComp(perLicenseComp:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerLicenseComp?perLicenseComp=' + perLicenseComp, vdPerson);
  }
  
  updateVdPersonByPerHomeAddress(perHomeAddress:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerHomeAddress?perHomeAddress=' + perHomeAddress, vdPerson);
  }
  
  updateVdPersonByPerMail(perMail:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerMail?perMail=' + perMail, vdPerson);
  }
  
  updateVdPersonByPerHomePhone(perHomePhone:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerHomePhone?perHomePhone=' + perHomePhone, vdPerson);
  }
  
  updateVdPersonByPerCellphone(perCellphone:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerCellphone?perCellphone=' + perCellphone, vdPerson);
  }
  
  updateVdPersonByPerGroup(perGroup:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerGroup?perGroup=' + perGroup, vdPerson);
  }
  
  updateVdPersonByCreatedbyid(createdbyid:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByCreatedbyid?createdbyid=' + createdbyid, vdPerson);
  }
  
  updateVdPersonByUpdatedbyid(updatedbyid:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByUpdatedbyid?updatedbyid=' + updatedbyid, vdPerson);
  }
  
  updateVdPersonByPerParentId(perParentId:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerParentId?perParentId=' + perParentId, vdPerson);
  }
  
  updateVdPersonByPerParTypeDocId(perParTypeDocId:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerParTypeDocId?perParTypeDocId=' + perParTypeDocId, vdPerson);
  }
  
  updateVdPersonByPerParCityId(perParCityId:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerParCityId?perParCityId=' + perParCityId, vdPerson);
  }
  
  updateVdPersonByPerParSexId(perParSexId:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerParSexId?perParSexId=' + perParSexId, vdPerson);
  }
  
  updateVdPersonByPerParCountryId(perParCountryId:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerParCountryId?perParCountryId=' + perParCountryId, vdPerson);
  }
  
  updateVdPersonByPerParNacionalityId(perParNacionalityId:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerParNacionalityId?perParNacionalityId=' + perParNacionalityId, vdPerson);
  }
  
  updateVdPersonByPerParStatusId(perParStatusId:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerParStatusId?perParStatusId=' + perParStatusId, vdPerson);
  }
  
  updateVdPersonByPerBirthday(perBirthday:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByPerBirthday?perBirthday=' + perBirthday, vdPerson);
  }
  
  updateVdPersonByDueat(dueat:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByDueat?dueat=' + dueat, vdPerson);
  }
  
  updateVdPersonByCreatedat(createdat:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByCreatedat?createdat=' + createdat, vdPerson);
  }
  
  updateVdPersonByUpdatedat(updatedat:any, vdPerson:VdsPeople) {
      return this.http.post(this.basePath + '/updateVdPersonByUpdatedat?updatedat=' + updatedat, vdPerson);
  }
  
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsPeoplePerParentWithPerFirstName(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsPeoplePerParentWithPerFirstName' + '?' + attributes);
  }
  
  findVdsParamsPerParTypeDocWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsPerParTypeDocWithParOrder' + '?' + attributes);
  }
  
  findVdsParamsPerParCityWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsPerParCityWithParOrder' + '?' + attributes);
  }
  
  findVdsParamsPerParSexWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsPerParSexWithParOrder' + '?' + attributes);
  }
  
  findVdsParamsPerParCountryWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsPerParCountryWithParOrder' + '?' + attributes);
  }
  
  findVdsParamsPerParNacionalityWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsPerParNacionalityWithParOrder' + '?' + attributes);
  }
  
  findVdsParamsPerParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsPerParStatusWithParOrder' + '?' + attributes);
  }
  
  
  filterVdsPeopleByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsPeopleByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByUpdatedby/' + ids + '?' + attributes);
  }
  
  filterVdsPeopleByPerParent(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByPerParent/' + ids + '?' + attributes);
  }
  
  filterVdsPeopleByPerParTypeDoc(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByPerParTypeDoc/' + ids + '?' + attributes);
  }
  
  filterVdsPeopleByPerParCity(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByPerParCity/' + ids + '?' + attributes);
  }
  
  filterVdsPeopleByPerParSex(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByPerParSex/' + ids + '?' + attributes);
  }
  
  filterVdsPeopleByPerParCountry(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByPerParCountry/' + ids + '?' + attributes);
  }
  
  filterVdsPeopleByPerParNacionality(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByPerParNacionality/' + ids + '?' + attributes);
  }
  
  filterVdsPeopleByPerParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPeopleByPerParStatus/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
