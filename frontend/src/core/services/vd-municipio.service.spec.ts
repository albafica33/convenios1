/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:29 GMT-0400 (Bolivia Time)
 * Time: 0:27:29
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:29 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:29
 *
 * Caution: es-sections will be replaced by script execution
 */

import { TestBed } from '@angular/core/testing';
//<es-section>

import { VdMunicipioService } from './vd-municipio.service';

describe('VdMunicipioService', () => {
  let service: VdMunicipioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VdMunicipioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

//</es-section>
