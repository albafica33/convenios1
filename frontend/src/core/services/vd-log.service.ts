/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:27 GMT-0400 (Bolivia Time)
 * Time: 0:27:27
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:27 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:27
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsLogs} from "../models/vdsLogs";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdLogService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-logs`;
  dataChange: BehaviorSubject<VdsLogs[]> = new BehaviorSubject<VdsLogs[]>([]);
  vdLogData: VdsLogs = new VdsLogs();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsLogs[] {
    return this.dataChange.value;
  }

  getDataVdsLogs(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsLogs(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsLogs[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsLogs(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdLog(vdLog:VdsLogs) {
    return this.http.post(this.basePath, vdLog);
  }
  getVdLog(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdLog(id:any, vdLog:VdsLogs) {
    return this.http.put(this.basePath + '/' + id, vdLog);
  }
  deleteVdLog(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByLogObjId(logObjId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByLogObjId/' + logObjId + '?' + attributes);
  }
  
  findOneByLogDescription(logDescription:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByLogDescription/' + logDescription + '?' + attributes);
  }
  
  findOneByLogGroup(logGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByLogGroup/' + logGroup + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByLogParStatusId(logParStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByLogParStatusId/' + logParStatusId + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdLogByUid(Id:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByUid?Id=' + Id, vdLog);
  }
  
  updateVdLogById(id:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogById?id=' + id, vdLog);
  }
  
  updateVdLogByLogObjId(logObjId:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByLogObjId?logObjId=' + logObjId, vdLog);
  }
  
  updateVdLogByLogDescription(logDescription:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByLogDescription?logDescription=' + logDescription, vdLog);
  }
  
  updateVdLogByLogGroup(logGroup:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByLogGroup?logGroup=' + logGroup, vdLog);
  }
  
  updateVdLogByCreatedbyid(createdbyid:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByCreatedbyid?createdbyid=' + createdbyid, vdLog);
  }
  
  updateVdLogByUpdatedbyid(updatedbyid:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByUpdatedbyid?updatedbyid=' + updatedbyid, vdLog);
  }
  
  updateVdLogByLogParStatusId(logParStatusId:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByLogParStatusId?logParStatusId=' + logParStatusId, vdLog);
  }
  
  updateVdLogByDueat(dueat:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByDueat?dueat=' + dueat, vdLog);
  }
  
  updateVdLogByCreatedat(createdat:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByCreatedat?createdat=' + createdat, vdLog);
  }
  
  updateVdLogByUpdatedat(updatedat:any, vdLog:VdsLogs) {
      return this.http.post(this.basePath + '/updateVdLogByUpdatedat?updatedat=' + updatedat, vdLog);
  }
  
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsParamsLogParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsLogParStatusWithParOrder' + '?' + attributes);
  }
  
  
  filterVdsLogsByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsLogsByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsLogsByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsLogsByUpdatedby/' + ids + '?' + attributes);
  }
  
  filterVdsLogsByLogParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsLogsByLogParStatus/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
