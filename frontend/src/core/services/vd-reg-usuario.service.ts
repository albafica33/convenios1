/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:34 GMT-0400 (Bolivia Time)
 * Time: 0:27:34
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:34 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:34
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsRegUsuario} from "../models/vdsRegUsuario";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdRegUsuarioService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-reg-usuario`;
  dataChange: BehaviorSubject<VdsRegUsuario[]> = new BehaviorSubject<VdsRegUsuario[]>([]);
  vdRegUsuarioData: VdsRegUsuario = new VdsRegUsuario();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsRegUsuario[] {
    return this.dataChange.value;
  }

  getDataVdsRegUsuario(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsRegUsuario(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsRegUsuario[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsRegUsuario(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdRegUsuario(vdRegUsuario:VdsRegUsuario) {
    return this.http.post(this.basePath, vdRegUsuario);
  }
  getVdRegUsuario(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdRegUsuario(id:any, vdRegUsuario:VdsRegUsuario) {
    return this.http.put(this.basePath + '/' + id, vdRegUsuario);
  }
  deleteVdRegUsuario(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByIdUsuario(idUsuario:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdUsuario/' + idUsuario + '?' + attributes);
  }
  
  findOneByIdReg(idReg:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdReg/' + idReg + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdRegUsuarioByUid(Id:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioByUid?Id=' + Id, vdRegUsuario);
  }
  
  updateVdRegUsuarioById(id:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioById?id=' + id, vdRegUsuario);
  }
  
  updateVdRegUsuarioByIdUsuario(idUsuario:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioByIdUsuario?idUsuario=' + idUsuario, vdRegUsuario);
  }
  
  updateVdRegUsuarioByIdReg(idReg:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioByIdReg?idReg=' + idReg, vdRegUsuario);
  }
  
  updateVdRegUsuarioByCreatedbyid(createdbyid:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioByCreatedbyid?createdbyid=' + createdbyid, vdRegUsuario);
  }
  
  updateVdRegUsuarioByUpdatedbyid(updatedbyid:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioByUpdatedbyid?updatedbyid=' + updatedbyid, vdRegUsuario);
  }
  
  updateVdRegUsuarioByDueat(dueat:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioByDueat?dueat=' + dueat, vdRegUsuario);
  }
  
  updateVdRegUsuarioByCreatedat(createdat:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioByCreatedat?createdat=' + createdat, vdRegUsuario);
  }
  
  updateVdRegUsuarioByUpdatedat(updatedat:any, vdRegUsuario:VdsRegUsuario) {
      return this.http.post(this.basePath + '/updateVdRegUsuarioByUpdatedat?updatedat=' + updatedat, vdRegUsuario);
  }
  
  
  findVdsUsersUsuarioWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUsersUsuarioWithEstado' + '?' + attributes);
  }
  
  findVdsMatrizRegWithEstadoCumplimiento(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsMatrizRegWithEstadoCumplimiento' + '?' + attributes);
  }
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  
  filterVdsRegUsuarioByUsuario(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsRegUsuarioByUsuario/' + ids + '?' + attributes);
  }
  
  filterVdsRegUsuarioByReg(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsRegUsuarioByReg/' + ids + '?' + attributes);
  }
  
  filterVdsRegUsuarioByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsRegUsuarioByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsRegUsuarioByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsRegUsuarioByUpdatedby/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
