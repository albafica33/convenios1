/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:33 GMT-0400 (Bolivia Time)
 * Time: 0:27:33
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:33 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:33
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsMatriz} from "../models/vdsMatriz";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdMatrizService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-matriz`;
  dataChange: BehaviorSubject<VdsMatriz[]> = new BehaviorSubject<VdsMatriz[]>([]);
  vdMatrizData: VdsMatriz = new VdsMatriz();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsMatriz[] {
    return this.dataChange.value;
  }

  getDataVdsMatriz(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsMatriz(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsMatriz[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsMatriz(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdMatriz(vdMatriz:VdsMatriz) {
    return this.http.post(this.basePath, vdMatriz);
  }
  getVdMatriz(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdMatriz(id:any, vdMatriz:VdsMatriz) {
    return this.http.put(this.basePath + '/' + id, vdMatriz);
  }
  deleteVdMatriz(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstadoCumplimiento(estadoCumplimiento:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstadoCumplimiento/' + estadoCumplimiento + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByCompromiso(compromiso:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCompromiso/' + compromiso + '?' + attributes);
  }
  
  findOneByEntidadResponsable(entidadResponsable:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEntidadResponsable/' + entidadResponsable + '?' + attributes);
  }
  
  findOneByContacto(contacto:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByContacto/' + contacto + '?' + attributes);
  }
  
  findOneByObservacion(observacion:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByObservacion/' + observacion + '?' + attributes);
  }
  
  findOneByTema(tema:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByTema/' + tema + '?' + attributes);
  }
  
  findOneByFechaCumplimiento(fechaCumplimiento:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByFechaCumplimiento/' + fechaCumplimiento + '?' + attributes);
  }
  
  findOneByFechaModificacion(fechaModificacion:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByFechaModificacion/' + fechaModificacion + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdMatrizByUid(Id:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByUid?Id=' + Id, vdMatriz);
  }
  
  updateVdMatrizById(id:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizById?id=' + id, vdMatriz);
  }
  
  updateVdMatrizByEstadoCumplimiento(estadoCumplimiento:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByEstadoCumplimiento?estadoCumplimiento=' + estadoCumplimiento, vdMatriz);
  }
  
  updateVdMatrizByCreatedbyid(createdbyid:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByCreatedbyid?createdbyid=' + createdbyid, vdMatriz);
  }
  
  updateVdMatrizByUpdatedbyid(updatedbyid:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByUpdatedbyid?updatedbyid=' + updatedbyid, vdMatriz);
  }
  
  updateVdMatrizByCompromiso(compromiso:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByCompromiso?compromiso=' + compromiso, vdMatriz);
  }
  
  updateVdMatrizByEntidadResponsable(entidadResponsable:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByEntidadResponsable?entidadResponsable=' + entidadResponsable, vdMatriz);
  }
  
  updateVdMatrizByContacto(contacto:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByContacto?contacto=' + contacto, vdMatriz);
  }
  
  updateVdMatrizByObservacion(observacion:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByObservacion?observacion=' + observacion, vdMatriz);
  }
  
  updateVdMatrizByTema(tema:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByTema?tema=' + tema, vdMatriz);
  }
  
  updateVdMatrizByFechaCumplimiento(fechaCumplimiento:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByFechaCumplimiento?fechaCumplimiento=' + fechaCumplimiento, vdMatriz);
  }
  
  updateVdMatrizByFechaModificacion(fechaModificacion:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByFechaModificacion?fechaModificacion=' + fechaModificacion, vdMatriz);
  }
  
  updateVdMatrizByDueat(dueat:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByDueat?dueat=' + dueat, vdMatriz);
  }
  
  updateVdMatrizByCreatedat(createdat:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByCreatedat?createdat=' + createdat, vdMatriz);
  }
  
  updateVdMatrizByUpdatedat(updatedat:any, vdMatriz:VdsMatriz) {
      return this.http.post(this.basePath + '/updateVdMatrizByUpdatedat?updatedat=' + updatedat, vdMatriz);
  }
  
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  
  filterVdsMatrizByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsMatrizByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsMatrizByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsMatrizByUpdatedby/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
