/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:28 GMT-0400 (Bolivia Time)
 * Time: 0:27:28
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:28 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:28
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsEstadoCivil} from "../models/vdsEstadoCivil";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdEstadoCivilService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-estado-civil`;
  dataChange: BehaviorSubject<VdsEstadoCivil[]> = new BehaviorSubject<VdsEstadoCivil[]>([]);
  vdEstadoCivilData: VdsEstadoCivil = new VdsEstadoCivil();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsEstadoCivil[] {
    return this.dataChange.value;
  }

  getDataVdsEstadoCivil(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsEstadoCivil(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsEstadoCivil[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsEstadoCivil(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdEstadoCivil(vdEstadoCivil:VdsEstadoCivil) {
    return this.http.post(this.basePath, vdEstadoCivil);
  }
  getVdEstadoCivil(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdEstadoCivil(id:any, vdEstadoCivil:VdsEstadoCivil) {
    return this.http.put(this.basePath + '/' + id, vdEstadoCivil);
  }
  deleteVdEstadoCivil(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstado(estado:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstado/' + estado + '?' + attributes);
  }
  
  findOneByDescripcion(descripcion:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDescripcion/' + descripcion + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdEstadoCivilByUid(Id:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilByUid?Id=' + Id, vdEstadoCivil);
  }
  
  updateVdEstadoCivilById(id:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilById?id=' + id, vdEstadoCivil);
  }
  
  updateVdEstadoCivilByEstado(estado:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilByEstado?estado=' + estado, vdEstadoCivil);
  }
  
  updateVdEstadoCivilByDescripcion(descripcion:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilByDescripcion?descripcion=' + descripcion, vdEstadoCivil);
  }
  
  updateVdEstadoCivilByCreatedby(createdby:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilByCreatedby?createdby=' + createdby, vdEstadoCivil);
  }
  
  updateVdEstadoCivilByUpdatedby(updatedby:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilByUpdatedby?updatedby=' + updatedby, vdEstadoCivil);
  }
  
  updateVdEstadoCivilByDueat(dueat:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilByDueat?dueat=' + dueat, vdEstadoCivil);
  }
  
  updateVdEstadoCivilByCreatedat(createdat:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilByCreatedat?createdat=' + createdat, vdEstadoCivil);
  }
  
  updateVdEstadoCivilByUpdatedat(updatedat:any, vdEstadoCivil:VdsEstadoCivil) {
      return this.http.post(this.basePath + '/updateVdEstadoCivilByUpdatedat?updatedat=' + updatedat, vdEstadoCivil);
  }
  
  
  
  //</es-section>
}
