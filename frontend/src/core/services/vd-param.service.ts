/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:26 GMT-0400 (Bolivia Time)
 * Time: 0:27:26
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:26 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:26
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsParams} from "../models/vdsParams";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdParamService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-params`;
  dataChange: BehaviorSubject<VdsParams[]> = new BehaviorSubject<VdsParams[]>([]);
  vdParamData: VdsParams = new VdsParams();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsParams[] {
    return this.dataChange.value;
  }

  getDataVdsParams(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsParams(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsParams[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsParams(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdParam(vdParam:VdsParams) {
    return this.http.post(this.basePath, vdParam);
  }
  getVdParam(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdParam(id:any, vdParam:VdsParams) {
    return this.http.put(this.basePath + '/' + id, vdParam);
  }
  deleteVdParam(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByParOrder(parOrder:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByParOrder/' + parOrder + '?' + attributes);
  }
  
  findOneByParCod(parCod:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByParCod/' + parCod + '?' + attributes);
  }
  
  findOneByParDescription(parDescription:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByParDescription/' + parDescription + '?' + attributes);
  }
  
  findOneByParAbbr(parAbbr:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByParAbbr/' + parAbbr + '?' + attributes);
  }
  
  findOneByParGroup(parGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByParGroup/' + parGroup + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByParDictionaryId(parDictionaryId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByParDictionaryId/' + parDictionaryId + '?' + attributes);
  }
  
  findOneByParStatusId(parStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByParStatusId/' + parStatusId + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdParamByUid(Id:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByUid?Id=' + Id, vdParam);
  }
  
  updateVdParamById(id:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamById?id=' + id, vdParam);
  }
  
  updateVdParamByParOrder(parOrder:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByParOrder?parOrder=' + parOrder, vdParam);
  }
  
  updateVdParamByParCod(parCod:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByParCod?parCod=' + parCod, vdParam);
  }
  
  updateVdParamByParDescription(parDescription:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByParDescription?parDescription=' + parDescription, vdParam);
  }
  
  updateVdParamByParAbbr(parAbbr:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByParAbbr?parAbbr=' + parAbbr, vdParam);
  }
  
  updateVdParamByParGroup(parGroup:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByParGroup?parGroup=' + parGroup, vdParam);
  }
  
  updateVdParamByCreatedbyid(createdbyid:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByCreatedbyid?createdbyid=' + createdbyid, vdParam);
  }
  
  updateVdParamByUpdatedbyid(updatedbyid:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByUpdatedbyid?updatedbyid=' + updatedbyid, vdParam);
  }
  
  updateVdParamByParDictionaryId(parDictionaryId:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByParDictionaryId?parDictionaryId=' + parDictionaryId, vdParam);
  }
  
  updateVdParamByParStatusId(parStatusId:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByParStatusId?parStatusId=' + parStatusId, vdParam);
  }
  
  updateVdParamByDueat(dueat:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByDueat?dueat=' + dueat, vdParam);
  }
  
  updateVdParamByCreatedat(createdat:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByCreatedat?createdat=' + createdat, vdParam);
  }
  
  updateVdParamByUpdatedat(updatedat:any, vdParam:VdsParams) {
      return this.http.post(this.basePath + '/updateVdParamByUpdatedat?updatedat=' + updatedat, vdParam);
  }
  
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsDictionariesParDictionaryWithDicCode(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsDictionariesParDictionaryWithDicCode' + '?' + attributes);
  }
  
  findVdsParamsParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsParStatusWithParOrder' + '?' + attributes);
  }
  
  
  filterVdsParamsByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsParamsByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsParamsByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsParamsByUpdatedby/' + ids + '?' + attributes);
  }
  
  filterVdsParamsByParDictionary(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsParamsByParDictionary/' + ids + '?' + attributes);
  }
  
  filterVdsParamsByParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsParamsByParStatus/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
