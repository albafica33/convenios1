/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:26 GMT-0400 (Bolivia Time)
 * Time: 0:27:26
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:26 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:26
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsMails} from "../models/vdsMails";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdMailService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-mails`;
  dataChange: BehaviorSubject<VdsMails[]> = new BehaviorSubject<VdsMails[]>([]);
  vdMailData: VdsMails = new VdsMails();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsMails[] {
    return this.dataChange.value;
  }

  getDataVdsMails(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsMails(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsMails[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsMails(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdMail(vdMail:VdsMails) {
    return this.http.post(this.basePath, vdMail);
  }
  getVdMail(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdMail(id:any, vdMail:VdsMails) {
    return this.http.put(this.basePath + '/' + id, vdMail);
  }
  deleteVdMail(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByMaiPort(maiPort:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiPort/' + maiPort + '?' + attributes);
  }
  
  findOneByMaiDescription(maiDescription:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiDescription/' + maiDescription + '?' + attributes);
  }
  
  findOneByMaiUserAccount(maiUserAccount:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiUserAccount/' + maiUserAccount + '?' + attributes);
  }
  
  findOneByMaiUserPassword(maiUserPassword:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiUserPassword/' + maiUserPassword + '?' + attributes);
  }
  
  findOneByMaiHost(maiHost:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiHost/' + maiHost + '?' + attributes);
  }
  
  findOneByMaiProtocol(maiProtocol:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiProtocol/' + maiProtocol + '?' + attributes);
  }
  
  findOneByMaiBusId(maiBusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiBusId/' + maiBusId + '?' + attributes);
  }
  
  findOneByMaiParStatusId(maiParStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiParStatusId/' + maiParStatusId + '?' + attributes);
  }
  
  findOneByMaiGroup(maiGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiGroup/' + maiGroup + '?' + attributes);
  }
  
  findOneByMaiSubject(maiSubject:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiSubject/' + maiSubject + '?' + attributes);
  }
  
  findOneByMaiTo(maiTo:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiTo/' + maiTo + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByMaiBcc(maiBcc:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiBcc/' + maiBcc + '?' + attributes);
  }
  
  findOneByMaiCc(maiCc:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiCc/' + maiCc + '?' + attributes);
  }
  
  findOneByMaiText(maiText:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiText/' + maiText + '?' + attributes);
  }
  
  findOneByMaiHtml(maiHtml:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaiHtml/' + maiHtml + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdMailByUid(Id:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByUid?Id=' + Id, vdMail);
  }
  
  updateVdMailById(id:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailById?id=' + id, vdMail);
  }
  
  updateVdMailByMaiPort(maiPort:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiPort?maiPort=' + maiPort, vdMail);
  }
  
  updateVdMailByMaiDescription(maiDescription:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiDescription?maiDescription=' + maiDescription, vdMail);
  }
  
  updateVdMailByMaiUserAccount(maiUserAccount:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiUserAccount?maiUserAccount=' + maiUserAccount, vdMail);
  }
  
  updateVdMailByMaiUserPassword(maiUserPassword:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiUserPassword?maiUserPassword=' + maiUserPassword, vdMail);
  }
  
  updateVdMailByMaiHost(maiHost:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiHost?maiHost=' + maiHost, vdMail);
  }
  
  updateVdMailByMaiProtocol(maiProtocol:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiProtocol?maiProtocol=' + maiProtocol, vdMail);
  }
  
  updateVdMailByMaiBusId(maiBusId:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiBusId?maiBusId=' + maiBusId, vdMail);
  }
  
  updateVdMailByMaiParStatusId(maiParStatusId:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiParStatusId?maiParStatusId=' + maiParStatusId, vdMail);
  }
  
  updateVdMailByMaiGroup(maiGroup:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiGroup?maiGroup=' + maiGroup, vdMail);
  }
  
  updateVdMailByMaiSubject(maiSubject:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiSubject?maiSubject=' + maiSubject, vdMail);
  }
  
  updateVdMailByMaiTo(maiTo:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiTo?maiTo=' + maiTo, vdMail);
  }
  
  updateVdMailByUpdatedby(updatedby:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByUpdatedby?updatedby=' + updatedby, vdMail);
  }
  
  updateVdMailByCreatedby(createdby:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByCreatedby?createdby=' + createdby, vdMail);
  }
  
  updateVdMailByMaiBcc(maiBcc:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiBcc?maiBcc=' + maiBcc, vdMail);
  }
  
  updateVdMailByMaiCc(maiCc:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiCc?maiCc=' + maiCc, vdMail);
  }
  
  updateVdMailByMaiText(maiText:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiText?maiText=' + maiText, vdMail);
  }
  
  updateVdMailByMaiHtml(maiHtml:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByMaiHtml?maiHtml=' + maiHtml, vdMail);
  }
  
  updateVdMailByDueat(dueat:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByDueat?dueat=' + dueat, vdMail);
  }
  
  updateVdMailByCreatedat(createdat:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByCreatedat?createdat=' + createdat, vdMail);
  }
  
  updateVdMailByUpdatedat(updatedat:any, vdMail:VdsMails) {
      return this.http.post(this.basePath + '/updateVdMailByUpdatedat?updatedat=' + updatedat, vdMail);
  }
  
  
  findVdsParamsMaiParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsMaiParStatusWithParOrder' + '?' + attributes);
  }
  
  
  filterVdsMailsByMaiParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsMailsByMaiParStatus/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
