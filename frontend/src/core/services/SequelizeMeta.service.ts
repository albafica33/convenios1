/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:32:45 GMT-0400 (Bolivia Time)
 * Time: 0:32:45
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:32:45 GMT-0400 (Bolivia Time)
 * Last time updated: 0:32:45
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {Sequelizemeta} from "../models/sequelizemeta";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class SequelizemetaService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/sequelizemeta`;
  dataChange: BehaviorSubject<Sequelizemeta[]> = new BehaviorSubject<Sequelizemeta[]>([]);
  sequelizemetaData: Sequelizemeta = new Sequelizemeta();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): Sequelizemeta[] {
    return this.dataChange.value;
  }

  getDataSequelizemeta(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllSequelizemeta(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:Sequelizemeta[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllSequelizemeta(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createSequelizemeta(sequelizemeta:Sequelizemeta) {
    return this.http.post(this.basePath, sequelizemeta);
  }
  getSequelizemeta(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateSequelizemeta(id:any, sequelizemeta:Sequelizemeta) {
    return this.http.put(this.basePath + '/' + id, sequelizemeta);
  }
  deleteSequelizemeta(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByName(name:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByName/' + name + '?' + attributes);
  }
  
  
  updateSequelizemetaByName(name:any, sequelizemeta:Sequelizemeta) {
      return this.http.post(this.basePath + '/updateSequelizemetaByName?name=' + name, sequelizemeta);
  }
  
  
  
  //</es-section>
}
