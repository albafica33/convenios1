/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:29 GMT-0400 (Bolivia Time)
 * Time: 0:27:29
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:29 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:29
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsProvincia} from "../models/vdsProvincia";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdProvinciaService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-provincia`;
  dataChange: BehaviorSubject<VdsProvincia[]> = new BehaviorSubject<VdsProvincia[]>([]);
  vdProvinciaData: VdsProvincia = new VdsProvincia();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsProvincia[] {
    return this.dataChange.value;
  }

  getDataVdsProvincia(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsProvincia(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsProvincia[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsProvincia(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdProvincia(vdProvincia:VdsProvincia) {
    return this.http.post(this.basePath, vdProvincia);
  }
  getVdProvincia(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdProvincia(id:any, vdProvincia:VdsProvincia) {
    return this.http.put(this.basePath + '/' + id, vdProvincia);
  }
  deleteVdProvincia(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstado(estado:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstado/' + estado + '?' + attributes);
  }
  
  findOneByProvincia(provincia:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByProvincia/' + provincia + '?' + attributes);
  }
  
  findOneByAbreviacion(abreviacion:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByAbreviacion/' + abreviacion + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByIdCiudad(idCiudad:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdCiudad/' + idCiudad + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdProvinciaByUid(Id:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByUid?Id=' + Id, vdProvincia);
  }
  
  updateVdProvinciaById(id:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaById?id=' + id, vdProvincia);
  }
  
  updateVdProvinciaByEstado(estado:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByEstado?estado=' + estado, vdProvincia);
  }
  
  updateVdProvinciaByProvincia(provincia:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByProvincia?provincia=' + provincia, vdProvincia);
  }
  
  updateVdProvinciaByAbreviacion(abreviacion:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByAbreviacion?abreviacion=' + abreviacion, vdProvincia);
  }
  
  updateVdProvinciaByCreatedby(createdby:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByCreatedby?createdby=' + createdby, vdProvincia);
  }
  
  updateVdProvinciaByUpdatedby(updatedby:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByUpdatedby?updatedby=' + updatedby, vdProvincia);
  }
  
  updateVdProvinciaByIdCiudad(idCiudad:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByIdCiudad?idCiudad=' + idCiudad, vdProvincia);
  }
  
  updateVdProvinciaByDueat(dueat:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByDueat?dueat=' + dueat, vdProvincia);
  }
  
  updateVdProvinciaByCreatedat(createdat:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByCreatedat?createdat=' + createdat, vdProvincia);
  }
  
  updateVdProvinciaByUpdatedat(updatedat:any, vdProvincia:VdsProvincia) {
      return this.http.post(this.basePath + '/updateVdProvinciaByUpdatedat?updatedat=' + updatedat, vdProvincia);
  }
  
  
  findVdsCiudadCiudadWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsCiudadCiudadWithEstado' + '?' + attributes);
  }
  
  
  filterVdsProvinciaByCiudad(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsProvinciaByCiudad/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
