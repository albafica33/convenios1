/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:24 GMT-0400 (Bolivia Time)
 * Time: 0:27:24
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:24 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:24
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsRoles} from "../models/vdsRoles";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdRoleService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-roles`;
  dataChange: BehaviorSubject<VdsRoles[]> = new BehaviorSubject<VdsRoles[]>([]);
  vdRoleData: VdsRoles = new VdsRoles();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsRoles[] {
    return this.dataChange.value;
  }

  getDataVdsRoles(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsRoles(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsRoles[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsRoles(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdRole(vdRole:VdsRoles) {
    return this.http.post(this.basePath, vdRole);
  }
  getVdRole(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdRole(id:any, vdRole:VdsRoles) {
    return this.http.put(this.basePath + '/' + id, vdRole);
  }
  deleteVdRole(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByRolCode(rolCode:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByRolCode/' + rolCode + '?' + attributes);
  }
  
  findOneByRolDescription(rolDescription:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByRolDescription/' + rolDescription + '?' + attributes);
  }
  
  findOneByRolAbbr(rolAbbr:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByRolAbbr/' + rolAbbr + '?' + attributes);
  }
  
  findOneByRolGroup(rolGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByRolGroup/' + rolGroup + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByRolParStatusId(rolParStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByRolParStatusId/' + rolParStatusId + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdRoleByUid(Id:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByUid?Id=' + Id, vdRole);
  }
  
  updateVdRoleById(id:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleById?id=' + id, vdRole);
  }
  
  updateVdRoleByRolCode(rolCode:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByRolCode?rolCode=' + rolCode, vdRole);
  }
  
  updateVdRoleByRolDescription(rolDescription:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByRolDescription?rolDescription=' + rolDescription, vdRole);
  }
  
  updateVdRoleByRolAbbr(rolAbbr:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByRolAbbr?rolAbbr=' + rolAbbr, vdRole);
  }
  
  updateVdRoleByRolGroup(rolGroup:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByRolGroup?rolGroup=' + rolGroup, vdRole);
  }
  
  updateVdRoleByCreatedbyid(createdbyid:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByCreatedbyid?createdbyid=' + createdbyid, vdRole);
  }
  
  updateVdRoleByUpdatedbyid(updatedbyid:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByUpdatedbyid?updatedbyid=' + updatedbyid, vdRole);
  }
  
  updateVdRoleByRolParStatusId(rolParStatusId:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByRolParStatusId?rolParStatusId=' + rolParStatusId, vdRole);
  }
  
  updateVdRoleByDueat(dueat:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByDueat?dueat=' + dueat, vdRole);
  }
  
  updateVdRoleByCreatedat(createdat:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByCreatedat?createdat=' + createdat, vdRole);
  }
  
  updateVdRoleByUpdatedat(updatedat:any, vdRole:VdsRoles) {
      return this.http.post(this.basePath + '/updateVdRoleByUpdatedat?updatedat=' + updatedat, vdRole);
  }
  
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsParamsRolParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsRolParStatusWithParOrder' + '?' + attributes);
  }
  
  
  filterVdsRolesByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsRolesByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsRolesByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsRolesByUpdatedby/' + ids + '?' + attributes);
  }
  
  filterVdsRolesByRolParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsRolesByRolParStatus/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
