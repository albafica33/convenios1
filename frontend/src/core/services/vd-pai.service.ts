/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:27 GMT-0400 (Bolivia Time)
 * Time: 0:27:27
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:27 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:27
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsPais} from "../models/vdsPais";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdPaiService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-pais`;
  dataChange: BehaviorSubject<VdsPais[]> = new BehaviorSubject<VdsPais[]>([]);
  vdPaiData: VdsPais = new VdsPais();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsPais[] {
    return this.dataChange.value;
  }

  getDataVdsPais(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsPais(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsPais[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsPais(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdPai(vdPai:VdsPais) {
    return this.http.post(this.basePath, vdPai);
  }
  getVdPai(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdPai(id:any, vdPai:VdsPais) {
    return this.http.put(this.basePath + '/' + id, vdPai);
  }
  deleteVdPai(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstado(estado:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstado/' + estado + '?' + attributes);
  }
  
  findOneByPais(pais:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPais/' + pais + '?' + attributes);
  }
  
  findOneByAbreviacion(abreviacion:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByAbreviacion/' + abreviacion + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdPaiByUid(Id:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByUid?Id=' + Id, vdPai);
  }
  
  updateVdPaiById(id:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiById?id=' + id, vdPai);
  }
  
  updateVdPaiByEstado(estado:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByEstado?estado=' + estado, vdPai);
  }
  
  updateVdPaiByPais(pais:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByPais?pais=' + pais, vdPai);
  }
  
  updateVdPaiByAbreviacion(abreviacion:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByAbreviacion?abreviacion=' + abreviacion, vdPai);
  }
  
  updateVdPaiByCreatedby(createdby:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByCreatedby?createdby=' + createdby, vdPai);
  }
  
  updateVdPaiByUpdatedby(updatedby:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByUpdatedby?updatedby=' + updatedby, vdPai);
  }
  
  updateVdPaiByDueat(dueat:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByDueat?dueat=' + dueat, vdPai);
  }
  
  updateVdPaiByCreatedat(createdat:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByCreatedat?createdat=' + createdat, vdPai);
  }
  
  updateVdPaiByUpdatedat(updatedat:any, vdPai:VdsPais) {
      return this.http.post(this.basePath + '/updateVdPaiByUpdatedat?updatedat=' + updatedat, vdPai);
  }
  
  
  
  //</es-section>
}
