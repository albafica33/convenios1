/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:31 GMT-0400 (Bolivia Time)
 * Time: 0:27:31
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:31 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:31
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsUsers} from "../models/vdsUsers";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdUserService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-users`;
  dataChange: BehaviorSubject<VdsUsers[]> = new BehaviorSubject<VdsUsers[]>([]);
  vdUserData: VdsUsers = new VdsUsers();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsUsers[] {
    return this.dataChange.value;
  }

  getDataVdsUsers(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsUsers(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsUsers[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsUsers(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdUser(vdUser:VdsUsers) {
    return this.http.post(this.basePath, vdUser);
  }
  getVdUser(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdUser(id:any, vdUser:VdsUsers) {
    return this.http.put(this.basePath + '/' + id, vdUser);
  }
  deleteVdUser(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstado(estado:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstado/' + estado + '?' + attributes);
  }
  
  findOneByUserName(userName:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUserName/' + userName + '?' + attributes);
  }
  
  findOneByUserHash(userHash:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUserHash/' + userHash + '?' + attributes);
  }
  
  findOneByCorreo(correo:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCorreo/' + correo + '?' + attributes);
  }
  
  findOneByRecordatorio(recordatorio:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByRecordatorio/' + recordatorio + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByIdPersona(idPersona:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdPersona/' + idPersona + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdUserByUid(Id:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByUid?Id=' + Id, vdUser);
  }
  
  updateVdUserById(id:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserById?id=' + id, vdUser);
  }
  
  updateVdUserByEstado(estado:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByEstado?estado=' + estado, vdUser);
  }
  
  updateVdUserByUserName(userName:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByUserName?userName=' + userName, vdUser);
  }
  
  updateVdUserByUserHash(userHash:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByUserHash?userHash=' + userHash, vdUser);
  }
  
  updateVdUserByCorreo(correo:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByCorreo?correo=' + correo, vdUser);
  }
  
  updateVdUserByRecordatorio(recordatorio:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByRecordatorio?recordatorio=' + recordatorio, vdUser);
  }
  
  updateVdUserByCreatedby(createdby:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByCreatedby?createdby=' + createdby, vdUser);
  }
  
  updateVdUserByUpdatedby(updatedby:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByUpdatedby?updatedby=' + updatedby, vdUser);
  }
  
  updateVdUserByIdPersona(idPersona:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByIdPersona?idPersona=' + idPersona, vdUser);
  }
  
  updateVdUserByDueat(dueat:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByDueat?dueat=' + dueat, vdUser);
  }
  
  updateVdUserByCreatedat(createdat:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByCreatedat?createdat=' + createdat, vdUser);
  }
  
  updateVdUserByUpdatedat(updatedat:any, vdUser:VdsUsers) {
      return this.http.post(this.basePath + '/updateVdUserByUpdatedat?updatedat=' + updatedat, vdUser);
  }
  
  
  findVdsPersonaPersonaWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsPersonaPersonaWithEstado' + '?' + attributes);
  }
  
  
  filterVdsUsersByPersona(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsUsersByPersona/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
