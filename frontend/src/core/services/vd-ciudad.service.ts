/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:28 GMT-0400 (Bolivia Time)
 * Time: 0:27:28
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:28 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:28
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsCiudad} from "../models/vdsCiudad";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdCiudadService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-ciudad`;
  dataChange: BehaviorSubject<VdsCiudad[]> = new BehaviorSubject<VdsCiudad[]>([]);
  vdCiudadData: VdsCiudad = new VdsCiudad();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsCiudad[] {
    return this.dataChange.value;
  }

  getDataVdsCiudad(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsCiudad(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsCiudad[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsCiudad(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdCiudad(vdCiudad:VdsCiudad) {
    return this.http.post(this.basePath, vdCiudad);
  }
  getVdCiudad(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdCiudad(id:any, vdCiudad:VdsCiudad) {
    return this.http.put(this.basePath + '/' + id, vdCiudad);
  }
  deleteVdCiudad(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstado(estado:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstado/' + estado + '?' + attributes);
  }
  
  findOneByCiudad(ciudad:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCiudad/' + ciudad + '?' + attributes);
  }
  
  findOneByAbrev(abrev:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByAbrev/' + abrev + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByIdPais(idPais:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdPais/' + idPais + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdCiudadByUid(Id:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByUid?Id=' + Id, vdCiudad);
  }
  
  updateVdCiudadById(id:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadById?id=' + id, vdCiudad);
  }
  
  updateVdCiudadByEstado(estado:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByEstado?estado=' + estado, vdCiudad);
  }
  
  updateVdCiudadByCiudad(ciudad:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByCiudad?ciudad=' + ciudad, vdCiudad);
  }
  
  updateVdCiudadByAbrev(abrev:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByAbrev?abrev=' + abrev, vdCiudad);
  }
  
  updateVdCiudadByCreatedby(createdby:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByCreatedby?createdby=' + createdby, vdCiudad);
  }
  
  updateVdCiudadByUpdatedby(updatedby:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByUpdatedby?updatedby=' + updatedby, vdCiudad);
  }
  
  updateVdCiudadByIdPais(idPais:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByIdPais?idPais=' + idPais, vdCiudad);
  }
  
  updateVdCiudadByDueat(dueat:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByDueat?dueat=' + dueat, vdCiudad);
  }
  
  updateVdCiudadByCreatedat(createdat:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByCreatedat?createdat=' + createdat, vdCiudad);
  }
  
  updateVdCiudadByUpdatedat(updatedat:any, vdCiudad:VdsCiudad) {
      return this.http.post(this.basePath + '/updateVdCiudadByUpdatedat?updatedat=' + updatedat, vdCiudad);
  }
  
  
  
  //</es-section>
}
