/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:30 GMT-0400 (Bolivia Time)
 * Time: 0:27:30
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:30 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:30
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsPersona} from "../models/vdsPersona";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdPersonaService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-persona`;
  dataChange: BehaviorSubject<VdsPersona[]> = new BehaviorSubject<VdsPersona[]>([]);
  vdPersonaData: VdsPersona = new VdsPersona();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsPersona[] {
    return this.dataChange.value;
  }

  getDataVdsPersona(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsPersona(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsPersona[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsPersona(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdPersona(vdPersona:VdsPersona) {
    return this.http.post(this.basePath, vdPersona);
  }
  getVdPersona(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdPersona(id:any, vdPersona:VdsPersona) {
    return this.http.put(this.basePath + '/' + id, vdPersona);
  }
  deleteVdPersona(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByEstado(estado:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByEstado/' + estado + '?' + attributes);
  }
  
  findOneByTelefono(telefono:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByTelefono/' + telefono + '?' + attributes);
  }
  
  findOneByCelular(celular:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCelular/' + celular + '?' + attributes);
  }
  
  findOneByNombres(nombres:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByNombres/' + nombres + '?' + attributes);
  }
  
  findOneByPaterno(paterno:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByPaterno/' + paterno + '?' + attributes);
  }
  
  findOneByMaterno(materno:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByMaterno/' + materno + '?' + attributes);
  }
  
  findOneByCasada(casada:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCasada/' + casada + '?' + attributes);
  }
  
  findOneByCi(ci:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCi/' + ci + '?' + attributes);
  }
  
  findOneByCorreo(correo:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCorreo/' + correo + '?' + attributes);
  }
  
  findOneByDireccion(direccion:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDireccion/' + direccion + '?' + attributes);
  }
  
  findOneByCreatedby(createdby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedby/' + createdby + '?' + attributes);
  }
  
  findOneByUpdatedby(updatedby:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedby/' + updatedby + '?' + attributes);
  }
  
  findOneByCiExpedido(ciExpedido:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCiExpedido/' + ciExpedido + '?' + attributes);
  }
  
  findOneByIdEstadoCivil(idEstadoCivil:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdEstadoCivil/' + idEstadoCivil + '?' + attributes);
  }
  
  findOneByIdSexo(idSexo:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdSexo/' + idSexo + '?' + attributes);
  }
  
  findOneByIdMunicipio(idMunicipio:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdMunicipio/' + idMunicipio + '?' + attributes);
  }
  
  findOneByIdProvincia(idProvincia:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdProvincia/' + idProvincia + '?' + attributes);
  }
  
  findOneByIdCiudad(idCiudad:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdCiudad/' + idCiudad + '?' + attributes);
  }
  
  findOneByIdPais(idPais:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByIdPais/' + idPais + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdPersonaByUid(Id:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByUid?Id=' + Id, vdPersona);
  }
  
  updateVdPersonaById(id:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaById?id=' + id, vdPersona);
  }
  
  updateVdPersonaByEstado(estado:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByEstado?estado=' + estado, vdPersona);
  }
  
  updateVdPersonaByTelefono(telefono:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByTelefono?telefono=' + telefono, vdPersona);
  }
  
  updateVdPersonaByCelular(celular:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByCelular?celular=' + celular, vdPersona);
  }
  
  updateVdPersonaByNombres(nombres:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByNombres?nombres=' + nombres, vdPersona);
  }
  
  updateVdPersonaByPaterno(paterno:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByPaterno?paterno=' + paterno, vdPersona);
  }
  
  updateVdPersonaByMaterno(materno:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByMaterno?materno=' + materno, vdPersona);
  }
  
  updateVdPersonaByCasada(casada:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByCasada?casada=' + casada, vdPersona);
  }
  
  updateVdPersonaByCi(ci:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByCi?ci=' + ci, vdPersona);
  }
  
  updateVdPersonaByCorreo(correo:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByCorreo?correo=' + correo, vdPersona);
  }
  
  updateVdPersonaByDireccion(direccion:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByDireccion?direccion=' + direccion, vdPersona);
  }
  
  updateVdPersonaByCreatedby(createdby:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByCreatedby?createdby=' + createdby, vdPersona);
  }
  
  updateVdPersonaByUpdatedby(updatedby:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByUpdatedby?updatedby=' + updatedby, vdPersona);
  }
  
  updateVdPersonaByCiExpedido(ciExpedido:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByCiExpedido?ciExpedido=' + ciExpedido, vdPersona);
  }
  
  updateVdPersonaByIdEstadoCivil(idEstadoCivil:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByIdEstadoCivil?idEstadoCivil=' + idEstadoCivil, vdPersona);
  }
  
  updateVdPersonaByIdSexo(idSexo:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByIdSexo?idSexo=' + idSexo, vdPersona);
  }
  
  updateVdPersonaByIdMunicipio(idMunicipio:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByIdMunicipio?idMunicipio=' + idMunicipio, vdPersona);
  }
  
  updateVdPersonaByIdProvincia(idProvincia:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByIdProvincia?idProvincia=' + idProvincia, vdPersona);
  }
  
  updateVdPersonaByIdCiudad(idCiudad:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByIdCiudad?idCiudad=' + idCiudad, vdPersona);
  }
  
  updateVdPersonaByIdPais(idPais:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByIdPais?idPais=' + idPais, vdPersona);
  }
  
  updateVdPersonaByDueat(dueat:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByDueat?dueat=' + dueat, vdPersona);
  }
  
  updateVdPersonaByCreatedat(createdat:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByCreatedat?createdat=' + createdat, vdPersona);
  }
  
  updateVdPersonaByUpdatedat(updatedat:any, vdPersona:VdsPersona) {
      return this.http.post(this.basePath + '/updateVdPersonaByUpdatedat?updatedat=' + updatedat, vdPersona);
  }
  
  
  findVdsCiudadCiExpedoWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsCiudadCiExpedoWithEstado' + '?' + attributes);
  }
  
  findVdsEstadoCivilEstadoCivilWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsEstadoCivilEstadoCivilWithEstado' + '?' + attributes);
  }
  
  findVdsSexoSexoWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsSexoSexoWithEstado' + '?' + attributes);
  }
  
  findVdsMunicipioMunicipioWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsMunicipioMunicipioWithEstado' + '?' + attributes);
  }
  
  findVdsProvinciaProvinciaWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsProvinciaProvinciaWithEstado' + '?' + attributes);
  }
  
  findVdsCiudadCiudadWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsCiudadCiudadWithEstado' + '?' + attributes);
  }
  
  findVdsPaisPaisWithEstado(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsPaisPaisWithEstado' + '?' + attributes);
  }
  
  
  filterVdsPersonaByCiExpedo(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPersonaByCiExpedo/' + ids + '?' + attributes);
  }
  
  filterVdsPersonaByEstadoCivil(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPersonaByEstadoCivil/' + ids + '?' + attributes);
  }
  
  filterVdsPersonaBySexo(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPersonaBySexo/' + ids + '?' + attributes);
  }
  
  filterVdsPersonaByMunicipio(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPersonaByMunicipio/' + ids + '?' + attributes);
  }
  
  filterVdsPersonaByProvincia(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPersonaByProvincia/' + ids + '?' + attributes);
  }
  
  filterVdsPersonaByCiudad(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPersonaByCiudad/' + ids + '?' + attributes);
  }
  
  filterVdsPersonaByPais(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsPersonaByPais/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
