/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:27:31 GMT-0400 (Bolivia Time)
 * Time: 0:27:31
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:27:31 GMT-0400 (Bolivia Time)
 * Last time updated: 0:27:31
 *
 * Caution: es-sections will be replaced by script execution
 */

import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient,HttpErrorResponse} from "@angular/common/http";
import '../../helpers/utils';
import {BehaviorSubject} from "rxjs";

//<es-section>
import {VdsModules} from "../models/vdsModules";
//</es-section>

@Injectable({
  providedIn: 'root'
})
export class VdModuleService {

  //<es-section>
  basePath: string = `${environment.backend.server.webpath}/api-${environment.system}/vds-modules`;
  dataChange: BehaviorSubject<VdsModules[]> = new BehaviorSubject<VdsModules[]>([]);
  vdModuleData: VdsModules = new VdsModules();
  //</es-section>

  constructor(private http: HttpClient) { }

  get data(): VdsModules[] {
    return this.dataChange.value;
  }

  getDataVdsModules(select = [], where = {}, order = [], limit:number = null, offset:number = null): void {
    this.getAllVdsModules(select, where, order, limit, offset).subscribe(async (res) => {
      let response = res as {status: string, message:string, data:VdsModules[]};
      this.dataChange.next(response.data);
    },(error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
    });
  }

  //<es-section>
  
  getAllVdsModules(select = [], where = {}, order = [], limit:number = null, offset:number = null) {
    let attributes = '';
    if(select.length) {
        attributes += 'select=' + select.toString() + '&';
    }
    if(Object.keys(where).length) {
        attributes += 'where=' + JSON.stringify(where) + '&';
    }
    if(order.length) {
        attributes += 'order=' + JSON.stringify(order) + '&';
    }
    if(limit) {
        attributes += 'limit=' + limit + '&';
    }
    if(offset) {
        attributes += 'offset=' + offset + '&';
    }
    return this.http.get(this.basePath + '?' + attributes);
  }
  createVdModule(vdModule:VdsModules) {
    return this.http.post(this.basePath, vdModule);
  }
  getVdModule(id:any) {
    return this.http.get(this.basePath + '/' + id);
  }
  updateVdModule(id:any, vdModule:VdsModules) {
    return this.http.put(this.basePath + '/' + id, vdModule);
  }
  deleteVdModule(id:any) {
    return this.http.delete(this.basePath + '/' + id);
  }

  
  
  findOneByUid(Id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUid/' + Id + '?' + attributes);
  }
  
  findOneById(id:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneById/' + id + '?' + attributes);
  }
  
  findOneByModCode(modCode:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByModCode/' + modCode + '?' + attributes);
  }
  
  findOneByModDescription(modDescription:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByModDescription/' + modDescription + '?' + attributes);
  }
  
  findOneByModAbbr(modAbbr:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByModAbbr/' + modAbbr + '?' + attributes);
  }
  
  findOneByModIcon(modIcon:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByModIcon/' + modIcon + '?' + attributes);
  }
  
  findOneByModGroup(modGroup:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByModGroup/' + modGroup + '?' + attributes);
  }
  
  findOneByCreatedbyid(createdbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedbyid/' + createdbyid + '?' + attributes);
  }
  
  findOneByUpdatedbyid(updatedbyid:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedbyid/' + updatedbyid + '?' + attributes);
  }
  
  findOneByModParentId(modParentId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByModParentId/' + modParentId + '?' + attributes);
  }
  
  findOneByModParStatusId(modParStatusId:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByModParStatusId/' + modParStatusId + '?' + attributes);
  }
  
  findOneByDueat(dueat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByDueat/' + dueat + '?' + attributes);
  }
  
  findOneByCreatedat(createdat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByCreatedat/' + createdat + '?' + attributes);
  }
  
  findOneByUpdatedat(updatedat:any, select = []) {
      let attributes = '';
      if(select.length) {
          attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findOneByUpdatedat/' + updatedat + '?' + attributes);
  }
  
  
  updateVdModuleByUid(Id:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByUid?Id=' + Id, vdModule);
  }
  
  updateVdModuleById(id:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleById?id=' + id, vdModule);
  }
  
  updateVdModuleByModCode(modCode:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByModCode?modCode=' + modCode, vdModule);
  }
  
  updateVdModuleByModDescription(modDescription:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByModDescription?modDescription=' + modDescription, vdModule);
  }
  
  updateVdModuleByModAbbr(modAbbr:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByModAbbr?modAbbr=' + modAbbr, vdModule);
  }
  
  updateVdModuleByModIcon(modIcon:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByModIcon?modIcon=' + modIcon, vdModule);
  }
  
  updateVdModuleByModGroup(modGroup:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByModGroup?modGroup=' + modGroup, vdModule);
  }
  
  updateVdModuleByCreatedbyid(createdbyid:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByCreatedbyid?createdbyid=' + createdbyid, vdModule);
  }
  
  updateVdModuleByUpdatedbyid(updatedbyid:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByUpdatedbyid?updatedbyid=' + updatedbyid, vdModule);
  }
  
  updateVdModuleByModParentId(modParentId:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByModParentId?modParentId=' + modParentId, vdModule);
  }
  
  updateVdModuleByModParStatusId(modParStatusId:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByModParStatusId?modParStatusId=' + modParStatusId, vdModule);
  }
  
  updateVdModuleByDueat(dueat:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByDueat?dueat=' + dueat, vdModule);
  }
  
  updateVdModuleByCreatedat(createdat:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByCreatedat?createdat=' + createdat, vdModule);
  }
  
  updateVdModuleByUpdatedat(updatedat:any, vdModule:VdsModules) {
      return this.http.post(this.basePath + '/updateVdModuleByUpdatedat?updatedat=' + updatedat, vdModule);
  }
  
  
  findVdsUserRolesCreatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesCreatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsUserRolesUpdatedbyWithUsrRolGroup(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsUserRolesUpdatedbyWithUsrRolGroup' + '?' + attributes);
  }
  
  findVdsParamsModParStatusWithParOrder(select = []) {
      let attributes = '';
      if(select.length) {
           attributes += 'select=' + select.toString();
      }
      return this.http.get(this.basePath + '/findVdsParamsModParStatusWithParOrder' + '?' + attributes);
  }
  
  
  filterVdsModulesByCreatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsModulesByCreatedby/' + ids + '?' + attributes);
  }
  
  filterVdsModulesByUpdatedby(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsModulesByUpdatedby/' + ids + '?' + attributes);
  }
  
  filterVdsModulesByModParStatus(ids:any, level:number = 0, select = [], order = []) {
        let attributes = '';
        if(select.length) {
             attributes += 'select=' + select.toString() + '&';
        }
        if(order.length) {
             attributes += 'order=' + JSON.stringify(order) + '&';
        }
        if(level) {
             attributes += 'level=' + level + '&';
        }
        return this.http.get(this.basePath + '/filterVdsModulesByModParStatus/' + ids + '?' + attributes);
  }
  
  //</es-section>
}
