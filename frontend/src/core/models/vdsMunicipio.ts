/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:49 GMT-0400 (Bolivia Time)
 * Time: 0:28:49
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:49 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:49
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsCiudad} from "./vdsCiudad";

//</es-section>

export class VdsMunicipio {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  estado: number;
  
  
  
  municipio: string;
  
  createdBy: string;
  
  updatedBy: string;
  
  
  
  id_ciudad: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdMunicipioCiudad:VdsCiudad;
  

  //</es-section>
}
