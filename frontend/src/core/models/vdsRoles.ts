/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:46 GMT-0400 (Bolivia Time)
 * Time: 0:28:46
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:46 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:46
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsUserRoles} from "./vdsUserRoles";

import {VdsParams} from "./vdsParams";

//</es-section>

export class VdsRoles {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  rol_code: string;
  
  rol_description: string;
  
  rol_abbr: string;
  
  rol_group: string;
  
  
  
  createdById: string;
  
  updatedById: string;
  
  rol_par_status_id: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdRoleCreatedby:VdsUserRoles;
  
  vdRoleUpdatedby:VdsUserRoles;
  
  vdRoleRolParStatus:VdsParams;
  

  //</es-section>
}
