/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:53 GMT-0400 (Bolivia Time)
 * Time: 0:28:53
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:53 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:53
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsUsers} from "./vdsUsers";

import {VdsMatriz} from "./vdsMatriz";

import {VdsUserRoles} from "./vdsUserRoles";

//</es-section>

export class VdsRegUsuario {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  
  
  id_usuario: string;
  
  id_reg: string;
  
  createdById: string;
  
  updatedById: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdRegUsuarioUsuario:VdsUsers;
  
  vdRegUsuarioReg:VdsMatriz;
  
  vdRegUsuarioCreatedby:VdsUserRoles;
  
  vdRegUsuarioUpdatedby:VdsUserRoles;
  

  //</es-section>
}
