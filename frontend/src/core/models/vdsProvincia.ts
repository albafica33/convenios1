/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:50 GMT-0400 (Bolivia Time)
 * Time: 0:28:50
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:50 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:50
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsCiudad} from "./vdsCiudad";

//</es-section>

export class VdsProvincia {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  estado: number;
  
  
  
  provincia: string;
  
  abreviacion: string;
  
  createdBy: string;
  
  updatedBy: string;
  
  
  
  id_ciudad: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdProvinciaCiudad:VdsCiudad;
  

  //</es-section>
}
