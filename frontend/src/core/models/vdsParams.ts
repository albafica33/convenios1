/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:48 GMT-0400 (Bolivia Time)
 * Time: 0:28:48
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:48 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:48
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsUserRoles} from "./vdsUserRoles";

import {VdsDictionaries} from "./vdsDictionaries";

//</es-section>

export class VdsParams {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  par_order: number;
  
  
  
  par_cod: string;
  
  par_description: string;
  
  par_abbr: string;
  
  par_group: string;
  
  
  
  createdById: string;
  
  updatedById: string;
  
  par_dictionary_id: string;
  
  par_status_id: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdParamCreatedby:VdsUserRoles;
  
  vdParamUpdatedby:VdsUserRoles;
  
  vdParamParDictionary:VdsDictionaries;
  
  vdParamParStatus:VdsParams;
  

  //</es-section>
}
