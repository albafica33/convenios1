/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:53 GMT-0400 (Bolivia Time)
 * Time: 0:28:53
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:53 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:53
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsUsers} from "./vdsUsers";

import {VdsRoles} from "./vdsRoles";

import {VdsParams} from "./vdsParams";

//</es-section>

export class VdsUserRoles {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  usr_rol_group: string;
  
  
  
  usr_id: string;
  
  rol_id: string;
  
  usr_rol_par_status_id: string;
  
  createdById: string;
  
  updatedById: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdUserRoleUsr:VdsUsers;
  
  vdUserRoleRol:VdsRoles;
  
  vdUserRoleUsrRolParStatus:VdsParams;
  
  vdUserRoleCreatedby:VdsUserRoles;
  
  vdUserRoleUpdatedby:VdsUserRoles;
  

  //</es-section>
}
