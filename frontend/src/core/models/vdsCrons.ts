/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:51 GMT-0400 (Bolivia Time)
 * Time: 0:28:51
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:51 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:51
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

//</es-section>

export class VdsCrons {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  cro_description: string;
  
  cro_expression: string;
  
  cro_group: string;
  
  cro_mai_id: string;
  
  createdBy: string;
  
  updatedBy: string;
  
  cro_function: string;
  
  
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  cro_status: number;
  
  
  
  
  

  //</es-section>
}
