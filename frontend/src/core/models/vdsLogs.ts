/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:48 GMT-0400 (Bolivia Time)
 * Time: 0:28:48
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:48 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:48
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsUserRoles} from "./vdsUserRoles";

import {VdsParams} from "./vdsParams";

//</es-section>

export class VdsLogs {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  log_obj_id: string;
  
  log_description: string;
  
  log_group: string;
  
  
  
  createdById: string;
  
  updatedById: string;
  
  log_par_status_id: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdLogCreatedby:VdsUserRoles;
  
  vdLogUpdatedby:VdsUserRoles;
  
  vdLogLogParStatus:VdsParams;
  

  //</es-section>
}
