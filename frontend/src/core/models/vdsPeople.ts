/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:46 GMT-0400 (Bolivia Time)
 * Time: 0:28:46
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:46 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:46
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsUserRoles} from "./vdsUserRoles";

import {VdsParams} from "./vdsParams";

//</es-section>

export class VdsPeople {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  per_first_name: string;
  
  per_second_name: string;
  
  per_first_lastname: string;
  
  per_second_lastname: string;
  
  per_license: string;
  
  per_license_comp: string;
  
  per_home_address: string;
  
  per_mail: string;
  
  per_home_phone: string;
  
  per_cellphone: string;
  
  per_group: string;
  
  
  
  createdById: string;
  
  updatedById: string;
  
  per_parent_id: string;
  
  per_par_type_doc_id: string;
  
  per_par_city_id: string;
  
  per_par_sex_id: string;
  
  per_par_country_id: string;
  
  per_par_nacionality_id: string;
  
  per_par_status_id: string;
  
  
  per_birthday: Date;
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdPersonCreatedby:VdsUserRoles;
  
  vdPersonUpdatedby:VdsUserRoles;
  
  vdPersonPerParent:VdsPeople;
  
  vdPersonPerParTypeDoc:VdsParams;
  
  vdPersonPerParCity:VdsParams;
  
  vdPersonPerParSex:VdsParams;
  
  vdPersonPerParCountry:VdsParams;
  
  vdPersonPerParNacionality:VdsParams;
  
  vdPersonPerParStatus:VdsParams;
  

  //</es-section>
}
