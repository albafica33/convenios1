/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:47 GMT-0400 (Bolivia Time)
 * Time: 0:28:47
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:47 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:47
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsParams} from "./vdsParams";

//</es-section>

export class VdsMails {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  mai_port: number;
  
  
  
  mai_description: string;
  
  mai_user_account: string;
  
  mai_user_password: string;
  
  mai_host: string;
  
  mai_protocol: string;
  
  mai_bus_id: string;
  
  mai_group: string;
  
  mai_subject: string;
  
  mai_to: string;
  
  updatedBy: string;
  
  createdBy: string;
  
  
  mai_bcc: string;
  
  mai_cc: string;
  
  mai_text: string;
  
  mai_html: string;
  
  
  mai_par_status_id: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdMailMaiParStatus:VdsParams;
  

  //</es-section>
}
