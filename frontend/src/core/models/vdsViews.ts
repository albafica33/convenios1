/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:52 GMT-0400 (Bolivia Time)
 * Time: 0:28:52
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:52 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:52
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsModules} from "./vdsModules";

import {VdsParams} from "./vdsParams";

//</es-section>

export class VdsViews {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  vie_code: string;
  
  vie_description: string;
  
  vie_route: string;
  
  vie_params: string;
  
  vie_icon: string;
  
  vie_group: string;
  
  createdBy: string;
  
  updatedBy: string;
  
  
  
  vie_module_id: string;
  
  vie_return_id: string;
  
  vie_par_status_id: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdViewVieModule:VdsModules;
  
  vdViewVieReturn:VdsViews;
  
  vdViewVieParStatus:VdsParams;
  

  //</es-section>
}
