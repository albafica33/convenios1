/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:51 GMT-0400 (Bolivia Time)
 * Time: 0:28:51
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:51 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:51
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsPersona} from "./vdsPersona";

//</es-section>

export class VdsUsers {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  estado: number;
  
  
  
  user_name: string;
  
  user_hash: string;
  
  correo: string;
  
  recordatorio: string;
  
  createdBy: string;
  
  updatedBy: string;
  
  
  
  id_persona: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdUserPersona:VdsPersona;
  

  //</es-section>
}
