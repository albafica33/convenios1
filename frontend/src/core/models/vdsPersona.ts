/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:50 GMT-0400 (Bolivia Time)
 * Time: 0:28:50
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:50 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:50
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsCiudad} from "./vdsCiudad";

import {VdsEstadoCivil} from "./vdsEstadoCivil";

import {VdsSexo} from "./vdsSexo";

import {VdsMunicipio} from "./vdsMunicipio";

import {VdsProvincia} from "./vdsProvincia";

import {VdsPais} from "./vdsPais";

//</es-section>

export class VdsPersona {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  estado: number;
  
  
  
  nombres: string;
  
  paterno: string;
  
  materno: string;
  
  casada: string;
  
  ci: string;
  
  correo: string;
  
  direccion: string;
  
  createdBy: string;
  
  updatedBy: string;
  
  
  
  ci_expedido: string;
  
  id_estado_civil: string;
  
  id_sexo: string;
  
  id_municipio: string;
  
  id_provincia: string;
  
  id_ciudad: string;
  
  id_pais: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  telefono: number;
  
  celular: number;
  
  
  
  
  
  vdPersonaCiExpedo:VdsCiudad;
  
  vdPersonaEstadoCivil:VdsEstadoCivil;
  
  vdPersonaSexo:VdsSexo;
  
  vdPersonaMunicipio:VdsMunicipio;
  
  vdPersonaProvincia:VdsProvincia;
  
  vdPersonaCiudad:VdsCiudad;
  
  vdPersonaPais:VdsPais;
  

  //</es-section>
}
