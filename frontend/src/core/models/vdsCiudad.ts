/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:49 GMT-0400 (Bolivia Time)
 * Time: 0:28:49
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:49 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:49
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

//</es-section>

export class VdsCiudad {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  estado: number;
  
  
  
  ciudad: string;
  
  abrev: string;
  
  createdBy: string;
  
  updatedBy: string;
  
  id_pais: string;
  
  
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  

  //</es-section>
}
