/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:52 GMT-0400 (Bolivia Time)
 * Time: 0:28:52
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:52 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:52
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsUserRoles} from "./vdsUserRoles";

import {VdsParams} from "./vdsParams";

//</es-section>

export class VdsModules {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  mod_code: string;
  
  mod_description: string;
  
  mod_abbr: string;
  
  mod_icon: string;
  
  mod_group: string;
  
  mod_parent_id: string;
  
  
  
  createdById: string;
  
  updatedById: string;
  
  mod_par_status_id: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdModuleCreatedby:VdsUserRoles;
  
  vdModuleUpdatedby:VdsUserRoles;
  
  vdModuleModParStatus:VdsParams;
  

  //</es-section>
}
