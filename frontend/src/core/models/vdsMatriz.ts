/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:53 GMT-0400 (Bolivia Time)
 * Time: 0:28:53
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:53 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:53
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsUserRoles} from "./vdsUserRoles";

//</es-section>

export class VdsMatriz {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  estado_cumplimiento: number;
  
  
  
  
  compromiso: string;
  
  entidad_responsable: string;
  
  contacto: string;
  
  observacion: string;
  
  tema: string;
  
  fecha_cumplimiento: string;
  
  
  createdById: string;
  
  updatedById: string;
  
  
  fecha_modificacion: Date;
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdMatrizCreatedby:VdsUserRoles;
  
  vdMatrizUpdatedby:VdsUserRoles;
  

  //</es-section>
}
