/**
 * Created by @ES Express Systems
 * User: Rafael Gutierrez Gaspar
 * Date: Thu Jun 10 2021 00:28:44 GMT-0400 (Bolivia Time)
 * Time: 0:28:44
 * Last User updated: Rafael Gutierrez Gaspar
 * Last date updated: Thu Jun 10 2021 00:28:44 GMT-0400 (Bolivia Time)
 * Last time updated: 0:28:44
 *
 * Caution: es-sections will be replaced by script execution
 */

//<es-section>

import {VdsParams} from "./vdsParams";

import {VdsUserRoles} from "./vdsUserRoles";

//</es-section>

export class VdsDictionaries {

  //<es-section>
  
  _id: string;
  
  
  id: number;
  
  
  
  
  dic_code: string;
  
  dic_description: string;
  
  dic_group: string;
  
  
  
  dic_par_status_id: string;
  
  createdById: string;
  
  updatedById: string;
  
  
  dueAt: Date;
  
  createdAt: Date;
  
  updatedAt: Date;
  
  
  
  
  
  
  
  
  
  vdDictionaryDicParStatus:VdsParams;
  
  vdDictionaryCreatedby:VdsUserRoles;
  
  vdDictionaryUpdatedby:VdsUserRoles;
  

  //</es-section>
}
