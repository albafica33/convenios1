import { Injectable } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {VdsUsers} from "../../core/models/vdsUsers";
import {CanActivate, Router} from "@angular/router";
import {ConvenioService} from "./convenio.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService{
  
  baseUrl = environment.backend.server.webpath+'/api-convenios/';
  currentUser: VdsUsers;
  isLoggedIn:boolean;
  
  constructor(
    private http: HttpClient,
    private router: Router,
    public convenioService: ConvenioService,
  ) { }
  // tslint:disable-next-line:typedef
  getUserDetails() {
    return localStorage.getItem('userData') ? JSON.parse(<string> localStorage.getItem('userData')) : null;
  }
  // tslint:disable-next-line:typedef
  setDataInLocalStorage(variableName: string, data: string) {
    localStorage.setItem(variableName, data);
  }
  getToken() {
    return localStorage.getItem('token');
  }
  clearStorage() {
    localStorage.clear();
  }
  
  
  async login(loginForm:FormGroup) {
    this.http.post(this.baseUrl + 'auths/rest-login', loginForm.value).subscribe(async res => {
      let responseLogin = res as { status: string, message: string, data: VdsUsers };
      if (responseLogin.data) {
        this.currentUser = responseLogin.data;
        sessionStorage.setItem('userData', JSON.stringify(responseLogin.data));
        localStorage.setItem('userData', JSON.stringify(responseLogin.data));
        this.router.navigate(['/dashboard']);
        this.isLoggedIn = true;
        this.convenioService.showToolBar = true;
        this.convenioService.showSideBar = true;
      }
    })
  }
  
  async signup(signupForm:FormGroup) {
    this.http.post(this.baseUrl + 'auths/rest-signup', signupForm.value).subscribe(async res => {
      let responseSignup = res as { status: string, message: string, data: VdsUsers };
      if (responseSignup.data) {
        this.currentUser = responseSignup.data;
        this.isLoggedIn = true;
        localStorage.setItem('userData', JSON.stringify(responseSignup.data));
        sessionStorage.setItem('userData', JSON.stringify(responseSignup.data));
        this.router.navigate(['/dashboard']);
        this.convenioService.showToolBar = true;
        this.convenioService.showSideBar = true;
      }
    })
  }
  
  logout() {
    return this.http.get(this.baseUrl + 'auths/rest-logout', {}).subscribe(res => {
      let response = res as { status: string, message: string, data: VdsUsers };
      // this.updateAuth()
      localStorage.setItem('userData', null);
      sessionStorage.setItem('userData', null);
      this.currentUser = null;
      this.isLoggedIn = false;
      this.router.navigate(['/login']);
    });
  }

  isAuthenticated() : boolean {
    let userData = localStorage.getItem('userData');
    userData = sessionStorage.getItem('userData');
    this.currentUser = JSON.parse(userData);
    if(this.currentUser){
      return true;
    }
    return false;
  }
}
