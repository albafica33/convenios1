import { Injectable } from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(
    private authService:AuthService,
    private router:Router
  ) { }
  
  
  canActivate(){
    let isAuthenteticated:boolean = false;
    if(localStorage.length) {
      isAuthenteticated = this.authService.isAuthenticated();
    }
    if (isAuthenteticated) {
      return true;
    } else {
      this.router.navigate(['/login']);
    }
    return false;
  }
  
}
