import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentsComponent } from './contents.component';
import {ContentsRoutingModule} from "./contents-routing.module";
import { DashboardComponent } from './dashboard/dashboard.component';
import { VdsMatrizComponent } from './crud/vds-matriz/vds-matriz.component';
import { VdsUsersComponent } from './crud/vds-users/vds-users.component';
import { VdsPeopleComponent } from './crud/vds-people/vds-people.component';
import {DeleteVdsMatrizComponent} from "./crud/vds-matriz/delete/delete-vds-matriz.component";
import {AddVdsMatrizComponent} from "./crud/vds-matriz/add/add-vds-matriz.component";
import {AddVdsPeopleComponent} from "./crud/vds-people/add/add-vds-people.component";
import {DeleteVdsPeopleComponent} from "./crud/vds-people/delete/delete-vds-people.component";
import {DeleteVdsUsersComponent} from "./crud/vds-users/delete/delete-vds-users.component";
import {AddVdsUsersComponent} from "./crud/vds-users/add/add-vds-users.component";
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule} from "@angular/material/form-field";
import {MAT_DATE_FORMATS} from "@angular/material/core";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatToolbarModule} from "@angular/material/toolbar";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MultilevelMenuService, NgMaterialMultilevelMenuModule} from "ng-material-multilevel-menu";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatMenuModule} from "@angular/material/menu";
import {LoginComponent} from "./auth/login/login.component";
import {RegisterComponent} from "./auth/register/register.component";
import {MatInputModule} from "@angular/material/input";
import {MatSortModule} from "@angular/material/sort";

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};


@NgModule({
  declarations: [
    ContentsComponent,
    DashboardComponent,
    
    VdsMatrizComponent,
    AddVdsMatrizComponent,
    DeleteVdsMatrizComponent,
    
    VdsUsersComponent,
    AddVdsUsersComponent,
    DeleteVdsUsersComponent,
    
    VdsPeopleComponent,
    AddVdsPeopleComponent,
    DeleteVdsPeopleComponent,
    
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    
    CommonModule,
    HttpClientModule,
    ContentsRoutingModule,
    
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatSidenavModule,
    NgMaterialMultilevelMenuModule,
    MatGridListModule,
    MatToolbarModule,
    MatMenuModule,
    MatInputModule,
    MatSortModule
  ],
  providers: [
    MultilevelMenuService,
    { provide: MatBottomSheetRef, useValue: {} },
    { provide: MAT_BOTTOM_SHEET_DATA, useValue: {} },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ],
  entryComponents: [
    VdsMatrizComponent,
    AddVdsMatrizComponent,
    DeleteVdsMatrizComponent,
    
    VdsPeopleComponent,
    AddVdsPeopleComponent,
    DeleteVdsPeopleComponent,
    
    VdsUsersComponent,
    AddVdsUsersComponent,
    DeleteVdsUsersComponent
  ],
  bootstrap: [ContentsComponent]
})
export class ContentsModule { }
