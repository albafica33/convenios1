import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {MatDrawer} from "@angular/material/sidenav";
import {MultilevelMenuService} from "ng-material-multilevel-menu";
import {AuthService} from "../services/auth.service";
import {ConvenioService} from "../services/convenio.service";

@Component({
  selector: 'app-contents',
  templateUrl: './contents.component.html',
  styleUrls: ['./contents.component.css']
})
export class ContentsComponent implements OnInit {
  
  ngOnInit(): void {
  }
}
