import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {VdsUsers} from "../../../../core/models/vdsUsers";
import {AuthService} from "../../../services/auth.service";
import {ConvenioService} from "../../../services/convenio.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  vdsUser:VdsUsers;
  isLogin:boolean = false;
  errorMessage: any;
  esLoginForm: FormGroup;
  
  constructor(
    private router:Router,
    private fb: FormBuilder,
    public authService:AuthService,
    public convenioService:ConvenioService
  ) {
    this.convenioService.showSideBar = false;
    this.convenioService.showToolBar = false;
    this.convenioService.sidebarOpened = false
    this.vdsUser = new VdsUsers();
    this.esLoginForm = this.fb.group({
      user_name: new FormControl('', Validators.required),
      user_hash: new FormControl('', Validators.required),
    });
  }
  ngOnInit() {
    this.isLogin = this.authService.isAuthenticated();

  }
  get f() { return this.esLoginForm.controls; }
  
}
