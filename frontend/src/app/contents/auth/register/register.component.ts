import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {VdsUsers} from "../../../../core/models/vdsUsers";
import {AuthService} from "../../../services/auth.service";
import {ConvenioService} from "../../../services/convenio.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  isLogin: boolean = false;
  errorMessage: any;
  esSignupForm:FormGroup;
  vdsUser:VdsUsers;
  
  constructor(
    private router:Router,
    private fb: FormBuilder,
    public authService:AuthService,
    public convenioService:ConvenioService
  ) {
    this.convenioService.showSideBar = false;
    this.convenioService.showToolBar = false;
    this.convenioService.sidebarOpened = false;
    this.vdsUser = new VdsUsers();
    this.esSignupForm = this.fb.group({
      user_name: new FormControl('', Validators.required),
      correo: new FormControl('', Validators.required),
      user_hash: new FormControl('', Validators.required),
    });
  }
  ngOnInit() {
    this.isLogin = this.authService.isAuthenticated();
  }
  
  get f() { return this.esSignupForm.controls; }
  
  
}
