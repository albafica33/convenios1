import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {VdUserService} from "../../../../core/services/vd-user.service";
import {ConvenioService} from "../../../services/convenio.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {VdsUsers} from "../../../../core/models/vdsUsers";
import {AddVdsUsersComponent} from "./add/add-vds-users.component";
import {DeleteVdsUsersComponent} from "./delete/delete-vds-users.component";
import {BehaviorSubject, fromEvent, merge, Observable} from "rxjs";
import {DataSource} from "@angular/cdk/table";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-vds-users',
  templateUrl: './vds-users.component.html',
  styleUrls: ['./vds-users.component.css']
})
export class VdsUsersComponent implements OnInit {
  
  displayedColumns = [
    'id',
    'id_persona',
    'user_name',
    'user_hash',
    'correo',
    'recordatorio',
    'estado',
    'createdBy',
    'updatedBy',
    'dueAt',
    'createdAt',
    'updatedAt',
    'actions'
  ];
  
  dataSource: VdUserDataSource | null;
  index: number;
  id: number;
  isLoading:boolean = true;
  
  constructor(
    public dialog: MatDialog,
    public vdUserService: VdUserService,
    public convenioService:ConvenioService
  ) {}
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter',  {static: true}) filter: ElementRef;
  
  ngOnInit() {
    this.loadData();
  }
  
  refresh() {
    this.loadData();
  }
  
  addNew() {
    let user = new VdsUsers();
    const dialogRef = this.dialog.open(AddVdsUsersComponent, {
      data: user ,
    });
    
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside userService
        await this.vdUserService.createVdUser(this.vdUserService.vdUserData).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsUsers};
          this.vdUserService.dataChange.value.push(response.data);
          this.refreshTable();
        });
      }
    });
  }
  
  startEdit(i: number, user:VdsUsers) {
    // this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(AddVdsUsersComponent, {
      data: user
    });
    
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside userService by id
        const foundIndex = this.vdUserService.dataChange.value.findIndex(x => x.id === user.id);
        await this.vdUserService.updateVdUser(user._id, this.vdUserService.vdUserData).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsUsers };
          this.vdUserService.dataChange.value[foundIndex] = response.data;
          
          // And lastly refresh table
          this.refreshTable();
        });
      }
    });
  }
  
  deleteItem(i: number, user:VdsUsers) {
    this.index = i;
    //this.id = id;
    const dialogRef = this.dialog.open(DeleteVdsUsersComponent, {
      data: user
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        
        const foundIndex = this.vdUserService.dataChange.value.findIndex(x => x.id === user.id);
        this.vdUserService.deleteVdUser(this.vdUserService.vdUserData.id).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsUsers };
          //this.userService.dataChange.value[foundIndex] = response.data;
          // for delete we use splice in order to remove single object from userService
          this.vdUserService.dataChange.value.splice(foundIndex, 1);
          this.refreshTable();
        })
      }
    });
  }
  
  
  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  
  
  /*   // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    // OLD METHOD:
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }*/
  
  
  
  public loadData() {
    // this.userService = new userService(this.httpClient);
    this.dataSource = new VdUserDataSource(this.vdUserService, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
    // .debounceTime(150)
    // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}

export class VdUserDataSource extends DataSource<VdsUsers> {
  _filterChange = new BehaviorSubject('');
  
  get filter(): string {
    return this._filterChange.value;
  }
  
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  
  filteredData: VdsUsers[] = [];
  renderedData: VdsUsers[] = [];
  
  constructor(public _userService: VdUserService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }
  
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<VdsUsers[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._userService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    
    this._userService.getDataVdsUsers();
    
    return merge(...displayDataChanges).pipe(map
    ( () => {
        // Filter data
        if (this._userService.data) {
          this.filteredData = this._userService.data.slice().filter((user: VdsUsers) => {
            let searchStr = '', pilatParamKeys = Object.keys(user);
            for (let i = 0 ; i < pilatParamKeys.length ; i++) {
              let pilatParamKey = pilatParamKeys[i];
              if(user[pilatParamKey]) {
                searchStr += user[pilatParamKey]+''.toLowerCase();
              }
            }
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        }
        
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }
  
  disconnect() {}
  
  
  /** Returns a sorted copy of the database data. */
  sortData(data: VdsUsers[]): VdsUsers[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      
      switch (this._sort.active) {
        case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
        case 'id_persona': [propertyA, propertyB] = [a.id_persona, b.id_persona]; break;
        case 'user_name': [propertyA, propertyB] = [a.user_name, b.user_name]; break;
        case 'user_hash': [propertyA, propertyB] = [a.user_hash, b.user_hash]; break;
        case 'correo': [propertyA, propertyB] = [a.correo, b.correo]; break;
        case 'recordatorio': [propertyA, propertyB] = [a.recordatorio, b.recordatorio]; break;
        case 'estado': [propertyA, propertyB] = [a.estado, b.estado]; break;
        case 'createdBy': [propertyA, propertyB] = [a.createdBy, b.createdBy]; break;
        case 'updatedBy': [propertyA, propertyB] = [a.updatedBy, b.updatedBy]; break;
        case 'dueAt': [propertyA, propertyB] = [a.dueAt.toString(), b.dueAt.toString()]; break;
        case 'createdAt': [propertyA, propertyB] = [a.createdAt.toString(), b.createdAt.toString()]; break;
        case 'updatedAt': [propertyA, propertyB] = [a.updatedAt.toString(), b.updatedAt.toString()]; break;
      }
      
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      
      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
  
  
  
}
