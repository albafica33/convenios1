import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteVdsUsersComponent } from './delete-vds-users.component';

describe('DeleteVdsUsersComponent', () => {
  let component: DeleteVdsUsersComponent;
  let fixture: ComponentFixture<DeleteVdsUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteVdsUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteVdsUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
