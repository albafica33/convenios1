import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {VdUserService} from "../../../../../core/services/vd-user.service";
import {VdsUsers} from "../../../../../core/models/vdsUsers";

@Component({
  selector: 'app-delete-vds-users',
  templateUrl: './delete-vds-users.component.html',
  styleUrls: ['./delete-vds-users.component.css']
})
export class DeleteVdsUsersComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<DeleteVdsUsersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VdsUsers,
    public vdUserService: VdUserService
  ) { }
  
  ngOnInit(): void {
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  confirmDelete(): void {
    this.vdUserService.vdUserData.id = this.data.id;
  }
}
