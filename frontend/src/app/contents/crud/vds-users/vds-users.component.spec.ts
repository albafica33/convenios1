import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VdsUsersComponent } from './vds-users.component';

describe('VdsUsersComponent', () => {
  let component: VdsUsersComponent;
  let fixture: ComponentFixture<VdsUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VdsUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VdsUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
