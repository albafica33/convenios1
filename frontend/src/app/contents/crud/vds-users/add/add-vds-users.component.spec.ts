import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVdsUsersComponent } from './add-vds-users.component';

describe('AddVdsUsersComponent', () => {
  let component: AddVdsUsersComponent;
  let fixture: ComponentFixture<AddVdsUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVdsUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVdsUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
