import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {VdsUsers} from "../../../../../core/models/vdsUsers";
import {VdUserService} from "../../../../../core/services/vd-user.service";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-vds-users',
  templateUrl: './add-vds-users.component.html',
  styleUrls: ['./add-vds-users.component.css']
})
export class AddVdsUsersComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<AddVdsUsersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VdsUsers,
    public vdUserService: VdUserService
  ) { }
  
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);
  
  ngOnInit(): void {
  }
  
  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }
  
  submit() {
    // emppty stuff
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  public confirmAdd(): void {
    this.vdUserService.vdUserData = this.data;
  }


}
