import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VdsMatrizComponent } from './vds-matriz.component';

describe('VdsMatrizComponent', () => {
  let component: VdsMatrizComponent;
  let fixture: ComponentFixture<VdsMatrizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VdsMatrizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VdsMatrizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
