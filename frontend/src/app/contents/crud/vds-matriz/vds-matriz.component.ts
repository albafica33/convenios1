import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {VdsMatriz} from "../../../../core/models/vdsMatriz";
import {AddVdsMatrizComponent} from "./add/add-vds-matriz.component";
import {VdMatrizService} from "../../../../core/services/vd-matriz.service";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {BehaviorSubject, fromEvent, merge, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {ConvenioService} from "../../../services/convenio.service";
import {DeleteVdsMatrizComponent} from "./delete/delete-vds-matriz.component";
import {DataSource} from "@angular/cdk/table";

@Component({
  selector: 'app-vds-matriz',
  templateUrl: './vds-matriz.component.html',
  styleUrls: ['./vds-matriz.component.css']
})
export class VdsMatrizComponent implements OnInit {
  
  displayedColumns = [
    'id',
    'estado_cumplimiento',
    'compromiso',
    'entidad_responsable',
    'contacto',
    'observacion',
    'tema',
    'fecha_cumplimiento',
    'createdById',
    'updatedById',
    'fecha_modificacion',
    'dueAt',
    'createdAt',
    'updatedAt',
    'actions'
  ];
  
  dataSource: VdMatrizDataSource | null;
  index: number;
  id: number;
  isLoading:boolean = true;
  
  constructor(
    public dialog: MatDialog,
    public vdMatrizService: VdMatrizService,
    public convenioService:ConvenioService
  ) {}
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter',  {static: true}) filter: ElementRef;
  
  ngOnInit() {
    this.loadData();
  }
  
  refresh() {
    this.loadData();
  }
  
  addNew() {
    let vdMatriz = new VdsMatriz();
    const dialogRef = this.dialog.open(AddVdsMatrizComponent, {
      data: vdMatriz ,
    });
    
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside vdMatrizService
        await this.vdMatrizService.createVdMatriz(this.vdMatrizService.vdMatrizData).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsMatriz};
          this.vdMatrizService.dataChange.value.push(response.data);
          this.refreshTable();
        });
      }
    });
  }
  
  startEdit(i: number, vdMatriz:VdsMatriz) {
    // this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(AddVdsMatrizComponent, {
      data: vdMatriz
    });
    
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside vdMatrizService by id
        const foundIndex = this.vdMatrizService.dataChange.value.findIndex(x => x.id === vdMatriz.id);
        await this.vdMatrizService.updateVdMatriz(vdMatriz._id, this.vdMatrizService.vdMatrizData).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsMatriz };
          this.vdMatrizService.dataChange.value[foundIndex] = response.data;
          
          // And lastly refresh table
          this.refreshTable();
        });
      }
    });
  }
  
  deleteItem(i: number, vdMatriz:VdsMatriz) {
    this.index = i;
    //this.id = id;
    const dialogRef = this.dialog.open(DeleteVdsMatrizComponent, {
      data: vdMatriz
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        
        const foundIndex = this.vdMatrizService.dataChange.value.findIndex(x => x.id === vdMatriz.id);
        this.vdMatrizService.deleteVdMatriz(this.vdMatrizService.vdMatrizData.id).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsMatriz };
          //this.vdMatrizService.dataChange.value[foundIndex] = response.data;
          // for delete we use splice in order to remove single object from vdMatrizService
          this.vdMatrizService.dataChange.value.splice(foundIndex, 1);
          this.refreshTable();
        })
      }
    });
  }
  
  
  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  
  
  /*   // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    // OLD METHOD:
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }*/
  
  
  
  public loadData() {
    // this.vdMatrizService = new vdMatrizService(this.httpClient);
    this.dataSource = new VdMatrizDataSource(this.vdMatrizService, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
    // .debounceTime(150)
    // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}

export class VdMatrizDataSource extends DataSource<VdsMatriz> {
  _filterChange = new BehaviorSubject('');
  
  get filter(): string {
    return this._filterChange.value;
  }
  
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  
  filteredData: VdsMatriz[] = [];
  renderedData: VdsMatriz[] = [];
  
  constructor(public _VdMatrizService: VdMatrizService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the vdMatriz changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }
  
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<VdsMatriz[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    
    const displayDataChanges = [
      this._VdMatrizService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    
    this._VdMatrizService.getDataVdsMatriz();
    
    return merge(...displayDataChanges).pipe(map( () => {
        // Filter data
        if (this._VdMatrizService.data) {
          this.filteredData = this._VdMatrizService.data.slice().filter((vdMatriz: VdsMatriz) => {
            let searchStr = '', pilatParamKeys = Object.keys(vdMatriz);
            for (let i = 0 ; i < pilatParamKeys.length ; i++) {
              let pilatParamKey = pilatParamKeys[i];
              if(vdMatriz[pilatParamKey]) {
                searchStr += vdMatriz[pilatParamKey]+''.toLowerCase();
              }
            }
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        }
        
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }
  
  disconnect() {}
  
  
  /** Returns a sorted copy of the database data. */
  sortData(data: VdsMatriz[]): VdsMatriz[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      
      switch (this._sort.active) {
        case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
        case 'estado_cumplimiento': [propertyA, propertyB] = [a.estado_cumplimiento, b.estado_cumplimiento]; break;
        case 'compromiso': [propertyA, propertyB] = [a.compromiso, b.compromiso]; break;
        case 'entidad_responsable': [propertyA, propertyB] = [a.entidad_responsable, b.entidad_responsable]; break;
        case 'contacto': [propertyA, propertyB] = [a.contacto, b.contacto]; break;
        case 'observacion': [propertyA, propertyB] = [a.observacion, b.observacion]; break;
        case 'tema': [propertyA, propertyB] = [a.tema, b.tema]; break;
        case 'fecha_cumplimiento': [propertyA, propertyB] = [a.fecha_cumplimiento, b.fecha_cumplimiento]; break;
        case 'createdById': [propertyA, propertyB] = [a.createdById, b.createdById]; break;
        case 'updatedById': [propertyA, propertyB] = [a.updatedById, b.updatedById]; break;
        case 'fecha_modificacion': [propertyA, propertyB] = [a.fecha_modificacion.toString(), b.fecha_modificacion.toString()]; break;
        case 'dueAt': [propertyA, propertyB] = [a.dueAt.toString(), b.dueAt.toString()]; break;
        case 'createdAt': [propertyA, propertyB] = [a.createdAt.toString(), b.createdAt.toString()]; break;
        case 'updatedAt': [propertyA, propertyB] = [a.updatedAt.toString(), b.updatedAt.toString()]; break;
      }
      
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      
      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
  
  
  
  
}
