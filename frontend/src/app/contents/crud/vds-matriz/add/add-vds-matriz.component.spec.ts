import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVdsMatrizComponent } from './add-vds-matriz.component';

describe('AddVdsMatrizComponent', () => {
  let component: AddVdsMatrizComponent;
  let fixture: ComponentFixture<AddVdsMatrizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVdsMatrizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVdsMatrizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
