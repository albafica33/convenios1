import {Component, Inject, OnInit} from '@angular/core';
import {VdMatrizService} from "../../../../../core/services/vd-matriz.service";
import {VdsMatriz} from "../../../../../core/models/vdsMatriz";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-vds-matriz',
  templateUrl: './add-vds-matriz.component.html',
  styleUrls: ['./add-vds-matriz.component.css']
})
export class AddVdsMatrizComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<AddVdsMatrizComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VdsMatriz,
    public vdMatrizService: VdMatrizService
  ) { }
  
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);
  
  ngOnInit(): void {
  }
  
  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }
  
  submit() {
    // emppty stuff
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  public confirmAdd(): void {
    this.vdMatrizService.vdMatrizData = this.data;
  }

}
