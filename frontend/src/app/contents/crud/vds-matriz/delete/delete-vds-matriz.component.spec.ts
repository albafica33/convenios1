import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteVdsMatrizComponent } from './delete-vds-matriz.component';

describe('DeleteVdsMatrizComponent', () => {
  let component: DeleteVdsMatrizComponent;
  let fixture: ComponentFixture<DeleteVdsMatrizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteVdsMatrizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteVdsMatrizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
