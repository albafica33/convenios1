import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DeleteVdsUsersComponent} from "../../vds-users/delete/delete-vds-users.component";
import {VdsMatriz} from "../../../../../core/models/vdsMatriz";
import {VdMatrizService} from "../../../../../core/services/vd-matriz.service";

@Component({
  selector: 'app-delete-vds-matriz',
  templateUrl: './delete-vds-matriz.component.html',
  styleUrls: ['./delete-vds-matriz.component.css']
})
export class DeleteVdsMatrizComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<DeleteVdsUsersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VdsMatriz,
    public vdMatrizService: VdMatrizService
  ) { }
  
  ngOnInit(): void {
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  confirmDelete(): void {
    this.vdMatrizService.vdMatrizData.id = this.data.id;
  }
}
