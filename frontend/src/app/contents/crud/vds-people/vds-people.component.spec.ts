import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VdsPeopleComponent } from './vds-people.component';

describe('VdsPeopleComponent', () => {
  let component: VdsPeopleComponent;
  let fixture: ComponentFixture<VdsPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VdsPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VdsPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
