import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, Validators} from "@angular/forms";
import {VdPersonService} from "../../../../../core/services/vd-person.service";
import {VdsPeople} from "../../../../../core/models/vdsPeople";

@Component({
  selector: 'app-add-vds-people',
  templateUrl: './add-vds-people.component.html',
  styleUrls: ['./add-vds-people.component.css']
})
export class AddVdsPeopleComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<AddVdsPeopleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VdsPeople,
    public vdPersonService: VdPersonService
  ) { }
  
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);
  
  ngOnInit(): void {
  }
  
  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }
  
  submit() {
    // emppty stuff
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  public confirmAdd(): void {
    this.vdPersonService.vdPersonData = this.data;
  }
  
  
}
