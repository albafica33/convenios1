import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVdsPeopleComponent } from './add-vds-people.component';

describe('AddVdsPeopleComponent', () => {
  let component: AddVdsPeopleComponent;
  let fixture: ComponentFixture<AddVdsPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVdsPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVdsPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
