import {Component, Inject, OnInit} from '@angular/core';
import {VdsPeople} from "../../../../../core/models/vdsPeople";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {VdPersonService} from "../../../../../core/services/vd-person.service";

@Component({
  selector: 'app-delete-vds-people',
  templateUrl: './delete-vds-people.component.html',
  styleUrls: ['./delete-vds-people.component.css']
})
export class DeleteVdsPeopleComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<DeleteVdsPeopleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VdsPeople,
    public vdPersonService: VdPersonService
  ) { }
  
  ngOnInit(): void {
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  confirmDelete(): void {
    this.vdPersonService.vdPersonData.id = this.data.id;
  }
}
