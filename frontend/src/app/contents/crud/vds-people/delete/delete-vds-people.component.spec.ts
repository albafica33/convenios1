import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteVdsPeopleComponent } from './delete-vds-people.component';

describe('DeleteVdsPeopleComponent', () => {
  let component: DeleteVdsPeopleComponent;
  let fixture: ComponentFixture<DeleteVdsPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteVdsPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteVdsPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
