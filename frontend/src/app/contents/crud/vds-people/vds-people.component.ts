import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {VdPersonService} from "../../../../core/services/vd-person.service";
import {ConvenioService} from "../../../services/convenio.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {VdsPeople} from "../../../../core/models/vdsPeople";
import {AddVdsPeopleComponent} from "./add/add-vds-people.component";
import {DeleteVdsPeopleComponent} from "./delete/delete-vds-people.component";
import {BehaviorSubject, fromEvent, merge, Observable} from "rxjs";
import {DataSource} from "@angular/cdk/table";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-vds-people',
  templateUrl: './vds-people.component.html',
  styleUrls: ['./vds-people.component.css']
})
export class VdsPeopleComponent implements OnInit {
  
  displayedColumns = [
    'id',
    'per_first_name',
    'per_second_name',
    'per_first_lastname',
    'per_second_lastname',
    // 'per_license',
    // 'per_license_comp',
    // 'per_home_address',
    'per_mail',
    'per_home_phone',
    'per_cellphone',
    // 'per_group',
    // 'createdById',
    // 'updatedById',
    'per_parent_id',
    'per_par_type_doc_id',
    // 'per_par_city_id',
    // 'per_par_sex_id',
    // 'per_par_country_id',
    // 'per_par_nacionality_id',
    // 'per_par_status_id',
    // 'per_birthday',
    // 'dueAt',
    'createdAt',
    // 'updatedAt',
    'actions'
  ];
  
  dataSource: VdsPersonDataSource | null;
  index: number;
  id: number;
  isLoading:boolean = true;
  
  constructor(
    public dialog: MatDialog,
    public vdPersonService: VdPersonService,
    public convenioService:ConvenioService
  ) {}
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter',  {static: true}) filter: ElementRef;
  
  ngOnInit() {
    this.loadData();
  }
  
  refresh() {
    this.loadData();
  }
  
  addNew() {
    let vdsPerson = new VdsPeople();
    const dialogRef = this.dialog.open(AddVdsPeopleComponent, {
      data: vdsPerson ,
    });
    
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside vdsPersonService
        await this.vdPersonService.createVdPerson(this.vdPersonService.vdPersonData).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsPeople};
          this.vdPersonService.dataChange.value.push(response.data);
          this.refreshTable();
        });
      }
    });
  }
  
  startEdit(i: number, vdsPerson:VdsPeople) {
    // this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    console.log(this.index);
    const dialogRef = this.dialog.open(AddVdsPeopleComponent, {
      data: vdsPerson
    });
    
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside vdsPersonService by id
        const foundIndex = this.vdPersonService.dataChange.value.findIndex(x => x.id === vdsPerson.id);
        await this.vdPersonService.updateVdPerson(vdsPerson._id, this.vdPersonService.vdPersonData).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsPeople };
          this.vdPersonService.dataChange.value[foundIndex] = response.data;
          
          // And lastly refresh table
          this.refreshTable();
        });
      }
    });
  }
  
  deleteItem(i: number, vdsPerson:VdsPeople) {
    this.index = i;
    //this.id = id;
    const dialogRef = this.dialog.open(DeleteVdsPeopleComponent, {
      data: vdsPerson
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        
        const foundIndex = this.vdPersonService.dataChange.value.findIndex(x => x.id === vdsPerson.id);
        this.vdPersonService.deleteVdPerson(this.vdPersonService.vdPersonData.id).subscribe(async (res) => {
          let response = res as { status: string, message: string, data: VdsPeople };
          //this.vdsPersonService.dataChange.value[foundIndex] = response.data;
          // for delete we use splice in order to remove single object from vdsPersonService
          this.vdPersonService.dataChange.value.splice(foundIndex, 1);
          this.refreshTable();
        })
      }
    });
  }
  
  
  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  
  
  /*   // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    // OLD METHOD:
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }*/
  
  
  
  public loadData() {
    // this.vdsPersonService = new vdsPersonService(this.httpClient);
    this.dataSource = new VdsPersonDataSource(this.vdPersonService, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
    // .debounceTime(150)
    // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}

export class VdsPersonDataSource extends DataSource<VdsPeople> {
  _filterChange = new BehaviorSubject('');
  
  get filter(): string {
    return this._filterChange.value;
  }
  
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  
  filteredData: VdsPeople[] = [];
  renderedData: VdsPeople[] = [];
  
  constructor(public _vdPersonService: VdPersonService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the vdsPerson changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }
  
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<VdsPeople[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._vdPersonService.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    
    this._vdPersonService.getDataVdsPeople();
    
    return merge(...displayDataChanges).pipe(map( () => {
        // Filter data
        if (this._vdPersonService.data) {
          this.filteredData = this._vdPersonService.data.slice().filter((vdsPerson: VdsPeople) => {
            let searchStr = '', pilatParamKeys = Object.keys(vdsPerson);
            for (let i = 0 ; i < pilatParamKeys.length ; i++) {
              let pilatParamKey = pilatParamKeys[i];
              if(vdsPerson[pilatParamKey]) {
                searchStr += vdsPerson[pilatParamKey]+''.toLowerCase();
              }
            }
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        }
        
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }
  
  disconnect() {}
  
  
  /** Returns a sorted copy of the database data. */
  sortData(data: VdsPeople[]): VdsPeople[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      
      switch (this._sort.active) {
        case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
        case 'per_first_name': [propertyA, propertyB] = [a.per_first_name, b.per_first_name]; break;
        case 'per_second_name': [propertyA, propertyB] = [a.per_second_name, b.per_second_name]; break;
        case 'per_first_lastname': [propertyA, propertyB] = [a.per_first_lastname, b.per_first_lastname]; break;
        case 'per_second_lastname': [propertyA, propertyB] = [a.per_second_lastname, b.per_second_lastname]; break;
        case 'per_license': [propertyA, propertyB] = [a.per_license, b.per_license]; break;
        case 'per_license_comp': [propertyA, propertyB] = [a.per_license_comp, b.per_license_comp]; break;
        case 'per_home_address': [propertyA, propertyB] = [a.per_home_address, b.per_home_address]; break;
        case 'per_mail': [propertyA, propertyB] = [a.per_mail, b.per_mail]; break;
        case 'per_home_phone': [propertyA, propertyB] = [a.per_home_phone, b.per_home_phone]; break;
        case 'per_cellphone': [propertyA, propertyB] = [a.per_cellphone, b.per_cellphone]; break;
        case 'per_group': [propertyA, propertyB] = [a.per_group, b.per_group]; break;
        case 'createdById': [propertyA, propertyB] = [a.createdById, b.createdById]; break;
        case 'updatedById': [propertyA, propertyB] = [a.updatedById,b.updatedById]; break;
        case 'per_parent_id': [propertyA, propertyB] = [a.per_parent_id, b.per_parent_id]; break;
        case 'per_par_type_doc_id': [propertyA, propertyB] = [a.per_par_type_doc_id, b.per_par_type_doc_id]; break;
        case 'per_par_city_id': [propertyA, propertyB] = [a.per_par_city_id, b.per_par_city_id]; break;
        case 'per_par_sex_id': [propertyA, propertyB] = [a.per_par_sex_id, b.per_par_sex_id]; break;
        case 'per_par_country_id': [propertyA, propertyB] = [a.per_par_country_id, b.per_par_country_id]; break;
        case 'per_par_nacionality_id': [propertyA, propertyB] = [a.per_par_nacionality_id, b.per_par_nacionality_id]; break;
        case 'per_par_status_id': [propertyA, propertyB] = [a.per_par_status_id, b.per_par_status_id]; break;
        case 'per_birthday': [propertyA, propertyB] = [a.per_birthday.toString(), b.per_birthday.toString()]; break;
        case 'dueAt': [propertyA, propertyB] = [a.dueAt.toString(), b.dueAt.toString()]; break;
        case 'createdAt': [propertyA, propertyB] = [a.createdAt.toString(), b.createdAt.toString()]; break;
        case 'updatedAt': [propertyA, propertyB] = [a.updatedAt.toString(), b.updatedAt.toString()]; break;
      }
      
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      
      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
  
  
  
}
