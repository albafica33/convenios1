import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {VdsMatrizComponent} from "./crud/vds-matriz/vds-matriz.component";
import {VdsPeopleComponent} from "./crud/vds-people/vds-people.component";
import {VdsUsersComponent} from "./crud/vds-users/vds-users.component";
import {ContentsComponent} from "./contents.component";
import {AuthGuardService} from "../services/auth-guard.service";

const routes: Routes = [
  { path: '', component:ContentsComponent, pathMatch: 'full', canActivate: [AuthGuardService]},
  { path: 'dashboard', component:DashboardComponent, pathMatch: 'full', canActivate: [AuthGuardService] },
  { path: 'crud/matriz', component:VdsMatrizComponent, pathMatch: 'full', canActivate: [AuthGuardService]  },
  { path: 'crud/people', component:VdsPeopleComponent, pathMatch: 'full', canActivate: [AuthGuardService]  },
  { path: 'crud/users', component:VdsUsersComponent, pathMatch: 'full', canActivate: [AuthGuardService]  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ContentsRoutingModule {

}
