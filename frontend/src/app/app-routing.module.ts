import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {MyBarChartComponent} from "./components/my-bar-chart/my-bar-chart.component";
import {MyDoughnutChartComponent} from "./components/my-doughnut-chart/my-doughnut-chart.component";
import {MyRadarChartComponent} from "./components/my-radar-chart/my-radar-chart.component";
import {MyPieChartComponent} from "./components/my-pie-chart/my-pie-chart.component";
import {ChartsComponent} from "./components/charts/charts.component";
import {MapsComponent} from "./components/maps/maps.component";
import {LoginComponent} from "./contents/auth/login/login.component";
import {RegisterComponent} from "./contents/auth/register/register.component";
import {AuthGuardService} from "./services/auth-guard.service";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: '', loadChildren: () => import('./contents/contents.module').then(m => m.ContentsModule), canActivate:[AuthGuardService]},
  {path:'dashboard', component:DashboardComponent, canActivate:[AuthGuardService]},
  {path: '**', component: DashboardComponent, canActivate:[AuthGuardService]},
  
  // {path: 'bar-chart', component: MyBarChartComponent},
  // {path: 'doughnut-chart', component: MyDoughnutChartComponent},
  // {path: 'radar-chart', component: MyRadarChartComponent},
  // {path: 'pie-chart', component: MyPieChartComponent},
  // {path: 'map', component: MapsComponent},
  // {path: 'chart', component: ChartsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
