import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDrawer} from "@angular/material/sidenav";
import {ConvenioService} from "./services/convenio.service";
import {AuthService} from "./services/auth.service";
declare var $:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  @ViewChild('drawer') drawer: MatDrawer;
  
  appConfig = {
    paddingAtStart: true,
    classname: 'my-custom-class',
    listBackgroundColor: '#424242',
    fontColor: '#ffffff',
    fontSize: '12px',
    backgroundColor: '#ffffff',
    selectedListFontColor: 'red',
  };
  
  constructor(
    public convenioService: ConvenioService,
    public authService: AuthService
  ) {}
  
  ngOnInit(): void {
    this.convenioService.setMenu();
  }
  
  toggleMenu() {
    if (this.drawer) {
      this.convenioService.drawer = this.drawer;
    } else if (this.convenioService.drawer) {
      this.drawer = this.convenioService.drawer;
    }
    this.drawer.toggle();
  }
  
  
  menuIsReady(event): void {
    if (this.drawer) {
      this.drawer.toggle();
    }
  }
  
  toolBarChange() {
    this.convenioService.showToolBar = false;
  }
  
  
  
  selectedItem (event):void {
  
  }
  
}
