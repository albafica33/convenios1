export const environment = {
  production: true,
  system: 'convenios',
  backend: {
    server: {
      webpath: 'http://127.0.0.1:8000'
    }
  },
  frontend: {
    template: {
      assets: '/assets',
    },
  }
};
